<?php

namespace App\Services;

use Carbon\Carbon;
use Symfony\Component\DomCrawler\Crawler;

class ForumAuctionsCrawler extends BaseCrawler {

    public $baseUrl = 'https://www.forumauctions.co.uk/index.php?option=com_calender&view=archives&tmpl=auction';
    public $auctionUrl = '';
    public function __construct($batch)
    {
        $this->filename = 'Forum_auction' . date('Y-m-d_H-i-s') . '.csv';
        $this->auctionUrl = $batch->input;
        parent::__construct($batch);
    }

    public function startCrawling()
    {
        $lots = [];
        $lot_links = [];
        $links = [];

        if(!empty($this->auctionUrl)){
            $links[]= $this->auctionUrl;
        } else {
            for($i = 1;$i <= 15; $i++){
                $response = $this->client->request('POST', $this->baseUrl, ['form_params' => ['page_no' => $i]]);
                if ($response->getStatusCode() == 200) {
                    $page = new Crawler($response->getBody()->getContents(), $this->baseUrl);
                    $page->filter('.auction-arc__button a')->each(function($item) use(&$links){
                        $links[] = $item->link()->getUri();

                    });
                }
            }
        }

        foreach ($links as $link){
            //if(count($lot_links) > 100) break;
            $response = $this->client->request('GET', $link);
            if ($response->getStatusCode() == 200) {
                $page = new Crawler($response->getBody()->getContents(), $link);
                $page->filter('body .pagination li .paginate')->each(function($item) use(&$lot_links){
                    if(is_numeric($item->text())){
                        $uri = $item->link()->getUri();
                        parse_str($uri, $output);
                        if($output['page_no'] == 2){
                            $lot_links[] = preg_replace('/page_no=2/', 'page_no=1', $uri, -1);
                        }
                        $lot_links[] = $uri;
                        dump(count($lot_links));
                    }
                });
            }
        }
        foreach ($lot_links as $l_link){
            //if(count($lots) > 100) break;
            $response = $this->client->request('GET', $l_link);
            if ($response->getStatusCode() == 200) {
                $page = new Crawler($response->getBody()->getContents(), $l_link);
                $page->filter('body .catalogue-view__img a')->each(function($item) use(&$lots){
                    $item_link = $item->link()->getUri();
                    $response = $this->client->request('GET', $item_link);
                    $page = new Crawler($response->getBody()->getContents(), $item_link);
                    if($page->filter('body .hammer_price .price-label')->count()){
                        if(preg_match('/Unsold/', trim($page->filter('body .hammer_price .price-label')->text()))){
                            $lots[] = $this->getLotInfo($page, $item_link);
                            dump(count($lots));
                        }
                    }
                });

            }
        }
        if ($lots) {
            $this->headers = array_keys($lots[0]);
        }
        return $lots;
    }

    public function getLotInfo($crawler, $link){
        $lot = [];
        $price = $this->getLotPrice($crawler);
        $photos = $this->getLotPhotos($crawler);
        $auctionDate = $this->getStartDate($crawler);
        $endDate = $auctionDate->format('Y-m-d H:i:s');
        $startDate = $auctionDate->subDays(3)->format('Y-m-d H:i:s');
        $lot['id'] = $this->getLotId($crawler);
        $lot['Title'] = $this->getLotTitle($crawler);
        $lot['Description'] = $this->getDescription($crawler);
        $lot['Category'] = 'Works on Paper';
        $lot['Sub Category'] = '';
        $lot['Partner'] = $this->partner_id;
        $lot['Shipping description'] = '';
        $lot['Auction House Fee'] = 25;
        $lot['Minimum bid amount'] = isset($price['price_from']) ? $price['price_from'] * 80 / 100 : 0;
        $lot['Price range from'] = isset($price['price_from']) ? $price['price_from'] : 0;
        $lot['Price range to'] = isset($price['price_to']) ? $price['price_to'] : 0;
        $lot['Price to auto-accept bid'] = isset($price['price_from']) ? $price['price_from'] : 0;
        $lot['Start date'] = $startDate;
        $lot['End date'] = $endDate;
        $lot['Seller'] = $this->seller_id;
        $lot['Currency'] = isset($price['currency']) ? $price['currency'] : 'usd';
        $lot['Lot photos'] = implode(',', $photos);

        return $lot;
    }

    public function getLotPrice($crawler){
        $price = explode('-', $crawler->filter('body .lot-estimate .price-label')->text());
        return [
            'currency' => 'gbp',
            'price_from' => preg_replace(
                '/[^0-9]/',
                '',
                $price[0]
            ),
            'price_to' => preg_replace(
                '/[^0-9]/',
                '',
                $price[1]
            ),
        ];
    }

    public function getLotPhotos($crawler){
        $photos = [];
        $crawler->filter('body .lot-images img')->each(function($item) use(&$photos){
            $uri = $this->loadImage($item->image()->getUri());
            $exploded = explode('.', $uri);
            $extension = end($exploded);
            if($extension == 'jpg' || $extension == 'png'){
                $photos[] = $uri;
            }
        });
        return $photos;
    }

    public function getStartDate($crawler){
        $dateHtml = null;

        if (!$dateHtml)
            return Carbon::now();

        return Carbon::parse(trim($dateHtml->text()));
    }

    public function getLotId($crawler){
        parse_str($crawler->getUri(), $output);
        return $output['lot_id'];
    }

    public function getLotTitle($crawler){
        return preg_replace(
            '/[^a-zA-Z0-9\_\-\[\]\|\(\)\s]/',
            '',
                $this->processContent(
                    $crawler->filter('body .lot-author-info')->text())
        );
    }

    public function getDescription($crawler){
        return $this->processContent(
            trim(mb_convert_encoding($crawler->filter('body .lot-description__inner')->html(), 'UTF-8'))
        );
    }

    public function processContent($content) {
        return str_replace([
            "\n\r",
            "\0",
            "\x0B",
            "\n",
            "\r",
            "\t",
            "£",
            "   ",
            "\0xE2",
            "\0x94",
            "\0xAC",
            "\0xE2",
            "\0x94",
            "\0x9C",
            "\0xE2",
            "\0x8C",
            "\0x90",
            PHP_EOL
        ], '', $content);
    }

}