<?php

namespace App\Listeners\Bid;

use App\Models\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class AbstractBidListener implements ShouldQueue
{
    protected $bid;
    protected $buyer;
    protected $lot;
    protected $seller;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function setData($event)
    {
        $this->bid = $event->bid;
        $this->bid->load(['lot', 'lot.seller', 'buyer']);
        $this->buyer = $this->bid->buyer;
        $this->lot = $this->bid->lot;
        $this->seller = $this->lot->seller;
    }

    protected function getData()
    {
        $data = [
            'buyer' => $this->buyer,
            'seller' => $this->seller,
            'bid' => $this->bid,
            'lot' => $this->lot
        ];
        return $data;
    }

    protected function sendMail(User $to, $name)
    {
        $name = 'App\Mail\Bid\\' . $name;
        Mail::to((object)['email' => $to->email, 'name' => $to->full_name])
            ->send(new $name($this->getData()));
    }
}
