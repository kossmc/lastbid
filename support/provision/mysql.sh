#!/usr/bin/env bash
echo ">>> Installing Mysql"
# This script automatically installs and configures MariaDB 10.1.

#sudo apt-get install software-properties-common > /dev/null 2>&1

# update
sudo apt-get update -y > /dev/null 2>&1

# install mariadb
export DEBIAN_FRONTEND="noninteractive"
sudo debconf-set-selections <<< "mysql-server mysql-server/root_password password root" > /dev/null 2>&1
sudo debconf-set-selections <<< "mysql-server mysql-server/root_password_again password root" > /dev/null 2>&1
sudo apt-get -y install mysql-server > /dev/null 2>&1

# ensure it is running
sudo systemctl start mysql.service > /dev/null 2>&1
sudo systemctl enable mysql.service > /dev/null 2>&1
sudo /etc/init.d/mysql restart > /dev/null 2>&1

### post-install setup

# allow remote access (required to access from our private network host. Note that this is completely insecure if used in any other way)
mysql -u root -proot -e "GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' IDENTIFIED BY 'root' WITH GRANT OPTION; FLUSH PRIVILEGES;" > /dev/null 2>&1
# create main database of project
mysql -u root -proot -e "CREATE DATABASE lastbid_db DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_general_ci;" > /dev/null 2>&1

echo ">>> Final restart Mysql"
# restart
sudo /etc/init.d/mysql restart > /dev/null 2>&1

sudo rm /etc/mysql/mysql.conf.d/mysqld.cnf > /dev/null
sudo cp /lastbid/support/mysql/mysqld.cnf /etc/mysql/mysql.conf.d/mysqld.cnf > /dev/null
sudo systemctl restart mysql.service > /dev/null