<div class="page-wrap">
    <div class="full-width-page clearfix">
        <div class="image-head-page" style="background-image:url(img/about_us_wallpaper_210821018.png);"></div>
        <div class="wrap">

            <div class="title-page">
                <h1>About us</h1>
            </div>
            <div class="content">
                {!! $data->text !!}
            </div>
            <div class="title-about-partners title-page">
                <h3>About the team</h3>
            </div>

            <div class="about-partners">
                <div class="row">
                    <div class="col-sm-6">
                        <img src="uploads/{!! $data->team_member_1_image !!}" alt="">
                        <p class="about-partners--title">{!! $data->team_member_1_name !!}</p>
                        <p class="about-partners--text">
                            {!! $data->team_member_1_text !!}
                        </p>
                    </div>
                    <div class="col-sm-6">
                        <img src="uploads/{!! $data->team_member_2_image !!}" alt="">
                        <p class="about-partners--title">{!! $data->team_member_2_name !!}</p>
                        <p class="about-partners--text">
                            {!! $data->team_member_2_text !!}
                        </p>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6">
                        <img src="uploads/{!! $data->team_member_3_image !!}" alt="">
                        <p class="about-partners--title">{!! $data->team_member_3_name !!}</p>
                        <p class="about-partners--text">
                            {!! $data->team_member_3_text !!}
                        </p>
                    </div>
                    <div class="col-sm-6">
                        <img src="uploads/{!! $data->team_member_4_image !!}" alt="">
                        <p class="about-partners--title">{!! $data->team_member_4_name !!}</p>
                        <p class="about-partners--text">
                            {!! $data->team_member_4_text !!}
                        </p>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6">
                        <img src="uploads/{!! $data->team_member_5_image !!}" alt="">
                        <p class="about-partners--title">{!! $data->team_member_5_name !!}</p>
                        <p class="about-partners--text">
                            {!! $data->team_member_5_text !!}
                        </p>
                    </div>
                    <div class="col-sm-6">
                        <img src="uploads/{!! $data->team_member_6_image !!}" alt="">
                        <p class="about-partners--title">{!! $data->team_member_6_name !!}</p>
                        <p class="about-partners--text">
                            {!! $data->team_member_6_text !!}
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

