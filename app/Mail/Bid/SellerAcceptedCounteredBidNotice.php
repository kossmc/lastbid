<?php

namespace App\Mail\Bid;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

use App\Traits\BidNoticeTrait;

class SellerAcceptedCounteredBidNotice extends Mailable
{
    use Queueable, SerializesModels, BidNoticeTrait;

    /**
     * Email subject.
     *
     */
    protected $bid_notice_subject = 'Congratulations! Your bid was accepted by buyer';

    /**
     * Email template.
     *
     */
    protected $bid_notice_template = 'emails.seller_accepted_countered_bid_notice';
}
