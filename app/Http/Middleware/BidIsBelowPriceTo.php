<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\Repositories\AutodeclinedBidRepository;

class BidIsBelowPriceTo
{
    /**
     * The autodeclined bid repository implementation.
     *
     * @var AutodeclinedBidRepository
     */
    private $autodeclinedBid;

    /**
     * Create a new middleware instance.
     *
     * @param AutodeclinedBidRepository $lot
     * @return void
     */
    public function __construct(AutodeclinedBidRepository $autodeclinedBid)
    {
        $this->autodeclinedBid = $autodeclinedBid;
    }
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $lot = $request->lot;
        $amount = $request->exists('amount') ? $request->amount : 0;
        if ($lot->bidIsBelowPriceTo($amount)) {
            return $next($request);
        } else {
            $data = [
                'user_id' => Auth::id(),
                'lot_id' => $lot->id,
                'amount' => $amount
            ];
            $this->autodeclinedBid->create($data);
            return response()->json([
                'error' => true,
                'message' => 'Your bid is above lot price range. Please decrease your bid.'
            ]);
        }
    }
}
