<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::any('logout', 'Auth\LoginController@logout')->name('logout');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')
    ->middleware(['logout', 'guest'])
    ->name('password.reset');
// OAuth Routes
Route::get('auth/{provider}', 'Auth\AuthController@redirectToProvider')
    ->where(['provider' => '^(facebook|google)$'])
    ->name('socialAuth');
Route::get('auth/{provider}/callback', 'Auth\AuthController@handleProviderCallback')
    ->where(['provider' => '^(facebook|google)$'])
    ->name('socialAuthCallback');

Route::get('/', 'MainController@home')
    ->name('home');

Route::post('setTimezone', function () {
    session(['time_zone' => request()->get('time_zone')]);
});

Route::get('about', function () {
    $meta = \App\Models\PageMeta::byPage('about');
    return view('pages.about', compact('meta'))->with(['data'=>\DB::table('about_us')->first()]);
})->name('about');

Route::get('contact', 'MainController@contact')
    ->name('contact');

Route::post('contact', 'MainController@contactSend')
    ->name('contact-send');

Route::get('how-it-works', function () {
    $meta = \App\Models\PageMeta::byPage('how_it_works');
    return view('pages.how_it_works', compact('meta'))->with(['data'=>\DB::table('how_it_works')->first()]);
})->name('how-it-works');

Route::get('faq', 'FaqController@index')->name('faq');

Route::get('ajax_login', 'Auth\LoginController@ajaxLoginForm');
Route::get('lets_talk', 'MainController@letsTalk');
Route::post('ajax_login', 'Auth\LoginController@ajax_login');
Route::get('ajax_reg', 'Auth\RegisterController@ajax_reg');
Route::post('ajax_reg', 'Auth\RegisterController@ajax_reg_save');
Route::post('ajax_add_info', 'Auth\RegisterController@ajax_add_info');
Route::post('ajax_forgot_form', 'Auth\ForgotPasswordController@ajax_forgot_form');
Route::post('ajax_forgot_pass', 'Auth\ForgotPasswordController@ajax_forgot_pass');
Route::post('ajax_get_counters', 'MainController@getCounters')
    ->middleware('hasRole:buyer,seller');
Route::get('ajax_get_terms', 'MainController@getTerms');
Route::get('ajax_get_terms_custom', 'MainController@getTermsCustom');
Route::get('ajax_get_policy', 'MainController@getPolicy');
Route::get('lots/ajax_get_specifics_table/{catTitle}', 'SellerController@getCategorySpecificTable');
Route::get('terms/{alias}/get','TermsController@getTerms');

Route::get('terms', function (){
    $meta = \App\Models\PageMeta::byPage('terms');
    return view('pages.terms', compact('meta'))->with(['terms'=>\DB::table('site_terms_conditions')->first()->text]);
});
Route::get('policy', function (){
    $meta = \App\Models\PageMeta::byPage('policy');
    return view('pages.policy', compact('meta'));
});
Route::get('privacy', function (){
    $meta = \App\Models\PageMeta::byPage('policy');
    return view('pages.policy', compact('meta'));
});

Route::post('email_success', 'Auth\RegisterController@successEmailPopup');

Route::get('events/{year?}/{month?}/{day?}', 'EventController@show_list')
    ->where([
        'year'=>'^(19|20)\d\d$',
        'month'=>'^(0[1-9]|1[012])$',
        'day'=>'^(0[1-9]|[12][0-9]|3[01])$'
    ])->name('events');

Route::get('events/{slug}', 'EventController@view')
    ->name('event');

Route::get('news', 'BlogController@show_list')
    ->name('news_posts');
Route::get('news/{slug}', 'BlogController@view')
    ->name('news_post');


Route::get('partners', 'PartnerController@show_list');

Route::get('search', 'CatalogController@search')
    ->name('search');

// catalog and lot page
Route::group(['prefix' => 'catalog'], function () {

    // list of all categories
    Route::get('/', 'CatalogController@categories')
        ->name('catalog');

    // make a bid from lot page
    Route::post('{id}-{slug}/bid', 'BidController@create')
        ->where(['id' => '^[0-9]+$'])
        ->middleware(['hasRole:buyer', 'cardDetailsFilled','emailVerified', 'lotIsBiddable', 'bidIsAboveMinAmount', 'bidConfirmed'])
        ->name('place_bid');

    // add message from lot message
    Route::post('{id}-{slug}/message', 'MessageController@create')
        ->where(['id' => '^[0-9]+$'])
        ->name('add_message')
        ->middleware(['canUserSendMessage','hasRole:buyer,seller', 'lotIsDiscussable', 'messageNotBlank', 'userIsSeller']);

    // lot page
    Route::get('{id}-{slug}', 'CatalogController@lot')
        ->where(['id' => '^[0-9]+$'])
        ->middleware('recentlyLotsViews', 'countLotViews')
        ->name('lot');

    // category page
    Route::get('{slug?}', 'CatalogController@category')->where('slug', '.*')
        ->name('category');

});


// Admin Interface Routes
Route::group(['prefix' => 'admin', 'middleware' => 'hasRole:admin'], function () {

    Route::get('dashboard', 'Admin\AdminController@dashboard');

    Route::group(['prefix' => 'dashboard'], function () {
        Route::get('active-bids', 'Admin\AdminController@activeBids');
    });

    Route::post('purge', 'Admin\AdminController@purge')->name("admin.purge");

    // Backpack\CRUD: Define the resources for the entities you want to CRUD.
    CRUD::resource('user', 'Admin\UserCrudController')->with(function () {
        // add extra routes to this resource
        Route::get('user/add_seller/new', 'Admin\UserCrudController@addSellerForm');
        Route::post('user/add_seller/save', 'Admin\UserCrudController@saveSellerForm');
    });
    CRUD::resource('category', 'Admin\CategoryCrudController')->with(function () {
        Route::get('move-lots', 'Admin\CategoryCrudController@moveLotsForm')
        ->name('category.move_lots');
        Route::post('move-lots-to', 'Admin\CategoryCrudController@moveLots')
        ->name('category.move_lots_to');
    });
    CRUD::resource('lot', 'Admin\LotCrudController')->with(function () {
        // add extra routes to this resource
        Route::get('lot/import', 'Admin\LotCrudController@importForm')
            ->name('lot-import-form');
        Route::post('lot/import', 'Admin\LotCrudController@importProcess')
            ->name('lot-import-process');
        Route::get('move-lots-to-category', 'Admin\LotCrudController@moveLotsForm')
            ->name('lots.move_lots');
        Route::post('move-lots-to-category', 'Admin\LotCrudController@moveLots')
            ->name('lots.move_lots_to');
    });

    CRUD::resource('lot-file', 'Admin\LotFileCrudController')->with(function () {
        Route::get('lot-file/rollback/{id}', 'Admin\LotFileCrudController@rollbackProcess');
    });

    CRUD::resource('event', 'Admin\EventCrudController');
    CRUD::resource('blog', 'Admin\BlogCrudController');
    CRUD::resource('page-meta', 'Admin\PageMetaCrudController');
    CRUD::resource('partner', 'Admin\PartnerCrudController');
    CRUD::resource('specific', 'Admin\SpecificCrudController')->with(function () {
        // add extra routes to this resource
        Route::get('specific/import', 'Admin\SpecificCrudController@importForm');
        Route::post('specific/import', 'Admin\SpecificCrudController@importProcess');
    });
    CRUD::resource('seller', 'Admin\SellerCrudController')->with(function () {
        // add extra routes to this resource
        Route::get('seller/details/{id}/{year}/{month}', 'Admin\SellerCrudController@sellerMonthDetail')
            ->where(['id' => '^[0-9]+$', 'year'=>'^[0-9]{4}$','month'=>'^[0-9]{1,2}$'])
            ->name('seller-month-details');
        Route::post('seller/invoice/{id}/{year}/{month}', 'Admin\SellerCrudController@sendMonthSellerInvoice')
            ->where(['id' => '^[0-9]+$', 'year' => '^[0-9]{4}$', 'month' => '^[0-9]{1,2}$'])
            ->name('seller-send-month-invoice');
        Route::get('seller/password_reset/{id}', 'Admin\SellerCrudController@resetSellerPassword')
            ->where(['id' => '^[0-9]+$']);
    });
    CRUD::resource('slider', 'Admin\SliderCrudController');
    CRUD::resource('permission', '\Backpack\PermissionManager\app\Http\Controllers\PermissionCrudController');
    CRUD::resource('role', '\Backpack\PermissionManager\app\Http\Controllers\RoleCrudController');
    CRUD::resource('faq_category', 'Admin\FaqCategoryCrudController');
    CRUD::resource('faq_question', 'Admin\FaqQuestionCrudController');
    CRUD::resource('category-alias', 'Admin\CategoryAliasCrudController');
    CRUD::resource('terms-conditions', 'Admin\TermsConditionsCrudController')->with(function () {
        // add extra routes to this resource
        Route::get('terms-conditions/parse', 'Admin\TermsConditionsCrudController@parse');
    });
    CRUD::resource('contact-us', 'Admin\ContactUsController');
    CRUD::resource('site-terms-conditions', 'Admin\SiteTermsConditionsCrudController');
    CRUD::resource('how-it-works', 'Admin\HowItWorksController');
    CRUD::resource('about-us', 'Admin\AboutUsController');

    CRUD::resource('category-icons', 'Admin\CategoryIconsController');
});


Route::group(['prefix' => 'profile', 'middleware' => 'hasRole:buyer'], function () {

    Route::get('account-info/confirm-email/{token}', 'Auth\RegisterController@confirmEmail')
        ->where(['token' => '^[a-zA-Z0-9]{30}$'])
        ->name('account-confirm-email');

    Route::post('account-info/confirm-email', 'Auth\RegisterController@confirmEmailResend')
        ->name('account-confirm-email-resend');

    Route::get('account-info', 'UserController@account')
        ->name('account-info');

    Route::get('account-info/edit', 'UserController@accountEdit')
        ->name('account-info-edit');

    Route::post('account-info/edit', 'UserController@accountEditSave')
        ->name('account-info-edit-save');

    Route::get('account-info/edit/confirm-email/{token}', 'UserController@accountConfirmEmail')
        ->where(['token' => '^[a-zA-Z0-9]{30}$'])
        ->name('account-info-edit-confirm-email');

    Route::post('account-info/edit/password', 'UserController@accountEditPassword')
        ->name('account-info-edit-password');

    /*Route::get('payment-methods', 'UserController@payment')
        ->name('payment-methods');

    Route::get('payment-methods/card/add', 'UserController@paymentCardForm')
        ->name('payment-methods-card-add');

    Route::post('payment-methods/card/add', 'UserController@paymentNewCard')
        ->name('payment-methods-card-add-save');

    Route::get('payment-methods/card/edit/{id}', 'UserController@paymentEditCardForm')
        ->where(['id' => '^[0-9]+$'])
        ->name('payment-methods-card-edit');

    Route::post('payment-methods/card/edit/{id}', 'UserController@paymentEditCard')
        ->where(['id' => '^[0-9]+$'])
        ->name('payment-methods-card-edit-save');

    Route::get('payment-methods/card/delete/{id}', 'UserController@paymentDeleteCard')
        ->where(['id' => '^[0-9]+$'])
        ->name('payment-methods-card-delete');

    Route::get('payment-methods/card/default/{id}', 'UserController@paymentSetDefaultCard')
        ->where(['id' => '^[0-9]+$'])
        ->name('payment-methods-card-default');

    Route::get('payment-methods/bank-account/add', 'UserController@paymentBankAccountForm')
        ->name('payment-methods-bank-add');

    Route::get('payment-methods/accounts', 'UserController@paymentBankAccounts')
        ->name('payment-methods-accounts');

    Route::post('payment-methods/bank-account/add', 'UserController@paymentBankAccountNew')
        ->name('payment-methods-bank-add-new');

    Route::get('payment-methods/bank-account/edit/{id}', 'UserController@paymentBankAccountEditForm')
        ->where(['id' => '^[0-9]+$'])
        ->name('payment-methods-bank-edit');

    Route::post('payment-methods/bank-account/edit/{id}', 'UserController@paymentBankAccountEditSave')
        ->where(['id' => '^[0-9]+$'])
        ->name('payment-methods-bank-edit-save');

    Route::get('payment-methods/bank-account/default/{id}', 'UserController@paymentBankAccountSetDefault')
        ->where(['id' => '^[0-9]+$'])
        ->name('payment-methods-bank-default');

    Route::get('payment-methods/bank-account/delete/{id}', 'UserController@paymentBankAccountDelete')
        ->where(['id' => '^[0-9]+$'])
        ->name('payment-methods-bank-delete');*/

    Route::get('shipping-info', 'UserController@shipping')
        ->name('shipping-info');

    Route::get('shipping-info/new', 'UserController@shippingNewForm')
        ->name('shipping-info-new');

    Route::post('shipping-info/new', 'UserController@shippingNewSave')
        ->name('shipping-info-new');

    Route::get('shipping-info/edit/{id}', 'UserController@shippingEditForm')
        ->where(['id' => '^[0-9]+$'])
        ->name('shipping-info-edit');

    Route::post('shipping-info/edit/{id}', 'UserController@shippingEditSave')
        ->where(['id' => '^[0-9]+$'])
        ->name('shipping-info-edit-save');

    Route::get('shipping-info/default/{id}', 'UserController@shippingSetDefault')
        ->where(['id' => '^[0-9]+$'])
        ->name('shipping-info-default');

    Route::get('shipping-info/delete/{id}', 'UserController@shippingDelete')
        ->where(['id' => '^[0-9]+$'])
        ->name('shipping-info-delete');

    Route::get('subscriptions', 'UserController@subscriptions')
        ->name('subscriptions');

    Route::post('subscriptions', 'UserController@subscriptionsSave')
        ->name('subscriptions-save');
});

Route::group(['prefix' => 'seller-profile', 'middleware' => 'hasRole:seller'], function () {

    Route::get('account-info', 'UserController@account')
        ->name('seller-account-info');

    Route::get('payment-methods', 'UserController@sellerPaymentBankAccounts')
        ->name('seller-payment-methods');

    Route::get('payment-methods/bank-account/add', 'UserController@paymentBankAccountForm')
        ->name('seller-payment-methods-bank-add');

    Route::post('payment-methods/bank-account/add', 'UserController@paymentBankAccountNew')
        ->name('seller-payment-methods-bank-add-new');

    Route::get('payment-methods/bank-account/edit/{id}', 'UserController@paymentBankAccountEditForm')
        ->where(['id' => '^[0-9]+$'])
        ->name('seller-payment-methods-bank-edit');

    Route::post('payment-methods/bank-account/edit/{id}', 'UserController@paymentBankAccountEditSave')
        ->where(['id' => '^[0-9]+$'])
        ->name('seller-payment-methods-bank-edit-save');

    Route::get('payment-methods/bank-account/default/{id}', 'UserController@paymentBankAccountSetDefault')
        ->where(['id' => '^[0-9]+$'])
        ->name('seller-payment-methods-bank-default');

    Route::get('payment-methods/bank-account/delete/{id}', 'UserController@paymentBankAccountDelete')
        ->where(['id' => '^[0-9]+$'])
        ->name('seller-payment-methods-bank-delete');
});

// seller lots and lot's bids
Route::group(['prefix' => 'lots', 'middleware' => 'hasRole:seller'], function () {
    // seller's list of active/closed or sold lots
    Route::get('', 'SellerController@activeLots')
        ->name('seller_active_lots');

    Route::get('close/{id}', 'SellerController@closeLot')
        ->name('seller_close_lot')->where(['id' => '^[0-9]+$']);

    Route::get('closed', 'SellerController@closedLots')
        ->name('seller_closed_lots');

    Route::get('sold', 'SellerController@soldLots')
        ->name('seller_sold_lots');

    Route::get('bids', 'SellerController@bids')
        ->name('seller_bids');

    // accept/decline bid
    Route::get('{method}/{id}', 'SellerController@switchBidStatus')
        ->where(['method' => '^(decline|accept)$', 'id' => '^[0-9]+$'])
        ->middleware('bidIsMine')
        ->name('seller_switch_bid_status');

    // counter bid
    Route::post('counter/{id}', 'SellerController@counterBid')
        ->where(['id' => '^[0-9]+$'])
        ->middleware('bidIsMine', 'counterBidConfirmed', 'bidIsAboveMinAmount', 'bidIsBelowPriceTo')
        ->name('seller_counter_bid');

    Route::get('edit/{id}', 'SellerController@editLots')
        ->name('seller_edit_lots');

    Route::post('save/{id}', 'SellerController@saveLots')
        ->name('seller_save_lots');

    Route::get('create/', 'SellerController@createLots')
        ->name('seller_create_lots');
    Route::get('create-disabled/', 'SellerController@createLotsDisabled')
        ->name('seller_create_lots_disabled');

    Route::post('store/', 'SellerController@storeLots')
        ->name('seller_store_lots');

    Route::get('bulk', 'SellerController@addBulk')
        ->name('seller_bulk_upload');
    Route::get('bulk-disabled', 'SellerController@addBulkDisabled')
        ->name('seller_bulk_upload_disabled');
    Route::post('bulk', 'SellerController@bulkProcess')
        ->name('seller_bulk_upload_save');

//     seller's list of received/sent/declined bids for specific lot
    Route::get('{id}', 'SellerController@lotBids')
        ->middleware('lotIsMine')
        ->name('seller_lot');
});

// buyer bids
Route::group(['prefix' => 'bids', 'middleware' => 'hasRole:buyer'], function () {
    // buyers's list of received/sent/declined or sold lots
    Route::get('', 'BuyerController@receivedBids')
        ->name('buyer_received_bids');

    Route::get('sent', 'BuyerController@sentBids')
        ->name('buyer_sent_bids');

    Route::get('declined', 'BuyerController@declinedBids')
        ->name('buyer_declined_bids');
    
    // buyers's list of bids for specific lot
    Route::get('{id}', 'BuyerController@lot')
        ->where(['id' => '^[0-9]+$'])
        ->middleware('lotHasMyBid')
        ->name('buyer_lot');

    // accept/decline bid
    Route::get('{method}/{id}', 'BuyerController@switchBidStatus')
        ->where(['method' => '^(decline|accept)$', 'id' => '^[0-9]+$'])
        ->middleware('bidIsMine')
        ->name('buyer_switch_bid_status');

    // counter bid
    Route::post('counter/{id}', 'BuyerController@counterBid')
        ->where(['id' => '^[0-9]+$'])
        ->middleware('bidIsMine', 'counterBidConfirmed', 'bidIsAboveMinAmount')
        ->name('buyer_counter_bid');
});



Route::group(['prefix' => 'messages', 'middleware' => 'hasRole:buyer,seller'], function () {
    Route::get('', 'MessageController@received')
        ->name('messages_received');

    Route::get('sent', 'MessageController@sent')
        ->name('messages_sent');

    Route::get('archived', 'MessageController@archived')
        ->name('messages_archived');

    Route::get('search', 'MessageController@search')
        ->name('messages_search');

    Route::post('reply/{message}', 'MessageController@reply')
        ->name('message_reply');
        //->middleware('messageIsMine')

    Route::post('{method}', 'MessageController@switchStatus')
        ->where(['method' => '^(read|unread|archive|restore|delete)$'])
        ->name('message_switch_status');
        //->middleware('messagesAreMine');

    Route::get('/{id}', 'MessageController@view')
        ->where(['id' => '[0-9]+'])
        ->name('message_view');
});

/*Invoices download*/
Route::group(['prefix' => 'download'], function () {

    Route::get('invoice/seller/{id}', 'DownloadController@sellerInvoice')
        ->where(['id' => '^[0-9]+$'])
        ->name('show-seller-invoice')
        ->middleware('hasRole:admin');

    Route::get('invoice/buyer/{id}', 'DownloadController@buyerInvoice')
        ->where(['id' => '^[0-9]+$'])
        ->name('show-buyer-invoice')
        ->middleware('hasRole:buyer,seller');
});
Route::get('purchases', 'PurchasesController@listLots')
    ->middleware('hasRole:buyer')
    ->name('purchases');

// not implemented yet
Route::get('managers', 'ManagersController@show')
    ->middleware('hasRole:seller')
    ->name('managers');
// not implemented yet



Route::group(['prefix' => 'parser', 'middleware' => 'hasRole:admin'], function () {
    Route::get('', 'Parser\BatchController@select')->name('parser-home');
    Route::post('process', 'Parser\BatchController@process');
//    Route::post('status', 'Parser\BatchController@status');
    Route::get('download/{id}', 'Parser\BatchController@download')
        ->name('download-parser-csv')->where('id', '[0-9]+');
//    Route::post('artnet/process', 'Parser\ArtnetController@process')->name('artnet-parser-process');

});

// Landing
Route::get('landing', 'LandingController@index')->name('landing');
Route::get('landing/about', 'LandingController@about')->name('landing_about');

Route::get('landing/faq', 'FaqController@index_landing')->name('landing_faq');

Route::get('landing/events/{year?}/{month?}/{day?}', 'EventController@show_list_landing')
    ->where([
        'year'=>'^(19|20)\d\d$',
        'month'=>'^(0[1-9]|1[012])$',
        'day'=>'^(0[1-9]|[12][0-9]|3[01])$'
    ])->name('landing_events');
Route::get('landing/events/{slug}', 'EventController@view_landing')
    ->name('landing_event');

Route::get('landing/news', 'BlogController@show_list_landing')
    ->name('landing_news_posts');
Route::get('landing/news/{slug}', 'BlogController@view_landing')
    ->name('landing_news_post');

Route::get('landing/how-it-works', function () {
    return view('landing.how_it_works');
})->name('landing_how_it_works');

Route::post('landing', 'LandingController@store');
Route::post('subscribe', 'LandingController@subscribe');

Route::get('landing/privacy', function (){
    return view('landing.policy');
});

Route::get('landing/policy', function (){
    return view('landing.policy');
});

Route::get('landing/terms', function (){
    return view('landing.terms');
});