<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BiddingTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bid_attempts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('bid_id');
            $table->integer('user_id');
            $table->enum('status', ['pending', 'accepted', 'declined', 'countered', 'expired'])->default('pending');
            $table->decimal('amount', 16, 2);
            $table->timestamps();
            $table->string('message', 500)->nullable();
        });
        Schema::table('bids', function (Blueprint $table) {
            $table->enum('last_bid_by', ['buyer', 'seller'])->default('buyer');
            $table->dropColumn('amount');
            $table->dropColumn('parent_id');
            $table->dropColumn('counter_user_id');
            $table->dropColumn('message');
        });
        $sql = 'TRUNCATE `bids`';
        DB::connection()->getPdo()->exec($sql);
        $sql = 'UPDATE `lots` SET `bids_count`=0';
        DB::connection()->getPdo()->exec($sql);
        $sql = 'ALTER TABLE `bids` CHANGE `status` `status` ENUM(\'pending\',\'accepted\',\'declined\',\'expired\') NOT NULL DEFAULT \'pending\';';
        DB::connection()->getPdo()->exec($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bid_attempts');
        $sql = 'ALTER TABLE `bids` CHANGE `status` `status` ENUM(\'pending\',\'accepted\',\'declined\',\'expired\',\'countered\') NOT NULL DEFAULT \'pending\';';
        DB::connection()->getPdo()->exec($sql);
        Schema::table('bids', function (Blueprint $table) {
            $table->dropColumn('last_bid_by');
            $table->integer('parent_id')->default(null);
            $table->decimal('amount', 16, 2);
            $table->integer('counter_user_id')->default(null);
            $table->string('message', 500)->nullable();
        });
    }
}
