<?php
namespace App\Traits;

trait BidNoticeTrait
{

    /**
     * The bid instance.
     *
     * @var Bid
     */
    public $bid;
    /**
     * The buyer instance.
     *
     * @var User
     */
    public $buyer;
    /**
     * The lot instance.
     *
     * @var Lot
     */
    public $lot;

    /**
     * The seller instance.
     *
     * @var User
     */
    public $seller;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->bid = $data['bid'];
        $this->buyer = $data['buyer'];
        $this->lot = $data['lot'];
        $this->seller = $data['seller'];
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject($this->bid_notice_subject)->view($this->bid_notice_template);
    }
}
