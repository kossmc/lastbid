<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'first_name' => 'LastBid',
            'last_name' => 'Admin',
            'email' => 'admin@gmail.com',
            'password' => bcrypt('123'),
            'phone'=>'123-456-789',
            //'address'=>'Somewhere in green-green forest',
            //'card_number'=>'0000-0000-0000-0000',
            //'cvv'=>'1234',
            //'exp_date'=> \Carbon\Carbon::now(),
            'created_at' =>\Carbon\Carbon::now(),
            'updated_at' =>\Carbon\Carbon::now(),
        ]);
        $faker = Faker\Factory::create();
        DB::table('users')->insert([
            'first_name' => $faker->firstName,
            'last_name' => $faker->lastName,
            'email' => $faker->email,
            'password' => bcrypt('123'),
            'phone'=>$faker->phoneNumber,
//            'address'=>$faker->address,
//            'card_number'=>$faker->creditCardNumber,
//            'cvv'=> $faker->numberBetween(1000, 9999),
//            'exp_date'=> $faker->creditCardExpirationDate,
            'created_at' =>\Carbon\Carbon::now(),
            'updated_at' =>\Carbon\Carbon::now(),
        ]);
        DB::table('users')->insert([
            'first_name' => $faker->firstName,
            'last_name' => $faker->lastName,
            'email' => $faker->email,
            'password' => bcrypt('123'),
            'phone'=>$faker->phoneNumber,
//            'address'=>$faker->address,
//            'card_number'=>$faker->creditCardNumber,
//            'cvv'=> $faker->numberBetween(1000, 9999),
//            'exp_date'=> $faker->creditCardExpirationDate,
            'created_at' =>\Carbon\Carbon::now(),
            'updated_at' =>\Carbon\Carbon::now(),
        ]);
        DB::table('users')->insert([
            'first_name' => $faker->firstName,
            'last_name' => $faker->lastName,
            'email' => $faker->email,
            'password' => bcrypt('123'),
            'phone'=>$faker->phoneNumber,
//            'address'=>$faker->address,
//            'card_number'=>$faker->creditCardNumber,
//            'cvv'=> $faker->numberBetween(1000, 9999),
//            'exp_date'=> $faker->creditCardExpirationDate,
            'created_at' =>\Carbon\Carbon::now(),
            'updated_at' =>\Carbon\Carbon::now(),
        ]);
    }
}
