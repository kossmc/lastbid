<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class UpdateUserPasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'password_old' => 'required|password_correct',
            'password' =>'required|string|min:6|confirmed',
        ];
    }

    public function messages()
    {
        return [
            'password_old.password_correct' => 'Entered password incorrect',
            'password_old.required' => 'Enter current password',
            'password.required' => 'Enter new password',
            'password.min' => 'New password should be at least 6 characters',
            'password.confirmed' => 'New password and confirmed password not same',
        ];
    }
}
