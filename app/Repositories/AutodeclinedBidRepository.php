<?php
namespace App\Repositories;

use Bosnadev\Repositories\Eloquent\Repository;
use App\Models\AutodeclinedBid;

class AutodeclinedBidRepository extends Repository
{
    public function model()
    {
        return AutodeclinedBid::class;
    }

    public function creater(array $data)
    {
        if ($autodeclinedBid = $this->model->create($data)) {
            return $autodeclinedBid;
        }
        return false;
    }

}
