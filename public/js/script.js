var bidTimeLeft;
var try_send_message = false;
var can_user_send_message = false;
var $html;
var onloadTargetOffset;
var nextStep;
var modalContainer = '#modal-win2',
    modalWindow = '#modal-id';
    modalCookie = '#modal-cookie';
var reloadGlobal;

// these two functions termsModal and policyModal can be optimized into one
// by passing two args into function: url and selector
// or not to do it?
function setTimezone() {
    $.ajax({
        'type':'post',
        'url': '/setTimezone',
        'data': {time_zone: moment.tz.guess()}
    });
}
function modal_Terms_Policy(url, element) {

    $.ajax({
        'type':'get',
        'url': url,
        'data': {},
        'success': function (data) {
            if (data.form) {
                showForm(data.form);
                $("." + element).scrollbar({
                    'disableBodyScroll': false,
                    'ignoreMobile': false
                });
                $("." + element).addClass('scrollbar-outer');
            }
        }
    });
}
function modal_Terms_Policy_Custom(url, element, lot_id = 0) {

    $.ajax({
        'type':'get',
        'url': url,
        'data': {lot_id: lot_id},
        'success': function (data) {
            if (data.form) {
                showForm(data.form);
                $("." + element).addClass('scrollbar-outer');

                $('#scrollColorCustom').smoothWheel();

                $('#scrollColorCustom').slimScroll({
                    distance: '5px',
                    opacity: 1,
                    color: '#CAAC8A'
                }).bind('slimscroll', function(e, pos){
                    if (pos == 'bottom') {
                        document.getElementById('lastBidTermsBtn').disabled = false;
                    }
                });
            }
        }
    });
}

// function termsModal() {
//     $.ajax({
//         'type':'get',
//         'url': '/ajax_get_terms',
//         'data': {},
//         'success': function (data) {
//             if (data.form) {
//                 showForm(data.form);
//                 $('.wrapper-terms-conditions').scrollbar({
//                     'disableBodyScroll': false,
//                     'ignoreMobile': true
//                 });
//                 $('.wrapper-terms-conditions').addClass('scrollbar-outer');
//             }
//         }
//     });
// }
// function policyModal() {
//     $.ajax({
//         'type':'get',
//         'url': '/ajax_get_policy',
//         'data': {},
//         'success': function (data) {
//             if (data.form) {
//                 showForm(data.form);
//                 $('.wrapper-private-policy').scrollbar({
//                     'disableBodyScroll': false,
//                     'ignoreMobile': true
//                 });
//                 $('.wrapper-private-policy').addClass('scrollbar-outer');
//             }
//         }
//     });
// }



function auth(type, next, reload) {
    if ((type == 'register' || type == 'add_info') && $('#register-form a[disabled]').exists()) {
        return false;
    }
    if (next) {
        nextStep = next;
    }
    if (reload) {
        reloadGlobal = true;
    }
    var url,
        ajaxType = "GET",
        form,
        callback = function(data) {
            closeModal();
            if (data.top) {
                $('header div.top-link').html(data.top);
                $('.auth-link').hide();
            }
            if (data.form) {
                showForm(data.form);
            } else if (data.status) {
                if (data.reload && reloadGlobal) {
                    location.reload(true);
                }
                if (typeof nextStep === 'function') {
                    var fn = nextStep;
                    nextStep = '';
                    return fn();
                }
            }
        },
        data = function(type) {
            switch (type) {
                case 'login_form':
                    url = '/ajax_login';
                    break;
                case 'login':
                    url = '/ajax_login';
                    ajaxType = "POST";
                    form = '#reg-form';
                    break;
                case 'register_form':
                    url = '/ajax_reg';
                    break;
                case 'register':
                    url = '/ajax_reg';
                    ajaxType = "POST";
                    form = '#register-form';
                    break;
                case 'forgot_form':
                    url = '/ajax_forgot_form';
                    ajaxType = "POST";
                    break;
                case 'forgot':
                    url = '/ajax_forgot_pass';
                    ajaxType = "POST";
                    form = '#forgot-pass-form';
                    break;
                case 'add_info':
                    url = '/ajax_add_info';
                    ajaxType = "POST";
                    form = '#register-form';
                    break;
                case 'resend_confirmation':
                    url = '/profile/account-info/confirm-email';
                    ajaxType = "POST";
                    break;
                case 'email_verified':
                    url = '/email_success';
                    ajaxType = "POST";
                    break;
                case 'lets_talk':
                    url = '/lets_talk';
                    break;
            }
            return {
                type: ajaxType,
                url: url,
                data: form ? $(form).serialize() : {},
                success: callback
            }
        };

    $.ajax(data(type));
    if(ajaxType === 'GET' && type === 'login_form' || type === 'register_form'){
        delete $.ajaxSettings.headers["X-CSRF-TOKEN"]
        $.getJSON('https://api.ipify.org?format=jsonp&callback=?', function(data) {
            $('#ip').val(data.ip)
        });
        $.ajaxSettings.headers["X-CSRF-TOKEN"] = $('meta[name="csrf-token"]').attr('content');
    }

}
function closeModal() {
    $(modalWindow).modal('hide').empty();
    $(modalContainer).empty();
    $('.modal-backdrop.fade.in').remove();
    $('body').removeClass('modal-open');
}

function closeAndBuy(){
    $(modalWindow).modal('hide').empty();
    $(modalContainer).empty();
    $('.modal-backdrop.fade.in').remove();
    $('body').removeClass('modal-open');
    setCookie('accept_lastbid_terms', 1, 365);
    buy();
}

function getCookie(name) {
    var value = "; " + document.cookie;
    var parts = value.split("; " + name + "=");
    if (parts.length == 2) return parts.pop().split(";").shift();
}

function setCookie(name,value,days) {
    var expires = "";
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days*24*60*60*1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + (value || "")  + expires + "; path=/";
}
function autoAuth() {
    var authType = getCookie('auth');
    if (authType) {
        return auth(authType);
    }
    return false;
}

function counter(url) {
    if ($('#form-counter-bid').hasClass('button-pushed')) {
        return false;
    }
    var data = {
            '_token': $('meta[name="csrf-token"]').attr('content')
    };
    if ($('#form-counter-bid').exists()) {
        url = $('#form-counter-bid').attr('action');
        data = $('#form-counter-bid').serialize();
        $('#form-counter-bid').addClass('button-pushed');
    }
        $('.place-bid-wrapper').removeClass('place-bid-error');
    $.ajax({
        type: 'POST',
        url: url,
        data: data,
        success: function (data) {
            if (data.error) {
                $('.place-bid-wrapper').addClass('place-bid-error');
				$('.bid-amount-error').html(data.message);
                $('#form-counter-bid').removeClass('button-pushed');
            } else if (data.form) {
                $('body').append(data.form);
                $('#counter_bid_modal').modal('show');

                polyfillSetPlaceholders();


                $('#form-counter-bid').attr('action', url);
                $('#counter_bid_modal').on('hidden.bs.modal', function () {
                    $(this).remove();
                });
                $('.textarea-scrollbar').scrollbar({
                    'disableBodyScroll': false,
                    'ignoreMobile': true
                });
                $('.scroll-textarea').addClass('scrollbar-outer');
            }
            else if (data.link) {
                window.location = data.link;
            }
            else {
                //reload
            }
        }
    });

}

function polyfillSetPlaceholders() {

    if ($html.data('useragent').search(/MSIE 9.0/) > 0) {

        console.log('detect ie9');

        $("input, textarea").each(function () {

            if ($(this).val() == "" && $(this).attr("placeholder") != "") {

                $(this).val($(this).attr("placeholder"));

                $(this).focus(function () {
                    if ($(this).val() == $(this).attr("placeholder")) $(this).val("");
                });
                $(this).blur(function () {
                    if ($(this).val() == "")
                        $(this).val($(this).attr("placeholder"));
                });

            }
            $(this).blur(function () {
                if ($(this).val() == "")
                    $(this).val($(this).attr("placeholder"));
            });


        });

    }
}


/* checking html5 form validation functions */
function hasHtml5Validation() {
    return typeof document.createElement('input').checkValidity === 'function';
}

function showForm(html) {

    $(modalContainer).html(html);
    $(modalWindow).modal('show');
    if (document.querySelector('#popup-comment')) {
        $('#popup-comment').val($('#buy-form #message').val());
    }
    /*input masc init*/
    if (document.querySelector('#exp_date')) {
        $('#exp_date').inputmask({mask: "99/99", placeholder: "mm/yy"});
    }
    if (document.querySelector('#modal-id')) {
        $('.textarea-scrollbar').scrollbar({
            'disableBodyScroll': false,
            'ignoreMobile': true
        });
        $('.scroll-textarea').addClass('scrollbar-outer');
    }
    if(document.querySelector('.second-step-register')){
        var card = new CardValidation('.second-step-register', true);


    }
    if(document.querySelector('#register-form')){

        $('a[disabled]').on('click', function(e){
            $(this).attr('href', null);
        });


        var wrapper = $('.wrap-check-policy');
        // $('.btn-sign-up-with').on('click', function(){
        //     $('.sign-with-account').show();
        //     $('.sign-without-account').hide();
        //     $('.row.wrapper-sign-up-with').hide();
        // });

        $('.wrapper-sign-up-agreement').scrollbar({
            onScroll: function(y, x){

                if(Math.abs(Math.floor(y.scroll) - y.maxScroll) <= 1){
                    wrapper.find('input[type="checkbox"]').removeAttr('disabled');
                    wrapper.find('label').removeClass('disabled-label');
                }
            }
        });
        $('.wrapper-sign-up-agreement').addClass('scrollbar-outer');

        wrapper.find('input[type="checkbox"]').change(function() {
            if(this.checked) {
                wrapper.find('.btn').removeClass('btn-sign-up-disabled').removeAttr('disabled');
            }
            else{
                wrapper.find('.btn').addClass('btn-sign-up-disabled').attr('disabled', 'disabled');
            }
        });

        // wrapper.find('.btn-sign-up-next-step').on('click', function(){
        //     var nav = $('.nav.sign-up-tabs');
        //     var tabContent = $('.tab-content');
        //     nav.find('li:nth-child(1)').removeClass('active');
        //     nav.find('li:nth-child(2)').addClass('active');
        //     nav.find('[href="#auth-tab2"]').attr('data-toggle', 'tab')
        //     $('#auth-tab1').removeClass('active in');
        //     $('#auth-tab2').addClass('active in');
        //
        //     var card = new CardValidation('#register-form', true);
        // });
    }

    if($('#register-form>.container-form').length > 0){
        $('#register-form>.container-form')[0].scrollTop = '0';
    }

    polyfillSetPlaceholders();

}

function buy() {
    $('.place-bid-wrapper').removeClass('place-bid-error');
    if (parseFloat($('#offer-value')[0].value)) {
        $.ajax({
            type: "POST",
            url: $('#buy-form').attr('action'),
            data: $('#buy-form').serialize(),
            success: function (data) {
                $('#make-offer-btn').removeClass('button-pushed');
                if (data.auth) {
                    return auth('login_form', buy);
                }
                if (data.buyer !== undefined && !data.buyer) {
                   return showForm(data.modal);
                }
                if (data.error) {
                    $('.place-bid-wrapper').addClass('place-bid-error');
                    $('.bid-amount-error').html(data.message);
                }
                else if (data.status) {
                    // showForm(data.congWindow);
                    $('#modal-id').modal('hide').empty();
                    $('#modal-win2').empty();
                    $('.buy-it-wrap').html(data.congWindow).addClass('congratulations-wrap');
                    /*set necessary classes*/
                    // $('#modal-win2 button[data-dismiss="modal"]').addClass('reload_page');
                } else if (data.error) {
                    $('#modal-id').modal('hide');
                    $('#modal-win2').modal('hide');
                    alert('You already have 5 bids to current product!');
                } else if (data.form) {
                    showForm(data.form);
                }
            }
        });
    }
}

function stickyBuyItBlock(elem) {
    var targetSelector = elem != undefined ? elem : $('#buy-it-wrap');
    var targetOffset = $(targetSelector).offset();
    var targetParentOffset = $(targetSelector).parent().offset();

    var targetOffsetY = targetOffset.top;
    var targetHeight = $(targetSelector).outerHeight();
    var stopperBlock = $('.most-popular');
    var stopperBlockOffset = $(stopperBlock).offset();
    var windowTop = $(window).scrollTop();

    // console.log(windowTop + ' > ' + targetOffsetY + ':' + (windowTop > ( targetOffsetY ) ) );

    if (windowTop > ( onloadTargetOffset.top ) && !( ( windowTop + targetHeight ) > stopperBlockOffset.top )) {
        // console.log('sticky start');
        $(targetSelector).removeClass('sticky-stop');
        $(targetSelector).addClass('sticky-start');
        $(targetSelector).css('top', (windowTop - targetParentOffset.top )).css('bottom', 'auto');
    }
    else if (( windowTop + targetHeight ) > stopperBlockOffset.top) {
        // console.log('sticky stop');
        $(targetSelector).removeClass('sticky-start');
        $(targetSelector).addClass('sticky-stop');
        $(targetSelector).css('top', ( $(targetSelector).parent().outerHeight(true) - targetHeight ));
    }
    else {
        if ($(targetSelector).hasClass('sticky-start')) {
            // console.log('hasclass sticky start!');
            $(targetSelector).css('top', ( windowTop - targetParentOffset.top ));

            if (parseInt($(targetSelector).css('top')) < 0) {
                $(targetSelector).removeClass('sticky-start');
                $(targetSelector).css('top', 0);
                // console.log('target selector css top < 0 !');
            }
        }
    }
}

/*Send message to seller from lot page*/
function send_message() {
    try_send_message = true;
    close_message_popup();
    $.ajax({
        type: "POST",
        url: $('#message-form').attr('action'),
        data: $('#message-form').serialize(),
        success: function (data) {
            if (data.status) {
                try_send_message = false;
                clear_message_form();
                showFlash(data.message);
            } else if (data.error) {
                alert(data.error);
                show_message_popup();
            } else if (data.form) {
                showForm(data.form);
            } else if (data.seller) {
                location.reload();
            }
        }
    });
}




function showFlash(message) {
    if (message) {
        var before = $('div.bids-view');
        before.addClass('bids-view--top-notification');
        $(message).insertBefore(before);
    }
}

function get_message_popup() {
    if (can_user_send_message) {
        return true;
    }
    $.ajax({
        type: "POST",
        url: $('#message-form').attr('action'),
        data: {"can_user_send_message": false, "_token": window.Laravel.csrfToken},
        success: function (data) {
            if (data.auth) {
                try_send_message = true;
                auth('login_form', show_message_popup)
            }
            if (data.status) {
                can_user_send_message = true;
                show_message_popup();
            }
        }
    });
    return false;
}

function show_message_popup() {
    $('#popup-overlay').show();
    $('.popup.popup-ask-question').show();
    $('.textarea-scrollbar').scrollbar({
        'disableBodyScroll': false,
        'ignoreMobile': true
    });
    $('.scroll-textarea').addClass('scrollbar-outer');
}

function close_message_popup() {
    $('#popup-overlay').hide();
    $('.popup.popup-ask-question').hide();
}

function clear_message_form() {
    $('form#message-form textarea[name="letter-reply-msg"]').val('');
}

function initMap() {
    var locationAdress = {lat: Number($('#latitude').val()), lng: Number($('#longitude').val())};
    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 15,
        center: locationAdress
    });
    var marker = new google.maps.Marker({
        position: locationAdress,
        map: map
    });
}

function updateCounters() {
    var messagesCounters = $('.messages-counter'),
        bidsCounters = $('.bids-counter');

    $.ajax({
        type: 'POST',
        url: '/ajax_get_counters',
        data: {
            '_token': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (data) {
            if (data.bids_count > 0) {
                bidsCounters.addClass('active');
            }
            else {
                messagesCounters.removeClass('active');
            }
            bidsCounters.find('.counter').html(data.bids_count);
            if (data.messages_count > 0) {
                messagesCounters.addClass('active');
            }
            else {
                messagesCounters.removeClass('active');
            }
            messagesCounters.find('.counter').html(data.messages_count);
        }
    });
}

/*checks if element exist on page*/
jQuery.fn.exists = function () {
    return jQuery(this).length > 0;
};

/*add or replace params in url*/
function insertParams(params) {
    var result;
    var ii = params.length;
    var queryString = document.location.search.substr(1);
    var kvps = queryString ? queryString.split('&') : [];
    var kvp;
    var skipParams = [];
    var i = kvps.length;
    while (i--) {
        kvp = kvps[i].split('=');
        if (kvp[0].slice(-2) != '[]') {
            ii = params.length;
            while (ii--) {
                if (params[ii][0] == kvp[0]) {
                    kvp[1] = params[ii][1];
                    kvps[i] = kvp.join('=');
                    skipParams.push(ii);
                }
            }
        }
    }
    ii = params.length;
    while (ii--) {
        if (skipParams.indexOf(ii) === -1) {
            kvps.push(params[ii].join('='));
        }
    }
    result = kvps.length ? '?' + kvps.join('&') : '';
    return result;
}

function updateUrl($params) {
    var newUrl = document.location.pathname + insertParams($params);
    history.replaceState('', '', newUrl);
    document.location = newUrl;
}

function getPopupContent(alias,modal,title){
    var res = false;
    $.ajax({
        async: false,
        url: '/terms/'+alias+'/get',
        type: 'get',
        dataType: 'json',
        //data: data,
        success: function (result) {
            if(result.terms){
                $('.'+modal+' .partner-name').html(title);
                $('.'+modal+' .inner-wrapper-sign-up-agreement').html(result.terms.text);
                $("#accept").data('type', alias);
                res = true;
            } else {
                buy();
            }
        }
    });
    return res;
}

function selectType() {

    var select = $('.select-style');
    var select_ul = select.find('ul');

    select.find('span').on('click', function(){

        $(this).toggleClass('up-down-arrow');
        var ul = $(this).next('ul');

        if(ul.is(':hidden')){
            ul.show();
        }
        else{
            ul.hide();
        }
    });


    select_ul.find('li').on('click', function(e){
        if(e.target.className == 'arrow-icon') return;

        catSpecificTable.getTable(e.target.textContent);

        var select = $(this).closest('.select-style');
        var selct_opt = select.find('option');
        var select_ul = select.find('>ul');

        select.find('span').toggleClass('up-down-arrow');

        select.find('span').addClass('selected-suffix');

        select.find('span').text($(e.target).text());
        selct_opt.attr('value', $(e.target).text());
        console.log($(this).attr("data-id"))
        if($(this).attr("data-id") != undefined) {
            selct_opt.attr('value', $(this).attr("data-id"));
        }
        selct_opt.text($(e.target).text());
        select_ul.hide();
    });
}


$(document).ready(function () {

    uncheckedRadio();

    $(".category-icon").mouseover(function () {
        $(this).find('img').attr('src', $(this).find('img').data("hover"));
    }).mouseout(function () {
        $(this).find('img').attr('src', $(this).find('img').data("src"));
    });

    /*CSRF protection for AJAX requests*/
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    setTimezone();
    autoAuth();
    /* add placeholders for inputs and textareas in IE9 */
    $html = $('html');
    $html.attr('data-useragent', navigator.userAgent);

    polyfillSetPlaceholders();

    selectType();

     /* in Firefox after refreshing page - uncheck all checked checkboxes */
      if ($('form#bulk_action').exists()) {
          $('form#bulk_action')[0].reset();
      }

    /*tool tip init*/
    $('[data-toggle="tooltip"]').tooltip();
    /*Event listeners*/
    /*send offer on enter*/
    $('#buy-form').submit(function (e) {
        $('.place-bid-wrapper').addClass('place-bid-error');
        e.preventDefault();

        //var dataAttrs = $(this).find('.place-bid-js').data();

        var from = parseInt($(this).find('.price').attr("data-from"));

        var amount = parseInt($(this).find('#offer-value').val());


        if(from > amount){
            $('.place-bid-wrapper').addClass('place-bid-error');
            $('.bid-amount-error').html('Sorry, your bid is too low');
            $('#offer-value').keyup(function () {

                var from = parseInt($(this).parents('#buy-form').find('.price').attr("data-from"));

                var amount = parseInt($(this).val());

                if(from > amount){
                    $('.place-bid-wrapper').addClass('place-bid-error');
                    $('.bid-amount-error').html('Sorry, your bid is too low');
                }else{
                    $('.place-bid-wrapper').removeClass('place-bid-error');
                    $('.bid-amount-error').html('');

                }
            });
            return
        }

        // if(!getCookie('accept_'+dataAttrs.alias+'_terms')){
        //     var content = getPopupContent(dataAttrs.alias, dataAttrs.targetModal, dataAttrs.partnerName);
        //     if(!content || dataAttrs.alias === 'lastbid'){
        //         $('.new-modal-1').modal('hide').empty();
        //         $('.modal-content').empty();
        //         $('.modal-backdrop.fade.in').remove();
        //         $('body').removeClass('modal-open');
        //         if(!getCookie('accept_lastbid_terms')) {
        //             modal_Terms_Policy_Custom('/ajax_get_terms_custom', 'wrapper-terms-conditions');
        //         }
        //         return;
        //     }
        //     if(content){
        //         document.querySelector('[data-modal=' + dataAttrs.targetModal + ']').classList.add('show');
        //     }
        //
        //     $("#accept").click(function(){
        //         setCookie('accept_'+$(this).data('type')+'_terms', 1, 365);
        //         $('.new-modal-1').modal('hide').empty();
        //         $('.modal-content').empty();
        //         $('.modal-backdrop.fade.in').remove();
        //         $('body').removeClass('modal-open');
        //         if(!getCookie('accept_lastbid_terms')) {
        //             modal_Terms_Policy_Custom('/ajax_get_terms_custom', 'wrapper-terms-conditions');
        //         }
        //         buy();
        //     });
        // } else {
        //     buy();
        // }
        buy();

    });
    /*offer product button*/
    $('#make-offer-btn').click(function (e) {
		
        e.preventDefault();
		if (!$(this).hasClass('button-pushed')) {
			$(this).addClass('button-pushed');
			polyfillSetPlaceholders();

			if (hasHtml5Validation()) {
				var bid_sum = $('#offer-value')[0];

				if (bid_sum.checkValidity()) {
					buy();
				}
				else {
					bid_sum.reportValidity();
					$(this).removeClass('button-pushed');
				}
			}
			else {
				var bid_sum = $('#offer-value');

				var pattern = bid_sum.attr('pattern');
				var value = bid_sum.val();
				var regExp = new RegExp(pattern);

				if (regExp.test(value)) {
					buy();
				}
				else {
					bid_sum.css({
						'border': '1px solid #f77d7d',
						'color': '#f77d7d'
					});
					bid_sum.val('Please fill up this field');
					setTimeout(function(){
						bid_sum.removeAttr('style');
						bid_sum.val(bid_sum.attr('placeholder'));
					}, 1000);
					$(this).removeClass('button-pushed');
				}
			}
		}
    });

    $('#scrollColor').slimScroll({
        distance: '5px',
        opacity: 1,
        color: '#CAAC8A'
    }).bind('slimscroll', function(e, pos){
        if (pos == 'bottom') {
            document.getElementById('accept').disabled = false;
        }
    });

    $('#scrollColor1').slimScroll({
        distance: '5px',
        opacity: 1,
        color: '#CAAC8A'
    });

    $('.closeBtn').on('click', function(){
        $('.popup').removeClass('show');
    })

	$('body').on('hidden.bs.modal', '#modal-id', function () {
		$('#make-offer-btn').removeClass('button-pushed');
        closeModal();
	});

    /*buy product button*/
    $('#buy-btn').click(function (e) {
        e.preventDefault();
       buy();
    });
    /*Add info save button during buying*/
    $('#modal-win2').on('click', '#deposit-info-btn', function (e) {
        e.preventDefault();
		$('#buy-form #message').val($('#popup-comment').val());
        var confirmed_input = $('#buy-form input#confirmed'),
            confirmed = parseInt(confirmed_input.val());
        if (confirmed === 0) {
            confirmed_input.attr('value', '1');
        }
        buy();
    });
    /*close pop-up if user not agree with deposit*/
    $('#modal-win2').on('click', 'a#decline', function (e) {
        e.preventDefault();
		$('#buy-form #message').val($('#popup-comment').val());
        $('#modal-id').modal('hide').empty();
        // $('#modal-win');
    });

    /*remove modal from page html when modal on product page close*/
    $('#modal-win2').on('click', '[data-dismiss="modal"]', function (e) {
        e.preventDefault();
        closeModal();
        try_send_message = false;
    });
    $('body').on('click', 'div.modal-backdrop.in', closeModal);
    /*list of categories in left sidebar - toggling*/
    $('.sidebar-wrap nav ul li.level em').on('click', function(){
        $(this).closest('.level').toggleClass('active');
    });
    $('.select-category ul li.level em').on('click', function(){
        $(this).closest('.level').toggleClass('active');
    });
    // close/open categories block
    $('.open-categories-left a.categories-left').on('click', function(e){
        e.preventDefault();
        $(this).closest('.open-categories-left').toggleClass('active');
        $('#modal-win').toggleClass('active');
    });


    //sticky buy-it block on product page
    if ( document.querySelector('#buy-it-wrap') ) {
        onloadTargetOffset = $('#buy-it-wrap').offset();
        stickyBuyItBlock();

        $(window).on('scroll', function () {
            stickyBuyItBlock();
        });
    };

    $(".product-group").colorbox({rel: 'product-group', transition: "none", width: "90%", height: "90%"});
    if ($(".product-group").exists()) {
        /*preload lot image*/
        new Image().src = $(".product-group").first().attr('href');
    }

    $('.top-primary-menu .icon-search_icon').on('click', function(e){
        e.preventDefault();
        $('nav.top-primary-menu').addClass('non-visible');
        $('nav.search-active').addClass('visible');
		$('#search-query').focus();
    });

    $(document).mouseup(function(e) {
        var container = $("nav.search-active");
        // e.preventDefault();
        // e.stopImmediatePropagation();

        // if the target of the click isn't the container nor a descendant of the container
        if (!container.is(e.target) && container.has(e.target).length === 0) {
            $('nav.top-primary-menu').removeClass('non-visible');
            $('nav.search-active').removeClass('visible');        }
    });

    if ($(window).width() > 786) {
        $('.alert').hide().delay(750).slideDown(400);
    };
    $('.close_btn').click(function() {
        $('.close_btn').fadeOut(200);
        $('.alert').slideUp(400);
    });
    $('.counter-bid-modal').on('click', function () {
		if ($('.popup-view-bids').exists()) {
			$('#popup-overlay').hide();
			$('.popup-view-bids').hide();
		}
		counter($(this).data('href'));
		return false;
    });
    $('.current-bid-tabs-nav').click(function () {
        $('.full-info').removeClass('full-info');
    });
	$('.icon-esc_icon').on('click', function () {
		$('.lot-top-notification').hide();
		$('.bids-view--top-notification').removeClass('bids-view--top-notification');
	});

	if ($('.messages-counter').exists() || $('.bids-counter').exists()) {
        setInterval(updateCounters, 100000);
    }

    $('.current-bid-tabs-nav-level2 > li a').on('click', function(){
        $('.current-bid-tabs-content').addClass('full-info');
    });
    $('.page-profile-content>ul>li>a[data-toggle="tab"]').on('click', function (e) {

        $('.current-bid-tabs-content.full-info').removeClass('full-info');
        $('.tab-lots a[data-toggle="tab"]').parent().removeClass('active');
        $('.tab-lots .tab-pane.active').removeClass('active');
		$('.current-bid-tabs-nav-level2 > li').removeClass('active');
    });
	$('.btn-close-exapanded-bid').on('click', function(){
        $('.current-bid-tabs-content.full-info').removeClass('full-info');
        $('.tab-lots a[data-toggle="tab"]').parent().removeClass('active');
        $('.tab-lots .tab-pane.active').removeClass('active');
		$('.current-bid-tabs-nav-level2 > li').removeClass('active');
	});

    $('.btn-ask-question-seller, .link-to-request-shipping-estination').on('click', function () {
        if (get_message_popup()) {
            show_message_popup();
        }
    });
    // close modals
    $('#popup-overlay, .btn-cancel-send-msg').on('click', function () {
        try_send_message = false;
        close_message_popup();
    });

    $('.btn-clear-reply-msg').on('click', function () {
        clear_message_form();
    });

    $('.btn-send-msg').on('click', function () {
        send_message();
    });

    /*sorting lot on seller lots page*/
    $('.icon-sorting_up').on('click', function () {
        updateUrl([['sort_latest', 'ASC']]);
    });
    $('.icon-sorting_up.down').on('click', function () {
        updateUrl([['sort_latest', 'DESC']]);
    });



	$('.product-price-range-details p').on('click', function(){

		$(this).toggleClass('active');
		if($(this).next().is(':visible')){
			$(this).next().slideUp();
		}
		else{
			$(this).next().slideDown();

		}
    });
    
    // from product page back to catalog or subcatalog depends on where came from
    $('.product-link-back').on('click', function(e) {

        var prev_link = document.referrer;
        var tail = prev_link.substr(prev_link.lastIndexOf('/')+1, prev_link.length);
        var previous;

        if (document.referrer) {
            previous = tail === "catalog" ? "http://" + location.host + "/catalog"
                                          : "http://" + location.host + "/catalog/" + tail;
        }
        else{
            previous = "http://" + location.host + "/catalog";
        }

        $(this).attr('href', previous);
    });
    if (document.querySelector('.faq-page')) {
        (function(){
            var questions = document.querySelector('.faq-category-list');
            var answers = document.querySelector('.category-wrap-item');
            var activeQuestion = document.querySelector('.faq-category-list [data-section-id="section-1"]');
            var activeCategory = document.querySelector('.category-wrap-item [data-section-id="section-1"]');
            var searchPage = document.querySelector('.search-faq');

            function questionsClickHandler(e) {
                var question = e.target;
                var sectionId = question.dataset.sectionId;
                if (!sectionId) return;

                var category = answers.querySelector('[data-section-id="' + sectionId + '"]');

                activeQuestion.classList.remove('active');
                question.classList.add('active');
                activeQuestion = question;

                activeCategory.classList.remove('active');
                category.classList.add('active');
                activeCategory = category;
            }

            function asnwresClickHandler(e) {
                var answer = e.target;

                while (answer != this) {
                    if ( answer.classList.contains('faq-answer') ) {
                        answer.classList.toggle('active');
                        return;
                    }

                    answer = answer.parentNode;
                }
            }

            answers.addEventListener('click', asnwresClickHandler);

            if (!searchPage) {
                activeQuestion.classList.add('active');
                activeCategory.classList.add('active');
                questions.addEventListener('click', questionsClickHandler)
            }
        })();
    }
    $(document).on("submit", "#subscribeToNL", function (e) {
        var form = $(this);
        e.preventDefault();
        var data = $(form).serialize();
        var url = "/" + form.attr('action');
        var emailErrorElem = $('#subscribeEmailFalse');

        if(!validateEmail(this)) return;

        $.ajax({
            type: "POST",
            data: data,
            dataType: "json",
            url: url,
            success: function (data) {
                $('#lets-talk-header').text('');
                $('#lets-talk-notice').text('');

                if (data.status) {
                    $("#lets-talk-header").text(data.message);
                    $("#lets-talk-notice").text(data.note);

                    $("#letsTalk").show();
                }
            },
            error: function (errors) {
                //var errorBlock = this.find(".error-notification")
            },
            complete: function () {
                $( "#subscribeToNL input" ).val("");
            }
        });

        var letsTalk = document.getElementById("letsTalk");

        var close_letsTalk = document.getElementById("letsTalk_close");

        close_letsTalk.onclick = function () {
            letsTalk.style.display = "none";
        };

        function validateEmail(form) {
            let email = form.getElementsByTagName('input')[0].value;

            let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            let isValidate = re.test(String(email).toLowerCase())

            if(isValidate){
                emailErrorElem.hide();
                return isValidate;
            }else{
                emailErrorElem.show();
                return isValidate;
            }
        };
    });
});

var catSpecificTable = {
  getTable : function(catTitle){
      $.ajax({
          type: "GET",
          dataType: "json",
          url: "/lots/ajax_get_specifics_table/" + catTitle,
          success: function (data) {
              $("#category-specific-table").html(data.form);
          },
          error: function (errors) {
              console.log("error geting specifics table");
          },
          complete: function(){
              uncheckedRadio();
          }
      });
  }
};

function uncheckedRadio(){
    // $('input[type=radio]').click(function(e){
    //     if($(e).prop('checked')){
    //         $(e).prop('checked', false);
    //     }
    // });
}

var expandLotDescription = class {
    constructor (selectorBtns, winWidth) {
        this.selectorBtns = selectorBtns;
        this.winWidth = winWidth;
        let obj = this;

        $(selectorBtns).each(function(index){
            obj.init(index);
        });
    }

    toggle(e) {
        if(this.elemTarget.length > 0 && this.isWinWidth()) {
            $(this.elemTarget).toggle('slow');
            $(this.elemBtn).parent().toggleClass('active');
        }
    }

    init(index) {
        $(this.selectorBtns).eq(index).click(this, function(e){
            e.data.prepare(e.target);
        });
    }

    prepare(e){
        this.elemBtn = e;
        this.elemTarget = $(e).parent().next();
        this.toggle();
    }

    isWinWidth() {
        if(this.winWidth === undefined || this.winWidth > document.documentElement.clientWidth){
            return true;
        }

        return false;
    }
};

