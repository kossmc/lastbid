@extends('landing.content')
@section('title','News')

@section('content_page')
    @include('blog.view_content', ['landing' => true])
@endsection