<?php

namespace App\Http\Middleware;

use App\Models\Lot;
use App\Models\User;
use App\Models\ViewedLot;
use Illuminate\Support\Facades\Auth;
use Closure;

class CountLotViews
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = $this->getUser();
        $this->updateLotCount($user, $request->id);
        return $next($request);
    }

    private function getUser()
    {
        $user = null;
        if (!Auth::guest()) {
            $user = Auth::user();
            if ($quest_id = request()->cookie('guest_id')) {
                $pastViewed = ViewedLot::where ('quest_id', $quest_id)
                    ->orWhere('user_id', $user->id)
                    ->get()
                    ->pluck('lot_id')
                    ->unique();
                ViewedLot::where ('quest_id', $quest_id)->orWhere('user_id', $user->id)->delete();
                if ($pastViewed && $pastViewed->isNotEmpty()) {
                    $user->userViewedLots()->sync($pastViewed);
                }
                forgetCookie('guest_id');
            }
        } else {
            $user = request()->cookie('guest_id');
            if (!$user) {
                $user = $this->generateGuestId();
                addCookie('guest_id', $user);
            }
        }
        return $user;
    }

    private function generateGuestId()
    {
        $id = uniqid();
        $exist = ViewedLot::where('quest_id', $id)->exists();
        if ($exist) {
            $id = $this->generateGuestId();
        }
        return $id;
    }

    private function updateLotCount($user, $lot_id)
    {
        $viewed = null;
        if ($user instanceof User) {
            $viewed = $user->whereHas('userViewedLots', function ($query) use ($lot_id) {
                $query->where('lot_id', $lot_id);
            })->get();
        } else {
            $viewed = ViewedLot::guestLotsViewed($user, $lot_id)->get();
        }
        if (!$viewed || $viewed->isEmpty()) {
            $lot = Lot::find($lot_id);
            if ($lot) {
                $lot->increment('views_count');
                if ($user instanceof User) {
                    $user->userViewedLots()->attach($lot_id);
                } else {
                    ViewedLot::create(['quest_id' => $user, 'lot_id' => $lot_id]);
                }
            }
        }
    }
}
