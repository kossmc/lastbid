<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */

$factory->define(\App\Models\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});
/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(\App\Models\Category::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->word,
    ];
});
/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(\App\Models\Product::class, function (Faker\Generator $faker) {
    return [
        'category_id'=>$faker->numberBetween(1, 5),
        'name' => $faker->word,
        'description' => $faker->sentence,
        'reserve_price' => $faker->numberBetween(1, 20000),
        'final_price' => 0
    ];
});
/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(\App\Models\FaqCategory::class, function (Faker\Generator $faker) {
    return [
        'title' => $faker->sentence,
        'active' => 1,
        'order' => 0,
    ];
});
/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(\App\Models\FaqQuestion::class, function (Faker\Generator $faker) {
    return [
        'faq_category_id' => \App\Models\FaqCategory::all()->random()->id,
        'question' => $faker->sentence,
        'answer' => $faker->sentence,
        'active' => 1,
        'order' => 0,
    ];
});

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(\App\Models\CategoryAlias::class, function (Faker\Generator $faker) {
    return [
        'category_id' => \App\Models\Category::all()->random()->id,
        'alias' => $faker->sentence,
    ];
});
