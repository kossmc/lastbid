#!/usr/bin/env bash
echo "Setting up your project..."

echo "[CMS] Running composer install command..."
cd /lastbid/
composer install > /dev/null 2>&1

echo "[CMS] Running npm install command..."
npm install > /dev/null 2>&1

echo "[CMS] Creating environment configuration..."
php -r "file_exists('.env') || copy('.env.example', '.env');"

echo "Setting permissions..."
chmod 777 -R /lastbid/

echo "[CMS] Running artisan commands..."
php artisan key:generate > /dev/null 2>&1
php artisan migrate > /dev/null 2>&1
php artisan optimize > /dev/null 2>&1
php artisan db:seed --class=AboutUsSeeder
php artisan db:seed --class=ContactUsSeeder
php artisan db:seed --class=HowItWorksSeeder
php artisan db:seed --class=SiteTermsConditionsSeeder
chmod -R 0777 storage
chmod -R 0777 bootstrap
sudo service supervisor restart all
php artisan queue:restart