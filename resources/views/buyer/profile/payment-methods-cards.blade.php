@extends('buyer.profile.profile')
@section('title', 'Payment Methods')
@section('profile_content')
    <div class="buyer-account">
        <section class="payment">
            <div class="title">
                <ul>
                    <li class="active"><a href="javascript:void 0;">My cards</a></li>
                    <li><a href="{{route('payment-methods-accounts')}}">Bank accounts</a></li>
                </ul>
                <div class="btn-wrap"><a class="btn" href="{{route('payment-methods-card-add')}}">Add new card</a></div>
            </div>
            @forelse($cards as $card)
                <section class="payment-card
                @if ($card->is_visa)payment-card--visa @endif
                @if ($card->is_master)payment-card--master-card @endif
                @if ($card->is_american)payment-card--american @endif">
                    <div class="card-wrapper">
                        <div class="chip"></div>
                        <div class="code">@if ($card->is_visa || $card->is_master)<span class="x4"></span><span class="x4"></span><span
                                    class="x4"></span>@else<span class="x4"></span><span
                                    class="x6"></span>@endif<strong>{{$card->card_number}}</strong>
                        </div>
                        <div class="expirision-date"><em>
                                valid<br>thru </em><span></span><strong></strong><span>                          </span>
                        </div>
                        <div class="owner">{{$card->card_name}}</div>
                        <div class="card-type"></div>
                    </div>
                    <div class="card-info">
                        <div class="head">@if($card->default)<a class="default icon-done" href="#">Default
                                card</a>@else<a class="set-default" href="
                        {{route('payment-methods-card-default', $card->id)}}">Set as default</a>@endif<a
                        class="link" href="javascript:void 0;" data-toggle="modal" data-target="#conf_modal_{{$card->id}}">Delete
                                card</a><a class="link" href="{{route('payment-methods-card-edit',$card->id)
                                }}">Edit</a></div>
                        <div class="body">
                            <p><span>Card Number:</span>**** **** **** {{$card->card_number}}</p>
                            <p><span>Card owner:</span>{{$card->card_name}}</p>
                            <p><span>Billing address:</span>{{$card->city}}, {{$card->country}}</p>
                        </div>
                    </div>
                </section>
            @empty
                <h1 class="notification-no-content">You have no active cards!</h1>
            @endforelse
        </section>
    </div>
@endsection
@section('custom_modal')
    @include('buyer.inc.confirm_deleting_pop_up',
    ['items'=>$cards,'route'=>'payment-methods-card-delete', 'name'=>'card'])
@endsection
