<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\SpecificImportRequest;
use App\Models\LotSpecificOptions;
use App\Models\SpecificOption;
use App\Repositories\ImportSpecificRepository;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use App\Http\Requests\StoreSpecificRequest as StoreRequest;
use App\Http\Requests\UpdateSpecificRequest as UpdateRequest;

class SpecificCrudController extends CrudController
{
    public function setup()
    {

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Specific');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/specific');
        $this->crud->setEntityNameStrings('specific', 'specifics');

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */

//        $this->crud->setFromDb();

        // ------ CRUD FIELDS
        // $this->crud->addField($options, 'update/create/both');
        // $this->crud->addFields($array_of_arrays, 'update/create/both');
         $this->crud->addFields([
             [
                 'name' => 'name',
                 'label' => "Specific name"
             ],
             [
                 // 1-n relationship
                 'label' => "Category", // Table column heading
                 'type' => "select",
                 'name' => 'category_id', // the column that contains the ID of that connected entity;
                 'entity' => 'category', // the method that defines the relationship in your Model
                 'attribute' => "title", // foreign key attribute that is shown to user
                 'model' => "App\Models\Category", // foreign key model
             ],
             [
                 'name' => 'required',
                 'label' => 'Required / Optional',
                 'type' => 'radio',
                 'options' => [
                     0 => "Optional",
                     1 => "Required"
                 ],
             ],
             [
                 'name' => 'order',
                 'label' => "Order (Weight/Priority)",
                 'default' => 0
             ],
             [
                 'name' => 'type',
                 'label' => 'Type',
                 'type' => 'enum',
                 'attributes' => [
                     'id' => 'type-selector'
                 ]
             ],
             [
                 'name' => 'use_in_filter',
                 'label' => 'Use this specific in filter?',
                 'type' => 'checkbox',
                 'attributes' => [
                     'id' => 'use-in-filter'
                 ]
             ],
             [
                 'name' => 'filter_type',
                 'label' => 'Filter type',
                 'type' => 'enum',
                 'wrapperAttributes' => [
                     'id' => 'filter-type',
                     'style' => 'display:none'
                 ]
             ],
             [ // Table
                 'name' => 'new_options',
                 'label' => 'Add new option',
                 'type' => 'vue_table',
                 'entity_singular' => 'option', // used on the "Add X" button
                 'columns' => [
                     'name' => 'Name',
                 ],
                 'wrapperAttributes' => [
                     'id' => 'options-table'
                 ]
             ],
         ], 'create');
         $this->crud->addFields([
             [
                 'name' => 'name',
                 'label' => "Specific name"
             ],
             [
                 // 1-n relationship
                 'label' => "Category", // Table column heading
                 'type' => "select",
                 'name' => 'category_id', // the column that contains the ID of that connected entity;
                 'entity' => 'category', // the method that defines the relationship in your Model
                 'attribute' => "title", // foreign key attribute that is shown to user
                 'model' => "App\Models\Category",
                 'attributes' => [
                     'disabled'=>'disabled'
                 ]
             ],
             [
                 'name' => 'required',
                 'label' => 'Required / Optional',
                 'type' => 'radio',
                 'options' => [
                     0 => "Optional",
                     1 => "Required"
                 ],
             ],
             [
                 'name' => 'order',
                 'label' => "Order (Weight/Priority)",
                 'default' => 0
             ],
             [
                 'name' => 'type',
                 'label' => 'Type',
                 'type' => 'enum',
                 'attributes' => [
                     'id' => 'type-selector'
                 ]
             ],
             [
                 'name' => 'use_in_filter',
                 'label' => 'Use this specific in filter?',
                 'type' => 'checkbox',
                 'attributes' => [
                     'id' => 'use-in-filter'
                 ]
             ],
             [
                 'name' => 'filter_type',
                 'label' => 'Filter type',
                 'type' => 'enum',
                 'wrapperAttributes' => [
                     'id' => 'filter-type',
                     'style' => 'display:none'
                 ]
             ],
             [ // Table
                 'name' => 'new_options',
                 'label' => 'Options',
                 'type' => 'vue_table',
                 'entity_singular' => 'option', // used on the "Add X" button
                 'columns' => [
                     'name' => 'Name',
                 ],
                 'wrapperAttributes' => [
                     'id' => 'ex-options-table'
                 ]
             ],
         ], 'update');
        // $this->crud->removeField('name', 'update/create/both');
        // $this->crud->removeFields($array_of_names, 'update/create/both');

        // ------ CRUD COLUMNS
        // $this->crud->addColumn(); // add a single column, at the end of the stack
        // $this->crud->addColumns(); // add multiple columns, at the end of the stack
         $this->crud->addColumns([
             [
                 'name' => 'name',
                 'label' => "Specific name"
             ],
             [
                 // 1-n relationship
                 'label' => "Category", // Table column heading
                 'type' => "select",
                 'name' => 'category_id', // the column that contains the ID of that connected entity;
                 'entity' => 'category', // the method that defines the relationship in your Model
                 'attribute' => "title", // foreign key attribute that is shown to user
                 'model' => "App\Models\Category", // foreign key model
             ],
             [
                 // 1-n relationship
                 'label' => "Options", // Table column heading
                 'type' => "select_multiple",
                 'name' => 'id', // the column that contains the ID of that connected entity;
                 'entity' => 'specific_options', // the method that defines the relationship in your Model
                 'attribute' => "name", // foreign key attribute that is shown to user
                 'model' => "App\Models\SpecificOption", // foreign key model
             ],
             [
                 'name' => 'required',
                 'label' => 'Required',
                 'type' => 'boolean',
                 // optionally override the Yes/No texts
                 'options' => [0 => 'Optional', 1 => 'Required'],
             ],
             [
                 'name' => 'type',
                 'label' => 'Type',
                 'type' => 'enum'
             ],
             [
                 'name' => 'order',
                 'label' => "Order (Weight/Priority)",
             ]
         ]);
        // $this->crud->removeColumn('column_name'); // remove a column from the stack
        // $this->crud->removeColumns(['column_name_1', 'column_name_2']); // remove an array of columns from the stack
        // $this->crud->setColumnDetails('column_name', ['attribute' => 'value']); // adjusts the properties of the passed in column (by name)
        // $this->crud->setColumnsDetails(['column_1', 'column_2'], ['attribute' => 'value']);

        // ------ CRUD BUTTONS
        // possible positions: 'beginning' and 'end'; defaults to 'beginning' for the 'line' stack, 'end' for the others;
        // $this->crud->addButton($stack, $name, $type, $content, $position); // add a button; possible types are: view, model_function
        // $this->crud->addButtonFromModelFunction($stack, $name, $model_function_name, $position); // add a button whose HTML is returned by a method in the CRUD model
        // $this->crud->addButtonFromView($stack, $name, $view, $position); // add a button whose HTML is in a view placed at resources\views\vendor\backpack\crud\buttons
        // $this->crud->removeButton($name);
        // $this->crud->removeButtonFromStack($name, $stack);
        // $this->crud->removeAllButtons();
        // $this->crud->removeAllButtonsFromStack('line');
        $this->crud->addButtonFromView('top', 'import_lots', 'import_lots');
        // ------ CRUD ACCESS
        // $this->crud->allowAccess(['list', 'create', 'update', 'reorder', 'delete']);
        // $this->crud->denyAccess(['list', 'create', 'update', 'reorder', 'delete']);

        // ------ CRUD REORDER
        // $this->crud->enableReorder('label_name', MAX_TREE_LEVEL);
        // NOTE: you also need to do allow access to the right users: $this->crud->allowAccess('reorder');

        // ------ CRUD DETAILS ROW
        // $this->crud->enableDetailsRow();
        // NOTE: you also need to do allow access to the right users: $this->crud->allowAccess('details_row');
        // NOTE: you also need to do overwrite the showDetailsRow($id) method in your EntityCrudController to show whatever you'd like in the details row OR overwrite the views/backpack/crud/details_row.blade.php

        // ------ REVISIONS
        // You also need to use \Venturecraft\Revisionable\RevisionableTrait;
        // Please check out: https://laravel-backpack.readme.io/docs/crud#revisions
        // $this->crud->allowAccess('revisions');

        // ------ AJAX TABLE VIEW
        // Please note the drawbacks of this though:
        // - 1-n and n-n columns are not searchable
        // - date and datetime columns won't be sortable anymore
        // $this->crud->enableAjaxTable();

        // ------ DATATABLE EXPORT BUTTONS
        // Show export to PDF, CSV, XLS and Print buttons on the table view.
        // Does not work well with AJAX datatables.
        // $this->crud->enableExportButtons();

        // ------ ADVANCED QUERIES
        // $this->crud->addClause('active');
        // $this->crud->addClause('type', 'car');
        // $this->crud->addClause('where', 'name', '==', 'car');
        // $this->crud->addClause('whereName', 'car');
        // $this->crud->addClause('whereHas', 'posts', function($query) {
        //     $query->activePosts();
        // });
        // $this->crud->addClause('withoutGlobalScopes');
        // $this->crud->addClause('withoutGlobalScope', VisibleScope::class);
        //$this->crud->with('specific_options'); // eager load relationships
        // $this->crud->orderBy();
        // $this->crud->groupBy();
        // $this->crud->limit();
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $opts = $this->_getOptions($request, 'new_options');
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        $this->data['entry']->new_options = $opts;
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        return $redirect_location;
    }

    private function _getOptions($request, $field)
    {
        $opts = null;
        if ($request->has($field)) {
            $opts = $request->{$field};
            $request->request->remove($field);
        }
        return $opts;
    }

    public function destroy($id)
    {
        //delete all specific options
        SpecificOption::where('specific_id', $id)->delete();
        //delete all specific lot options
        LotSpecificOptions::where('specific_id', $id)->delete();

        return $this->crud->delete($id);
    }

    public function importForm()
    {
        return view('admin.import_form', ['crud' => $this->crud]);
    }

    public function importProcess(SpecificImportRequest $request, ImportSpecificRepository $import)
    {
        $path = $request->csv_file->store('csv');
        if ($total = $import->processFile(storage_path('app/' . $path))) {
            \Alert::success($total . ' specific(s) has ben imported successfully')->flash();
        } else {
            \Alert::error('Couldn\'t process CSV file or file is empty')->flash();
        }
        return redirect($this->crud->route);
    }
}
