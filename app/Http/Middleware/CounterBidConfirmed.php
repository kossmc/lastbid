<?php

namespace App\Http\Middleware;

use Closure;

class CounterBidConfirmed
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->exists(['amount', 'message'])) {
            return $next($request);
        } else {
            return response()->json([
                'status' => false,
                'form' => view()->make('inc.modal_counterbid')->render()
            ]);
        }
    }
}
