<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddChangesToTermsConditionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('terms_conditions', function (Blueprint $table) {
            $table->string('text')->nullable()->change();
            $table->tinyInteger('auto_update')->after('text')->nullable();
            $table->tinyInteger('has_crawler')->after('auto_update')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('terms_conditions', function (Blueprint $table) {
            //
        });
    }
}
