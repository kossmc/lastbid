# Default deploy_to directory is /var/www/my_app_name
set :deploy_to, '/var/www/html/stage'

# Default branch is :stage
set :branch, :"stage"

# add user deployer
role :app, %w{root@lbid-stage.getcider.com:20099}

namespace :deploy do
  after :updated, :updated do
    on roles(:all), in: :groups, limit: 3, wait: 10 do
       #execute "cd #{release_path} && composer update"
       execute "cd #{release_path} && php artisan migrate"
       execute "cd #{release_path} && php artisan optimize"
       execute "cd #{release_path} && composer dump-autoload"
       execute "cd #{release_path} && chmod -R 0777 storage"
       execute "cd #{release_path} && chmod -R 0777 bootstrap"
       execute "cd #{release_path} && chmod -R 0777 public/uploads"
       execute "cd #{release_path} && php artisan storage:link"
       execute "cd #{release_path} && php artisan cache:clear"
       execute "cd #{release_path} && php artisan view:clear"
       execute "service supervisor restart all"
       execute "cd #{release_path} && php artisan queue:restart"
    end
  end
end