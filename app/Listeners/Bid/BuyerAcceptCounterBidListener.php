<?php

namespace App\Listeners\Bid;

use App\Events\Bid\BuyerAcceptCounterBid;
use App\Models\Bid;

class BuyerAcceptCounterBidListener extends SendGridAbstractBidListener
{
    /**
     * Buyer Email subject.
     */
    private $buyerSubject = 'You have accepted a counter offer';

    /**
     * Buyer Email template.
     */
    private $buyerTemplate = 'a081feec-12ec-4bbb-ac4f-1559d43f5b15';

    /**
     * Seller Email subject.
     */
    private $sellerSubject = 'Congratulations! Your counter offer was accepted by buyer';

    /**
     * Seller Email template.
     */
    private $sellerTemplate = 'cc64751e-4d29-4448-b3b0-d5ad3f7adac3';

    /**
     * Handle the event.
     *
     * @param  BuyerAcceptCounterBid  $event
     * @return void
     */
    public function handle(BuyerAcceptCounterBid $event)
    {
        $this->setData($event);
        // set attachment for buyer
        /*notification to seller*/
        $this->sendMail($this->seller, $this->getData($this->sellerTemplate), $this->sellerSubject);
        /*notification to buyer*/
        $this->sendMail($this->buyer, $this->getData($this->buyerTemplate), $this->buyerSubject);
    }
}
