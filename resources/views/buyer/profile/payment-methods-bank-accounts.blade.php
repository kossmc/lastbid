@extends('buyer.profile.profile')
@section('title', 'Payment Methods')
@section('profile_content')
    <div class="buyer-account">
        <section class="bank-account">
            <div class="title">
                <ul>
                    <li><a href="{{route('payment-methods')}}">My cards</a></li>
                    <li class="active"><a href="javascript:void 0;">Bank accounts</a></li>
                </ul>
                <div class="btn-wrap"><a class="btn" href="{{route('payment-methods-bank-add')}}">Add bank account</a></div>
            </div>
            @forelse($accounts as $account)
            <section class="bank-account-card">
                <div class="card-info">
                    <div class="head">@if($account->default)<a class="default icon-done" href="javascript:void
                    0;">Default bank account</a>@else<a class="set-default" href="
                    {{route('payment-methods-bank-default',$account->id )}}">Set as default</a>@endif<a
                    class="link" href="javascript:void 0;" data-toggle="modal" data-target="#conf_modal_{{$account->id}}"
                        >Delete bank account</a><a
                    class="link" href="{{ route('payment-methods-bank-edit',$account->id)}}">Edit</a></div>
                    <div class="body">
                        <p><span>Account holder's name:</span>{{$account->holder_name}}</p>
                        <p><span>Account number:</span>{{substr($account->account_number,-4)}}</p>
                        <p><span>ABA Number/Routing number: </span>{{$account->routing_number}}</p>
                        <p><span>Bank name:</span>{{$account->bank_name}}</p>
                        <p><span>Bank address: </span>
                            {{$account->address?$account->address.', ':''}}
                            {{$account->city?$account->city.', ':''}}
                            {{$account->country?$account->country.' ':''}}
                            {{$account->zip?$account->zip:''}}
                        </p>
                        <p><span>SWIFT/BIC code: </span>{{$account->swift_code}}</p>
                    </div>
                </div>
            </section>
            @empty
                <h1 class="notification-no-content">You have no active bank accounts</h1>
            @endforelse
    </div>
@endsection
@section('custom_modal')
    @include('buyer.inc.confirm_deleting_pop_up',
    ['items'=>$accounts,'route'=>'payment-methods-bank-delete', 'name'=>'bank account'])
@endsection
