@if ($parent->latestLots->isNotEmpty())
    @foreach($parent->latestLots->take(4) as $lot)
        @include('inc.lot_item')
    @endforeach
@elseIf ($parent->children->isNotEmpty())
    @foreach ($parent->children->take(1) as $category)
        @if ($category->latestLots->isNotEmpty())
            @foreach ($category->latestLots->take(4) as $lot)
                @include('inc.lot_item')
            @endforeach
        @endif
    @endforeach
@endif
