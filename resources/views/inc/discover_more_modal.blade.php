<div id="discoverMore" class="modal">
    <div class="modal-content">
        <span class="close">&times;</span>
        <h2>DISCOVER MORE</h2>
        <form method="post" action="subscribe" id="form-discover">
            <div class="notification-sent-msg">
                <p></p>
            </div>
            <div class="item">
                <input type="email" placeholder="Email" name="email" required
                       class="{{ $errors->has('email') ? 'has-error--bgcolor' : '' }}">
            </div>
            <div class="wrap-check-policy">
                <input type="checkbox" id="t-agreement-discover" name="terms" value="yes">
                <label class="" for="t-agreement-discover">
                    I accept LastBid's <a href="{{ url('/terms') }}" target="_blank" class="terms-agreement-link">Terms & Conditions</a> and <a href="{{ url('/privacy') }}" target="_blank" class="terms-agreement-link">Privacy Policy</a>
                </label>
            </div>
            <div class="item">
                <button class="btn popup-btn" type="submit" disabled="disabled">send</button>
            </div>
        </form>
    </div>
</div>

@include('notifications/lets_talk')

@section('remote_js_scripts')
    <script>
        var isSending = false;
        $(document).on("submit", "#form-discover, #form-landing", function (e) {
            var form = $(this);
            e.preventDefault();

            if (isSending) {
                return false;
            }
            isSending = true;

            var data = $(form).serialize();
            var url = "/" + form.attr('action');

            $.ajax({
                type: "POST",
                data: data,
                dataType: "json",
                url: url,
                beforeSend: function(request) {
                    request.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content'));
                },
                success: function (data) {
                    $('#lets-talk-header').text('');
                    $('#lets-talk-notice').text('');

                    if (data.status) {
                        $(modal).hide();

                        $("#lets-talk-header").text(data.message);
                        $("#lets-talk-notice").text(data.note);

                        $("#letsTalk").show();
                    }
                },
                error: function (errors) {
                    //var errorBlock = this.find(".error-notification")
                },
                complete: function () {
                    isSending = false;

                    $( "#form-landing input,textarea" ).val("");
                }
            });

        });
        var letsTalk = document.getElementById("letsTalk");
        var modal = document.getElementById("discoverMore");

        var span = document.getElementsByClassName("close")[0];
        var close_letsTalk = document.getElementById("letsTalk_close")

        $(document).on("click", ".discoverMoreBtn", function () {
            console.log("inside");
        });



        $(document).on('click', '.discoverMoreBtn', function(){
            console.log('click')
            modal.style.display = "block";
        })

        span.onclick = function () {
            modal.style.display = "none";
        };
        close_letsTalk.onclick = function () {
            letsTalk.style.display = "none";
        };

        window.onclick = function (event) {
            if (event.target == modal) {
                modal.style.display = "none";
            } else if (event.target == letsTalk) {
                letsTalk.style.display = "none";
            }
        };

        $(document).on('change', '#t-agreement-discover', function (e) {
            if(e.target.checked) {
                $(this).closest('form').find('button[type="submit"]').attr('disabled',false);
            }
            if(!e.target.checked){
                $(this).closest('form').find('button[type="submit"]').attr('disabled',true);
            }
        })
    </script>
@endsection