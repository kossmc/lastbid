<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterParserBatchesTable extends Migration
{
    /*fix for Unknown database type enum requested, Doctrine\DBAL\Platforms\MySQL57Platform may not support it.*/
    public function __construct()
    {
        DB::getDoctrineSchemaManager()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');
    }
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('parser_batches', function (Blueprint $table) {
            $table->unsignedInteger('job_id')->nullable(true)->change();
            $table->string('type');
            $table->text('input')->nullable(true);
            $table->dropColumn('archive_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('parser_batches', function (Blueprint $table) {
            $table->dropColumn('url');
        });
    }
}
