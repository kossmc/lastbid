@extends('layouts.main')

@include('inc.page_metas', ['defTitle' => 'Auction Calendar'])

@section('top-links')
    @include('inc.top_links')
@stop
@section('content')
    @include('events.content_list', ['landing' => false])
@endsection
@php

@endphp
@section('remote_js_scripts')
    <script src="{{asset('js/zabuto_calendar.min.js')}}"></script>
    <script>
        $(document).ready(function () {

            $('#calendar').zabuto_calendar({
                data: {!! $calendar !!},
                year: '{{$date->format('Y')}}',
                month: '{{$date->format('m')}}',
                cell_border: true,
                action: function () {
                    return this.title ? window.open(this.title, '_self') : false;
                },
                nav_icon: {
                    prev: '<span class="arrow-icon arrow-icon-back" onclick="actionsCalendar.prev()"></span>',
                    next: '<span class="arrow-icon arrow-icon-nex" onclick="actionsCalendar.next()"></span>'
                }
            });

            window.actionsCalendar = {
                month: (action) => {
                    let month = moment(
                        Date.parse(
                            '01 ' + $('.calendar-month-header')
                                .find('span')
                                .text())
                    );

                    if(action === 'prev') month = month.subtract(1, 'months').format("M");
                    if(action === 'next' || action === undefined ) month = month.add(1, 'months').format("M");

                    return month < 10 ? '0' + month : month;
                },
                year: () => {
                    return moment($('.calendar-month-header').find('span').text()).year();
                },
                prev: () => {
                    let month = actionsCalendar.month('prev');
                    let year = month == 12 ? actionsCalendar.year() - 1 : actionsCalendar.year();

                    window.open('/events/' + year + '/' + month, '_self');
                },
                next: () => {
                    let month = actionsCalendar.month('next');
                    let year = month == 1 ? actionsCalendar.year() + 1 : actionsCalendar.year();

                    window.open('/events/' + year + '/' + month, '_self');
                },
                current: () => {
                    window.open('/events/' + actionsCalendar.year() + '/' + actionsCalendar.month(), '_self');
                }
            };

            $('.calendar-month-header td:nth-child(2) span').click(() => {actionsCalendar.current()});
        });
    </script>
@stop