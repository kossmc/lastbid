<?php

namespace App\Http\Controllers;


use App\Models\Lot;
use Illuminate\Support\Facades\Auth;
use App\Repositories\LotRepository;

class PurchasesController extends Controller
{
    /**
     * The lot repository implementation.
     *
     * @var LotRepository
     */
    private $lot;

    /**
     * Create a new controller instance.
     *
     * @param LotRepository $lot
     * @return void
     */
    public function __construct(LotRepository $lot)
    {
        $this->lot = $lot;
    }
    public function listLots()
    {
        $sort = request()->intersect('sort_by', 'sort_direction');
        $lots = $this->lot->buyerSoldLots($sort);
        return view('buyer.purchases', ['lots'=>$lots, 'sort'=>$sort]);
    }
}
