<?php

namespace App\Listeners\Form;

use App\Events\Form\CountactUsForm;

class CountactUsFormListener extends SendGridAbstractFormListener
{
    /**
     * Buyer Email subject.
     */
    private $subject = 'Follow-up from Contact Us form';

    /**
     * Buyer Email template.
     */
    private $template = '95f3b6a7-b7d2-49db-8b5b-57f78b1d684c';

    /**
     * Handle the event.
     *
     * @param  CountactUsForm  $event
     * @return void
     */
    public function handle(CountactUsForm $event)
    {
        $this->setData($event);
        $this->sendMail($this->getData($this->template), $this->subject);
    }
}
