<?php

namespace App\Http\Middleware;

use Closure;
use App\Repositories\LotRepository;

class LotIsMine
{
    /**
     * The lot repository implementation.
     *
     * @var LotRepository
     */
    private $lot;

    /**
     * Create a new middleware instance.
     *
     * @param LotRepository $lot
     * @return void
     */
    public function __construct(LotRepository $lot)
    {
        $this->lot = $lot;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($lot = $this->lot->isMine($request->id)) {
            $request->lot = $lot;
            return $next($request);
        }
        abort(404);
    }
}
