@extends('layouts.main')
@section('title','Login in')
@section('top-links')
    @include('inc.top_links')
@stop
@section('content')
    <div class="wrap">
        <div class="container-form-wrap" style="margin-top: 0">
            <div class="container-forms">
                <div class="container-info">
                    <div class="info-item">
                        <div class="table">
                            <div class="table-cell">
                                <div class="text">If you already have an account click the button below to enter your
                                    account.
                                </div>
                                <div class="btn">
                                    Login
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="info-item">
                        <div class="table">
                            <div class="table-cell">
                                <div class="text">If you don't already have an account click the button below to create
                                    your
                                    account.
                                </div>
                                <div class="btn" id="show-reg-btn">
                                    <a href="{{url('register')}}">Register</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <form id="reg-form" method="POST" action="{{ route('login') }}">
                    <div class="container-form">
                        <div class="form-item log-in">
                            <div class="table">
                                {{csrf_field()}}
                                <div class="table-cell">
                                    <div class="title">Log in
                                        @if (session()->has('email_changed'))
                                        <p style="color:red;">{{session()->pull('email_changed')}}</p>
                                        @endif
                                    </div>
                                    <div class="row">
                                        <div class="inp-wrap">
                                            <input type="text" placeholder="Your email" name="email"
                                                   value="{{ old('email') }}"
                                                   required
                                                   autofocus>
                                        </div>
                                        <div class="inp-wrap">
                                            <input type="password" placeholder="Your password" name="password" required>
                                        </div>
                                        @if ($errors->has('email'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                                </span>
                                        @endif
                                        @if ($errors->has('password'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="remember-wrap">
                                        <div class="item">
                                            <input id="field1" class="sort-box-cb" name="remember"
                                                   type="checkbox" checked>
                                            <label for="field1">Remember me</label>
                                        </div>
                                        <div class="item">
                                            <a href="{{url('password/reset')}}">Reset password</a>
                                        </div>
                                    </div>
                                    <div class="btn-wrap">
                                        <button type="submit" href="#" class="btn">Log in</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
{{--<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Log in</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Log in
                                </button>

                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    Forgot Your Password?
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>--}}
@endsection
