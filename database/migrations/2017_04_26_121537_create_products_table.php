<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('category_id');
            $table->unsignedInteger('partner_id')->nullable();
            $table->string('name');
            $table->string('sub_title')->nullable();
            $table->string('slug');
            $table->text('description')->nullable();
            $table->text('shipping')->nullable();
            $table->float('reserve_price', 20, 2)->default(0.00);
            $table->float('shipping_price', 20, 2)->default(0.00);
            $table->float('auc_house_premium', 20, 2)->default(0.00);
            $table->dateTime('start_date')->default(\Carbon\Carbon::now());
            $table->dateTime('end_date')->default(\Carbon\Carbon::now());
            $table->unsignedInteger('views_count')->default(0);
            $table->unsignedInteger('bids_count')->default(0);
            $table->text('photos')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
