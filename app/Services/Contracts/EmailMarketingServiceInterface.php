<?php

namespace App\Services\Contracts;

/**
 * Interface EmailMarketingServiceInterface
 * @package App\Services\Contracts
 */
interface EmailMarketingServiceInterface
{
    /**
     * @param string $email
     * @param string $listId
     * @return bool
     */
    public function subscribe($email, $listId);

    /**
     * @param string $email
     * @param string $listId
     * @return bool
     */
    public function isSubscribed($email, $listId);
}
