<div class="block-card-lot block-card-lot--lot-info-simple">
    <p class="title">Lot info</p>
    <div class="bid-price">
        {!! format_price($lot->range_from, $lot->range_to,$lot->currency) !!}
        <p>Suggested bid range</p>
    </div>
    <div class="bottom-text">Does not include auction house premium and shipping/delivery fees</div>
    @if ($lot->is_active)
        <div class="time-wrapper">
            <div class="time-wrap-line">
                <div class="time-line">
                    <div class="time-line-left" style="width:{{ percent_time_left($lot->start_date, $lot->end_date) }}%"></div>
                </div>
                <span class="time-text">Lot expire in: <span class="expires-date">{{ format_time_left($lot->end_date) }}</span></span>
            </div>
        </div>
    @endif
</div>
