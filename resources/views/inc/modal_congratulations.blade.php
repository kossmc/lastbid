@if($autoAccept)
    <div class="block-card-lot block-card-lot--winner">
        <span class="icon-winner_icon"></span>
        <p class="title">You are the winner</p>
        <p class="bid-info"><span class="icon-done"></span>
            <span>{{ $lot->seller->short_name }}</span> accepted your last bid {{ $bid->attempts_count }}/{{ env('BIDS_LIMIT') }}
        </p>
        <div class="info">

            <div class="bid-price">
                {!! format_price($bid->attempt->amount, null, $lot->currency) !!}
                <p>Does not include auction house premium and shipping/delivery fees</p>
            </div>

            <div class="info-bid">
                <p class="title">Total bid price consists of:</p>
                <div>
                    <p>Bid price:</p>
                    <div class="bid-price"> {!! format_price($bid->attempt->amount, null, $lot->currency) !!} </div>
                </div>
                <div>
                    <p>Auction house premium:</p>
                    <div class="bid-price">{!! format_price($lot->bidFee($bid->attempt->amount), null, $lot->currency) !!}</div>
                </div>
                <div>
                    <p>Total bid price:</p>
                    <div class="bid-price">{!! format_price($lot->bidWithFee($bid->attempt->amount), null, $lot->currency) !!}</div>
                </div>
            </div>

        </div>

        <div class="btn-holder">
            <a class="btn-lot" href="{{ route('faq') }}">how to pay</a>
        </div>
        <div class="card-bottom-part">
            <p><span>Start:</span>{{ $lot->start_date->format('j F Y, h:i A') }} EST</p>
            <p><span>End:</span>{{ $lot->end_date->format('j F Y, h:i A') }} EST</p>
        </div>
    </div>
@else
    <div class="block-card-lot block-card-lot--congratulations"><span class="icon-congrat"></span>
        <p class="title">Thank you</p>

        <div class="time-wrapper">
            <div class="time-wrap-line">
                <div class="time-line">
                    <div class="time-line-left" style="width:{{ percent_time_left($bid->attempt->start_date, $bid->attempt->end_date) }}%"></div>
                </div>
                <span class="time-text">Your bid expires in:<span class="expires-date">{{ format_time_left
            ($bid->attempt->end_date) }}</span></span>
            </div>
        </div>

        <p class="bid-info">
            <span class="icon-done"></span>You have placed a bid {{ $bid->attempts_count }}/{{ env('BIDS_LIMIT') }}  (maximum {{ env('BIDS_LIMIT') }} bids per lot)
        </p>

        <div class="info">
            <div class="bid-price">
                {!! format_price($bid->attempt->amount, null, $lot->currency) !!}
                <p>Does not include auction,
                    house premium
                    and shipping/delivery fees</p>
            </div>
            <div class="info-bid">
                <p class="title">Total bid price consists of:</p>
                <div>
                    <p>Bid price:</p>
                    <div class="bid-price"> {!! format_price($bid->attempt->amount, null, $lot->currency) !!} </div>
                </div>
                <div>
                    <p>Auction house premium:</p>
                    <div class="bid-price">{!! format_price($lot->bidFee($bid->attempt->amount), null, $lot->currency) !!}</div>
                </div>
                <div>
                    <p style="font-size: 14px;">Total bid price:</p>
                    <div class="bid-price" style="font-size: 14px;">{!! format_price($lot->bidWithFee($bid->attempt->amount), null, $lot->currency) !!}</div>
                </div>
            </div>
        </div>
        
        <div class="time-wrapper">
            <p> Please wait for a response on your bid.</p>
            <p>(You will receive an email once the auction house has reviewed your bid.)</p>
            <div class="time-wrap-line">
                <div class="time-line">
                    <div class="time-line-left" style="width:{{ percent_time_left($lot->start_date, $lot->end_date) }}%"></div>
                </div>
                <span class="time-text">Lot expires in: <span class="expires-date">{{ format_time_left($lot->end_date)
            }}</span></span>
            </div>
        </div>
    </div>
@endif

