<?php
use Illuminate\Support\Facades\Cookie;
if (!function_exists('redirectToAuth')) {
    function redirectToAuth($type, $referrer = null)
    {
        return redirect($referrer? $referrer:'/')->withCookie(cookie('auth', $type, 360, null,
            null, false, false));
    }
}
if (!function_exists('forgetCookie')) {
    function forgetCookie($type)
    {
        return Cookie::queue(Cookie::forget($type));
    }
}
if (!function_exists('addCookie')) {
    function addCookie($name,$value)
    {
        return Cookie::queue(Cookie::make($name, $value));
    }
}
