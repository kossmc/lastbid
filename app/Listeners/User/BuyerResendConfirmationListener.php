<?php

namespace App\Listeners\User;

use App\Events\User\BuyerResendConfirmation;

class BuyerResendConfirmationListener extends SendGridAbstractUserListener
{
    /**
     * Buyer Email subject.
     */
    private $buyerSubject = 'Email confirmation';

    /**
     * Buyer Email template.
     */
    private $buyerTemplate = 'df0e86d8-1829-4118-ba2c-e2630240e57c';

    /**
     * Handle the event.
     *
     * @param  BuyerResendConfirmation $event
     * @return void
     */
    public function handle(BuyerResendConfirmation $event)
    {
        $this->setData($event);
        /*notification to buyer*/
        $this->sendMail($this->getData($this->buyerTemplate), $this->buyerSubject);
    }
}
