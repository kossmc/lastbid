<?php

namespace App\Console\Commands;

use App\Services\GrahamBudCrawler as Crawler;
use Illuminate\Console\Command;

class Spider extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'grab:graham_bud';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Grab items from graham bud auction';
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $crawler = new Crawler(new \GuzzleHttp\Client());
        $crawler->parse();
//        GrahamBudCrawler::dispatch(['t'=>1]);
        return 1;
    }
}
