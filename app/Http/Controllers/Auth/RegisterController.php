<?php

namespace App\Http\Controllers\Auth;

use App\Events\User\BuyerRegistered;
use App\Events\User\BuyerResendConfirmation;
use App\Http\Requests\UserRegisterRequest;
use App\Jobs\UpdateUserLocation;
use App\Models\BuyerCard;
use App\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('ajax_add_info', 'confirmEmail',
            'confirmEmailResend', 'successEmailPopup');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'phone' => 'string|max:255',
            'address' => 'string|max:255',
            'card_number' => 'string|max:255',
            'cvv' => 'string|max:255',
            'exp_date' => 'string|max:255',
        ]);
    }

    /**
     * Handle a registration request for the application. Send user to popup
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function register(UserRegisterRequest $request)
    {
        return $this->showRegistrationForm();
        /*Stripe validation logic*/
//        $this->validator($request->all())->validate();
//        $data = $request->only('email', 'cvv', 'exp_date', 'card_number');
//        $exp  = explode('/', $data['exp_date']);
//        $data['exp_month'] = isset($exp[0])? $exp[0]:null;
//        $data['exp_year'] = isset($exp[1])? $exp[1]:null;
//        $stripe = new StripeService();
//        $stripeCardToken = $stripe->getCardToken($data['card_number'], $data['exp_month'], $data['exp_year'], $data['cvv']);
//        if (isset($stripeCardToken['error'])) {
//            Session::put('error', $stripeCardToken['error']);
//            return redirect()->back()->withInput();
//        }
//        event(new Registered($user = $this->create($request->all())));
//
//        $this->guard()->login($user);
//
//        return $this->registered($request, $user)
//            ?: redirect($this->redirectPath());
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        $user = User::create([
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'email' => $data['email'],
            'password' => $data['password'],
            'phone' => $data['phone'],
            //'address' => $data['address'],
            //'card_number' => $data['card_number'],
            //'cvv' => $data['cvv'],
            //'exp_date' => $data['exp_date']
        ]);
        $user->assignRole('buyer');
        Event::fire(new BuyerRegistered($user, request()->server('HTTP_REFERER')));
        return $user;
    }

    // @todo: combine with ajax_reg_save
    public function ajax_reg()
    {
        forgetCookie('auth');
        $terms = view('inc.terms');
        $dbTerms = \DB::table('site_terms_conditions')->first();
        return response()->json([
            'form' => view()
                ->make('auth.modal_register')
                ->with(['terms'=> $dbTerms ?  $dbTerms->text : $terms])
                ->render()
        ]);
    }

    public function ajax_reg_save()
    {
        $socialAuth = session()->get('socialAuth');
        if ( ! $socialAuth) {
            $data = request()->intersect(['first_name', 'last_name', 'email', 'password', 'password_confirmation', 'terms', 'ip']);
        } else {
            $data = request()->intersect(['first_name', 'last_name', 'email', 'terms', 'ip']);
            $data['password'] = $data['password_confirmation'] = Str::random(6);
        }
        $validator = Validator::make($data, [
            'first_name' => 'required|string|max:192',
            'last_name' => 'required|string|max:192',
            'email' => 'required|string|email|max:192|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'terms' => 'required'
        ]);
        if ($validator->fails()) {
            request()->flash();
            return response()->json([
                'status' => false,
                'form' => view()
                    ->make('auth.modal_register', ['errors' => $validator->errors()])
                    ->with(['terms'=>\DB::table('site_terms_conditions')->first()->text])
                    ->render()
            ]);
        }

        $ip = isset($data['ip']) ? $data['ip'] : '';
        $user = new User($data);
        if ($user->save()) {
            dispatch((new UpdateUserLocation($user->id, $ip))->onQueue('default'));

            /*Add for default role user*/
            $user->assignRole('buyer');
            if ($socialAuth) {
                $user->authSocial()->create([
                    'provider' => $socialAuth->provider,
                    'provider_id' => $socialAuth->id,
                    'token' => $socialAuth->token
                ]);
                Session::pull('socialAuth');
            }
            // Login and "remember" the given user...
            Auth::login($user, true);

            //Event after register user
            Event::fire(new BuyerRegistered($user, request()->server('HTTP_REFERER')));

            return response()->json([
                'status' => true,
                'top' => view()->make('inc.top_links')->render(),
                'form' => view()->make('auth.modal_add_info')->with(['type'=>'register'])->render()
            ]);
        }


        $terms = view('inc.terms');
        $dbTerms = \DB::table('site_terms_conditions')->first();
        return response()->json([
            'status' => false,
            'form' => view()
                ->make('auth.modal_register')
                ->with(['terms'=> $dbTerms ?  $dbTerms->text : $terms])
                ->render()
        ]);
    }

    public function ajax_add_info()
    {
        $data = request()->only('city', 'address','state','zip', 'country',
            'card_cvv', 'month', 'year', 'cc', 'card_name');

        $data['month'] = date('m', strtotime($data['month']));

        $user = Auth::user();
        $validator = Validator::make($data, [
            'city' => 'required|string|max:192',
            'address' => 'required|string|max:192',
            'country' => 'required|string|max:192',
            'card_cvv' => 'required',
            'month' => 'required',
            'year' => 'required',
            'cc' => 'required',
        ]);

        if ($validator->fails()) {
            request()->flash();
            return response()->json([
                'status' => false,
                'form' => view()->make(
                    'auth.modal_add_info',
                    ['errors' => $validator->errors(), 'user' => $user]
                )->render()
            ]);
        }

        if ( ! BuyerCard::validateCard($data)) {
            request()->flash();
            return response()
                ->json([
                    'status' => false,
                    'form' => view()->make(
                        'auth.modal_add_info',
                        ['user' => $user, 'message' => 'Your card info is incorrect!']
                    )->render()
                ]);
        }

        $user->cards()->create([]);
        $user->addresses()->create([
            'address_name'=>'Billing',
            'address'=>$data['address'],
            'city'=>$data['city'],
            'country'=>$data['country'],
            'state'=>$data['state'],
            'zip'=>$data['zip']
        ]);

        return response()->json([
            'status' => true,
            'form' => view()->make('auth.modal_register_with_card_congrats')->render()
        ]);
    }

    public function confirmEmail($token)
    {
        $user = User::where('email_token', $token)->firstOrFail();
        $user->email_token = null;
        $user->email_confirmed = 1;
        $user->save();
        return redirectToAuth('email_verified', request()->query('referrer','/'));
    }

    public function confirmEmailResend()
    {
        Event::fire(new BuyerResendConfirmation(Auth::user(), request()->server('HTTP_REFERER')));
        return response()->json([
            'status' => true,
            'form' => view()->make('auth.modal_confirmation_resent')->render()
        ]);
    }

    public function successEmailPopup()
    {
        forgetCookie('auth');
        return response()->json([
            'status' => true,
            'form' => view()->make('auth.modal_confirmation_success')->render()
        ]);
    }

    /**
     * Show the application registration form in popup.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        return redirectToAuth('register_form');
    }
}
