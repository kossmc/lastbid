@extends('buyer.profile.profile')
@section('title', 'Payment Methods')
@section('profile_content')
    <div class="buyer-account">
        <section class="payment">
            <h3>Add new card
            </h3>
            <p>We take your security very seriously and use <a href="#">Special System</a> for payment processing. For security
                purposes, you may see a temporary authorization to validate your identity and payment information.</p>
            @include('inc.top_errors_list')
            <form id="add-payment-card" action="{{route('payment-methods-card-add-save')}}" method="POST" autocomplete="off">
                @include('buyer.profile._card_form', ['card' => $card, 'edit'=> false])
                <div class="form-controll form-controll--newline form-controll--large">
                    <div class="btn-wrap">
                        <button class="btn" type="submit" disabled>Add card</button>
                        <a class="btn2" href="{{route('payment-methods')}}">Cancel</a>
                    </div>
                </div>
            </form>
        </section>
    </div>

@endsection
@section('user_script')

	$(window).on('load', function(){

		var card = new CardValidation('#add-payment-card');

        // remove error class on inputs after they are filled up
        $('.has-error--bgcolor').on('blur', function(){
            if($(this).val() != '' || $(this).val() == $(this).attr('placeholder')){
                $(this).removeClass('has-error--bgcolor');
            }
        });

	});


    (function(){

    //var card = new CardValidation('#add-payment-card');

    var select = $('.select-style');
    var select_ul = select.find('ul');

    select.find('span').on('click', function(){

    $(this).toggleClass('up-down-arrow');
    var ul = $(this).next('ul');

    if(ul.is(':hidden')){
    ul.show();
    }
    else{
    ul.hide();
    }
    });

    select_ul.find('li').on('click', function(){

    var select = $(this).closest('.select-style');
    var selct_opt = select.find('option');
    var select_ul = select.find('ul');

    select.find('span').toggleClass('up-down-arrow');

    select.find('span').addClass('selected-suffix');

    select.find('span').text($(this).text());
    selct_opt.attr('value', $(this).text());
    selct_opt.text($(this).text());
    select_ul.hide();
    });


    }());
@stop

