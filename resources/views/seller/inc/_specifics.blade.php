<div id="specificTable" style="">
    @if(!empty($category) && !$category->specifics->isEmpty())
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3>Specifics</h3>
        </div>

        <div class="panel-body">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Specific Name</th>
                        <th>Specific Type</th>
                        <th>Required</th>
                        <th>Specific Options</th>
                    </tr>
                </thead>
                <tbody>

                @foreach($category->specifics as $specific)
                <tr>
                    <td>{{ $specific->name }}</td>
                    <td>{{ $specific->type }}</td>
                    <td>{{ $specific->required ? 'yes' : 'no' }}</td>
                    <td>
                        @if($specific->is_text)
                            <div class="text">
                                <label>
                                    <input name="specifics_data[{{ $specific->id }}]" type="text"
                                           value="{{ $specifics && $specifics[$specific->id] ? $specifics[$specific->id]->value : old('specifics_data.'.$specific->id, null)}}">
                                </label>
                            </div>
                        @else
                            @foreach($specific->specific_options as $option)
                                <div class="{{ $specific->is_select ? 'radio' : $specific->is_select? : 'checkbox' }}">
                                    <label>
                                        <input
                                                name="specifics_data[{{ $option->specific_id }}]"
                                                type="{{ $specific->is_select ? 'radio' : 'checkbox' }}"
                                                value="{{ $option->id }}"

                                                @if(!empty($specifics) && !empty($specifics[$option->specific_id]) && $specifics[$option->specific_id]->specific_option_id == $option->id)
                                                    {{ 'checked' }}
                                                @endif
                                        >
                                        {{ $option->name }}
                                    </label>
                                </div>
                            @endforeach
                        @endif
                    </td>
                </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    @endif
</div>
