<?php

namespace App\Services;

use Carbon\Carbon;
use Symfony\Component\DomCrawler\Crawler;

/**
 * Class SterlingVaultCrawler
 * @package App\Services
 */
class SterlingVaultCrawler extends BaseCrawler
{
    public $baseUrl = 'https://whitelabel-2.globalauctionplatform.com/';

    public $categories = [];

    public $crowler;

    /**
     * SterlingVaultCrawler constructor.
     * @param $batch
     */
    public function __construct($batch)
    {
        $this->crowler = new Crawler();

        $this->filename = 'Sterling_' . $batch->input . '_' . date('Y-m-d_H-i-s') . '.csv';
        parent::__construct($batch);
    }

    /*
     *  Start parse data
     */
    public function startCrawling()
    {
        $response = $this->client->request('GET', $this->getUrl());
        if ($response->getStatusCode() != 200) {
            \Log::info(print_r($response, true));
            return false;
        }

        $content = $this->processContent(
            $response->getBody()->getContents()
        );

        $this->crowler->add($content);

        $this->getCategories();

        $auctionDate = $this->getStartDate();
        $startDate = $auctionDate->format('Y-m-d H:i:s');
        $endDate = $auctionDate->addMonth()->format('Y-m-d H:i:s');

        $lots = [];

        foreach ($this->categories as $category) {

            $pagesCount = ceil($category['count'] / 60);
            $page = 1;
            do {
                $url = $this->baseUrl . $category['link'] . '&page=' . $page;

                $response = $this->client->request('GET', $url);

                if ($response->getStatusCode() != 200) {
                    \Log::info(print_r($response, true));
                    return false;
                }

                $content = $this->processContent(
                    $response->getBody()->getContents()
                );

                $lotLinks = $this->getLots($content);
                foreach ($lotLinks as $lotLink) {

                    $url = $this->baseUrl . $lotLink['link'] . '?clienturl=Sterling-Vault';

                    $response = $this->client->request('GET', $url);

                    if ($response->getStatusCode() != 200) {
                        \Log::info(print_r($response, true));
                        return false;
                    }


                    $content = $this->processContent(
                        $response->getBody()->getContents()
                    );

                    if ($lot = $this->getLotInfo($content)) {

                        $photos = [];
                        if (isset($lot['photos'])) {
                            foreach ($lot['photos'] as $photoUrl) {
                                $photos[] = $this->loadImage($photoUrl);
                            }
                        }

                        $lots[] = [
                            'Lot Id' => $lot['lot_id'],
                            'Title' => $lotLink['title'],
                            'Description' => $lot['description'],
                            'Category' => $category['title'],
                            'Sub Category' => '',
                            'Partner' => $this->partner_id,
                            'Shipping description' => '',
                            'Auction House Fee' => 20,
                            'Minimum bid amount' => isset($lot['price_from']) ? $lot['price_from'] * 0.5 : 0,
                            'Price range from' => isset($lot['price_from']) ? $lot['price_from'] : 0,
                            'Price range to' => isset($lot['price_to']) ? $lot['price_to'] : 0,
                            'Price to auto-accept bid' => isset($lot['price_from']) ? $lot['price_from'] * 0.7 : 0,
                            'Start date' => $startDate,
                            'End date' => $endDate,
                            'Seller' => $this->seller_id,
                            'Currency' => isset($lot['currency']) ? $lot['currency'] : 'usd',
                            'Lot photos' => implode(',', $photos)
                        ];
                    } else {
                        \Log::info(print_r($lotLink, true));
                    }

                }

                $page++;
            } while ($pagesCount >= $page);
        }

        if (!empty($lots)) {
            $this->headers = array_keys($lots[0]);
        }

        return $lots;
    }

    /**
     * Get main full url
     * @return string
     */
    public function getUrl()
    {
        return $this->baseUrl . '/en-gb/auction-catalogues/sterling-vault/catalogue-id-sterli1-' . $this->batch->input . '?clienturl=Sterling-Vault';
    }

    /**
     * Get auction categories
     */
    public function getCategories()
    {

        $this->categories = $this->crowler->filter('#FilteredSearch_categoryCode a')->each(function (Crawler $node, $i) {

            $count = (int)filter_var($node->text(), FILTER_SANITIZE_NUMBER_INT);

            return [
                'title' => trim($node->filter('.label')->text()),
                'link' => $node->attr('href'),
                'count' => $count,
            ];
        });
    }

    /**
     * Get start auction
     * @return Carbon
     */
    public function getStartDate()
    {

        $dateHtml = $this->crowler->filter('.first-sale-date')->first();

        if (!$dateHtml)
            return Carbon::now();

        return Carbon::parse(trim($dateHtml->text()));
    }


    /**
     * Get lot data
     * @param $content
     * @return array
     */
    public function getLots($content)
    {

        $crawler = new Crawler();
        $crawler->add($content);

        $lotLinks = $crawler->filter('#results .item h1 a')->each(function (Crawler $node, $i) {
            return [
                'link' => str_replace('https://', 'http://', trim($node->attr('href'))),
                'title' => trim($node->text())
            ];
        });

        return $lotLinks;
    }

    /**
     * Get lot info
     * @param $content
     * @return array
     */
    public function getLotInfo($content)
    {

        $lot = [];

        try {
            $crawler = new Crawler();
            $crawler->add($content);

            $lotData = $crawler->filter('.lot-number')->first();
            $lot['lot_id'] = (int)filter_var($lotData->text(), FILTER_SANITIZE_NUMBER_INT);

            $lot['currency'] = 'usd';

            $count = $crawler->filter('.lot-meta .estimate')->count();
            if ($count > 0) {
                $amountData = $crawler->filter('.lot-meta .estimate')->first();
            } else {
                $count = $crawler->filter('#priceGuidePanel p .lot-status')->count();
                if ($count > 0) {
                    $amountData = $crawler->filter('#priceGuidePanel p .lot-status')->first();
                }
            }

            if (isset($amountData)) {
                $estimate = $amountData->text();

                if (substr_count($estimate, 'GBP') > 0) {
                    $lot['currency'] = 'gbp';
                }

                list($from, $to) = explode('-', trim($estimate));
                $from = preg_replace('/[^0-9]/', '', $from);
                $to = preg_replace('/[^0-9]/', '', $to);
                $lot['price_from'] = $from;
                $lot['price_to'] = $to;
            }

            $descData = $crawler->filter('.lot-text-copy .description')->first();
            $lot['description'] = '<p>' . $descData->text() . '</p>';

            $lot['photos'] = $crawler->filter('.lot-image .image-holder img')->each(function (Crawler $node, $i) {
                return $node->attr('data-lazy');
            });

        } catch (\InvalidArgumentException $e) {
            \Log::error($e->getMessage() . ' invalid 1');
            return false;
        } catch (\Exception $e) {
            \Log::error($e->getMessage() . 'invalid 2');
            return false;
        }

        return $lot;
    }
}