
    <div class="page-wrap">
        <div class="wrap">
            <div class="faq-page page-categories clearfix @isset($faqQuery) search-faq @endisset">
                <div class="sidebar-wrap">
                    <div class="group-wrap">
                        @if ($categories->isNotEmpty())
                        <h1 class="faq-heading">FAQ</h1>
                        <ul class="faq-category-list">
                            @foreach($categories as $category)
                                <li data-section-id="section-{{$category->id}}">{{$category->title}}</li>
                            @endforeach
                        </ul>
                        @endif
                    </div>
                </div>
                <div class="category-wrap-item">
                    @if($search)
                        <div class="faq-form clearfix">
                            <p class="faq-form-results">Search results for
                                <b>@isset($faqQuery)&quot; {{$faqQuery}} &quot;@endisset</b>
                                <a href="{{route('faq')}}">&nbsp;return back to full list</a>
                            </p>
                            <form method="GET" action="{{route('faq')}}" accept-charset="UTF-8" role="search">
                                <input placeholder="Search" type="text" name="faq_query" id="faq-query">
                                <button type="submit"><em class="icon-search_icon"></em></button>
                            </form>
                        </div>
                    @endif
                    @if ($categories->isNotEmpty())
                        @foreach($categories as $category)
                            <section class="answers-section" data-section-id="section-{{$category->id}}">
                                <h2 class="category-title">{{$category->title}}</h2>
                                <dl>
                                    @foreach($category->questions as $question)
                                        <div class="faq-answer">
                                            <dt>{{$question->question}}</dt>
                                            <hr>
                                            <dd>{{$question->answer}}</dd>
                                        </div>
                                    @endforeach
                                </dl>
                            </section>
                        @endforeach
                    @else
                        <h2 class="category-title">Sorry! We find nothing for your query! You can try other query</h2>
                        <p>
                            <a href="{{route('faq')}}">or click here to return back to full questions list</a>
                        </p>
                    @endif
                </div>
            </div>
        </div>
    </div>

