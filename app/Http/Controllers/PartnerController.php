<?php

namespace App\Http\Controllers;

use App\Repositories\PartnerRepository;

class PartnerController extends Controller
{
    /**
     * The event repository implementation.
     *
     * @var PartnerRepository
     */
    private $partner;

    /**
     * Create a new controller instance.
     *
     * @param PartnerRepository $partner
     * @return void
     */
    public function __construct(PartnerRepository $partner)
    {
        $this->partner = $partner;
    }

    public function show_list()
    {
        $partners = $this->partner->paginateSort();
        $meta = \App\Models\PageMeta::byPage('partners');
        return view('partners.list', compact(['partners', 'meta']));
    }
}
