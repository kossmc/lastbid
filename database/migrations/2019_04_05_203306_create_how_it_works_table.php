<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHowItWorksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('how_it_works', function (Blueprint $table) {
            $table->increments('id');
            $table->string('video');
            $table->string('section_1_text');
            $table->string('section_2_text');
            $table->string('section_3_text');
            $table->string('section_4_text');
            $table->string('image_section_2');
            $table->string('image_section_3');
            $table->string('image_section_4');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('how_it_works');
    }
}
