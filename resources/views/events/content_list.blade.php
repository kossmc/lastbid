    <div class="page-wrap">
        <div class="wrap">
            <div class="title-evets">
                <h1>Auction Calendar</h1>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-4 col-lg-3">
                    <div id="calendar"></div>
                </div>
                <div class="col-xs-12 col-sm-8 col-lg-9">
                    <div class="row all-evets-wrap">
                        @foreach($events as $event)
                            <div class="col-xs-6 col-sm-12 item clearfix">
                                <div class="image"><a href="{{ $event->url }}">
                                        <img src="{{$event->getImagePath()}}" alt="{{ $event->title }}">
{{--                                        <img src="{{$event->getImagePath(326,216)}}" alt="{{ $event->title }}">--}}
                                    </a>
                                </div>
                                <div class="desc-wrap">
                                    <div class="title">
                                        <h3>
                                            <a href="{{ $event->url }}">{{ $event->title }}</a>
                                        </h3>
                                    </div>
                                    <div class="status-wrap clearfix">
                                        @if($event->event_date)
                                            <div class="item-status {{$event->event_date ? 'icon-time':''}}">{{$event->event_date->format('g:i a')}}</div>
                                        @endif
                                            <div class="item-status {{$event->location ? 'icon-place':''}}">{{$event->location or ' '}}</div>

                                    </div>
                                    @if($event->event_date)
                                        <div class="data">{{$event->event_date->format('j F Y')}}</div>
                                    @endif
                                    <div class="content">
                                        {{ strip_tags($event->brief) }}
                                    </div>
                                    {{--@isset($event->partners)--}}
                                        {{--@if ($event->partners->isNotEmpty())--}}
                                            {{--<img class="partner-logo" src="{{asset('uploads/'.$event->partners->first()->image)}}" alt="">--}}
                                        {{--@endif--}}
                                    {{--@endisset--}}
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>

            @include('inc.paginator_with_page_selector', ['items'=>$events])
        </div>
    </div>
