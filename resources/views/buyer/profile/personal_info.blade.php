@extends('buyer.profile.profile')
@section('title', 'Account info')
@section('profile_content')
    <div class="buyer-account">
        <section class="account-info">
            <h3>Profile</h3>
            <p><span>First Name:</span>{{$user->first_name}}</p>
            <p><span>Last Name:</span>{{$user->last_name}}</p>
            <p><span>Your Email:</span><b class="is__email">{{$user->email}}</b></p>
            <div class="btn-wrap">
                <a class="btn btn-buyer-edit-info" href="{{route('account-info-edit')}}">Edit info</a>
                {{--<a class="btn2 btn-buyer-confirm-deleting" href="javascript: void 0;">Delete account </a>--}}
            </div>
        </section>
        <section class="account-info">
            {{--<h3>Password</h3>--}}
            <div class="btn-wrap"><a class="btn btn-buyer-change-pswrd" href="javascript: void 0;">Change
                    password</a></div>
        </section>
    </div>
@endsection
@section('custom_modal')
    <div id="popup-overlay"></div>
    <div class="popup buyer-account---confirm-deleting">
        <div class="title">Confirm deleting</div>
        <p>This action will delete your bank account from the system.<br>Are you sure you want to continue?</p>
        <div class="btn-wrap"><a class="btn" href="#">ok</a><a class="btn2" href="javascript: void 0;" data-dismiss="modal">cancel</a></div>
    </div>
    @if (session()->has('password_changed'))
        @php($t=session()->pull('password_changed')){{--remove value from session--}}
        <div class="popup buyer-account---pswrd-changed-success"><span class="icon-congrat"></span>
            <div class="title">password changed</div>
            <p>Your password has been<br>successfully changed!</p>
            <div class="btn-wrap"><a class="btn btn--success" href="#" data-dismiss="modal">ok</a></div>
        </div>
    @endif
    <div class="popup buyer-account---change-pswrd">
        <form action="{{route('account-info-edit-password')}}" method="post">
            <div class="title">change password
            </div>
            @if ($errors->has('password_old'))
                <span class="error-notification">
                    {{ $errors->first('password_old') }}
                </span>
            @endif
            @if ($errors->has('password'))
                <span class="error-notification">
                        {{ $errors->first('password') }}
                    </span>
            @endif
            @if ($errors->has('password_confirmation'))
                <span class="error-notification">
                        {{ $errors->first('password_confirmation') }}
                    </span>
            @endif
            <input type="password" name="password_old" placeholder="Current password" value=""
            @if ($errors->has('password_old')){{' class="has-error--bgcolor" '}}@endif>
            <input type="password" name="password" placeholder="New password" value=""
            @if ($errors->has('password')){{' class="has-error--bgcolor" '}}@endif>
            <input type="password" name="password_confirmation" placeholder="Repeat new password" value=""
            @if ($errors->has('password_confirmation')){{' class="has-error--bgcolor" '}}@endif>
            <div class="btn-wrap"><button class="btn btn-buyer-change-pswrd" type="submit">save new password</button></div>
            {{csrf_field()}}
        </form>
    </div>
@endsection
@section('user_script')
    (function(){

    var btn_buyer_confirm_deleting = $('.btn-buyer-confirm-deleting');
    var btn_buyer_change_pswrd = $('.btn-buyer-change-pswrd');
    var btn_buyer_confirm_deleting = $('.btn-buyer-confirm-deleting');
    var btn_buyer_change_pswrd = $('.btn-buyer-change-pswrd');
    var errors = $('.popup.buyer-account---change-pswrd .error-notification');
    var success = $('.buyer-account---pswrd-changed-success');

    function showPasswordPopUp() {
    $('#popup-overlay').show();
    $('.popup.buyer-account---change-pswrd').show();
    }
    // close/open modals
    btn_buyer_change_pswrd.on('click', showPasswordPopUp);

    btn_buyer_confirm_deleting.on('click', function(){
    $('#popup-overlay').show();
    $('.popup.buyer-account---confirm-deleting').show();
    });

    $('#popup-overlay, [data-dismiss="modal"]').on('click', function(){
    $('#popup-overlay').hide();
    $('.popup').hide();
    });
    if (errors.length){
    showPasswordPopUp();
    }
    if (success.length){
    $('#popup-overlay').show();
    success.show();
    }
    }());
@endsection


