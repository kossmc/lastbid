<?php

namespace App\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * These middleware are run during every request to your application.
     *
     * @var array
     */
    protected $middleware = [
        \Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode::class,
        \Illuminate\Foundation\Http\Middleware\ValidatePostSize::class,
        \App\Http\Middleware\TrimStrings::class,
        \Illuminate\Foundation\Http\Middleware\ConvertEmptyStringsToNull::class,
    ];

    /**
     * The application's route middleware groups.
     *
     * @var array
     */
    protected $middlewareGroups = [
        'web' => [
            \App\Http\Middleware\EncryptCookies::class,
            \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
            \Illuminate\Session\Middleware\StartSession::class,
            // \Illuminate\Session\Middleware\AuthenticateSession::class,
            \Illuminate\View\Middleware\ShareErrorsFromSession::class,
            \App\Http\Middleware\VerifyCsrfToken::class,
            \Illuminate\Routing\Middleware\SubstituteBindings::class,
        ],

        'api' => [
            'throttle:60,1',
            'bindings',
        ],
    ];

    /**
     * The application's route middleware.
     *
     * These middleware may be assigned to groups or used individually.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'auth' => \Illuminate\Auth\Middleware\Authenticate::class,
        'bindings' => \Illuminate\Routing\Middleware\SubstituteBindings::class,
        'can' => \Illuminate\Auth\Middleware\Authorize::class,
        'guest' => \App\Http\Middleware\RedirectIfAuthenticated::class,
        'throttle' => \Illuminate\Routing\Middleware\ThrottleRequests::class,
        'lotIsBiddable' =>\App\Http\Middleware\LotIsBiddable::class,
        'hasRole' =>\App\Http\Middleware\HasRole::class,
        'cardDetailsFilled' =>\App\Http\Middleware\CardDetailsFilled::class,
        'bidConfirmed' =>\App\Http\Middleware\BidConfirmed::class,
        'bidIsAboveMinAmount' =>\App\Http\Middleware\BidIsAboveMinAmount::class,
        'bidIsBelowPriceTo' =>\App\Http\Middleware\BidIsBelowPriceTo::class,
        'lotIsMine' =>\App\Http\Middleware\LotIsMine::class,
        'bidIsMine' =>\App\Http\Middleware\BidIsMine::class,
        'counterBidConfirmed' =>\App\Http\Middleware\CounterBidConfirmed::class,
        'lotHasMyBid' =>\App\Http\Middleware\LotHasMyBid::class,
        'messageNotBlank' => \App\Http\Middleware\MessageNotBlank::class,
        'userIsSeller' => \App\Http\Middleware\UserIsSeller::class,
        'canUserSendMessage' => \App\Http\Middleware\CanUserSendMessage::class,
        'lotIsDiscussable' => \App\Http\Middleware\LotIsDiscussable::class,
        'emailVerified' => \App\Http\Middleware\EmailVerified::class,
        'recentlyLotsViews' => \App\Http\Middleware\RecentlyLotsViews::class,
        'countLotViews' => \App\Http\Middleware\CountLotViews::class,
        'logout' => \App\Http\Middleware\LogOutUser::class,
    ];
}
