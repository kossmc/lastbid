<?php

namespace App\Http\Middleware;

use App\Models\RecentlyLot;
use Closure;
use Illuminate\Support\Facades\Auth;

class RecentlyLotsViews
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $this->checkIfAuthenticated();
        $this->saveLotsList($this->addLotToList($request->id, RecentlyLot::recentlyLotsIds()));
        return $next($request);
    }

    public function checkIfAuthenticated()
    {
        if ( ! Auth::guest() && $lots = request()->cookie('lots')) {
            $savedLots = Auth::user()->recentlyLots->pluck('id');
            if ($savedLots) {
                $lots = $savedLots->merge($lots)->unique()
                    ->take(-env('countSeenLots', 10));
            }
            Auth::user()->recentlyLots()->sync($lots);
            forgetCookie('lots');
        }
    }

    private function addLotToList($id, $lotsList)
    {
        if ($lotsList->isEmpty()) {
            $lotsList->push($id);
        } else {
            if (is_numeric($i = $lotsList->search($id))) {
                $lotsList->forget($i);
                if (Auth::user()) {
                    Auth::user()->recentlyLots()->detach($id);
                }
            }
            $lotsList->count() < env('countSeenLots', 10) ? $lotsList->push($id) :
                $lotsList = $lotsList->push($id)->take(- env('countSeenLots', 10));
        }
        return $lotsList;
    }

    private function saveLotsList($lotsList)
    {
        $lotsList = $lotsList->toArray();
        if (Auth::guest()) {
          return  addCookie('lots', $lotsList);
        }
        return Auth::user()->recentlyLots()->sync($lotsList);
    }
}
