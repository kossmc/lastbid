<?php

namespace App\Http\Controllers;

use App\Repositories\BlogRepository;
use App\Repositories\Criteria\Blog\IsActive;

/**
 * Class BlogController
 * @package App\Http\Controllers
 */
class BlogController extends Controller
{
    /**
     * The blog repository implementation.
     * @var BlogRepository
     */
    private $blog;

    /**
     * Create a new controller instance.
     * @param BlogRepository $blog
     */
    public function __construct(BlogRepository $blog)
    {
        $this->blog = $blog;
    }

    /**
     * Get all news
     * @return mixed
     */
    private function all()
    {
        //$this->blog->pushCriteria(new IsActive());

        return $this->blog->order()->active()->paginate(request('per_page', env('PAGINATE_EVENTS')));
    }

    /**
     * Get news
     * @param $slug
     * @return mixed
     */
    private function get($slug)
    {
        if ($blog_post = $this->blog->findBySlug($slug)) {
            return ['blog_post' => $blog_post, 'blog_posts' => $this->blog->latest(6, [$blog_post->id])];
        }

        return null;
    }

    /**
     * Show all blog
     */
    public function show_list()
    {
        $blog_posts = $this->all();
        $meta = \App\Models\PageMeta::byPage('news');
        return view('blog.list', compact('blog_posts', 'meta'));
    }

    /**
     * View detail blog
     * @param null $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function view($slug = null)
    {
        $blog_post = $this->get($slug);
        if(!$blog_post)
            abort(404);

        return view('blog.view', $blog_post);
    }

    /**
     * Show all blog in landing
     */
    public function show_list_landing()
    {
        \URL::forceRootUrl(env('APP_LANDING_URL'));

        return view('landing.blog', ['blog_posts' => $this->all()]);
    }

    /**
     * View detail blog landing
     * @param null $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function view_landing($slug = null)
    {
        \URL::forceRootUrl(env('APP_LANDING_URL'));

        $blog_post = $this->get($slug);
        if(!$blog_post)
            abort(404);

        return view('landing.blog_view', $blog_post);
    }
}
