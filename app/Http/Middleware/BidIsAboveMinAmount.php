<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\Repositories\AutodeclinedBidRepository;

class BidIsAboveMinAmount
{
    /**
     * The autodeclined bid repository implementation.
     *
     * @var AutodeclinedBidRepository
     */
    private $autodeclinedBid;

    /**
     * Create a new middleware instance.
     *
     * @param AutodeclinedBidRepository $lot
     * @return void
     */
    public function __construct(AutodeclinedBidRepository $autodeclinedBid)
    {
        $this->autodeclinedBid = $autodeclinedBid;
    }
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $lot = $request->lot;
        $amount = $request->input('amount',0);
        if ($lot->bidIsAboveMinAmount($amount)) {
            return $next($request);
        } else {
            if ((int)$amount) {
                $data = [
                    'user_id' => Auth::id(),
                    'lot_id' => $lot->id,
                    'amount' => $amount
                ];
                $this->autodeclinedBid->create($data);
            }
            return response()->json([
                'error' => true,
                'message' => 'Your bid is below the minimum amount set by the auction house. Please raise your bid.'
            ]);
        }
    }
}
