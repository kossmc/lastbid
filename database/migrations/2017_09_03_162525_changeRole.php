<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeRole extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = 'UPDATE `roles` SET `name`=\'buyer\' WHERE `name`=\'user\'';
        DB::connection()->getPdo()->exec($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $sql = 'UPDATE `roles` SET `name`=\'user\' WHERE `name`=\'buyer\'';
        DB::connection()->getPdo()->exec($sql);
    }
}
