<?php


namespace App\Services;

use Carbon\Carbon;

class ArtNetDirectoriesCrawler extends BaseCrawler
{
    public $filename;

    public $headers;

    public function __construct($batch)
    {
        $this->filename = 'Artnet_Auction_House_Directories_'.Carbon::now()->format('Y-m-d_H-i-s').'.csv';
        $this->headers = $this->getHeaders();
        parent::__construct($batch);
    }

    public function startCrawling()
    {
        $directory = collect([]);
        $url = 'http://www.artnet.com/api/ahdirectory/SearchAuctionHouses?MembersOnly=false&Sort=1';
        $response = $this->client->request('GET', $url);
        foreach (json_decode($response->getBody()->getContents())->Directory->Groups as $group) {
            foreach ($group->Items as $house) {
                $directoryEntry = [
                    'Name' => $house->Name,
                    'MemberUrl' => $house->MemberUrl ? 'http://www.artnet.com'.$house->MemberUrl : '',
                ];
                if (isset($house->Locations) && count($house->Locations)) {
                    foreach ($house->Locations as $i => $location) {
                        unset($location->LocationId, $location->IsEuroAddress, $location->LocationName);
                        $directoryEntry['Location_'.($i+1).'_zip'] = $location->Zip ? $location->Zip: '';
                        $directoryEntry['Location_'.($i+1).'_state'] = $location->State ? $location->State: '';
                        $directoryEntry['Location_'.($i+1).'_country'] = $location->Country ? $location->Country: '';
                        $directoryEntry['Location_'.($i+1).'_address'] = collect(
                            [
                                'GalleryAddress1'=>$location->GalleryAddress1,
                                'GalleryAddress2'=>$location->GalleryAddress2
                            ]
                        )->filter()
                            ->transform(function ($item, $key) {
                                return $key.' : '.$item;
                            })->implode(','.PHP_EOL);
                        $directoryEntry['Location_'.($i+1).'_phone'] = collect([
                            $this->clearPhone($location->PhoneNumber1),
                            $this->clearPhone($location->PhoneNumber2),
                        ])->filter()->implode(','.PHP_EOL);
                        $directoryEntry['Location_'.($i+1).'_fax'] = $location->Fax ? $this->clearPhone($location->Fax) : '';
                        $directoryEntry['Location_'.($i+1).'_url'] = $location->ProprietaryUrl ? $location->ProprietaryUrl : '';
                        $directoryEntry['Location_'.($i+1).'_email'] = $location->GalleryEmail ? $location->GalleryEmail : '';
                    }
                }
                $directory[] = $directoryEntry;
            }
        }
        if ($directory->isNotEmpty()) {
            return $directory->sortBy('Name')->values();
        } else {
            return [];
        }
    }
    private function getHeaders()
    {
        $headers = ['Name', 'MemberUrl'];
        for ($i = 1; $i < 11; $i++) {
            $headers[] = 'Location_'.$i.'_zip';
            $headers[] = 'Location_'.$i.'_state';
            $headers[] = 'Location_'.$i.'_country';
            $headers[] = 'Location_'.$i.'_address';
            $headers[] = 'Location_'.$i.'_phone';
            $headers[] = 'Location_'.$i.'_fax';
            $headers[] = 'Location_'.$i.'_url';
            $headers[] = 'Location_'.$i.'_email';
        }
        return $headers;
    }

    private function clearPhone($phone)
    {
        $phone = trim(str_replace(['-','+', ' '], '', $phone));
        return $phone ? '"'.$phone.'"' : '';
    }
}
