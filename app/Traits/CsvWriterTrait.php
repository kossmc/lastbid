<?php

namespace App\Traits;
use League\Csv\Writer;

trait CsvWriterTrait
{
    public function saveAsFile($data)
    {
        $writer = $this->getWriter();
        try {
            $writer->insertOne($this->getHeaders());
            $writer->insertAll($data);
            return $this->tempFolder.$this->fileName;
        } catch (CannotInsertRecord $e) {
            \Log::info(print_r($e->getRecords(), true));
        }
        return false;
    }

    private function getWriter()
    {
        if (!is_dir($this->tempFolder)) {
            mkdir($this->tempFolder, 0777, true);
        }
        $writer = Writer::createFromPath($this->tempFolder.$this->fileName, 'w+'); //the CSV file
        $writer->setDelimiter(";"); //the delimiter will be the tab character
        $writer->setNewline("\r\n"); //use windows line endings for compatibility with some csv libraries
        $writer->setOutputBOM(Writer::BOM_UTF8);//adding the BOM sequence on output
        return $writer;
    }
}
