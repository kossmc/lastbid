<?php

namespace App\Http\Requests;

class LotUpdateRequest extends \Backpack\CRUD\app\Http\Requests\CrudRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // only allow updates if the user is logged in
        return \Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
             'title' => 'required|min:1|max:255',
             'subtitle' => 'nullable|min:1|max:255',
             'description' => 'nullable|min:1|max:65534',
             'partner_id' => 'required|numeric',
             'shipping_description' => 'nullable|min:1|max:65534',
             'auction_house_fee' => 'required|numeric|between:0,99.99',
             'min_bid_amount' => 'required|numeric|min:1|less_than_field:range_from',
             'end_date' => 'required|date|after_or_equal:start_date',
             'range_from' => 'required|numeric',
             'range_to' => 'required|numeric|greater_than_field:range_from',
             'photos.*' => 'mimes:jpeg,gif,png|nullable',
             'auto_accept_price' => 'nullable|numeric|greater_than_field:min_bid_amount'
        ];
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */


    public function attributes()
    {
        return [
            //
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            //
        ];
    }
}
