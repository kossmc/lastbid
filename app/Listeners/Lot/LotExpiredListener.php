<?php

namespace App\Listeners\Lot;

use App\Events\Lot\LotExpired;
use App\Repositories\BidRepository;

class LotExpiredListener extends SendGridAbstractLotListener
{

    /**
     * Seller Email subject.
     */
    private $sellerSubject = 'Lot was closed ';

    /**
     * Seller Email template.
     */
    private $sellerTemplate = '9f114953-9e04-4bd2-8caf-feabe2c8d9e9';

    /**
     * The bid repository implementation.
     *
     * @var BidRepository
     */
    private $bid;


    /**
     * Create a new listener instance.
     *
     * @param BidRepository $bid
     * @return void
     */
    public function __construct(BidRepository $bid)
    {
        $this->bid = $bid;
    }

    /**
     * Handle the event.
     *
     * @param LotExpired $event
     * @return void
     */
    public function handle(LotExpired $event)
    {
        $this->setData($event);

        /*notification to seller*/
        //$this->sendMail($this->seller, $this->getData($this->sellerTemplate), $this->sellerSubject);

        // decline all pending bids for expired lot
        foreach ($this->lot->bids()->pending()->get() as $bid) {
            $this->bid->declineBySystem($bid);
        }
    }
}
