<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ViewedLot extends Model
{
    protected $table = 'lots_users_views';
    protected $guarded = [];
    public $timestamps = false;

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */
    public function scopeGuestLotsViewed($query, $quest_id, $lot_id = null)
    {
        return $query->where('quest_id', $quest_id)
            ->when($lot_id, function ($query) use ($lot_id) {
                return $query->where('lot_id', $lot_id);
            });
    }
}
