<?php

namespace App\Mail\Bid;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

use App\Traits\BidNoticeTrait;

class SellerCounteredBackBidNotice extends Mailable
{
    use Queueable, SerializesModels, BidNoticeTrait;

    /**
     * Email subject.
     *
     */
    protected $bid_notice_subject = 'Your bid was countered by buyer';

    /**
     * Email template.
     *
     */
    protected $bid_notice_template = 'emails.seller_countered_back_bid_notice';
}
