@if($isFile)

    @if(isset($fileName))
        <h3>Hello, file from the crawler has been created successfully</h3>
    @else
        <h3>Hello, No items listed for auction</h3>
    @endif

@else
    <h3>Hello, file from the crawler hasn't been created</h3>

    <p>Please try again or contact your administrator.</p>
@endif

<p><strong>Parser:</strong> {{$type}}</p>

<p><strong>Input:</strong> {{$input}}</p>

