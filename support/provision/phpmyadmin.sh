# intstall phpmyadmin
echo ">>> Installing phpmyadmin"

export DEBIAN_FRONTEND="noninteractive"
sudo debconf-set-selections <<< "phpmyadmin phpmyadmin/dbconfig-install boolean true"
sudo debconf-set-selections <<< "phpmyadmin phpmyadmin/app-password-confirm password root"
sudo debconf-set-selections <<< "phpmyadmin phpmyadmin/mysql/admin-pass password root"
sudo debconf-set-selections <<< "phpmyadmin phpmyadmin/mysql/app-pass password root"
sudo debconf-set-selections <<< "phpmyadmin phpmyadmin/reconfigure-webserver multiselect none"
sudo apt-get -y install phpmyadmin > /dev/null 2>&1
sudo rm /lastbid/public/phpmyadmin > /dev/null 2>&1
sudo ln -s /usr/share/phpmyadmin /lastbid/public

echo ">>> Installing phpmyadmin completed!"