<?php

namespace App\Models;

use App\Events\User\ResetPassword;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Backpack\CRUD\CrudTrait;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Support\Facades\Auth;

class User extends Authenticatable
{
    use Notifiable;
    use CrudTrait;
    use HasRoles;
    /*
   |--------------------------------------------------------------------------
   | GLOBAL VARIABLES
   |--------------------------------------------------------------------------
   */
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'last_name', 'first_name', 'email', 'password','phone',
        'address','card_number','cvv','exp_date', 'company_name',
        'middle_name', 'suffix', 'new_email', 'email_token','email_confirmed','terms_link',
        'timezone'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['full_name'];
    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public function cardDetailsFilled()
    {
        return $this->cards->isNotEmpty();
    }
    /**
     * Send the password reset notification.
     *
     * @param  string $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        \Event::fire(new ResetPassword($token, $this));
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function lots()
    {
        return $this->hasMany(Lot::class, 'user_id', 'id');
    }

    public function bids()
    {
        return $this->hasMany(Bid::class, 'user_id', 'id');
    }

    public function cards()
    {
        return $this->hasMany(BuyerCard::class, 'user_id', 'id')
            ->orderByDesc('default')->latest();
    }

    public function accounts()
    {
        return $this->hasMany(BuyerAccount::class, 'user_id', 'id')
            ->orderByDesc('default')->latest();
    }

    public function addresses()
    {
        return $this->hasMany(BuyerAddress::class, 'user_id', 'id')
            ->orderByDesc('default')->latest();
    }

    public function getAddressFormatted(){
        $address = $this->hasMany(BuyerAddress::class, 'user_id', 'id')
            ->orderByDesc('default')->first();
        if($address){
            return implode('<br />',[
                $address->address_name,
                $address->address,
                $address->city,
                $address->state,
                $address->country,
                $address->zip
            ]);
        } else {
            return '';
        }

    }

    public function subscriptions()
    {
        return $this->belongsToMany(Subscription::class, 'subscription_user',
            'user_id', 'subscription_id');
    }

    public function authSocial()
    {
        return $this->hasMany(AuthSocial::class, 'user_id','id');
    }

    public function recentlyLots()
    {
        return $this->belongsToMany(Lot::class, 'lot_user', 'user_id', 'lot_id');
    }

    public function userViewedLots()
    {
        return $this->belongsToMany(Lot::class, 'lots_users_views', 'user_id', 'lot_id');
    }

    public function fees()
    {
        return $this->hasMany(Fee::class, 'user_id', 'id');
    }
    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */
    /**
     * get user full name
     * @return string
     */

    public function getFullNameAttribute()
    {
        return $this->attributes['first_name'] . ' ' . $this->attributes['last_name'];
    }

    public function getShortNameAttribute()
    {
        return $this->attributes['first_name'] . ' ' . ucfirst(mb_substr($this->attributes['last_name'], 0, 1)).'.';
    }

    public function getNewBidsCountAttribute()
    {
        // @todo: cache this value
        if (!isset($this->attributes['new_bids_count'])) {
            if ($this->hasRole('buyer')) {
                $newBidsCount = $this->bids()->pending()->bySeller()->count();
            }
            else {
                $newBidsCount = $this->lots()
                    ->withCount('newBids')
                    ->where('bids_count', '>', 0)
                    ->limit(1000)
                    ->get()
                    ->sum('new_bids_count');
            }
            $this->attributes['new_bids_count'] = $newBidsCount;
        }
        return $this->attributes['new_bids_count'];
    }

    public function getNewMessagesCountAttribute()
    {
        // @todo: cache this value
        if (!isset($this->attributes['new_messages_count'])) {
            $this->attributes['new_messages_count'] =
            Message::byUser(Auth::id())
                ->received()
                ->active()
                ->unread()
                ->count('messages.id');
        }
        return $this->attributes['new_messages_count'];
    }

    public function getEmailTokenAttribute()
    {
        if ( empty($this->attributes['email_token'])) {
            $this->email_token = str_random(30);
            $this->save();
        }

        return $this->attributes['email_token'];
    }

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = bcrypt($password);
    }
}
