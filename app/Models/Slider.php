<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class Slider extends Model
{
    use CrudTrait;
    protected $table = 'slider';
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];
    public $timestamps = false;

    public function scopeActive($query)
    {
        return $query->where('status',1);
    }

    public function scopeSortByOrder($query)
    {
        return $query->orderBy('lft');
    }
    public function scopeWithoutDate($query)
    {
        return $query->where('start_date', null)->where('end_date', null);
    }

    public function scopeWithDate($query)
    {
        return $query->where('start_date', '<', Carbon::now()->format('Y-m-d h:i:s'))
            ->where('end_date', '>', Carbon::now()->format('Y-m-d h:i:s'));
    }
    public static function boot()
    {
        parent::boot();
        static::deleting(function ($obj) {
            \Storage::disk('uploads')->delete($obj->image);
        });
    }

    public function setImageAttribute($value)
    {
        $attribute_name = "image";
        $disk = "uploads";
        $destination_path = "slider";

        $this->uploadFileToDisk($value, $attribute_name, $disk, $destination_path);
    }
}
