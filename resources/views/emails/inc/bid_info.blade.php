<p>Bid price: {!! format_price($bid->attempt->amount, null, $lot->currency) !!}</p>
<p>Bid status: {{ $bid->attempt->status }}</p>
<p>Bid created: {{ $bid->attempt->created_at }}</p>
<p>Bid end date: {{ $bid->attempt->created_at->addSeconds(env('BID_LIFETIME')) }}</p>
@isset($bid->attempt->message)
	<p>Bid message: {{$bid->attempt->message}} </p>
@endisset
