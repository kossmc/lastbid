<div class="short-description{{ $bid->attempt->is_declined || $bid->attempt->is_expired ? ' short-description--declined' : '' }}">
    @if ($lotLink)
        <a href="{{ route('buyer_lot', ['id' => $bid->lot_id]) }}">
    @endif
    <span class="title">
        @if (!$bid->attempt->is_my_bid)
            Counter bid from seller
        @elseif ($bid->attempt->is_accepted || $bid->attempt->is_declined)
            My bid
        @else
            My current bid <span class="value">{{ $bid->attempts_count }}/{{ env('BIDS_LIMIT') }}</span>
        @endif
    </span>
    <span class="price-value">{!! format_price($bid->attempt->amount, null, $bid->lot->currency) !!}</span>
    @if ($bid->attempt->is_declined)
        <span class="declined-label {{ $bid->attempt->status }}">Declined</span>
    @elseif ($bid->attempt->is_expired)
        <span class="declined-label {{ $bid->attempt->status }}">Expired</span>
    @elseif ($bid->attempt->is_accepted)
        <span class="declined-label {{ $bid->attempt->status }}">Accepted</span>
    @else
        <div class="bid-expiration">
            <span class="bid-expiration-wrapper">
                <span class="bid-expiration-progress" style="width:{{ percent_time_left($bid->attempt->start_date, $bid->attempt->end_date) }}%;"></span>
            </span>    
            <span>Bid expires in:</span>
            <span class="bid-expiration-countdown">{{ format_time_left($bid->attempt->end_date) }}</span>
        </div>
    @endif
    @if ($lotLink)
        </a>
    @endif
</div>
<div class="bid-partial-card">
    <div class="image">
        @if ($lotLink)
            <a href="{{ route('buyer_lot', ['id' => $bid->lot_id]) }}">
        @endif
        <img src="{{ $bid->lot->getImagePath(130, 130) }}" alt="">
        @if ($lotLink)
            </a>
        @endif
    </div>
    <div class="description">
        @if ($lotLink)
            <a href="{{ route('buyer_lot', ['id' => $bid->lot_id]) }}" class="title">
        @else
            <span class="title">
        @endif
        <h4>{{ $bid->lot->title }}</h4>
        @if ($lotLink)
            </a>
        @else
            </span>
        @endif
        <div class="lot-number-block">
            <span class="text-small">LastBid Lot No</span>
            <span class="text-normal lot-number">{{ $bid->lot->id }}</span>
        </div>
        <div class="price-block">
            <span class="text-bigger price">{!! format_price($bid->lot->range_from, $bid->lot->range_to, $bid->lot->currency) !!}</span>
            <span class="text-small">Suggested bid range</span>
        </div>
        @if ($bid->lot->is_active)
            <div class="info-bottom time-wrap-line">
                <div class="time-line">
                    <div class="time-line-left" style="width:{{ percent_time_left($bid->lot->start_date, $bid->lot->end_date) }}%"></div>
                </div>
                {{ format_time_left($bid->lot->end_date) }} left
            </div>
        @endif
    </div>
    <div class="total-bid-info">
        <div style="display: none">
            <span class="total-bids-counter">{{ $bid->lot->bids_count }}</span>
            <span  class="total-bids-label">total bids</span>
        </div>
        <div>
            <span class="view-bids-counter">{{ $bid->lot->views_count }}</span>
            <span class="view-bids-label">views</span>
        </div>
    </div>
    <a href="@yield('back_link', '#')" class="btn-close-exapanded-bid-arrow btn-close-exapanded-bid icon-right_arrow"></a> 
</div>
