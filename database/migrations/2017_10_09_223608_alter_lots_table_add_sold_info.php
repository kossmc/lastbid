<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterLotsTableAddSoldInfo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('lots', function (Blueprint $table) {
            $table->decimal('sold_amount', 16 , 2)->nullable();
            $table->unsignedInteger('buyer_id')->nullable();
            $table->timestamp('sold_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('lots', function (Blueprint $table) {
            $table->dropColumn('sold_amount');
            $table->dropColumn('buyer_id');
            $table->dropColumn('sold_date');
        });
    }
}
