<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class BidAttempt extends Model
{

    const STATUS_PENDING = 'pending';
    const STATUS_ACCEPTED = 'accepted';
    const STATUS_DECLINED = 'declined';
    const STATUS_COUNTERED = 'countered';
    const STATUS_EXPIRED = 'expired';

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $dates = ['created_at', 'updated_at'];

    protected $table = 'bid_attempts';
    //protected $primaryKey = 'id';
    public $timestamps = true;
    // protected $guarded = ['id'];
    protected $fillable = ['amount', 'user_id', 'status', 'message', 'is_counter'];
    // protected $hidden = [];
    // protected $dates = [];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'amount' => 'float'
    ];
    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['bid_price', 'bidder_short_name', 'bid_status', 'is_my_bid', 'counter_exists'];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    
    public function accept()
    {
        $this->status = self::STATUS_ACCEPTED;
        return $this->save();
    }

    public function decline()
    {
        $this->status = self::STATUS_DECLINED;
        return $this->save();
    }

    public function counter()
    {
        $this->status = self::STATUS_COUNTERED;
        return $this->save();
    }

    public function expire()
    {
        $this->status = self::STATUS_EXPIRED;
        return $this->save();
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function bid()
    {
        return $this->belongsTo(Bid::class, 'bid_id', 'id');
    }
    // @todo: rename to author() and add logic to get user or counter user
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function countersHistory()
    {
        return $this->hasMany(Bid::class, 'parent_id');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */
       public function scopeExpired($query) {
           return $query
               ->where('status', self::STATUS_PENDING)
               ->where('created_at', '<', DB::raw('DATE_SUB(NOW(), INTERVAL ' . env('BID_LIFETIME') . ' SECOND)'));
       }


    // @todo: add scopes for pending/sent/expired/declined bids

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /**
     * check if bid belongs to current user
     * @return bool
     */
    public function getIsMyBidAttribute()
    {
        return (array_key_exists('user_id', $this->attributes) && $this->attributes['user_id'] == auth()->id()) ?
            true : false;
    }

    /**
     * get bid status text explanation
     * @return string
     */
    public function getBidStatusAttribute()
    {
        if (array_key_exists('status', $this->attributes)) {
            switch ($this->attributes['status']) {
                case self::STATUS_DECLINED:
                    return 'declined';
                    break;
                case self::STATUS_ACCEPTED:
                    return 'accepted';
                    break;
                case self::STATUS_COUNTERED:
                    return 'countered';
                    break;
                case self::STATUS_EXPIRED:
                    return 'expired';
                    break;
                default:
                    return 'pending';
                    break;
            }
        }
        return 'undefined';
    }

    public function getStartDateAttribute()
    {
        return $this->created_at;
    }

    public function getEndDateAttribute()
    {
        return $this->created_at->addSeconds(env('BID_LIFETIME'));
    }

    public function getIsPendingAttribute()
    {
        return $this->status == self::STATUS_PENDING;
    }

    public function getIsAcceptedAttribute()
    {
        return $this->status == self::STATUS_ACCEPTED;
    }

    public function getIsDeclinedAttribute()
    {
        return $this->status == self::STATUS_DECLINED;
    }

    public function getIsCounteredAttribute()
    {
        return $this->status == self::STATUS_COUNTERED;
    }

    public function getIsExpiredAttribute()
    {
        return $this->status == self::STATUS_EXPIRED;
    }

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
