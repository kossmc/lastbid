@if ($errors->any())
    <span class="error-notification">
        @foreach($errors->all() as $error)
            {{$error}}<br>
        @endforeach
    </span>
@endif
