<?php

namespace App\Http\Controllers\Admin;

use App\Models\Category;
use App\Models\CategoryAlias;
use App\Models\Lot;
use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\CategoryRequest;
use App\Http\Requests\MoveLotsRequest;

class CategoryCrudController extends CrudController
{
    public function setUp()
    {

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Category');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/category');
        $this->crud->setEntityNameStrings('category', 'categories');

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */

        //        $this->crud->setFromDb();

        // ------ CRUD FIELDS
        $this->crud->addFields([
            [
                'name' => 'title',
                'label' => 'Category name'
            ]
            ,[ // Table
                'name' => 'aliases_json',
                'label' => 'Aliases',
                'type' => 'table',
                'entity_singular' => 'new alias', // used on the "Add X" button
                'columns' => [
                    'alias' => 'Alias name'
                ],
            ]
            ,[
                'name' => 'description',
                'label' => 'Category description',
                'type' => 'wysiwyg'
            ]
            ,[
                'name' => 'featured',
                'label' => 'Featured category',
                'type' => 'checkbox'
            ],
            [
                'name' => 'categories_icons_id',
                'label' => 'Category icon class name',
                'type' => 'select',
                'entity' => 'categories_icons',
                'attribute' => 'class_name',
                'model' => 'App\Models\CategoryIcons'
            ]
        ], 'both');
        $this->crud->addFields([
            [
                'name' => 'meta_description',
                'type' => 'textarea',
                'label' => 'Meta description'
            ],
        ], 'both');
        // $this->crud->addField($options, 'update/create/both');
        // $this->crud->addFields($array_of_arrays, 'update/create/both');
        // $this->crud->removeField('name', 'update/create/both');
        // $this->crud->removeFields($array_of_names, 'update/create/both');

        // ------ CRUD COLUMNS
        $this->crud->setColumns([
            [
                'name' => 'move',
                'type' => "model_function",
                'function_name' => 'getMoveCheckbox'
            ],
            [
                'name' => 'title',
                'label' => 'Category name'
            ],
            [
                // 1-n relationship
                'label' => "Aliases", // Table column heading
                'type' => "select_multiple",
                'name' => 'category_id', // the column that contains the ID of that connected entity;
                'entity' => 'aliases', // the method that defines the relationship in your Model
                'attribute' => "alias", // foreign key attribute that is shown to user
                'model' => "App\Models\CategoryAlias", // foreign key model
            ],
            [
                'name' => 'featured',
                'label' => 'Featured',
                'type' => 'boolean',
                // optionally override the Yes/No texts
                'options' => [0 => 'No', 1 => 'Yes'],
            ],
            [
                'name' => 'slug',
                'label' => 'Category url'
            ],
            ]);
        // $this->crud->addColumn(); // add a single column, at the end of the stack
        // $this->crud->addColumns(); // add multiple columns, at the end of the stack
        // $this->crud->removeColumn('column_name'); // remove a column from the stack
        // $this->crud->removeColumns(['column_name_1', 'column_name_2']); // remove an array of columns from the stack
        // $this->crud->setColumnDetails('column_name', ['attribute' => 'value']); // adjusts the properties of the passed in column (by name)
        // $this->crud->setColumnsDetails(['column_1', 'column_2'], ['attribute' => 'value']);

        // ------ CRUD BUTTONS
        $this->crud->addButtonFromView('top', 'move_lots', 'move_lots', 'end');
        // possible positions: 'beginning' and 'end'; defaults to 'beginning' for the 'line' stack, 'end' for the others;
        // $this->crud->addButton($stack, $name, $type, $content, $position); // add a button; possible types are: view, model_function
        // $this->crud->addButtonFromModelFunction($stack, $name, $model_function_name, $position); // add a button whose HTML is returned by a method in the CRUD model
        // $this->crud->addButtonFromView($stack, $name, $view, $position); // add a button whose HTML is in a view placed at resources\views\vendor\backpack\crud\buttons
        // $this->crud->removeButton($name);
        // $this->crud->removeButtonFromStack($name, $stack);

        // ------ CRUD ACCESS
        $this->crud->allowAccess(['list', 'create', 'update', 'delete']);
        // $this->crud->allowAccess(['list', 'create', 'update', 'reorder', 'delete']);
        // $this->crud->denyAccess(['list', 'create', 'update', 'reorder', 'delete']);

        // ------ CRUD REORDER
        $this->crud->enableReorder('title', 0);
        $this->crud->allowAccess('reorder');
        // NOTE: you also need to do allow access to the right users: $this->crud->allowAccess('reorder');

        // ------ CRUD DETAILS ROW
        // $this->crud->enableDetailsRow();
        // NOTE: you also need to do allow access to the right users: $this->crud->allowAccess('details_row');
        // NOTE: you also need to do overwrite the showDetailsRow($id) method in your EntityCrudController to show whatever you'd like in the details row OR overwrite the views/backpack/crud/details_row.blade.php

        // ------ REVISIONS
        // You also need to use \Venturecraft\Revisionable\RevisionableTrait;
        // Please check out: https://laravel-backpack.readme.io/docs/crud#revisions
        // $this->crud->allowAccess('revisions');

        // ------ AJAX TABLE VIEW
        // Please note the drawbacks of this though:
        // - 1-n and n-n columns are not searchable
        // - date and datetime columns won't be sortable anymore
        $this->crud->enableAjaxTable();

        // ------ DATATABLE EXPORT BUTTONS
        // Show export to PDF, CSV, XLS and Print buttons on the table view.
        // Does not work well with AJAX datatables.
        $this->crud->enableExportButtons();

        // ------ ADVANCED QUERIES
        // $this->crud->addClause('active');
        // $this->crud->addClause('type', 'car');
        // $this->crud->addClause('where', 'name', '==', 'car');
        // $this->crud->addClause('whereName', 'car');
        // $this->crud->addClause('whereHas', 'posts', function($query) {
        //     $query->activePosts();
        // });
        // $this->crud->addClause('withoutGlobalScopes');
        // $this->crud->addClause('withoutGlobalScope', VisibleScope::class);
        // $this->crud->with(); // eager load relationships
        // $this->crud->orderBy();
        // $this->crud->groupBy();
        // $this->crud->limit();
        $this->crud->setListView('admin.categories.backpack_list_categories');
    }

    public function store(CategoryRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud();
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        if ($request->has('aliases_json')) {
            $aliases = $request->input('aliases_json');
            $aliases = $aliases ? collect(json_decode($aliases, true))->filter() : collect([]);
            $this->saveAliases($aliases);
        }
        return $redirect_location;
    }

    public function update(CategoryRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud();
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        if ($request->has('aliases_json')) {
            $aliases = $request->input('aliases_json');
            $aliases = $aliases ? collect(json_decode($aliases, true))->filter() : collect([]);
            CategoryAlias::where('category_id', $this->crud->entry->id )->delete();
            $aliases = $aliases->map(function ($i) {
                unset($i['id']);
                return $i;
            });
            $this->saveAliases($aliases);
        } else {
            CategoryAlias::where('category_id', $this->crud->entry->id )->delete();
        }
        return $redirect_location;
    }

    private function saveAliases($aliases)
    {
        if ( !empty($aliases) && $aliases->isNotEmpty()) {
            $errors = [];
            foreach ($aliases as $alias) {
                $exist = CategoryAlias::where('alias', $alias['alias'])->exists();
                if (!$exist) {
                    $this->crud->entry->aliases()->create($alias);
                } else {
                    $errors[] = $alias['alias'];
                }
            }
        }
        if (!empty($errors) && count($errors)) {
            // show a error message
            $message = implode(', ', $errors);
            $message .= ' Already exist in database';
            \Alert::error($message)->flash();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return string
     */
    public function destroy($id)
    {
        $this->crud->hasAccessOrFail('delete');

        $category = $this->crud->model->withCount('children', 'lots')->findOrFail($id);
        if ($category->children_count || $category->lots_count) {
            return response('', 403);
        }

        return (string) $category->delete();
    }

    public function moveLotsForm()
    {
        if (request()->has('to_move')) {
            $catsIds = collect(request()->only('to_move'))->flatten();
            if ($catsIds && $catsIds->isNotEmpty()) {
                $categories = Category::all(['id','title'])->pluck('title', 'id');
                $categoriesFrom = $categories->filter(function ($title, $id) use ($catsIds) {
                    return $catsIds->search($id) !== false;
                });
                $categories = $categories->reject(function ($title, $id) use ($catsIds) {
                    return $catsIds->search($id) !== false;
                });
                return view('admin.move_lots', ['crud' => $this->crud,
                    'cats_to' => $categories, 'cats_from' => $categoriesFrom]);
            }
        }
        return redirect()->back();
    }

    public function moveLots(MoveLotsRequest $request)
    {
        $to = Category::find($request->to_cat);
        $from = $request->from_cat;
        $aliasesFromTo = CategoryAlias::whereIn('category_id', array_merge($from, [$to->id]))
            ->get(['alias'])
            ->map(function ($a) {
                return ['alias'=>$a->alias];
            });
        $aliases = Category::whereIn('id', $from)
            ->where('id','<>' ,$to->id)
            ->get(['title'])
            ->map(function ($a) {
                return ['alias'=>$a->title];
            })
            ->merge($aliasesFromTo)
            ->unique('alias')
            ->toArray();
        $lotsMoved = Lot::whereIn('category_id', $from)->update(['category_id' => $to->id]);
        $aliasesRemoved = CategoryAlias::whereIn('category_id', array_merge($from, [$to->id]))->delete();
        $catsRemoved = Category::whereIn('id', $from)->delete();
        $aliasesAdded = $to->aliases()->createMany($aliases);
        $message = $lotsMoved.' lots moved! '.$catsRemoved.' categories removed!';
        \Alert::info($message)->flash();
        return redirect(url($this->crud->route));
    }
}
