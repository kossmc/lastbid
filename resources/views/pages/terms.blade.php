@extends('layouts.main')

@include('inc.page_metas', ['defTitle' => 'TERMS AND CONDITIONS'])

@section('top-links')
    @include('inc.top_links')
@stop

@section('content')
    <div class="page-wrap page-terms">
        <div class="wrap">
            <div class="clearfix">
                <div class="content">
                    <div class="title-evets">
                        <h1>TERMS AND CONDITIONS</h1>
                    </div>
                    <div class="info">
                        @if(isset($terms))
                            {!! $terms !!}
                        @else
                            @include('inc.terms')
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection