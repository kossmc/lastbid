<?php

namespace App\Events\Message;

use App\Models\Message;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;

abstract class AbstractMessageEvent
{
    use Dispatchable, SerializesModels;

    public $message;

    /**
     * Create a new event instance.
     * @param Message $message
     * @return void
     */
    public function __construct(Message $message)
    {
        $this->message = $message;
    }
}
