<div @include('crud::inc.field_wrapper_attributes') >

    <div id="specificTable" style="display: none" v-show="specificsList.length">
        <input class="array-json" :value="output" type="hidden" id="{{ $field['name'] }}" name="{{ $field['name'] }}">
        <div class="panel panel-default">
            <div class="panel-heading"><h3>Specifics</h3>
                <p class="h4" style="color: red;" v-show="requiredNotSelected.length">
                    You have to set required specifics for this lot!</p>
            </div>
            <div class="panel-body">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>Specific Name</th>
                        <th>Specific Type</th>
                        <th>Required</th>
                        <th>Specific Options</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr v-for="(specific, index) in specificsList" :key="specific.id">
                        <td>@{{specific.name}}</td>
                        <td>@{{specific.type}}</td>
                        <td>@{{specific.required>0 ? 'Yes':'No'}}</td>
                        <td>
                            <dynamic-option
                                    :specific="specific"
                                    :index="index"
                                    :key="specific.id"
                            @option-changed="optionChanged(specific,index)"
                            @user-input-deleted=""
                            >

                            </dynamic-option>
                        </td>
                        <td>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <div class="form-group">
                    {{--<span @click="saveOptions()" class="btn btn-success" v-show="editedSpecifics.length">Save options</span>--}}
                    {{--<button class="btn btn-danger" data-dismiss="modal">Cancel</button>--}}
                </div>
            </div>
        </div>

    </div>
</div>
@if ($crud->checkIfFieldIsFirstOfItsType($field, $fields))
    {{--FIELD EXTRA CSS  --}}
    {{--push things in the after_styles section --}}

    {{--@push('crud_fields_styles')--}}
    {{--@endpush--}}


    {{--FIELD EXTRA JS --}}
    {{--push things in the after_scripts section --}}

    @push('crud_fields_scripts')
    <script type="text/javascript"
            src="{{asset('js/vue.js')}}"></script>
    <script type="text/x-template" id="dynamicOption">
        <div>
            <div v-if="specific.is_text">
                <input type="text" :id="specific.id" v-model="specific.selected">
            </div>

            <div v-if="specific.is_select && specific.specific_options">
                <div class="radio" v-for="opt in specific.specific_options">
                    <label>
                        <input type="radio"
                               v-model="specific.selected"
                               :value="opt.id"
                               :id="specific.id">
                        @{{opt.name}}
                    </label>
                </div>
            </div>

            <div v-if="specific.is_multi_select">
                <div class="checkbox" v-for="opt in specific.specific_options">
                    <label>
                        <input type="checkbox"
                               v-model="specific.selected"
                               :value="opt.id"
                               :id="opt.id">
                        @{{opt.name}}
                    </label>
                </div>
            </div>
        </div>
    </script>
    <script>
        var dynamicOption = Vue.component('dynamic-option', {
            template: '#dynamicOption',
            props: ['index', 'specific'],
            watch: {
                'specific.selected': function () {
                    this.$emit('option-changed', this.specific, this.index);
                    this.specific.edited = true;
                    if (this.specific.required != 0) {
                        if ((this.specific.is_select || this.specific.is_text) && this.specific.selected) {
                            this.specific.requiredNotSelected = false;
                        }
                        if ((this.specific.is_select || this.specific.is_text) && !this.specific.selected) {
                            this.specific.requiredNotSelected = true;
                        }
                        if (this.specific.is_multi_select && this.specific.selected.length) {
                            this.specific.requiredNotSelected = false;
                        }
                        if (this.specific.is_multi_select && !this.specific.selected.length) {
                            this.specific.requiredNotSelected = true;
                        }
                    }
                }
            },
            methods: {},
            data: function () {
                return {};
            }
        });
        var vm = new Vue({
            el: '#specificTable',
            data: {
                data:{!! $field['value'] !!},
                formErrorsUpdate: {}
            },
            computed: {
                specificsList: function () {
                    return this.data.cat_opts;
                },
                editedSpecifics: function () {
                    return this.data.cat_opts.filter(function (el) {
                        return el['edited'];
                    });
                },
                output: function () {
                    return JSON.stringify(this.editedSpecifics.filter(function (el) {
                        if (el['required'] != 0) {
                            return ((el['is_select'] || el['is_text']) && el['selected']) ||
                                    (el['is_multi_select'] && el['selected'].length);
                        }
                        return true;
                    }));
                },
                requiredNotSelected: function () {
                    return this.data.cat_opts.filter(function (el) {
                        return el['requiredNotSelected'];
                    });
                }
            },
            methods: {
                optionChanged: function (specific, index) {
//                        console.log(specific,index);
                },
                saveOptions: function () {
//                        console.log(this.editedSpecifics);
                }
            }
        });
    </script>
    @endpush
@endif
