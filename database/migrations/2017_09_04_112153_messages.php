<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Messages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('messages', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('lot_id');
            $table->text('text')->nullable();
            $table->timestamps();
        });
        Schema::create('message_user', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('message_id');
            $table->enum('type', ['sent', 'received'])->default('sent');
            $table->enum('state', ['read', 'unread'])->default('unread');
            $table->enum('status', ['active', 'archived', 'deleted'])->default('active');
            $table->index(['user_id', 'status', 'type'], 'user_status_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('messages');
        Schema::dropIfExists('message_user');
    }
}
