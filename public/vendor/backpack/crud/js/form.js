/*
*
* Backpack Crud / Form
*
*/

jQuery(function($){

    'use strict';
    /*show hide fields on specifics admin pages*/
    var specificType = document.getElementById('type-selector');
    if (specificType) {
        toggleField('#options-table', specificType.value !== 'text');
        specificType.addEventListener('change', function () {
            toggleField('#options-table', this.value !== 'text');
        });
    }
    var useInFilter = document.getElementById('use-in-filter');
    if (useInFilter) {
        toggleField('#filter-type', useInFilter.checked);
        useInFilter.addEventListener('change', function () {
            toggleField('#filter-type', this.checked);
        });
    }

    function toggleField(selector, show) {
        var $el = $(selector);
        if (show) {
            $el.show();
        } else {
            $el.hide();
        }
    }
    /*set specific id not editable*/
    var options_table = document.getElementById('ex-options-table');
    if (options_table) {
        $('#ex-options-table [ng-model="item.id"]').prop('disabled', true);
    }
});
