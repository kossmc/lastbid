<div class="block-card-lot block-card-lot--winner">
    <span class="icon-winner_icon"></span>
    <p class="title">You are the winner</p>
    <p class="bid-info"><span class="icon-done"></span>
        @if ($bid->by_seller)
            <span>{{ $lot->seller->short_name }}</span> accepted your last bid {{ $bid->attempts_count }}/{{ env('BIDS_LIMIT') }}
        @else
            You accepted counter bid from <span>{{ $lot->seller->short_name }}</span>
        @endif
    </p>
    <div class="info">

        <div class="bid-price">
            {!! format_price($bid->attempt->amount, null, $lot->currency) !!}
            <p>Does not include auction house premium and shipping/delivery fees</p>
        </div>

        <div class="info-bid">       
            <p class="title">Total bid price consists of:</p>
            <div>
                <p>Bid price:</p>
                <div class="bid-price"> {!! format_price($bid->attempt->amount, null, $lot->currency) !!} </div>
            </div>
            <div>   
                <p>Auction house premium:</p>
                <div class="bid-price">{!! format_price($lot->bidFee($bid->attempt->amount), null, $lot->currency) !!}</div>
            </div>
            <div>   
                <p>Total bid price:</p>
                <div class="bid-price">{!! format_price($lot->bidWithFee($bid->attempt->amount), null, $lot->currency) !!}</div>
            </div>
        </div>

    </div>

    <div class="btn-holder">
        <a class="btn-lot" href="{{ route('faq') }}">how to pay</a>
    </div>
    <div class="card-bottom-part">
        <p><span>Start:</span>{{ $lot->start_date->format('j F Y, h:i A') }} EST</p>
        <p><span>End:</span>{{ $lot->end_date->format('j F Y, h:i A') }} EST</p>
    </div>
</div>
