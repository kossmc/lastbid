@extends('backpack::layout')

@section('header')
    <section class="content-header">
        <h1>
            {{ trans('backpack::base.dashboard') }}<small>{{ trans('backpack::base.first_page_you_see') }}</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url(config('backpack.base.route_prefix', 'admin')) }}">{{ config('backpack.base.project_name') }}</a></li>
            <li class="active">{{ trans('backpack::base.dashboard') }}</li>
        </ol>
    </section>
@endsection


@section('content')
    <div class="row">
        <div class="tab-container col-md-12">

            <div class="nav-tabs-custom" id="form_tabs">
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active">
                        <a href="#tab_active_bids" aria-controls="tab_active_bids" role="tab" data-toggle="tab">Active bids</a>
                    </li>
                    <li role="presentation" class="">
                        <a href="#tab_messages" aria-controls="tab_messages" role="tab" data-toggle="tab">Messages</a>
                    </li>
                </ul>
            </div>

        </div>
        <div class="tab-content col-md-12">
            <div role="tabpanel" class="tab-pane active" id="tab_active_bids">
                Date from:  <input name="min" id="min" type="text"> Date to: <input name="max" id="max" type="text"><br /><br />
                Total amount of active bids: USD: <b id="usd"></b>, EUR: <b id="eur"></b>, GBP: <b id="gbp"></b>
                <br />
                <br />
                <table id="activeBids" class="table table-bordered table-striped display" style="width:100%">
                    <thead>
                    <tr>
                        <th>Lot title</th>
                        <th>Lot number</th>
                        <th>From</th>
                        <th>To</th>
                        <th>Partner</th>
                        <th>Estimate</th>
                        <th>Date of bid</th>
                        <th>Bid value</th>
                        <th>Time remaining</th>
                    </tr>
                    </thead>
                    <tfoot>
                    <tr>
                        <th>Lot title</th>
                        <th>Lot number</th>
                        <th>From</th>
                        <th>To</th>
                        <th>Partner</th>
                        <th>Estimate</th>
                        <th>Date of bid</th>
                        <th>Bid value</th>
                        <th>Time remaining</th>
                    </tr>
                    </tfoot>
                </table>
            </div>
            <div role="tabpanel" class="tab-pane" id="tab_messages">
                <div class="form-group col-md-12">
                    <p>Coming soon...</p>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="box box-default">
                <div class="box-header with-border">
                    <div class="box-title">{{ trans('backpack::base.login_status') }}</div>
                </div>

                <div class="box-body">{{ trans('backpack::base.logged_in') }}</div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="box box-default">
                <div class="box-header with-border">
                    <div class="box-title">Cloud flare purge</div>
                </div>

                <div class="box-body">

                    Example: https://www.lastbid.com/css/style.css, https://www.lastbid.com/css/landing.css

                    <form class="form-horizontal" role="form" method="POST" action="{{ route('admin.purge') }}">

                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <div class="col-md-10">
                                <input id="urls" type="text" class="form-control" name="urls" value="{{ old('urls') }}" required>

                                @if ($errors->has('urls'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('urls') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-2">
                                <button type="submit" class="btn btn-primary">
                                    Purge
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('after_styles')
    <!-- DATA TABLES -->
    <link href="{{ asset('vendor/adminlte/plugins/datatables/dataTables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{ asset('vendor/backpack/crud/css/crud.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/backpack/crud/css/form.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/backpack/crud/css/list.css') }}">

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/datepicker/0.6.5/datepicker.css">

    <!-- CRUD LIST CONTENT - crud_list_styles stack -->
    @stack('crud_list_styles')
@endsection

@section('after_scripts')
    <!-- DATA TABLES SCRIPT -->
    <script src="{{ asset('vendor/adminlte/plugins/datatables/jquery.dataTables.js') }}" type="text/javascript"></script>

    <script src="{{ asset('vendor/backpack/crud/js/crud.js') }}"></script>
    <script src="{{ asset('vendor/backpack/crud/js/form.js') }}"></script>
    <script src="{{ asset('vendor/backpack/crud/js/list.js') }}"></script>
    <script src="{{ asset('vendor/adminlte/plugins/datatables/dataTables.bootstrap.js') }}" type="text/javascript"></script>

    <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
    <script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
    <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>
    <script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/datepicker/0.6.5/datepicker.js"></script>

    <script type="text/javascript">
        jQuery(document).ready(function($) {

            $.fn.dataTable.ext.search.push(
                function (settings, data, dataIndex) {
                    var min = $("#min").val();
                    var max = $("#max").val();
                    if(!min.length && !max.length) { return true; }
                    var startDate = new Date(data[6]);
                    if (!min.length && startDate <= new Date(max)) { return true;}
                    if(!max.length && startDate >= new Date(min)) {return true;}
                    if (startDate <= new Date(max) && startDate >= new Date(min)) { return true; }
                    return false;
                }
            );


            $("#min").datepicker({ onSelect: function () { table.draw(); }, changeMonth: true, changeYear: true });
            $("#max").datepicker({ onSelect: function () { table.draw(); }, changeMonth: true, changeYear: true });

            var table = $('#activeBids').DataTable( {
                "dom": "Bfrtip",
                "buttons": [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                "ajax": "dashboard/active-bids",
                "columns": [
                    { "data": "title" },
                    { "data": "id" },
                    { "data": "user_name" },
                    { "data": "seller_name" },
                    { "data": "partner_title" },
                    { "data": "estimate" },
                    { "data": "bid_date" },
                    { "data": "amount" },
                    { "data": "time_left" },
                ]

            } );

            table.on( 'xhr', function () {
                var json = table.ajax.json();
                $('#usd').text(json.total.usd);
                $('#eur').text(json.total.eur);
                $('#gbp').text(json.total.gbp);
            } );

            // Event listener to the two range filtering inputs to redraw on input
            $('#min, #max').change(function () {
                table.draw();
            });
        });
    </script>

    <!-- CRUD LIST CONTENT - crud_list_scripts stack -->
    @stack('crud_list_scripts')
@endsection