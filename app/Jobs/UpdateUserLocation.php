<?php

namespace App\Jobs;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class UpdateUserLocation implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    private $user_id;
    private $ip;

    /**
     * Create a new job instance.
     *
     * @param $user_id
     * @param $ip
     */
    public function __construct($user_id, $ip)
    {
        $this->user_id = $user_id;
        $this->ip = $ip;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $timezone = 'America/New_York';

        try{
            $result =  file_get_contents('http://ip-api.com/json/'.$this->ip);
            $result = json_decode($result, true);
            if(isset($result['status']) && isset($result['timezone']) && $result['status'] == 'success'){
                $timezone  = $result['timezone'];
            }
        } catch(\Exception $e){};

        $user = User::find($this->user_id);
        if(!$user->hasAnyRole(['admin'])){
            $user->timezone = $timezone;
            $user->save();
        }
    }
}
