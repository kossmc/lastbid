# -*- mode: ruby -*-
# vi: set ft=ruby :

server_hostname       = "lastbid.local" # Name of VM
server_ip             = "10.10.10.11" # IP address of VM
server_cpus           = "2"   # Cores
server_memory         = "2048" # MB
server_swap           = "2048" # Options: false | int (MB) - Guideline: Between one or two times the server_memory
server_timezone       = "UTC"

mysql_forward_port    = 3312  # Port for forwarding (ex. you can use PgAdmin 3/4 with "localhost:54320")
mysql_root_password   = "root"   # We'll assume user "root"
mysql_main_database   = "lastbid_db" # Name of main database

nodejs_version        = "8.x"      # By default "latest" will equal the latest stable version
nodejs_packages       = [          # List any global NodeJS packages that you want to install
  "gulp",
  "pm2"
]

Vagrant.configure("2") do |config|
    config.vm.provision "file", source: "~/.ssh/id_rsa", destination: "~/.ssh/id_rsa"
    config.vm.provision "file", source: "~/.ssh/id_rsa.pub", destination: "~/.ssh/id_rsa.pub"

    config.ssh.shell = "bash -c 'BASH_ENV=/etc/profile exec bash'"
    config.vm.box = "ubuntu/xenial64"
    config.vm.hostname = server_hostname
    config.vm.post_up_message = "The VM is now running! Type vagrant ssh to manage this box."
    config.vm.network "private_network", ip: server_ip
    config.vm.network :forwarded_port, guest: 3306, host: mysql_forward_port

    config.vm.synced_folder ".", "/lastbid",
        :owner => "ubuntu",
        :group => "ubuntu",
        :create => true,
        :mount_options => ["dmode=777","fmode=777"]

    config.vm.provider "virtualbox" do |vb|
        vb.name = server_hostname
        vb.customize ["modifyvm", :id, "--cpus", server_cpus]
        vb.customize ["modifyvm", :id, "--memory", server_memory]
    end

    # Provision Base Packages
    config.vm.provision "shell", path: "./support/provision/base.sh", args: [server_swap, server_timezone]
    # Provision Mysql
    config.vm.provision "shell", path: "./support/provision/mysql.sh"
    # Install Nodejs
    config.vm.provision "shell", path: "./support/provision/nodejs.sh", args: nodejs_packages.unshift(nodejs_version)
    # Provision Nginx
    config.vm.provision "shell", path: "./support/provision/nginx.sh"
    # Provision PHP-FPM
    config.vm.provision "shell", path: "./support/provision/php-fpm.sh"
    # Autostart
    config.vm.provision :shell, :inline => 'echo 3 > /proc/sys/vm/drop_caches', :run => 'always'
    # phpmyadmin install
    config.vm.provision "shell", path: "./support/provision/phpmyadmin.sh"
    # purge apache2
    config.vm.provision "shell", path: "./support/provision/purge-apache.sh"
    # install supervisor
    config.vm.provision "shell", path: "./support/provision/supervisor.sh"
    # Deploy project
    config.vm.provision "shell", path: "./support/provision/deploy.sh", privileged: false
end
