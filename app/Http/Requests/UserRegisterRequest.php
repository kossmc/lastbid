<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required|string|max:192',
            'last_name' => 'required|string|max:192',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|string|max:192|min:6|confirmed',
            'card_number' => 'required',
            'cvv' => 'required',
            'exp_date' => 'required'
        ];
    }
}
