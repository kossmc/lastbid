<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class TermsConditionsJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 1;

    /**
     * The number of seconds the job can run before timing out.
     *
     * @var int
     */
    public $timeout = 2400;

    /**
     * Parser class
     *
     * @var string
     */
    protected $crawler;

    /**
     * Type of parse page
     * @var
     */
    public $alias;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($crawler, $alias = '')
    {
        $this->crawler = $crawler;
        $this->alias = $alias;
    }

    /**
     * Execute job
     * @return bool
     */
    public function handle()
    {
        $this->crawler = new $this->crawler();
        $this->crawler->startCrawling();

    }

    /**
     * @param \Exception $exception
     */
    public function failed(\Exception $exception)
    {
        //
    }
}
