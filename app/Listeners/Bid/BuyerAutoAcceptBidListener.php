<?php

namespace App\Listeners\Bid;

use App\Events\Bid\BuyerAcceptCounterBid;
use App\Events\Bid\BuyerAutoAcceptBid;
use App\Models\Bid;

class BuyerAutoAcceptBidListener extends SendGridAbstractBidListener
{
    /**
     * Buyer Email subject.
     */
    private $buyerSubject = 'You have accepted a bid offer';

    /**
     * Buyer Email template.
     */
    private $buyerTemplate = 'c35eca71-6f55-49f8-b92c-690429c9e54e';

    /**
     * Seller Email subject.
     */
    private $sellerSubject = 'Congratulations! Your bid was accepted by buyer';

    /**
     * Seller Email template.
     */
    private $sellerTemplate = '74512c18-2bd3-4fd8-9641-4554aa337bb9';

    /**
     * Handle the event.
     *
     * @param  BuyerAcceptCounterBid  $event
     * @return void
     */
    public function handle(BuyerAutoAcceptBid $event)
    {
        $this->setData($event);
        // set attachment for buyer
        /*notification to seller*/
        $this->sendMail($this->seller, $this->getData($this->sellerTemplate), $this->sellerSubject);
        /*notification to buyer*/
        $this->sendMail($this->buyer, $this->getData($this->buyerTemplate), $this->buyerSubject);
    }
}
