<a href="{{ url($crud->route.'/import') }}" class="btn btn-primary ladda-button" data-style="zoom-in">
    <span class="ladda-label">Import CSV</span>
</a>
