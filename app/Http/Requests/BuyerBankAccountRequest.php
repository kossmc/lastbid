<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BuyerBankAccountRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'holder_name' => 'required|string|max:190',
            'account_number' => 'required|string|max:190',
            'routing_number' => 'required|string|max:190',
            'bank_name' => 'required|string|max:190',
            'address' => 'required|string|max:190',
            'city' => 'required|string|max:190',
            'country' => 'required|string|max:190'
        ];
    }
}
