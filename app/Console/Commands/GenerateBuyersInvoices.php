<?php

namespace App\Console\Commands;

use App\Models\Lot;
use Illuminate\Console\Command;

class GenerateBuyersInvoices extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'buyer_invoices:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generates buyers invoices for sold lots';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Start generate invoices');
        $lots = Lot::where('lots.status', Lot::STATUS_SOLD)->get();
        $bar = $this->output->createProgressBar(count($lots));
        $lots->each(function ($lot) use ($bar) {
           $lot->generateBuyerInvoice();
            $bar->advance();
        });
        $bar->finish();
        $this->info('All invoices generated');
        return 1;
    }
}
