
$(document).ready(function () {

    $("#privacy").on('click touchstart', function(){
        modal_Terms_Policy('/ajax_get_policy', 'wrapper-private-policy')
    });

    function modal_Terms_Policy(url, element) {
        $.ajax({
            'type':'get',
            'url': "https://lastbid.com" +url,
            'data': {},
            'beforeSend': function(request) {
                request.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content'));
            },
            'success': function (data) {
                if (data.form) {
                    showForm(data.form);
                    $("." + element).scrollbar({
                        'disableBodyScroll': false,
                        'ignoreMobile': false
                    });
                    $("." + element).addClass('scrollbar-outer');
                }
            }
        });
    }

    function showForm(html) {

        $(modalContainer).html(html);
        $(modalWindow).modal('show');
        if (document.querySelector('#popup-comment')) {
            $('#popup-comment').val($('#buy-form #message').val());
        }
        /*input masc init*/
        if (document.querySelector('#exp_date')) {
            $('#exp_date').inputmask({mask: "99/99", placeholder: "mm/yy"});
        }
        if (document.querySelector('#modal-id')) {
            $('.textarea-scrollbar').scrollbar({
                'disableBodyScroll': false,
                'ignoreMobile': true
            });
            $('.scroll-textarea').addClass('scrollbar-outer');
        }
        if(document.querySelector('.second-step-register')){
            var card = new CardValidation('.second-step-register', true);


        }
        if(document.querySelector('#register-form')){

            $('a[disabled]').on('click', function(e){
                $(this).attr('href', null);
            });


            var wrapper = $('.wrap-check-policy');
            // $('.btn-sign-up-with').on('click', function(){
            //     $('.sign-with-account').show();
            //     $('.sign-without-account').hide();
            //     $('.row.wrapper-sign-up-with').hide();
            // });

            $('.wrapper-sign-up-agreement').scrollbar({
                onScroll: function(y, x){

                    if(Math.abs(Math.floor(y.scroll) - y.maxScroll) <= 1){
                        wrapper.find('input[type="checkbox"]').removeAttr('disabled');
                        wrapper.find('label').removeClass('disabled-label');
                    }
                }
            });
            $('.wrapper-sign-up-agreement').addClass('scrollbar-outer');

            wrapper.find('input[type="checkbox"]').change(function() {
                if(this.checked) {
                    wrapper.find('.btn').removeClass('btn-sign-up-disabled').removeAttr('disabled');
                }
                else{
                    wrapper.find('.btn').addClass('btn-sign-up-disabled').attr('disabled', 'disabled');
                }
            });

            // wrapper.find('.btn-sign-up-next-step').on('click', function(){
            //     var nav = $('.nav.sign-up-tabs');
            //     var tabContent = $('.tab-content');
            //     nav.find('li:nth-child(1)').removeClass('active');
            //     nav.find('li:nth-child(2)').addClass('active');
            //     nav.find('[href="#auth-tab2"]').attr('data-toggle', 'tab')
            //     $('#auth-tab1').removeClass('active in');
            //     $('#auth-tab2').addClass('active in');
            //
            //     var card = new CardValidation('#register-form', true);
            // });

            var select = $('.select-style');
            var select_ul = select.find('ul');

            select.find('span').on('click', function(){

                $(this).toggleClass('up-down-arrow');
                var ul = $(this).next('ul');

                if(ul.is(':hidden')){
                    ul.show();
                }
                else{
                    ul.hide();
                }
            });


            select_ul.find('li').on('click', function(){

                var select = $(this).closest('.select-style');
                var selct_opt = select.find('option');
                var select_ul = select.find('ul');

                select.find('span').toggleClass('up-down-arrow');

                select.find('span').addClass('selected-suffix');

                select.find('span').text($(this).text());
                selct_opt.attr('value', $(this).text());
                selct_opt.text($(this).text());
                select_ul.hide();
            });


        }

        polyfillSetPlaceholders();

    }


    function polyfillSetPlaceholders() {

        if ($html.data('useragent').search(/MSIE 9.0/) > 0) {

            console.log('detect ie9');

            $("input, textarea").each(function () {

                if ($(this).val() == "" && $(this).attr("placeholder") != "") {

                    $(this).val($(this).attr("placeholder"));

                    $(this).focus(function () {
                        if ($(this).val() == $(this).attr("placeholder")) $(this).val("");
                    });
                    $(this).blur(function () {
                        if ($(this).val() == "")
                            $(this).val($(this).attr("placeholder"));
                    });

                }
                $(this).blur(function () {
                    if ($(this).val() == "")
                        $(this).val($(this).attr("placeholder"));
                });


            });

        }
    }

    function closeModal() {
        $(modalWindow).modal('hide').empty();
        $(modalContainer).empty();
        $(modalContainer_two).empty();

        $('.modal-backdrop.fade.in').remove();
        $('body').removeClass('modal-open');
    }
});