<?php

namespace App\Events\Form;

use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;

class CountactUsForm
{
    use Dispatchable, SerializesModels;

    public $name;

    public $email;

    public $message;

    /**
     * Create a new event instance.
     * @param Array $data
     * @return void
     */
    public function __construct($data)
    {
        $this->name = $data['name'];
        $this->email = $data['email'];
        $this->message = $data['message'];
    }
}
