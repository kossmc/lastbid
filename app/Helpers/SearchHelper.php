<?php

if (!function_exists('searchResults')) {

    function searchResults($count, $query)
    {
        $phrase = '';
        $singularPhrase = '%s Search Result for: <span>%s</span>';
        $pluralPhrase = '%s Search Results for: <span>%s</span>';
        if ($count == 0) {
            $phrase = $pluralPhrase;
            $count = 'No';
        }
        elseif ($count == 1) {
            $phrase = $singularPhrase;
            $count = number_format($count, 0, '', ',');
        }
        else {
            $phrase = $pluralPhrase;
            $count = number_format($count, 0, '', ',');
        }
        return sprintf($phrase, $count, e($query));
    }
}