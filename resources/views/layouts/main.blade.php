<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-127225306-1"></script>

    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', 'UA-127225306-1');
    </script>
    <meta charset="utf-8">

    <title>{{ config('app.name', 'Laravel') }}: @yield('title')</title>
    <meta name="description" content="@yield('meta_description', 'UNSOLD AT AUCTION - FIND HIDDEN TREASURES FOR SALE VIA LASTBID')">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="INDEX,FOLLOW" />
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="canonical" href="{{ !empty($canonical) ? $canonical : request()->url()}}" />

    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">
    <!-- Styles -->
    <link href="{{ asset('css/jquery.formstyler.css') }}" rel="stylesheet">
    <link href="{{ asset('css/glyphter.css') }}" rel="stylesheet">
    <link href="{{ asset('css/colorbox.css') }}" rel="stylesheet">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css?ver=1.1') }}">
    <link href="{{ asset('css/jquery.scrollbar.css?ver=1.1') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css?ver='.time()) }}" rel="stylesheet">
    <link href="{{ asset('css/modifications.css?ver=1.1') }}" rel="stylesheet">
    @if (isActiveRoute('events'))
    <link href="{{ asset('css/zabuto_calendar.css?ver='.time()) }}" rel="stylesheet">
    @endif
    @yield('user_css')
    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>

    <script>
        (function(h,o,t,j,a,r){
            h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
            h._hjSettings={hjid:1079713,hjsv:6};
            a=o.getElementsByTagName('head')[0];
            r=o.createElement('script');r.async=1;
            r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
            a.appendChild(r);
        })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
    </script>
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.css"/>
	<style>
	    .cc-theme-block {
	        height: 120px;
	    }

	    .cc-banner {
	        border-top: 1px solid #E4E4E4;
	    }

	    .cc-compliance .cc-btn {
	        display: inline-block;
	        height: 44px;
	        padding: 0 40px;
	        border-radius: 22px;
	        line-height: 44px;
	        font-weight: bold;
	        border: 0;
	        font-size: 12px;
	        color: #fff;
	        text-transform: uppercase;
	        background-color: #CAAC8A;
	    }

	    @media  screen and (max-width: 480px) {
	        .cc-compliance .cc-btn {
	            display: block;
	        }
	    }
	</style>
    @if(env('APP_ENV') == 'production')
        <script type="text/javascript">
          window.heap=window.heap||[],heap.load=function(e,t){window.heap.appid=e,window.heap.config=t=t||{};var r=t.forceSSL||"https:"===document.location.protocol,a=document.createElement("script");a.type="text/javascript",a.async=!0,a.src=(r?"https:":"http:")+"//cdn.heapanalytics.com/js/heap-"+e+".js";var n=document.getElementsByTagName("script")[0];n.parentNode.insertBefore(a,n);for(var o=function(e){return function(){heap.push([e].concat(Array.prototype.slice.call(arguments,0)))}},p=["addEventProperties","addUserProperties","clearEventProperties","identify","resetIdentity","removeEventProperty","setEventProperties","track","unsetEventProperty"],c=0;c<p.length;c++)heap[p[c]]=o(p[c])};
          heap.load("3488702622");
        </script>
    @endif
</head>
<body>
    @yield('side_menu')
    <div class="footer-fix">
        <div class="content-wrap">
            <div class="wrap">
                <header>
                    <div class="top-link">
                        @yield('top-links')
                    </div>
                    <div class="logo">
                        <a href="/">
                            {!! !empty($h1_tag) ? '<h1>'.$h1_tag.'</h1>' : '' !!}
                            <img src="{{asset('img/logo.svg')}}" alt="LastBid">
                        </a>
                    </div>
                    <div class="bottom-logo-text">
                        <p class="text-center">Unsold at auction - find hidden treasures for sale via LastBid</p>
                    </div>
                </header>
                <div class="head-mobile top-primary-menu">
                    <button class="hamburger">&#9776;</button>
                    <button class="cross">&#735;</button>
                    <div class="search"><a href="#" class="icon-search_icon"></a></div>
                </div>
                <nav class="menu-mobile" style="display: none;">
                    <ul>
                    <li{!! areActiveRoutes(['catalog', 'category', 'lot']) ? ' class="active"' : '' !!}><a href="{{url('catalog')}}">Categories</a></li>
                    <li{!! isActiveRoute('how-it-works') ? ' class="active"' : '' !!}><a href="{{url('how-it-works')}}">How it works</a></li>
                    <li{!! areActiveRoutes(['events', 'event']) ? ' class="active"' : '' !!}><a href="{{url('events')}}">Auction Calendar</a></li>
                    <li{!! areActiveRoutes(['news_posts', 'news_post']) ? ' class="active"' : '' !!}><a href="{{url('news')}}">News</a></li>
                    <li{!! isActiveRoute('about') ? ' class="active"' : '' !!}><a href="{{url('about')}}">About Us</a></li>
                    <li{!! isActiveRoute('contact') ? ' class="active"' : '' !!}><a href="{{url('contact')}}">Contact Us</a></li>
                    </ul>
                </nav>
                <nav class="top-primary-menu">
                    <ul>
                        <li{!! areActiveRoutes(['catalog', 'category', 'lot']) ? ' class="active"' : '' !!}><a href="{{url('catalog')}}">Categories</a></li>
                        <li{!! isActiveRoute('how-it-works') ? ' class="active"' : '' !!}><a href="{{url('how-it-works')}}">How it works</a></li>
                        <li{!! areActiveRoutes(['events', 'event']) ? ' class="active"' : '' !!}><a href="{{url('events')}}">Auction Calendar</a></li>
                        <li{!! areActiveRoutes(['news_posts', 'news_post']) ? ' class="active"' : '' !!}><a href="{{url('news')}}">News</a></li>
                        <li{!! isActiveRoute('about') ? ' class="active"' : '' !!}><a href="{{url('about')}}">About Us</a></li>
                        <li{!! isActiveRoute('contact') ? ' class="active"' : '' !!}><a href="{{url('contact')}}">Contact Us</a></li>
                    </ul>
                    <div class="search"><a href="#" class="icon-search_icon"></a></div>
                </nav>
                <nav class="search-active non-visible">
                    <div class="search-head">
                        {!! Form::open(['method' => 'GET', 'url' => 'search', 'role' => 'search'])  !!}
                            <input placeholder="Enter lot name or number" type="text" name="query" id="search-query">
                            <button type="submit"><em class="icon-search_icon"></em></button>
                        {!! Form::close() !!}
                    </div>
                    <ul>
                        <li{!! areActiveRoutes(['catalog', 'category', 'lot']) ? ' class="active"' : '' !!}><a href="{{url('catalog')}}">Categories</a></li>
                        <li{!! isActiveRoute('about') ? ' class="active"' : '' !!}><a href="{{url('about')}}">About Us</a></li>
                        <li{!! areActiveRoutes(['news_posts', 'news_post']) ? ' class="active"' : '' !!}><a href="{{url('news')}}">News</a></li>
                        <li{!! areActiveRoutes(['events', 'event']) ? ' class="active"' : '' !!}><a href="{{url('events')}}">Events</a></li>
                        <li{!! isActiveRoute('how-it-works') ? ' class="active"' : '' !!}><a href="{{url('how-it-works')}}">How it works</a></li>
                        <li{!! isActiveRoute('contact') ? ' class="active"' : '' !!}><a href="{{url('contact')}}">Contact Us</a></li>
                    </ul>
                    <div class="search"><a href="#" class="icon-search_icon"></a></div>
                </nav>
            </div>
            @yield('content')
            @yield('about')
            <div class="footer clearfix">
                @include('notifications/lets_talk')
                <footer>
                    <div class="wrap">
                        <div class="col">
                            <ul>
                                <li><a href="{{url('catalog')}}">Categories</a></li>
                                <li><a href="{{url('how-it-works')}}">How it works</a></li>
                                <li><a href="{{url('partners')}}">Auction Partners</a></li>
                                <li><a href="{{url('events')}}"> Auction calendar</a></li>
                            </ul>
                            <ul>
                                <li><a href="{{url('news')}}">News</a></li>
                                <li><a href="{{url('about')}}">About Us</a></li>
                                <li><a href="{{url('contact')}}">Contact Us</a></li>
                                <li><a href="{{url('faq')}}">FAQ</a></li>
                            </ul>
                        </div>
                        <div class="col col-auth">
                            @if (Auth::guest())
                                @include('inc.auth_links')
                            @endif
                        </div>
                        <div class="col last">
                            <h3>SIGN UP to lastbid's newsletter</h3>
                            <div class="subscribe">
                                <form action="subscribe" id="subscribeToNL">
                                    <input type="text" name="email" required placeholder="name@example.com" >
                                    <span id="subscribeEmailFalse" class="help-block" style="display: none"><strong>*Please provide valid email and submit form again</strong></span>
                                    <input type="hidden" name="type" value="NL">
                                    <button type="submit"><em class="icon-send_icon"></em></button>
                                </form>
                                <div class="info">By subscribing, you agree to our <br>
                                <a href="javascript: void 0;" onclick="modal_Terms_Policy('/ajax_get_terms', 'wrapper-terms-conditions')">"Terms and Conditions"</a>
                                <br>
                                <a href="javascript: void 0;" class="privacy" onclick="modal_Terms_Policy('/ajax_get_policy', 'wrapper-private-policy')">"Privacy Policy" </a>
                                </div>
                            </div>
                        </div>
                        <div class="clear"></div>

                      	<div class="social">
                            <a target="_blank" href="https://www.facebook.com/LastBidAuctions" class="icon-facebook"></a>
                            <a target="_blank" href="https://www.instagram.com/lastbid_auctions" class="icon-instagram"></a>
                            <a target="_blank" href="https://www.linkedin.com/company/lastbid" class="icon-linkedin icon-linkedin-offset"></a>
                        </div>
                        <div class="copy">
                            <a href="javascript: void 0;" onclick="modal_Terms_Policy('/ajax_get_policy', 'wrapper-private-policy')">Privacy Policy </a>  &copy; <a href="#"> Copyright LastBid {{date('Y')}}</a>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
    </div>
    {{--Modal windows--}}
    <div id="modal-win"></div>
    <div id="modal-win2"></div>

    <div class="modal fade out" id="modal-cookie">
        <div class="container-form-wrap container-login-form">
            <a href="#" class="esc" data-dismiss="modal" aria-hidden="true">
                <em class="icon-esc_icon"></em>
                esc
            </a>
            <div class="container-forms">
                <iframe id="modal-cookie-content"></iframe>
            </div>
        </div>
    </div>
    {{--@include('inc.cookie_modal')--}}

    @yield('custom_modal')
    @include('inc.accept_modal')
<!-- Scripts -->
    <script src="{{ asset('js/jquery-1.12.3.min.js') }}"></script>
    <script src="{{ asset('js/jquery-ui.js') }}"></script>
    <script src="{{ asset('js/jquery.bxslider.js') }}"></script>
    <script src="{{ asset('js/jquery.formstyler.js') }}"></script>
    <script src="{{ asset('js/jquery.colorbox.min.js') }}"></script>
    <script src="{{ asset('js/jquery.inputmask.bundle.min.js') }}"></script>
    <script src="{{ asset('js/jquery.scrollbar.min.js') }}"></script>
    <!-- Latest compiled and minified bootstrap JS -->
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/script.js?id='.time()) }}"></script>
    <script src="{{ asset('js/validation.js') }}"></script>
    <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-59d4c600ff3f8344"></script>
    <script src="{{ asset('js/slim.min.js') }}"></script>
    <script src="{{ asset('js/moment.min.js') }}"></script>
    <script src="{{ asset('js/moment-tz-with-data.min.js') }}"></script>

    @yield('scripts')

    <script>
		$(document).ready(function(){

            $('.sidebar-category h3').on('click', function(){
                var $span = $(this).find('span');
                var content = $(this).next();
                if(content.is(':hidden')){
                    content.slideDown();
                }
                else{
                    content.slideUp();
                }
                $span.toggleClass('cat-icon-collapsed cat-icon-expanded');

            });


		  $('.slider-head .slider').bxSlider({
              minSlides: 1,
              maxSlides: 1,
              moveSlides: 1,
              slideMargin: 0,
              responsive: true,
              auto: true,
              autoStart: true,
              autoHover: true,
              infiniteLoop: true,
              controls: true,
              pager: true,
              touchEnabled: window.matchMedia("(max-width: 767px)").matches ? true : false
		  });
            $('.overlay').click(function () {
                $(this).remove();
            });
            $(".cross").hide();
            $(".menu-mobile").hide();
            $(".hamburger").click(function () {
                $(".menu-mobile").slideToggle("slow", function () {
                    $(".hamburger").hide();
                    $(".cross").show();
                });
            });
            $(".cross").click(function () {
                $(".menu-mobile").slideToggle("slow", function () {
                    $(".cross").hide();
                    $(".hamburger").show();
                });
            });
            (function ($) {
                $(function () {
                    $('.select select').styler();
                });
            })(jQuery);
		});
    </script>

    <script>
        $(document).ready(function () {
            @yield('user_script')
        });
    </script>

    <script>
        (function(h,o,t,j,a,r){
            h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
            h._hjSettings={hjid:1079713,hjsv:6};
            a=o.getElementsByTagName('head')[0];
            r=o.createElement('script');r.async=1;
            r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
            a.appendChild(r);
        })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
    </script>


<script src="https://cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.js"></script>
<script>
    var cookiePopup;
    window.addEventListener("load", function () {
        window.cookieconsent.initialise({
            "palette": {
                "popup": {
                    "background": "#333333",
                    "text": "#ffffff"
                },
                "button": {
                    "background": "#caac8a",
                    "text": "#ffffff"
                }
            },
            "dismissOnScroll":true,
            "content": {
                "message": "This site uses cookies to improve your browsing experience, \
                    perform analytics, research and to conduct advertising. Closing this banner or clicking \
                    \"ACCEPT ALL COOKIES\" indicates your agreement to the use of cookies on this device. \
                    <a href='/policy' id='btn-to-policy-popup' class='cc-link'><u>Cookies & Privacy Notice</u></a>\
                    To change your preference see: > ",
                "dismiss": 'Accept all Cookies!',
                "link": "Cookies Settings",
                "href": "https://www.whatismybrowser.com/guides/how-to-enable-cookies/auto",
                "target": "_blank",
            }
        }, function(popup){
            cookiePopup = popup;
        });

        let close = '<div class="close-cookies-popup">' +
            '<a href="" class="esc" id="btn-close-close-cookies-popup" data-dismiss="modal" aria-hidden="true">\n' +
            '        <em class="icon-esc_icon"></em>\n' +
            '        esc\n' +
            '    </a></div>';
        document.querySelector('.cc-window').insertAdjacentHTML("afterbegin", close);

        document.querySelector('#btn-close-close-cookies-popup').addEventListener('click', function(e){
            e.preventDefault();
            cookiePopup.close();
        });

        document.querySelector('#btn-to-policy-popup').addEventListener('click', function(e){
            e.preventDefault();
            // popupPolicy.open();

            modal_Terms_Policy('/ajax_get_policy', 'wrapper-private-policy');
        });
    });

</script>
@yield('remote_js_scripts')
</body>
</html>
