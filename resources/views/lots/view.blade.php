@inject('sliderService', '\App\Services\SliderService')
@extends('layouts.main')

@section('title', $lot->title)
@section('meta_description', $lot->meta_description)

@section('top-links')
    @include('inc.top_links')
@stop
@section('scripts')
    <script src="{{ asset('js/slider.js') }}"></script>
    <script src="{{ asset('js/smoothScroll.js') }}"></script>
@endsection
@section('side_menu')
<div class="open-categories-left">
    <a href="#" class="categories-left">
        <em class="icon-categories_icon"></em>
        <span class="text">categories</span>
    </a>
    <div class="sidebar-wrap">
        @include('inc.side_nav')
    </div>
    <div class="category-left-icons">
        <h3>Popular categories</h3>
        <div class="category-preview-wrap clearfix">
            @foreach ($featured as $category)
                @php
                    $icon_default = '';
                    $icon_hover = '';
                @endphp
                @foreach($categories_icons as $icons)
                    @if($icons->id == $category->categories_icons_id)
                        @php
                            $icon_default = $icons->image_default;
                            $icon_hover = $icons->image_hover;
                        @endphp
                    @endif
                @endforeach
                <a href="{{ $category->url }}" class="item category-icon">
                    @if($icon_default == '' || $icon_hover == '')
                        <span class="icon"><span class="vertical-middle"><em class="{{ $category->icon }}"></em></span></span>
                    @else
                        <span class="icon"><span class="vertical-middle"><img width="130px" src="{!! $icon_default !!}" data-src="{!! $icon_default !!}" data-hover="{!! $icon_hover !!}" alt=""></span></span>
                    @endif
                    <span class="name">{{ $category->title }}</span>
                </a>
            @endforeach
            <div class="clear"></div>
        </div>
    </div>
</div>
@endsection

@section('top_message')
    @if ($lotState->lotIsActive()->isSeller()->isLotOwner()->lotHasBids()->getState())

        <div class="lot-top-notification">
            You got {{ $lot->new_bids_count }} new bids. <a href="{{ route('seller_lot', ['id' => $lot->id]) }}">Manage bids</a>
            <span class="icon-esc_icon"></span>
        </div>

    @elseif ($lotState->lotIsSold()->isSeller()->isLotOwner()->getState())
        <div class="lot-top-notification">
            @if ($bid->by_seller)
                You accepted bid.
            @else
                Your bid was acceted.
            @endif
            <a href="{{ route('seller_lot', ['id' => $lot->id]) }}#{{ $bid->by_seller ? 'sent' : 'received' }}">View the winner</a>
        </div>
    @elseif (
        $lotState->lotIsExpired()->isSeller()->isLotOwner()->getState() ||
        $lotState->lotIsClosed()->isSeller()->isLotOwner()->getState()
    )
        <div class="lot-top-notification">
            Lot is closed. <a href="{{route('seller_closed_lots')}}">Manage this lot</a>
        </div>
    @elseif (
        $lotState->lotIsExpired()->getState() ||
        $lotState->lotIsClosed()->getState()
    )
        <div class="lot-top-notification">
            Lot is closed
        </div>
    @elseif (
        $lotState->lotIsSoldOuside()->getState() ||
        $lotState->lotIsSold()->isSeller()->isNotLotOwner()->getState() ||
        $lotState->lotIsSold()->isGuest()->getState() ||
        $lotState->lotIsSold()->isBuyer()->isNotWinner()->getState()
    )
        <div class="lot-top-notification">
            Sorry this lot was sold
        </div>
    @elseif ($lotState->lotIsActive()->isBuyer()->isCounterBid()->getState())
        <div class="lot-top-notification">
            You got counter bid. <a href="#" class="btn-respond">Please respond</a>
            <span class="icon-esc_icon"></span>
        </div>
    @endif
@endsection

@section('content')
    <div class="page-wrap">
        @yield('top_message')
        <div class="bids-view clearfix @hasSection ('top_message') bids-view--top-notification @endif">
            <div class="wrap">
                @include('inc.breadcrumbs', ['isLastLink'=>true])
                <div class="back-action">
                    <span class="lot-number-info"><span class="text">LastBid Lot No:</span><span class="number">{{ $lot->id }}</span></span>
                    {{--<a class="product-link-back" href=""><em class="icon-right_arrow"></em>Back</a>--}}
                </div>
                <div class="title">
                    <h1>{{ $lot->title}}</h1>
                    @if($lot->subtitle)
                    <h3>{{$lot->subtitle}}</h3>
                    @endif
                </div>
                <div class="info-product">
                    {{--<div class="item-info">--}}
                        {{--<span>Bids:</span>--}}
                        {{--{{ $lot->bids_count }}--}}
                    {{--</div>--}}
                    <div class="item-info">
                        <span>Views:</span>
                        {{ $lot->views_count }}
                    </div>
                    <div class="socials-items addthis_inline_share_toolbox"></div>
                </div>
            </div>
        </div>
        <div class="wrap clearfix">
            <div id="slider-product" class="slider-product">
                @if (is_array($lot->photos) && count($lot->photos) > 1)
                    <a href="#" class="prev slider-arrow"><em class="icon-right_arrow"></em></a>
                    <a href="#" class="next slider-arrow"><em class="icon-right_arrow"></em></a>
                @endif
                <div class="viewport">
                    @if (is_array($lot->photos) && count($lot->photos))

                        @if(count($lot->photos) > 1)
                            @include('.inc.slider._slide',
                                ['src' => array_last($lot->photos),
                                'class' => 'item clone-prev item-clone left-item clone-prev'])
                        @endif

                        @foreach ($lot->photos as $key => $img)
                            @include('.inc.slider._slide',
                                ['src' => $img,
                                'class' => $sliderService->getItemClassName($loop),
                                'slideId' => $key])
                        @endforeach
                    @else
                        <div class="item active">
                            <img src="{{asset('img/coming-soon.jpg')}}" alt="">
                            <a class="zoom-wrap icon-zoom product-group" href="{{asset('img/coming-soon.jpg')}}">
                                <span>zoom <br>image</span>
                            </a>
                        </div>
                    @endif
                </div>
            </div>


            @if (is_array($lot->photos) && count($lot->photos) > 1)
                @include('.inc.slider._nav',
                        ['sliderId' => 'slider-product',
                        'items' => $lot->photos])
            @endif
            <div class="product-info-wrap clearfix">

                {{--@include('lots.inc.buy-it-wrap', ['class' => 'hidden-sm'])--}}

                <div class="text-info">

                    <div class="row">
                        <div class="title-wrap-preview">
                            <h2>specifics</h2>
                            @if (
                                $lotState->lotIsActive()->isGuest()->getState() ||
                                $lotState->lotIsActive()->isBuyer()->getState() ||
                                $lotState->lotIsSold()->isBuyer()->isWinner()->getState()
                            )
                                <span class="btn-ask-question-seller icon-messages_icon">ask a question</span>
                            @endif
                        </div>
                        @if($lotSpecifics->isNotEmpty())
                        <div class="content-wrap">
                            <ul class="aligned-ul">
                                @foreach($lotSpecifics as $specific)
                                @if (!$specific->is_text)
                                    <li>
                                        <span class="first-col">{{$specific->name}}:</span>
                                        <span class="second-col">{{$specific->options}}</span>
                                    </li>
                                @endif
                                @endforeach

                                @foreach($lotSpecifics as $specific)
                                @if ($specific->is_text)
                                    <li class="lot-text-specific">
                                        <p>{{$specific->name}}:</p>
                                        <p>{{$specific->options}}</p>
                                    </li>
                                @endif
                                @endforeach
                            </ul>
                        </div>
                        @endif
                    </div>

                    <div class="row">
                        <div class="title-wrap-preview">
                            <h2>Description</h2>
                        </div>
                        <div class="content-wrap">
                            {!! $lot->description !!}
                        </div>
                    </div>

                    <div class="row">
                        <div class="title-wrap-preview">
                            <h2>How it works</h2>
                        </div>
                        <div class="content-wrap clearfix hidden-xs-weak">
                            <p>The contract between buyer and seller is strictly one with the auction house.</p>

                            <p>Your bid will be valid for a limited time only, and during this period it might be outbid
                                by others, or declined as too low. There will be the opportunity to increase the initial
                                offer up to 5 times to secure the lot.</p>

                            <p>If successful, the auction house will inform you by email, with precise details for payment and delivery.</p>
                        </div>
                    </div>

                    <div class="row">
                        <div class="title-wrap-preview">
                            <h2>Shipping</h2>
                        </div>
                        <div class="content-wrap hidden-xs-weak">
                            <p>Your final bid does not including shipping/delivery fees or any applicable taxes. If your bid is successful you can arrange to collect your item directly from the auction house once it has been paid for in full or you can request a shipping estimate from the auction house via the messaging window when placing your bid. Alternatively, you can of course make arrangements to have your item collected via the shipping company of your choice.</p>
                            {!! $lot->shipping_description !!}
                        </div>
                    </div>

                    <div class="row">
                        <div class="title-wrap-preview">
                            <h2>Offered by</h2>
                        </div>
                        <div class="content-wrap clearfix hidden-xs-weak">
                            <div class="provided-wrap-logo">
                                @isset ($lot->partner->image)
                                <img src="{{asset('uploads/'.$lot->partner->image)}}" alt="">
                                @endisset
                            </div>
                            <div class="provided-wrap-text">
                                @isset ($lot->partner->description)
                                <p>{!! $lot->partner->description !!}</p>
                                @endisset
                                @isset($lot->partner->terms_link)
                                <p><a class="" href="javascript: void 0;" onclick="modal_Terms_Policy_Custom('/ajax_get_terms_custom', 'wrapper-terms-conditions', '{{ $lot->id }}')">
                                        Terms &amp; Conditions</a></p>
                                @endisset
                            </div>
                        </div>
                    </div>
                </div>

                @include('lots.inc.buy-it-wrap', ['class' => 'hidden-xs'])

            </div>
            <div class="most-popular clearfix">
                <div class="title-wrap-preview">
                    <h2>You may also like</h2>
                </div>
                <div class="row">
                    @foreach($latest as $latestLot)
                        @include('inc.latest_lot', ['lot'=>$latestLot])
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection

@section('custom_modal')
    <div id="popup-overlay"></div>
    @if (
        $lotState->lotIsActive()->isGuest()->getState() ||
        $lotState->lotIsActive()->isBuyer()->getState() ||
        $lotState->lotIsSold()->isBuyer()->isWinner()->getState()
    )
        <div class="popup popup-ask-question">
            <div class="ask-question-head">
                <h3 class="popup-title">Ask the Seller a Question</h3>
                <div class="lot-view">
                    <img src="{{$lot->getImagePath(55, 69)}}" alt="lot name">
                    <h3>{{ $lot->title }}</h3>
                    <p>LastBid Lot No:<span> {{ $lot->id }}</span></p>
                </div>
            </div>
            <div class="ask-question-body">
                <form action="{{route('add_message',
                ['id'=>$lot->id, 'slug'=>$lot->slug])}}" id="message-form">
                <div class="reply-msg">
                    <p>Message to: <span>{{ (!empty($lot->seller->company_name)) ? $lot->seller->company_name : $lot->seller->full_name }}</span></p><span class="btn-clear-reply-msg">Clear
                        message</span>
                    <textarea name="letter-reply-msg" class="textarea-scrollbar"></textarea>
                </div>
                <div class="reply-btns"><span class="btn-send-msg">Send</span><span
                            class="btn-cancel-send-msg">Cancel</span></div>
                    {{csrf_field()}}
                    {{--<input type="hidden" name="id" value="{{$lot->id}}">--}}
                </form>
            </div>
        </div>
    @endif
    @if ($lotState->lotIsActive()->isBuyer()->isCounterBid()->getState())
        <div class="popup-scrolled popup-view-bids">
            @foreach ($bid->attempts as $attempt)
                @include('buyer.inc.attempt')
            @endforeach
        </div>
        @include('inc.modal_confirm_accept_bid',['item'=>$bid, 'route'=>'buyer_switch_bid_status'])
    @endif
@endsection

@section('user_script')

    $(document).ready(function () {
        new lotSlider('slider-product', 'slider-product-nav');
    });

    @if ($lotState->lotIsActive()->isBuyer()->isCounterBid()->getState())
        $(function() {
            var scrolled = $(window).scrollTop();
            var winHeight = $(window).height();
            $(window).scroll(function (event) {
                scrolled = $(window).scrollTop();
            });
            // open modals
            $('.btn-respond').on('click', function(){
                var top = scrolled +  winHeight * 0.15;
                $('#popup-overlay').show();
                $('.popup-view-bids').show().css('top', top + 'px');
                return false;
            });
            // close modals
            $('#popup-overlay').on('click', function(){
                $(this).hide();
                $('.popup-view-bids').hide();
            });
            $('#conf_modal_{{$bid->id}}').on('show.bs.modal', function (e) {
                $('#popup-overlay').hide();
                $('.popup-view-bids').hide();
            })
            $('#conf_modal_{{$bid->id}}').on('hidden.bs.modal', function (e) {
                var top = scrolled +  winHeight * 0.15;
                $('#popup-overlay').show();
                $('.popup-view-bids').show().css('top', top + 'px');
                return false;
            })
        });
    @endif
    $(function() {
        $('.provided-terms-link').on('click', function (e) {
            e.preventDefault();
            var popupOptions = 'resizable=yes, scrollbars=yes, width=400, height=500, left=350, top=250';
            window.open(this.href, 'window', popupOptions);
        })
    });

    new expandLotDescription('.product-info-wrap>.text-info .title-wrap-preview>h2', 769);
@endsection
