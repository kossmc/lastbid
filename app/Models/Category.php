<?php

namespace App\Models;

use App\Traits\MetaRelationTrait;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Backpack\CRUD\CrudTrait;

class Category extends Model
{
    use Sluggable;
    use CrudTrait;
    use MetaRelationTrait;
    /*
   |--------------------------------------------------------------------------
   | GLOBAL VARIABLES
   |--------------------------------------------------------------------------
   */
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'categories';
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    //protected $fillable = ['name', 'slug'];

    protected $guarded = ['aliases_json'];

    protected $appends = ['url'];

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title',
                'maxLength' => 191,
            ]
        ];
    }

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }
    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    public function getTreeIds()
    {
        $ids = $this->children->map(function ($child) {
            return $child->id;
        });
        $ids[] = $this->id;
        return $ids;
    }

    public function genMetaDescription()
    {
        $description = str_replace("&nbsp;", ' ', $this->description);

        $titleLen = mb_strlen($this->title);
        $descriptionLen = 150-$titleLen;

        return $this->title .' - '. mb_strimwidth( strip_tags($description), 0, $descriptionLen);
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function lots()
    {
        return $this->hasMany(Lot::class, 'category_id', 'id')->latest()->original();
    }

    public function latestLots()
    {
        return $this->hasMany(Lot::class, 'category_id', 'id')
            ->latest()
            ->active()
            ->original();
    }

    public function parent()
    {
        return $this->hasOne(Category::class, 'id', 'parent_id');
    }

    public function children()
    {
        return $this->hasMany(Category::class, 'parent_id', 'id')->orderBy('lft');
    }

    public function childrenWithLots()
    {
        return $this->hasMany(Category::class, 'parent_id', 'id')
            ->orderBy('lft')->with(['latestLots']);
    }

    public function filters()
    {
        return $this->hasMany(Specific::class,'category_id', 'id')
            ->with('specific_options')
            ->where('use_in_filter', 1)
            ->orderBy('order', 'DESC');
    }

    public function aliases()
    {
        return $this->hasMany(CategoryAlias::class, 'category_id', 'id');
    }

    public function specifics()
    {
        return $this->hasMany(Specific::class, 'category_id', 'id');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /**
     * Scope a query to only active events.
     *
     * @param \Illuminate\Database\Eloquent\Builder $builder
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeRoots($query)
    {
        return $query->whereNull('parent_id');
    }

    public function scopeOther($query)
    {
        return $query->where('featured', 0);
    }
    /**
     * Scope a query to only active events.
     *
     * @param \Illuminate\Database\Eloquent\Builder $builder
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeFeatured($query)
    {
        return $query->where('featured', 1);
    }

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /**
     * Generates an URL for category.
     *
     * @return string
     */
    public function getUrlAttribute()
    {
        //temporary disabled
        //return route('category', ['slug' => $this->getFullUri($this)]);
        return route('category', ['slug' => $this->slug]);
    }

    /**
     * Get full url to category contain parent categories url's
     * @param null $item
     * @return string
     */
    public function getFullUri($item = null){
        if(!$item->parent){
            return $item->slug;
        } else {
            $segments = [];
            $segments[] = $this->getFullUri($item->parent);
            $segments[] = $item->slug;
            return implode('/', $segments);
        }
    }

    /**
     * Get list of category aliases.
     *
     * @return string
     */
    public function getAliasesJsonAttribute()
    {
        return $this->aliases->map(function ($alias) {
            return [
                'id' => $alias->id,
                'alias' => $alias->alias,
            ];
        })->toJson();
    }

    /**
     * Virtual field for move lots functionality.
     *
     * @return string
     */
    public function getMoveCheckbox()
    {
        return '<input name=to_move[] type="checkbox" value="'.$this->id.'">';
    }
    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
