<div class="popup buyer-account---confirm-deleting"
     id="conf_modal_{{$item->id}}">
    @php
        $expired = false;
        $lot = \App\Models\Lot::find($item->lot_id);
        if($lot->end_date->timestamp < \Carbon\Carbon::now()->timestamp) {
            $expired = true;
        }
    @endphp
    @if($expired)
        <div class="title">sorry, this lot has already expired</div>
        <div class="btn-wrap">
            <a class="btn2" href="javascript: void 0;" data-dismiss="modal">close</a>
        </div>
    @else
        <div class="title">Confirm</div>
        <p>By accepting a bid you are obligated to complete this transaction.</p>
        <div class="btn-wrap">
            <a class="btn" href="{{ route($route, ['id' => $item->id, 'method' => 'accept']) }}">ok</a>
            <a class="btn2" href="javascript: void 0;" data-dismiss="modal">cancel</a>
        </div>
    @endif

</div>
