<?php

namespace App\Listeners\User;

use App\Listeners\User\SendGridAbstractUserListener;
use App\Events\User\BuyerRegistered;

class BuyerRegisteredListener extends SendGridAbstractUserListener
{
    /**
     * Buyer Email subject.
     */
    private $buyerSubject = 'Welcome to Last Bid!';

    /**
     * Buyer Email template.
     */
    private $buyerTemplate = '44b82990-4feb-4965-81b9-6fabbf755616';

    /**
     * Handle the event.
     *
     * @param  BuyerRegistered $event
     * @return void
     */
    public function handle(BuyerRegistered $event)
    {
        $this->setData($event);

        /*notification to buyer*/
        $this->sendMail($this->getData($this->buyerTemplate), $this->buyerSubject);
    }
}
