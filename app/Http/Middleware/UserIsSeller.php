<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class UserIsSeller
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ( ! Auth::guest() && Auth::user()->hasRole('seller')) {
            if ($request->ajax()) {
                return response()->json([
                    'status' => false,
                    'seller' => true
                ]);
            }
        }
        return $next($request);
    }
}
