<?php

use Illuminate\Database\Seeder;

class HowItWorksSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (DB::table('how_it_works')->count() == 0) {
            DB::table('how_it_works')->insert([
                'id' => 1,
                'section_1_text' => nl2br('Through the LastBid website, auction houses offer unsold auction lots to a worldwide audience of interested bidders.

There’s no extra cost to the buyer; they pay exactly the same as if they’d bought at the original sale.

Everyone will find something of interest... there are so many categories of unsold items to browse: from art to wine, from jewellery to watches, from motor cars to manuscripts.

Found something you like? Then it’s as easy as one, two, three: Choose, Bid, Win.'),
                'section_2_text' => nl2br('As soon as the auction closes, unsold lots are posted with LastBid and then sorted by LastBid category or  sub-category . Registered LastBid users will instantly receive an alert offering them the chance to make an offer on items in the LastBid category that interests them.'),
                'section_3_text' => nl2br('The bid will be valid for a limited time only and, during this period, it might be outbid by others or declined as too low. There will, of course, be the opportunity to increase the initial offer until it is accepted. Bidding can be done online 24/7.'),
                'section_4_text' => nl2br('If successful, the auction house will inform the winning bidder by email, with precise details for payment and delivery. It’s a seamless, automated and very straightforward process, one easily set up via our secure servers in just a few easy-to-follow steps.'),
                'video' => '',
                'image_section_2' => '',
                'image_section_3' => '',
                'image_section_4' => '',
            ]);
        }
    }
}
