@extends('layouts.inner')
@section('title', 'My received messages')
@section('inner_content')
    <div class="page-buyer page-buyer--msg">
        @include('inc.flash_message')
        <form class="search-message-box hidden-xs" action="{{route('messages_search')}}" method="get">
            <input pattern=".{3,}" required title="3 characters minimum" id="search_input"
                    type="text" name="q" placeholder="Search"/>
            <span class="btn-search-message icon-search_icon"></span>
        </form>
        <!-- Top tabs-->
        <ul class="nav nav-tabs profile-tabs-nav lots-tabs-nav">
            <li{!! isActiveRoute('messages_received') ? ' class="active"' : '' !!}>
                <a href="{{route('messages_received')}}">received</a></li>
            <li{!! isActiveRoute('messages_sent') ? ' class="active"' : '' !!}>
                <a href="{{route('messages_sent')}}">sent</a>
            </li>
            <li{!! isActiveRoute('messages_archived') ? ' class="active"' : '' !!}>
                <a href="{{route('messages_archived')}}">archived</a>
            </li>
        </ul>
        <!-- end of Top tabs-->
        @if (count($messages))
            <!-- Content of tabs-->
            <div class="tab-content profile-tab-content {!! isActiveRoute('messages_archived') ? ' tab-content--archived' : '' !!}">
                <div class="tab-pane fade in active">
                    <div class="tab-inner">
                        <ul class="message-list">
                            <form action="" method="post" id="bulk_action">
                                <li class="message-item message-item--placeholder">
                                    <div class="message-item--short">
                                        <div class="select-option ">
                                            <input type="checkbox" class="checkbox-placeholder"/>
                                            <label class="styled-checkbox"></label>
                                        </div>
                                        <div class="controllers">
                                            @if (isActiveRoute('messages_archived'))
                                                <span class="btn-msg-delete">delete</span>
                                                <span class="btn-msg-restore" data-href="{{ route('message_switch_status', ['method' => 'restore']) }}">restore</span>
                                            @elseif (isActiveRoute('messages_sent'))
                                                <span class="btn-msg-archive" data-href="{{ route('message_switch_status', ['method' => 'archive']) }}">archive</span>
                                            @else
                                                <span class="btn-msg-read-mark" data-href="{{ route('message_switch_status', ['method' => 'read']) }}">mark as read</span>
                                                <span class="btn-msg-unread-mark" data-href="{{ route('message_switch_status', ['method' => 'unread']) }}">mark as unread</span>
                                                <span class="btn-msg-archive" data-href="{{ route('message_switch_status', ['method' => 'archive']) }}">archive</span>
                                            @endif

                                        </div>
                                        <div class="placeholders">
                                            <div class="select-all visible-xs">Select All</div>
                                            <div class="person hidden-xs">
                                                @if (isActiveRoute('messages_received'))
                                                    From
                                                @elseif (isActiveRoute('messages_sent'))
                                                    To
                                                @elseif (isActiveRoute('messages_archived'))
                                                    From/To
                                                @endif
                                            </div>
                                            <div class="id-lot hidden-xs">LastBid Lot No</div>
                                            <div class="subject hidden-xs">Subject</div>
                                            <div class="lot-expires-date lot-expires-sort hidden-xs">Lot Expires
                                                <svg id='svg-arrow-down' width="6px" height="4px" viewBox="0 0 6 4" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                                <g id="Accounts" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <g id="Seller_messages_1_3" transform="translate(-1236.000000, -294.000000)" fill="#DCDCDC">
                                                        <g id="table_header" transform="translate(405.000000, 285.000000)">
                                                            <g id="sorting" transform="translate(831.000000, 3.000000)">
                                                                <polygon id="Rectangle" points="6 6 3 10 -1.07184979e-13 6"></polygon>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </g>
                                            </svg> 
                                            <svg id='svg-arrow-up' width="6px" height="4px" viewBox="0 0 6 4" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                                <g id="Accounts" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <g id="Seller_messages_1_3" transform="translate(-1236.000000, -288.000000)" fill="#DCDCDC">
                                                        <g id="table_header" transform="translate(405.000000, 285.000000)">
                                                            <g id="sorting" transform="translate(831.000000, 3.000000)">
                                                                <polygon id="Rectangle-Copy-3" transform="translate(3.000000, 2.000000) scale(1, -1) translate(-3.000000, -2.000000) " points="6 0 3 4 -1.07184979e-13 0"></polygon>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </g>
                                            </svg>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                @foreach($messages as $message)
                                    <li class="message-item @if ($message->is_read) message-item--read @else message-item--unread @endif"
                                    data-href="{{ $message->url }}">
                                        <div class="message-item--short">
                                            <div class="select-option">
                                                <input type="checkbox" name="messages_ids[]" value="{{$message->id}}"/>
                                                <label class="styled-checkbox"></label>
                                            </div>
                                            <div class="person">
                                                @if (isActiveRoute('messages_received'))
                                                    {{$message->sender->short_name or ''}}
                                                @elseif (isActiveRoute('messages_sent'))
                                                    {{$message->recipient->short_name}}
                                                @elseif (isActiveRoute('messages_archived'))
                                                    @if ($message->is_sent)
                                                        To: {{$message->recipient->short_name}}
                                                    @else
                                                        From: {{$message->sender->short_name or ''}}
                                                    @endif
                                                @endif
                                            </div>
                                            <div class="id-lot hidden-xs">{{$message->lot_id}}</div>
                                            <div class="subject hidden-xs">{{$message->lot->title}}</div>
                                            <div class="lot-expires-date">{{$message->lot->end_date->setTimezone(session('time_zone'))->format('j M Y')}}</div>

                                            <div class="id-lot visible-xs mobile">{{$message->lot_id}}</div>
                                            <div class="subject visible-xs mobile">{{$message->lot->title}}</div>
                                        </div>
                                    </li>
                                @endforeach
                                {{csrf_field()}}
                            </form>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- End of content of tabs-->
            <!-- Pagination-->
            @include('inc.paginator_with_page_selector',
            ['items'=>$messages, 'append'=>['sort_by_lot' => request('sort_by_lot',0)]])

        @else
            <p class="notification-no-content">No messages</p>
        @endif
    </div>
    <!-- end of Pagination-->
@endsection
@section('custom_modal')
    @if (isActiveRoute('messages_archived'))
        @include('messages.modal_confirm_delete')
    @endif
@endsection
@section('user_script')

// select all messages in each tab
var listOfMessages = $('.message-list');
var messages = listOfMessages.find('.message-item:not(.message-item--placeholder)');
var placeholderMessage = listOfMessages.find('.message-item--placeholder');
var placeholderCheckbox = $('input.checkbox-placeholder');

// click event on checkbox at placeholder
placeholderCheckbox.on('change', function (e) {

    if ($(this).is(':checked')) {
        listOfMessages.find('.message-item:not(:first-child)').addClass('message-item--selected')
        listOfMessages.find('.message-item').find('input[type="checkbox"]').prop('checked', true);

    } else {
        listOfMessages.find('.message-item').removeClass('message-item--selected');
        listOfMessages.find('.message-item').find('input[type="checkbox"]').prop('checked', false);
    }

    if ($('.tab-content').hasClass('tab-content--archived')) {
        changeCheckboxesInLastTab();
    }
    else {
        changeCheckboxes();
    }

});

// change event on the rest checkboxes
messages.find('input[type="checkbox"]').on('change', function (e) {

    if ($(this).is(':checked')) {
        $(this).prop('checked', true);
        $(this).closest('.message-item').addClass('message-item--selected');
    }
    else {
        $(this).prop('checked', false);
        $(this).closest('.message-item').removeClass('message-item--selected');
    }

    if ($('.tab-content').hasClass('tab-content--archived')) {
        changeCheckboxesInLastTab();
    }
    else {
        changeCheckboxes();
    }

});

var controllsThirdTab = {
    showAll: function () {
        placeholderMessage.find('.placeholders').hide();
        placeholderMessage.find('.controllers').show();
        placeholderMessage.find('.btn-msg-delete').css('display', 'inline-block');
        placeholderMessage.find('.btn-msg-restore').css('display', 'inline-block');
    },
    hideAll: function () {
        placeholderMessage.find('.placeholders').show();
        placeholderMessage.find('.controllers').hide();
        placeholderMessage.find('.btn-msg-delete').hide();
        placeholderMessage.find('.btn-msg-restore').hide();
    }
}

var controlls = {

    showAll: function () {
        placeholderMessage.find('.placeholders').hide();
        placeholderMessage.find('.controllers').show();
        placeholderMessage.find('.btn-msg-archive').css('display', 'inline-block');
        placeholderMessage.find('.btn-msg-unread-mark').css('display', 'inline-block');
        placeholderMessage.find('.btn-msg-read-mark').css('display', 'inline-block');
    },
    hideAll: function () {
        placeholderMessage.find('.placeholders').show();
        placeholderMessage.find('.controllers').hide();
        placeholderMessage.find('.btn-msg-unread-mark').hide();
        placeholderMessage.find('.btn-msg-read-mark').hide();
        placeholderMessage.find('.btn-msg-archive').hide();
    },
    showOnlyRead: function () {
        placeholderMessage.find('.placeholders').hide();
        placeholderMessage.find('.controllers').show();
        placeholderMessage.find('.btn-msg-archive').css('display', 'inline-block');
        placeholderMessage.find('.btn-msg-read-mark').css('display', 'inline-block');
    },
    showOnlyUnread: function () {
        placeholderMessage.find('.placeholders').hide();
        placeholderMessage.find('.controllers').show();
        placeholderMessage.find('.btn-msg-archive').css('display', 'inline-block');
        placeholderMessage.find('.btn-msg-unread-mark').css('display', 'inline-block');
    }
};

function changeCheckboxes() {

    controlls.hideAll();

    var array = $("input[type='checkbox']:checked:not(.checkbox-placeholder:checked)") || [];
    var all = array.length;
    var unread = 0;

    if (all == 0) {
        controlls.hideAll();
        if (placeholderCheckbox.is(':checked'))
            placeholderCheckbox.prop('checked', false);
        return false;
    }

    $.each(array, function () {
        if ($(this).closest('li').hasClass('message-item--unread')) {
            unread++;
        }
    });

    if (all - unread == 0) {
        controlls.showOnlyRead();
    }
    else if (unread == 0) {
        controlls.showOnlyUnread();
    }
    else {
        controlls.showAll();
    }
}

function changeCheckboxesInLastTab() {

    controllsThirdTab.hideAll();

    var array = $("input[type='checkbox']:checked:not(.checkbox-placeholder:checked)") || [];
    var all = array.length;

    if (all == 0) {
        controllsThirdTab.hideAll();
        if (placeholderCheckbox.is(':checked'))
            placeholderCheckbox.prop('checked', false);

    }
    if (all > 0) {
        controllsThirdTab.showAll();
    }
}


//click on message
$('.message-item:not(.message-item--placeholder)').on('click', function (e) {
    if ($(e.target).is('input')) {
        return true;
    }
    if ($(e.currentTarget).is('li')) {
        window.location = $(e.currentTarget).attr('data-href');
    }
});

// open modal dialog
$('.btn-msg-delete').on('click', function () {
    $('#popup-overlay').show();
    $('.popup-wrap').show();
    $('.popup-wrap').find('.popup').addClass('popup-active');

});
// close modal dialog
$('.popup-wrap').on('click', function (e) {
    $(this).hide();
    $('#popup-overlay').hide();
    $('.popup-wrap').find('.popup').removeClass('popup-active');

    if ($(e.target).hasClass('popup-btn-cancel')) {// delete action
        $('form#bulk_action').attr('action', '{{route('message_switch_status',['method'=>'delete'])}}').submit();
    }
});
// buttons actions
$('.btn-msg-read-mark, .btn-msg-unread-mark, .btn-msg-archive, .btn-msg-restore').on('click', function (e) {
    var el = $(e.target),
        href = el.attr('data-href');
    el.parents('form').attr('action', href).submit();
});
//lot exp sorting
$('.lot-expires-sort').on('click', function (e) {
    var search = window.location.search,
        sort_string = 'sort_by_lot=1';
    if (search.length > 0) {
        if (search.indexOf(sort_string) > -1) {
            return window.location.search = search.replace(sort_string, '');
        } else {
            return window.location.search = search + '&' + sort_string;
            }
    } else {
        return window.location.search = sort_string;
    }
});
$('.btn-search-message').on('click', function (e) {
    var el = $(e.target);
    if (el.siblings('input')[0].value.length > 2) {
        el.parents('form').submit();
    } else {
        $('#search_input').tooltip('show');
    }
});

@endsection
