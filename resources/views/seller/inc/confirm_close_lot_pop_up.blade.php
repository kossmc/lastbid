@if($items)
    <div id="popup-overlay"></div>
    @foreach($items as $item)
        <div class="popup buyer-account---confirm-deleting"
             id="conf_modal_{{$item->id}}">
            <div class="title">Confirm closing lot</div>
            <p>This action will close lot #{{$item->id}}.<br>Are you sure you want to
                continue?</p>
            <div class="btn-wrap"><a class="btn" href="{{ route($route,$item->id) }}">ok</a><a
                        class="btn2" href="javascript: void 0;" data-dismiss="modal">cancel</a></div>
        </div>
    @endforeach
@endif
