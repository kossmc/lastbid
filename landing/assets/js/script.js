
var $html;

var modalContainer = '#modal-win2',
    modalContainer_two = '#modal-win'
    modalWindow = '#modal-id';

// these two functions termsModal and policyModal can be optimized into one
// by passing two args into function: url and selector
// or not to do it?

function modal_Terms_Policy(url, element) {
    $.ajax({
        'type':'get',
        'url': "https://lastbid.com" +url,
        'data': {},
        'success': function (data) {
            if (data.form) {
                showForm(data.form);
                $("." + element).scrollbar({
                    'disableBodyScroll': false,
                    'ignoreMobile': false
                });
                $("." + element).addClass('scrollbar-outer');
            }
        }
    });
}

function polyfillSetPlaceholders() {

    if ($html.data('useragent').search(/MSIE 9.0/) > 0) {

        console.log('detect ie9');

        $("input, textarea").each(function () {

            if ($(this).val() == "" && $(this).attr("placeholder") != "") {

                $(this).val($(this).attr("placeholder"));

                $(this).focus(function () {
                    if ($(this).val() == $(this).attr("placeholder")) $(this).val("");
                });
                $(this).blur(function () {
                    if ($(this).val() == "")
                        $(this).val($(this).attr("placeholder"));
                });

            }
            $(this).blur(function () {
                if ($(this).val() == "")
                    $(this).val($(this).attr("placeholder"));
            });


        });

    }
}

function closeModal() {
    $(modalWindow).modal('hide').empty();
    $(modalContainer).empty();
    $(modalContainer_two).empty();

    $('.modal-backdrop.fade.in').remove();
    $('body').removeClass('modal-open');
}

/* checking html5 form validation functions */
function hasHtml5Validation() {
    return typeof document.createElement('input').checkValidity === 'function';
}

function showForm(html) {

    $(modalContainer).html(html);
    $(modalWindow).modal('show');
    if (document.querySelector('#popup-comment')) {
        $('#popup-comment').val($('#buy-form #message').val());
    }
    /*input masc init*/
    if (document.querySelector('#exp_date')) {
        $('#exp_date').inputmask({mask: "99/99", placeholder: "mm/yy"});
    }
    if (document.querySelector('#modal-id')) {
        $('.textarea-scrollbar').scrollbar({
            'disableBodyScroll': false,
            'ignoreMobile': true
        });
        $('.scroll-textarea').addClass('scrollbar-outer');
    }
    if(document.querySelector('.second-step-register')){
        var card = new CardValidation('.second-step-register', true);  
        

    }
    if(document.querySelector('#register-form')){

        $('a[disabled]').on('click', function(e){
            $(this).attr('href', null);     
        });


        var wrapper = $('.wrap-check-policy');
        // $('.btn-sign-up-with').on('click', function(){
        //     $('.sign-with-account').show();
        //     $('.sign-without-account').hide();
        //     $('.row.wrapper-sign-up-with').hide();
        // });

        $('.wrapper-sign-up-agreement').scrollbar({
            onScroll: function(y, x){
               
                if(Math.abs(Math.floor(y.scroll) - y.maxScroll) <= 1){
                    wrapper.find('input[type="checkbox"]').removeAttr('disabled');
                    wrapper.find('label').removeClass('disabled-label');
                }
            }
        });
        $('.wrapper-sign-up-agreement').addClass('scrollbar-outer');

        wrapper.find('input[type="checkbox"]').change(function() {
            if(this.checked) {
                wrapper.find('.btn').removeClass('btn-sign-up-disabled').removeAttr('disabled');
            }
            else{
                wrapper.find('.btn').addClass('btn-sign-up-disabled').attr('disabled', 'disabled');
            }
        });

        // wrapper.find('.btn-sign-up-next-step').on('click', function(){
        //     var nav = $('.nav.sign-up-tabs');
        //     var tabContent = $('.tab-content');
        //     nav.find('li:nth-child(1)').removeClass('active');
        //     nav.find('li:nth-child(2)').addClass('active');
        //     nav.find('[href="#auth-tab2"]').attr('data-toggle', 'tab')
        //     $('#auth-tab1').removeClass('active in');
        //     $('#auth-tab2').addClass('active in');
        //
        //     var card = new CardValidation('#register-form', true);
        // });

        var select = $('.select-style');
        var select_ul = select.find('ul');

        select.find('span').on('click', function(){

            $(this).toggleClass('up-down-arrow');
            var ul = $(this).next('ul');

            if(ul.is(':hidden')){
               ul.show();
            }
            else{
                ul.hide();
            }
        });


        select_ul.find('li').on('click', function(){

            var select = $(this).closest('.select-style');
            var selct_opt = select.find('option');
            var select_ul = select.find('ul');

            select.find('span').toggleClass('up-down-arrow');

            select.find('span').addClass('selected-suffix');

            select.find('span').text($(this).text());
            selct_opt.attr('value', $(this).text());
            selct_opt.text($(this).text());
            select_ul.hide();
        });


    }

    polyfillSetPlaceholders();

}

/*checks if element exist on page*/
jQuery.fn.exists = function () {
    return jQuery(this).length > 0;
};

/*add or replace params in url*/
function insertParams(params) {
    var result;
    var ii = params.length;
    var queryString = document.location.search.substr(1);
    var kvps = queryString ? queryString.split('&') : [];
    var kvp;
    var skipParams = [];
    var i = kvps.length;
    while (i--) {
        kvp = kvps[i].split('=');
        if (kvp[0].slice(-2) != '[]') {
            ii = params.length;
            while (ii--) {
                if (params[ii][0] == kvp[0]) {
                    kvp[1] = params[ii][1];
                    kvps[i] = kvp.join('=');
                    skipParams.push(ii);
                }
            }
        }
    }
    ii = params.length;
    while (ii--) {
        if (skipParams.indexOf(ii) === -1) {
            kvps.push(params[ii].join('='));
        }
    }
    result = kvps.length ? '?' + kvps.join('&') : '';
    return result;
}

$(document).ready(function () {

    // ONLY FROM LANDING
    $html = $('html');
    $html.attr('data-useragent', navigator.userAgent);

    polyfillSetPlaceholders();


    var video = document.getElementById("lastBidVideo");

    var playScrollVideo = true;

    function StartPauseHandler(e) {
        if (e.type === "pause") {
            playScrollVideo = false;
        } else if (e.type === "playing") {
            console.log("playing");
        } else {
            console.log('any');
        }
    }

    video.addEventListener('click touchstart', StartPauseHandler);
    video.addEventListener("pause", StartPauseHandler);
    video.addEventListener("playing", StartPauseHandler);

    function playVid() {
        video.muted = true;
        video.play();
    }

    function stopVid() {
        video.pause();
    }

    $(window).scroll(function () {
        if (playScrollVideo == true) {
            if ($(this).scrollTop() > 100) {
                $('#lastBidVideo').show();
                $('#placeholderVideo').hide();
                playVid();
            }
        }
    });

    $(document).on('click', '#lastBidVideo, #placeholderVideo', function (e) {
        if( !(navigator.userAgent.match(/Android/i)
            || navigator.userAgent.match(/webOS/i)
            || navigator.userAgent.match(/iPhone/i)
            || navigator.userAgent.match(/iPad/i)
            || navigator.userAgent.match(/iPod/i)
            || navigator.userAgent.match(/BlackBerry/i)
            || navigator.userAgent.match(/Windows Phone/i)
        )){
            e.preventDefault();

            stopVid();

            var modal = document.getElementById("videoPopup");
            modal.style.display = "block";

            var cloneVideo = video.cloneNode(true);
            cloneVideo.id = 'cloneVideo';

            $('.video-block').html(cloneVideo);
            cloneVideo.style.display = "show";
            cloneVideo.play();

            $('#cloneVideo').show();
            $('#lastBidVideo').show();
            $('#placeholderVideo').hide();
            $(window).on('click touchstart', function (event) {
                if (event.target == modal) {
                    modal.style.display = "none";
                    cloneVideo.pause();
                }
            });
        }

    });



    var letsTalk = document.getElementById("letsTalk");

    var modal = document.getElementById("discoverMore");
    var btn = document.getElementById("discoverMoreBtn");
    var span = document.getElementsByClassName("close")[0];
    var close_letsTalk = document.getElementById("letsTalk_close")
    btn.onclick = function () {
        modal.style.display = "block";
    };

    span.onclick = function () {
        modal.style.display = "none";
    };
    close_letsTalk.onclick = function () {
        letsTalk.style.display = "none";
    }

    window.onclick = function (event) {
        if (event.target == modal) {
            modal.style.display = "none";
        } else if (event.target == letsTalk) {
            letsTalk.style.display = "none";
        }
    };

    $("#privacy").on('click touchstart', function(){
        modal_Terms_Policy('/ajax_get_policy', 'wrapper-private-policy')
    })

});
