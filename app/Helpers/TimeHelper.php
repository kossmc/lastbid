<?php
use Carbon\Carbon;

if (!function_exists('format_time_left')) {
    function format_time_left($dateFrom, $dateTo = null, $precision = 2)
    {
//        if (is_null($dateTo)) {
//            $dateTo = Carbon::now()->timestamp;
//        }
//        // If not numeric then convert timestamps
//        if (!is_int($dateFrom)) {
//            $dateFrom = strtotime($dateFrom);
//        }
//        if (!is_int($dateTo)) {
//            $dateTo = strtotime($dateTo);
//        }

        if (is_null($dateTo)) {
            $dateTo = Carbon::now()->setTimezone(session('time_zone'))->timestamp;
        }
        // If not numeric then convert timestamps
        if (!is_int($dateFrom)) {
            $dateFrom = Carbon::createFromTimeString($dateFrom)->setTimezone(session('time_zone'))->timestamp;//strtotime($dateFrom);
        }
        if (!is_int($dateTo)) {
            $dateTo = Carbon::createFromTimeString($dateTo)->setTimezone(session('time_zone'))->timestamp;
        }
        // If dateFrom > dateTo then swap the 2 values
        if ($dateFrom > $dateTo) {
            list($dateFrom, $dateTo) = [$dateTo, $dateFrom];
        }
        // Set up intervals and diffs arrays
        $intervals = ['year' => 'y', 'month' => 'm', 'day' => 'd', 'hour' => 'h', 'minute' => 'm', 'second' => 's'];
        $diffs = [];
        foreach ($intervals as $interval => $short) {
            // Create temp time from dateFrom and interval
            $ttime = strtotime('+1 ' . $interval, $dateFrom);
            // Set initial values
            $add = 1;
            $looped = 0;
            // Loop until temp time is smaller than dateTo
            while ($dateTo >= $ttime) {
                // Create new temp time from dateFrom and interval
                $add ++;
                $ttime = strtotime('+' . $add . ' ' . $interval, $dateFrom);
                $looped ++;
            }
            $dateFrom = strtotime('+' . $looped . ' ' . $interval, $dateFrom);
            $diffs[$interval] = $looped;
        }
        $count = 0;
        $times = [];
        foreach ($diffs as $interval => $value) {
            // Break if we have needed precission
            if ($count >= $precision) {
                break;
            }
            // Add value and interval if value is bigger than 0
            if ($value > 0) {
                if ($value != 1) {
                    //$interval .= 's';
                }
                // Add value and interval to times array
                $times[] = $value . $intervals[$interval];
                $count ++;
            }
        }
        // Return string with times
        return implode(' ', $times);
    }
}

if (!function_exists('percent_time_left')) {
    function percent_time_left($dateFrom, $dateTo)
    {
        $minutes = $dateTo->diffInMinutes($dateFrom);
        if ($minutes > 0) {
            return 100 - round(Carbon::now()->diffInMinutes($dateFrom) /
                $dateTo->diffInMinutes($dateFrom) * 100, 2);
        }
        return 0;
    }
}
