<?php

namespace App\Services;

use GuzzleHttp\Client;
use League\Csv\Writer;

/**
 * Class BaseCrawler
 * @package App\Services
 */
class BaseCrawler
{
    /** @var string ParserBatch */
    public $batch;

    /** @var Client Http client. */
    public $client;

    /** @var Writer csv writer */
    public $writer;

    /** @var string filename.for result saving */
    public $filename = 'baseCrawler.csv';

    /** @var string temp file folder. */
    public $tempFolder;

    /** @var string temp img folder */
    public $tempPhotosFolder;

    /** @var array headers for csv file */
    public $headers = ['test'];

    /** @var int Lot seller id */
    public $seller_id;

    /** @var int Lot partner id */
    public $partner_id;

    /** @var bool if generate file */
    public $isFile = false;

    /**
     * BaseCrawler constructor.
     * @param $batch
     */
    public function __construct($batch)
    {
        $this->batch = $batch;
        $this->seller_id = !empty($batch->seller_id) ? $batch->seller_id : 0;
        $this->partner_id = !empty($batch->partner_id) ? $batch->partner_id : 0;
        $this->client = new Client(['http_errors' => false]);
        $this->tempFolder = storage_path('app/parser/');
        $this->tempPhotosFolder = storage_path('app/parser/photos/');

        if (!is_dir($this->tempFolder)) {
            mkdir($this->tempFolder, 0777, true);
        }

        if (!is_dir($this->tempPhotosFolder)) {
            mkdir($this->tempPhotosFolder, 0777, true);
        }

        $this->writer = Writer::createFromPath($this->tempFolder . $this->filename , 'w+'); //the CSV file
        $this->writer->setDelimiter(";"); //the delimiter will be the tab character
        $this->writer->setNewline("\r\n"); //use windows line endings for compatibility with some csv libraries
        $this->writer->setOutputBOM(Writer::BOM_UTF8); //adding the BOM sequence on output
    }
    /**
     * Start parser
     *
     * return file name with results
     *
     * @return string
     */
    public function parse()
    {
        $data = $this->startCrawling();

        if(is_array($data) && !empty($data)){
            $this->saveAsFile($data, $this->headers);
        }

        return is_array($data);
    }

    /**
     * Parsing logic go here
     *
     * array with results
     *
     * @return array
     */
    public function startCrawling()
    {
        return ['test'];
    }

    /**
     * @param $data
     * @param null $headers
     * @return bool|string
     */
    public function saveAsFile($data, $headers = null)
    {
        try {
            if ($headers) {
                $this->writer->insertOne($headers);
            }
            $this->writer->insertAll($data);

            $this->isFile = true;

            return $this->filename;

        } catch (\Exception $e) {
            \Log::info(print_r($e->getMessage(), true));
        }
        return false;
    }

    /**
     * @param $url
     * @param array $headers
     * @return string
     */
    public function loadImage($url, $headers = [])
    {
        $hash = md5($url);
        $url = preg_replace('/\s/', '%20', $url);
        $pathParts = pathinfo($url);
        $folder1 = substr($hash, 0, 2);
        $folder2 = substr($hash, 2, 2);
        $path = $this->tempPhotosFolder. $folder1 . '/' . $folder2 . '/';
        if (!is_dir($path)) {
            mkdir($path, 0777, true);
        }
        $filename = $hash . '.' . strtolower($pathParts['extension']);
        $resultFilename = '/' . $folder1 . '/' . $folder2 . '/' . $filename;

        if (file_exists($path . $filename)) {
            return $resultFilename;
        } elseif(!empty($headers)){
            if(copy($url, $path . $filename, stream_context_create($headers))){
                return $resultFilename;
            }
        } elseif (copy($url, $path . $filename)) {
            return $resultFilename;
        }
        return '';
    }

    public function processContent($content) {
        return str_replace(["\n", "\r"], ' ', $content);
    }
}
