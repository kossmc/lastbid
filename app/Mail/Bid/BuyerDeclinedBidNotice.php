<?php

namespace App\Mail\Bid;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

use App\Traits\BidNoticeTrait;

class BuyerDeclinedBidNotice extends Mailable
{
    use Queueable, SerializesModels, BidNoticeTrait;

    /**
     * Email subject.
     *
     */
    protected $bid_notice_subject = 'Your bid was declined by seller';

    /**
     * Email template.
     *
     */
    protected $bid_notice_template = 'emails.buyer_declined_bid_notice';
}
