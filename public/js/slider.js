var lotSlider = class {
    constructor(sliderId, navId) {
        this.setDefaultVars();
        this.setSliderId(sliderId);

        if(this.checkNessesarySlider()){
            this.initSlider(sliderId, navId);
        }
    }

    setDefaultVars() {
        this.activeItem = 'active';
        this.leftItem = 'left-item';
        this.rightItem = 'right-item';
        this.classItem = '.item';
        this.classViewport = '.viewport';
        this.cloneItem = 'item-clone';
        this.cloneItemPrev = 'clone-prev';
        this.cloneItemNext = 'clone-next';
        this.slideId = 'slide-';
        this.dataSlideId = 'slideid';
    }

    initSlider(sliderId, navId) {
        this.createClones();

        this.initNav(navId);

        this.next();
        this.prev();
    }

    next(){
        $(this.sliderId + ' .next').click(this, function(e){
            e.preventDefault();

            e.data.prevImage.removeClass(e.data.leftItem);
            e.data.activeImage.removeClass(e.data.activeItem).addClass(e.data.leftItem);
            e.data.nextImage.removeClass(e.data.rightItem).addClass(e.data.activeItem);

            e.data.checkLoop('right');

            e.data.setNextImage();

            e.data.toggleNavItem();
        })
    }

    prev() {
        $(this.sliderId + ' .prev').click(this, function(e){
            e.preventDefault();

            e.data.nextImage.removeClass(e.data.rightItem);
            e.data.activeImage.removeClass(e.data.activeItem).addClass(e.data.rightItem);
            e.data.prevImage.removeClass(e.data.leftItem).addClass(e.data.activeItem);

            e.data.checkLoop('left');

            e.data.setPrevImage();

            e.data.toggleNavItem();
        })
    }

    setSliderId(siderId) {
        if(siderId == null || siderId.length < 1) {
            throw "Slider initialized with null!";
        }

        this.sliderId = '#' + siderId;
    }

    createClones() {
        $(this.sliderId +' '+ this.classItem + ':not(.'+ this.cloneItemPrev +')')
            .clone()
            .prependTo(this.classViewport)
            .removeClass(this.activeItem)
            .removeClass(this.leftItem)
            .removeClass(this.rightItem)
            .addClass(this.cloneItemPrev)
            .addClass(this.cloneItem);

        $(this.sliderId +' '+ this.classItem + ':not(.'+ this.cloneItemPrev +')')
            .clone()
            .appendTo(this.classViewport)
            .removeClass(this.activeItem)
            .removeClass(this.leftItem)
            .removeClass(this.rightItem)
            .addClass(this.cloneItemNext)
            .addClass(this.cloneItem);

        $(this.sliderId + ' .' + this.leftItem).remove();

        this.setPrevImage();

    }

    get activeImage() {
        return $(this.sliderId).find('.'+this.activeItem).first();
    }

    get prevImage() {
        return $(this.sliderId).find('.'+this.leftItem).first();
    }

    get nextImage() {
        return $(this.sliderId).find('.'+this.rightItem).first() || '.'+this.activeImage.next();
    }

    setNextImage() {
        $(this.sliderId).find('.'+this.rightItem).removeClass(this.rightItem);

        $(this.activeImage).next().addClass(this.rightItem);
    }

    setPrevImage() {
        $(this.sliderId).find('.'+this.leftItem).removeClass(this.leftItem);

        $(this.activeImage).prev().addClass(this.leftItem);
    }

    checkLoop(direct) {
        if(this.ifActiveImgLast(direct)){
            this.moveImgs(direct);
        }
    }

    ifActiveImgLast(direct) {
        if(direct === 'left') {
            return $(this.activeImage).prevAll().length <= 1
        }else{
            return $(this.activeImage).nextAll().length <= 1
        }
    }

    moveImgs(direct) {
        let currentImgsClass = this.classItem;

        if($(this.activeImage).hasClass(this.cloneItemNext)){
            currentImgsClass += '.' + this.cloneItemPrev;
        }else{
            currentImgsClass += '.' + this.cloneItemNext;
        }

        if(direct === 'left'){
            $(this.sliderId + ' ' + this.classViewport).prepend($(this.sliderId + ' ' + currentImgsClass));
        }else{
            $(this.sliderId + ' ' + this.classViewport).append($(this.sliderId + ' ' + currentImgsClass));
        }
    }

    toggleImage(slideId){
        $(this.sliderId + ' .' + this.rightItem).removeClass(this.rightItem);
        $(this.sliderId + ' .' + this.leftItem).removeClass(this.leftItem);
        $(this.sliderId + ' .' + this.activeItem).removeClass(this.activeItem);

        let activeImg = $(this.sliderId + ' .' + this.slideId + slideId).eq(1);
        $(activeImg).addClass(this.activeItem);
        $(activeImg).next().addClass(this.rightItem);
        $(activeImg).prev().addClass(this.leftItem);
    }

    checkNessesarySlider() {
        if($(this.sliderId + ' ' + this.classItem).length > 1){
            return true;
        }
    }

    // ---- nav methods

    initNav(navId){
        if(navId !== undefined) {
            this.navId = '#' + navId;
            this.navItem = 'slider-nav-item';
            this.activeNavItem = 'active';
        }

        this.initNavItems();
    }

    initNavItems() {
        $(this.navId + ' .' + this.navItem).first().addClass(this.activeNavItem);

        $(this.navId + ' .' + this.navItem).click(this, function(e){
            e.data.actionNavItem(e.currentTarget);
        });
    }

    actionNavItem(elem){
        this.toggleImage(elem.dataset[this.dataSlideId]);

        this.toggleNavItem(elem);
    }

    toggleNavItem(slide) {
        if(!this.navId) return;

        $(this.navId + ' .' + this.activeNavItem).first().removeClass(this.activeNavItem);

        if(typeof slide === "undefined") {
            slide = $(this.activeImage).data(this.dataSlideId);
            $(this.navId + ' .' + this.slideId + slide).first().addClass(this.activeNavItem);
        }else if(typeof slide === "object") {
            $(slide).addClass(this.activeNavItem);
        }
    }
};