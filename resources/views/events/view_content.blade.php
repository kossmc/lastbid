
    <div class="page-wrap">
        <div class="article-page clearfix">
            <div class="image-post" style="background-image:url('{{asset($event->getHeaderImagePath(1668,338))}}');
                    "></div>
            <div class="wrap clearfix">
                <div class="content-post">
                    <div class="title-page">
                        <h3>{{$event->title}}</h3>
                    </div>
                    <div class="status-wrap clearfix">
                        @if($event->event_date)
                            <div class="item-status icon-time">{{$event->event_date->format('g:i a')}}</div>
                        @endif
                        @if($event->location)
                            <div class="item-status icon-place">{{$event->location}}</div>
                        @endif
                    </div>
                    @if($event->event_date)
                    <div class="data">{{$event->event_date->format('j F Y')}}</div>
                    @endif
                    @isset($event->partners)
                        @if ($event->partners->isNotEmpty())
                            <img class="partner-logo" src="{{asset('uploads/'.$event->partners->first()->image)}}" alt="">
                        @endif
                    @endisset
                    <div class="article">
                        {!! $event->body !!}
                    </div>
                </div>
                <div class="sidebar-post sidebar-post--blog">
                    <div class="title"><h3>Upcoming Events</h3>
                        <a href="{{url('events')}}" class="view">view all events</a></div>
                    <div class="row-post">
                        @foreach($events as $event)
                        <div class="item clearfix">
                            <div class="image"><a href="{{ $event->url }}"><img src="{{$event->getImagePath(90,60)}}"
                                                alt="{{$event->title }}"></a></div>
                            <div class="desc-wrap">
                                @if($event->event_date)
                                    <div class="data">{{$event->event_date->format('j F Y')}}</div>
                                @endif
                                <div class="status-wrap clearfix">
                                    @if($event->event_date)
                                        <div class="item-status">{{$event->event_date->format('g:i a')}}</div>
                                    @endif
                                    @if($event->location)
                                        <div class="item-status">{{$event->location}}</div>
                                    @endif
                                </div>
                                <div class="desc"><a href="{{ $event->url }}">{{ $event->title }}</a></div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
