<div class="item">
    <div class="image">
        <div class="vertical-middle">
            <a href="{{ $lot->url }}">
                <img src="{{$lot->getImagePath(225,225)}}" alt="">
            </a>
        </div>
    </div>
    <div class="desc-wrap">
        <div class="price">{!! format_price($lot->range_from, $lot->range_to, $lot->currency) !!}</div>
        <div class="desc">
            <a href="{{ $lot->url }}">{{ str_limit($lot->title, $limit = 80, $end = ' ...') }}</a>
        </div>
    </div>
</div>
