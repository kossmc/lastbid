#!/usr/bin/env bash

# Contains all arguments that are passed
NODE_ARG=($@)

# Number of arguments that are given
NUMBER_OF_ARG=${#NODE_ARG[@]}

# Prepare the variables for installing specific Nodejs version and Global Node Packages
if [[ $NUMBER_OF_ARG -gt 1 ]]; then
    # Nodejs version and Global Node Packages are given
    NODEJS_VERSION=${NODE_ARG[0]}
    NODE_PACKAGES=${NODE_ARG[@]:1}
elif [[ $NUMBER_OF_ARG -eq 1 ]]; then
    # Only Nodejs version is given
    NODEJS_VERSION=${NODE_ARG[0]}
else
    # Default Nodejs version when nothing is given
    NODEJS_VERSION=8.x
fi

echo ">>> Installing Node.js version " $NODEJS_VERSION

curl -sL https://deb.nodesource.com/setup_$NODEJS_VERSION | sudo -E bash - > /dev/null 2>&1
sudo apt-get install -y nodejs > /dev/null 2>&1

if [[ ! -z $NODE_PACKAGES ]]; then
    echo ">>> Start installing Global Node Packages"

    sudo npm install -g ${NODE_PACKAGES[@]} > /dev/null 2>&1
fi

sudo chmod 777 -R /usr/lib/node_modules/
