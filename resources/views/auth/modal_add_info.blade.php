<?php
$countries = [ "USA", "Albania", "Algeria", "American Samoa", "Angola", "Anguilla", "Antartica", "Antigua and Barbuda", "Argentina", "Armenia", "Aruba", "Ashmore and Cartier Island", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegovina", "Botswana", "Brazil", "British Virgin Islands", "Brunei", "Bulgaria", "Burkina Faso", "Burma", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Christmas Island", "Clipperton Island", "Cocos (Keeling) Islands", "Colombia", "Comoros", "Congo, Democratic Republic of the", "Congo, Republic of the", "Cook Islands", "Costa Rica", "Cote d'Ivoire", "Croatia", "Cuba", "Cyprus", "Czeck Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Europa Island", "Falkland Islands (Islas Malvinas)", "Faroe Islands", "Fiji", "Finland", "France", "French Guiana", "French Polynesia", "French Southern and Antarctic Lands", "Gabon", "Gambia, The", "Gaza Strip", "Georgia", "Germany", "Ghana", "Gibraltar", "Glorioso Islands", "Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guernsey", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Heard Island and McDonald Islands", "Holy See (Vatican City)", "Honduras", "Hong Kong", "Howland Island", "Hungary", "Iceland", "India", "Indonesia", "Iran", "Iraq", "Ireland", "Ireland, Northern", "Israel", "Italy", "Jamaica", "Jan Mayen", "Japan", "Jarvis Island", "Jersey", "Johnston Atoll", "Jordan", "Juan de Nova Island", "Kazakhstan", "Kenya", "Kiribati", "Korea, North", "Korea, South", "Kuwait", "Kyrgyzstan", "Laos", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia, Former Yugoslav Republic of", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Man, Isle of", "Marshall Islands", "Martinique", "Mauritania", "Mauritius", "Mayotte", "Mexico", "Micronesia, Federated States of", "Midway Islands", "Moldova", "Monaco", "Mongolia", "Montserrat", "Morocco", "Mozambique", "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcaim Islands", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romainia", "Russia", "Rwanda", "Saint Helena", "Saint Kitts and Nevis", "Saint Lucia", "Saint Pierre and Miquelon", "Saint Vincent and the Grenadines", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Scotland", "Senegal", "Seychelles", "Sierra Leone", "Singapore", "Slovakia", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Georgia and South Sandwich Islands", "Spain", "Spratly Islands", "Sri Lanka", "Sudan", "Suriname", "Svalbard", "Swaziland", "Sweden", "Switzerland", "Syria", "Taiwan", "Tajikistan", "Tanzania", "Thailand", "Tobago", "Toga", "Tokelau", "Tonga", "Trinidad", "Tunisia", "Turkey", "Turkmenistan", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "Uruguay", "Afghanistan", "Uzbekistan", "Vanuatu", "Venezuela", "Vietnam", "Virgin Islands", "Wales", "Wallis and Futuna", "West Bank", "Western Sahara", "Yemen", "Yugoslavia", "Zambia", "Zimbabwe"] ;
?>

<div class="modal fade" id="modal-id">
    <div class="container-form-wrap log-in form-second-step-register">
        <a href="#" class="esc" data-dismiss="modal" aria-hidden="true">
            <em class="icon-esc_icon"></em>
            esc
        </a>
        <div class="container-forms">
            <form action="" id="register-form" class="second-step-register">
                <div class="container-form">
                    <div class="form-item sign-up">
                        <div class="table">
                            <div class="table-cell">
                                <div class="f-sign text-center">
                                    <div class="title">confirm your account</div>

                                    @if (!empty($message))
                                        <div class="row">
                                            <span class="error-notification"
                                                  style="width: 400px; margin: 0 auto;">{{$message}}</span>
                                        </div>
                                    @endif

                                    <div class="row">
                                        <p class="second-step-register-title">Your Credit Card is used to validate your account. We do not store your Credit Card data</p>
                                    </div>

                                    <div id="auth-tab2">                                       

                                        <div class="sign-up-title-controll">
                                            <label>Credit Card info</label>
                                        </div>

                                        <div class="row sign-up-full-controll form-controll">
                                            <div class="inp-wrap">
                                                <input type="text" placeholder="Name on the Card" autofocus
                                                    name="card_name"
                                                    value="{{old('card_name')}}"
                                                    class="{{ $errors->has('card_name') ? 'has-error--bgcolor' : '' }}">
                                                @if ($errors->has('card_name'))
                                                    <span class="help-block">
                                                    <strong>{{ $errors->first('card_name') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="row sign-up-full-controll form-controll">
                                            <div class="inp-wrap">
                                                <input type="text" placeholder="Card number" name="cc"
                                                        class="ccFormatMonitor{{ $errors->has('cc') ? '
                                                           has-error--bgcolor' :'' }}"
                                                        value="{{old('cc')}}" maxlength="19" required >
                                                @if ($errors->has('cc'))
                                                    <span class="help-block">
                                                    <strong>{{ $errors->first('cc') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="row sign-up-three-control form-controll">
                                            <div class="inp-wrap-1">
                                                <div class="select-style" id="card_month_temp">
                                                    <span class="card_month">{{old('month', 'Exp. month')}}</span>
                                                    <ul class="select-month" style="display: none;">
                                                        <li>January</li>
                                                        <li>February</li>
                                                        <li>March</li>
                                                        <li>April</li>
                                                        <li>May</li>
                                                        <li>June</li>
                                                        <li>July</li>
                                                        <li>August</li>
                                                        <li>September</li>
                                                        <li>October</li>
                                                        <li>November</li>
                                                        <li>December</li>
                                                    </ul>
                                                    <select name="month" style="display: none;">
                                                        <option value="{{old('month', 'Exp. month')}}">{{old('month', 'Exp. month')}}</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="inp-wrap-2">
                                                <div class="select-style" id="card_year_temp">
                                                    <span class="card_year">{{old('year', 'Exp. year')}}</span>
                                                    <ul class="select-year" style="display: none;">
                                                        @php($year =\Carbon\Carbon::now())
                                                        @for ($i = 0; $i < 10; $i++)
                                                            <li>{{$year->year}}</li>
                                                            @php($year = $year->addYear())
                                                        @endfor
                                                    </ul>
                                                    <select name="year" style="display: none;">
                                                        <option value="{{old('year', 'Exp. year')}}">{{old('year', 'Exp. year')}}</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="inp-wrap-3">
                                                <input type="password" placeholder="CVV" name="card_cvv"
                                                    value="{{old('card_cvv')}}" maxlength="4" required 
                                                   class="{{ $errors->has('card_cvv') ? 'has-error--bgcolor' : '' }}">
                                            </div>
                                        </div>

                                        <div class="sign-up-title-controll ">
                                            <label>Billing address</label>
                                        </div>

                                        <div class="row sign-up-full-controll form-controll">
                                            <div class="inp-wrap">
                                                <input type="text" placeholder="Address" name="address"
                                                        value="{{old('address')}}" required 
                                                       class="{{ $errors->has('address') ? 'has-error--bgcolor' : '' }}">
                                                @if ($errors->has('address'))
                                                   <span class="help-block">
                                                    <strong>{{ $errors->first('address') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="row sign-up-full-controll form-controll">
                                            <div class="inp-wrap inp-wrap-half">
                                                <input type="text" placeholder="City" name="city"
                                                        value="{{old('city')}}" required 
                                                       class="{{ $errors->has('city') ? 'has-error--bgcolor' : '' }}">
                                                @if ($errors->has('city'))
                                                    <span class="help-block">
                                                    <strong>{{ $errors->first('city') }}</strong>
                                                </span>
                                                @endif
                                            </div>

                                            <div class="inp-wrap inp-wrap-half">
                                                <input type="text" placeholder="State / province" name="state"
                                                        value="{{old('state')}}" >
                                            </div>
                                        </div>

                                        {{--<div class="row sign-up-full-controll">--}}
                                            {{--<div class="inp-wrap inp-wrap-half">--}}
                                                {{--<input type="text" placeholder="Postal code / zip" name="zip"--}}
                                                        {{--value="{{old('zip')}}" >--}}
                                            {{--</div>--}}

                                            {{--<div class="inp-wrap inp-wrap-half">--}}
                                                {{--<input type="text" placeholder="Country" name="country"--}}
                                                        {{--value="{{old('country')}}" required --}}
                                                       {{--class="{{ $errors->has('country') ? 'has-error--bgcolor' : '' }}">--}}
                                                {{--@if ($errors->has('country'))--}}
                                                    {{--<span class="help-block">--}}
                                                    {{--<strong>{{ $errors->first('country') }}</strong>--}}
                                                {{--</span>--}}
                                                {{--@endif--}}
                                            {{--</div>--}}
                                        {{--</div>--}}

                                        <div class="row sign-up-full-controll form-controll">
                                            <div class="inp-wrap inp-wrap-half">
                                                <input type="text" placeholder="Postal code / zip" name="zip" value="{{old('zip')}}">
                                                {{--<div class="has-error">*Please enter your credit card's Zip/Postal Code</div>--}}
                                            </div>

                                            <div class="inp-wrap inp-wrap-half">
                                                <div class="select-style" id="country">
                                                    <span class="country up-down-arrow">Country</span>

                                                    <ul class="select-country" style="display: none;">
                                                        @foreach($countries as $id => $coutry)
                                                            <li data-id="{{ $id+1 }}"> {{ $coutry }} </li>
                                                        @endforeach
                                                    </ul>

                                                    <select name="country" id="country" style="display: none;">
                                                        <option value=""></option>
                                                    </select>
                                                </div>

                                                {{--<div class="has-error">*Please enter your credit card's Zip/Postal Code</div>--}}
                                            </div>
                                        </div>

                                        {{--<div class="row sign-up-title-controll">--}}
                                            {{--<p>Credit Card validation is required in order to place a--}}
                                                {{--bid</p>--}}
                                        {{--</div>--}}

                                        <div class="row buttons-row">
                                            <div class="btn-wrap">
                                                @if(isset($type) && $type == 'register')
                                                <a type="button" class="btn2" onclick="closeModal()">skip</a>
                                                @endif
                                                <a type="submit" class="btn" disabled="disabled"
                                                    onclick="auth('add_info')">confirm
                                                </a>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
  (function(){

    var select = $('#card_year_temp');
    var select_ul = select.find('ul');
    var selct_opt = select.find('option');

    select.find('span').on('click', function(){
      $(this).toggleClass('up-down-arrow');
      if(select_ul.is(':hidden')){
        select_ul.show();
      }
      else{
        select_ul.hide();
      }
    });
    select_ul.find('li').on('click', function(){
      select.find('span').toggleClass('up-down-arrow');
      if($(this).index() != 0){
        select.find('span').addClass('selected-suffix');
      }
      else{
        select.find('span').removeClass('selected-suffix');
      }
      select.find('span').text($(this).text());
      selct_opt.attr('value', $(this).text());
      selct_opt.text($(this).text());
      select_ul.hide();
    });


  }());

  (function(){

    var select = $('#card_month_temp');
    var select_ul = select.find('ul');
    var selct_opt = select.find('option');

    select.find('span').on('click', function(){
      $(this).toggleClass('up-down-arrow');
      if(select_ul.is(':hidden')){
        select_ul.show();
      }
      else{
        select_ul.hide();
      }
    });
    select_ul.find('li').on('click', function(){
      select.find('span').toggleClass('up-down-arrow');
      if($(this).index() != 0){
        select.find('span').addClass('selected-suffix');
      }
      else{
        select.find('span').removeClass('selected-suffix');
      }
      select.find('span').text($(this).text());
      selct_opt.attr('value', $(this).text());
      selct_opt.text($(this).text());
      select_ul.hide();
    });
  }());
  (function(){

    var select = $('#country');
    var select_ul = select.find('ul');
    var selct_opt = select.find('option');
    select.find('span').on('click', function(){
      $(this).toggleClass('up-down-arrow');
      if(select_ul.is(':hidden')){
        select_ul.show();
      }
      else{
        select_ul.hide();
      }
    });
    select_ul.find('li').on('click', function(){

      select.find('span').toggleClass('up-down-arrow');
      if($(this).index() != 0){
        select.find('span').addClass('selected-suffix');
      }
      else{
        select.find('span').removeClass('selected-suffix');
      }
      select.find('span').text($(this).text());

      selct_opt.attr('value', $(this).text());
      selct_opt.attr('data-id', this.dataset['id']);

      selct_opt.text($(this).text());
      select_ul.hide();
    });
  }());
</script>
