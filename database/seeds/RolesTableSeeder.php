<?php

use Illuminate\Database\Seeder;
use Backpack\PermissionManager\app\Models\Role;

class RolesTableSeeder extends Seeder
{
    public function run()
    {
        $roles = ['admin', 'seller', 'user'];
        foreach ($roles as $role) {
            $r = new Role(['name' => $role ]);
            $r->save();
        }
    }
}
