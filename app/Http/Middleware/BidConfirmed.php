<?php

namespace App\Http\Middleware;

use App\Models\TermsConditions;
use Closure;
use App\Repositories\BidRepository;

class BidConfirmed
{
    /**
     * The bid repository implementation.
     *
     * @var BidRepository
     */
    private $bid;

    /**
     * Create a new middleware instance.
     *
     * @param BidRepository $bid
     * @return void
     */
    public function __construct(BidRepository $bid)
    {
        $this->bid = $bid;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->exists(['amount', 'message', 'confirmed']) && $request->confirmed) {
            return $next($request);
        } else {
            $amount = $request->has('amount') ? $request->amount : 0;
            $lot = $request->lot;
            $terms = \DB::table('terms_conditions')->where('alias', request('alias'))->first();
            $defaultTerms = \DB::table('site_terms_conditions')->first()->text;
            return response()->json([
                'status' => false,
                'form' => view()
                    ->make('inc.modal_confirm_bid', compact(['amount', 'lot']))
                    ->with(['terms'=> $terms ? $terms->text : $defaultTerms])
                    ->render()
            ]);
    }
}
}
