<div id="letsTalk" class="modal">
    <div class="modal-content">
        <span class="close" id="letsTalk_close">&times;</span>
        <h2 id="lets-talk-header"></h2>
        <p id="lets-talk-notice"></p>
        <div class="round">
            <span>&#10003;</span>
        </div>
    </div>
</div>