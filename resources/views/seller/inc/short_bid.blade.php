@php ($attempt = $bid->attempts->first())
<li>
    <a data-toggle="tab" href="#tab-person{!! $bid->user_id !!}">
        <div class="bid-owner">
            <span class="name">{{ $bid->buyer->short_name }}</span>
            <span class="location">{{ $bid->buyer->address }}</span>
        </div>
        <div class="bid-price price{{ !($attempt->is_pending || $attempt->is_accepted) ? ' crossed' : '' }}">{!!
        format_price($attempt->amount, null, $bid->lot->currency) !!}</div>
        <div class="bid-expire">
            @if ($attempt->is_declined)
                <span class="not-available {{ $attempt->status }}">Declined</span>
            @elseif ($attempt->is_expired)
                <span class="not-available {{ $attempt->status }}">Expired</span>
            @elseif ($attempt->is_accepted)
                <span class="not-available {{ $attempt->status }}">Accepted</span>
            @else
                <div class="time-wrap-line">
                    <div class="time-line">
                        <div class="time-line-left" style="width:{{ percent_time_left($attempt->start_date, $attempt->end_date) }}%;"></div>
                    </div>
                    <span class="time-text">Bid expires in: <b>{{ format_time_left($attempt->end_date) }}</b></span>
                </div>
            @endif
        </div>
    </a>
</li>
