<?php

namespace App\Services;

use Carbon\Carbon;

/**
 * Class LyonTurnbullCrawler
 * @package App\Services
 */
class LyonTurnbullCrawler extends BaseCrawler
{
    public $availableDomains = [
        'http://auctions.lyonandturnbull.com/',
        'http://auctions.freemansauction.com/'
    ];

    public $baseUrl = '';

    public $categories = [];

    /**
     * LyonTurnbullCrawler constructor.
     * @param $batch
     */
    public function __construct($batch)
    {
        $this->filename = 'Lyon_' . $batch->input . '_' . date('Y-m-d_H-i-s') . '.csv';
        parent::__construct($batch);
    }

    /**
     * @return array|bool
     */
    public function startCrawling()
    {
        if (!$this->prepareUrl()) {
            return false;
        }

        $response = $this->client->request('GET', $this->getUrl());
        if ($response->getStatusCode() != 200) {
            \Log::info(print_r($response, true));
            return false;
        }

        $content = $this->processContent(
            $response->getBody()->getContents()
        );

        $this->getCategories($content);
        $auctionDate = $this->getStartDate($content);
        $startDate = $auctionDate->format('Y-m-d H:i:s');
        $endDate = $auctionDate->addMonth()->format('Y-m-d H:i:s');

        $lots = [];

        foreach ($this->categories as $category) {

            $pagesCount = ceil($category['count'] / 120);

            $page = 1;

            do {
                $response = $this->client->request('GET', $this->getUrl($page, $category['id']));

                if ($response->getStatusCode() != 200){
                    \Log::info(print_r($response, true));
                    return false;
                }

                $content = $this->processContent(
                    $response->getBody()->getContents()
                );

                $lotLinks = $this->getLots($content);

                foreach ($lotLinks as $lotLink) {

                    $response = $this->client->request('GET', $lotLink['link']);

                    if ($response->getStatusCode() != 200) {
                        \Log::info(print_r($response, true));
                        return false;
                    }


                    $content = $this->processContent(
                        $response->getBody()->getContents()
                    );

                    if ($lot = $this->getLotInfo($content)) {

                        $photos = [];
                        foreach ($lot['photos'] as $photoUrl) {
                            $photos[] = $this->loadImage($photoUrl);
                        }

                        $lots[] = [
                            'Title' => $lotLink['title'],
                            'Description' => $lot['description'],
                            'Category' => $category['title'],
                            'Sub Category' => '',
                            'Partner' => $this->partner_id,
                            'Shipping description' => '',
                            'Auction House Fee' => 20,
                            'Minimum bid amount' => isset($lot['price_from']) ? $lot['price_from'] * 0.5 : 0,
                            'Price range from' => isset($lot['price_from']) ? $lot['price_from'] : 0,
                            'Price range to' => isset($lot['price_to']) ? $lot['price_to'] : 0,
                            'Price to auto-accept bid' => isset($lot['price_from']) ? $lot['price_from'] * 0.7 : 0,
                            'Start date' => $startDate,
                            'End date' => $endDate,
                            'Seller' => $this->seller_id,
                            'Currency' => $lot['currency'],
                            'Lot photos' => implode(',', $photos)
                        ];

                    }
                }


                $page++;

            } while ($pagesCount > $page);
        }

        if (!empty($lots)) {
            $this->headers = array_keys($lots[0]);
        }

        return $lots;
    }

    /**
     * @param int $page
     * @param null $categoryId
     * @return string
     */
    public function getUrl($page = 1, $categoryId = null)
    {
        $url = $this->url . '&pg=' . $page;
        if (!empty($categoryId)) {
            $url .= '&department=' . $categoryId;
        }
        return $url;
    }

    /**
     * @return bool
     */
    public function prepareUrl()
    {
        if (empty($this->baseUrl)) {
            foreach ($this->availableDomains as $domain) {
                $url = $domain . 'asp/searchresults.asp?ps=120&st=D&sale_no=' . $this->batch->input;
                $response = $this->client->request('GET', $url);
                if ($response->getStatusCode() == 200) {
                    $content = $this->processContent(
                        $response->getBody()->getContents()
                    );
                    if ($this->isRightDomain($content)) {
                        $this->baseUrl = $domain;
                        break;
                    }
                }
            }
        }
        if (!empty($this->baseUrl)) {
            $this->url = $this->baseUrl . 'asp/searchresults.asp?ps=120&st=D&sale_no=' . $this->batch->input;
            return true;
        }
        return false;
    }

    public function getCategories($content)
    {
        if (preg_match_all('/<input type="checkbox" id="departments" value="([^"]+)" name="department" \/>\&nbsp;(.+?)\(([0-9]+)\)<\/p>/ui', $content, $matches)) {
            foreach ($matches[1] as $i => $categoryId) {
                $this->categories[] = [
                    'id' => str_replace(' ', '+', $categoryId),
                    'title' => trim($matches[2][$i]),
                    'count' => trim($matches[3][$i])
                ];
            }
        }
    }

    public function getLots($content)
    {
        $lotLinks = [];
        if (preg_match_all('/<div class="lot-image"><a href="([^"]+)"><img.+?title="([^"]+)".+?<div class="lot-meta">.+?<div class="lot-artist">.+?<\/div><br\/>([^<]+)<br \/>/ui', $content, $matches)) {
            foreach ($matches[1] as $i => $link) {
                $status = trim($matches[3][$i]);
                if ($status == 'Unsold') {
                    $lotLinks[] = [
                        'link' => str_replace('https://', 'http://', trim($link)),
                        'title' => trim($matches[2][$i])
                    ];
                }
            }
        }
        return $lotLinks;
    }

    public function isRightDomain($content)
    {
        return !preg_match('/Catalogue coming soon. To find out more about this auction please contact the specialist department./ui', $content);
    }

    public function getStartDate($content)
    {
        if (preg_match('/<div class="summary-content">.+?<h5>.+?<\/h5>.+?<p>.+?<br><b><\/b>([^<]+)<br/ui', $content, $matches)) {
            return Carbon::parse($matches[1]);
        }
        return Carbon::now();
    }

    public function getLotInfo($content)
    {
        $lot = [];
        if (preg_match('/<div class="lot_detail_description">(.+?)<\/div/ui', $content, $matches)) {
            $lot['currency'] = 'usd';
            $description = $matches[1];
            if (preg_match('/<b>Estimate (.+?)<\/b>/ui', $description, $matches)) {
                $priceRange = $matches[1];
                if (substr_count($priceRange, '£') > 0) {
                    $lot['currency'] = 'gbp';
                }
                // remove estimate from description
                $description = str_replace($matches[0], '', $description);
                list($from, $to) = explode('-', trim($priceRange));
                $from = preg_replace('/[^0-9]/', '', $from);
                $to = preg_replace('/[^0-9]/', '', $to);
                $lot['price_from'] = $from;
                $lot['price_to'] = $to;
            }
            // remove links from description
            if (preg_match_all('/<a .+?<\/a>/ui', $description, $matches)) {
                foreach ($matches[0] as $link) {
                    $description = str_replace($link, '', $description);
                }
            }
            $description = preg_replace('/<br>.+?Unsold/ui', '', $description);
            $lot['description'] = '<p>' . $description . '</p>';
            $lot['photos'] = [];

            if (preg_match_all('/src="(\/Full\/[^"]+)"/ui', $content, $matches)) {
                foreach ($matches[1] as $imageUrl) {
                    $lot['photos'][] = rtrim($this->baseUrl, '/') . trim($imageUrl);
                }
            }

            if (preg_match_all('/href="(https:\/\/auctions[^"]+\/Full\/[^"]+)"/ui', $content, $matches)) {
                foreach ($matches[1] as $imageUrl) {
                    $lot['photos'][] = str_replace('https://', 'http://', trim($imageUrl));
                }
            }
            if (preg_match_all("/href='(https:\/\/auctions[^']+\/Full\/[^']+)'/ui", $content, $matches)) {
                foreach ($matches[1] as $imageUrl) {
                    $lot['photos'][] = str_replace('https://', 'http://', trim($imageUrl));
                }
            }
            if (preg_match_all('/data-zoom-image="(https:\/\/auctions[^"]+\/Full\/[^"]+)"/ui', $content, $matches)) {
                foreach ($matches[1] as $imageUrl) {
                    $lot['photos'][] = str_replace('https://', 'http://', trim($imageUrl));
                }
            }

            return $lot;
        }
        return false;
    }
}
