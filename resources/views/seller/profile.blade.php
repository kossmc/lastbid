@extends('layouts.inner')
@section('title', 'Account info')
@php
    $account_routes = ['seller-account-info'];
    $payments_routes = ['seller-payment-methods','seller-payment-methods-bank-add'];
@endphp
@section('inner_content')
    @include('inc.flash_message')
    <ul class="nav nav-tabs profile-tabs-nav lots-tabs-nav">
        <li{!! areActiveRoutes($account_routes) ?' class="active"' : '' !!}>
            <a href="{{ route('seller-account-info') }}">account info</a></li>
        <li{!! areActiveRoutes($payments_routes) ? ' class="active"' : '' !!}>
            <a href="{{ route('seller-payment-methods') }}">bank accounts</a></li>
    </ul>
    <div class="tab-content profile-tab-content">
        <div class="tab-pane fade in active">
            @yield('profile_content')
        </div>
    </div>
@endsection
