<?php

namespace App\Listeners\User;

use App\Events\User\SellerAddNewBankAccount;
use Illuminate\Contracts\Queue\ShouldQueue;

class SellerAddNewBankAccountListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SellerAddNewBankAccount $event
     * @return void
     */
    public function handle(SellerAddNewBankAccount $event)
    {
        //
    }
}
