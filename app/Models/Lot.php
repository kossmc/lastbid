<?php

namespace App\Models;

use App\Models\Bid;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Backpack\CRUD\CrudTrait;
use Illuminate\Support\HtmlString;
use Carbon\Carbon;
use PDF;

/**
 * Class Lot
 *
 * @property int $id
 *
 * @package App\Models
 */
class Lot extends Model
{
    use Sluggable;
    use CrudTrait;

    const STATUS_ACTIVE = 'active';
    const STATUS_EXPIRED = 'expired';
    const STATUS_CLOSED = 'closed';
    const STATUS_SOLD = 'sold';
    const STATUS_SOLD_OUTSIDE = 'sold_outside';
    const BUYER_INVOICE_PATH = 'app/invoices/buyer/';
    const CURRENCY = ['usd' => 'USD', 'gbp' => 'GBP', 'eur' => 'EUR'];
    /*
   |--------------------------------------------------------------------------
   | GLOBAL VARIABLES
   |--------------------------------------------------------------------------
   */
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'lots';
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
//    protected $fillable = [
//        'title', 'subtitle', 'slug', 'category_id', 'description',
//        'min_bid_amount', 'shipping_description', 'start_date',
//        'end_date', 'photos', 'partner_id', 'user_id',
//        'range_from', 'range_to', 'status', 'specifics_data',
//        'auction_house_fee','sold_amount','buyer_id','sold_date','auto_accept_price', 'popular'
//    ];

    protected $guarded = [
        'clear_photos'
    ];
    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */

    protected $appends = ['url', 'image_path'];

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title',
                'maxLength' => 255,
            ]
        ];
    }

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }
    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */

    protected $casts = [
        'min_bid_amount' => 'float',
        'photos' => 'array'
    ];

    protected $dates = ['start_date', 'end_date', 'sold_date'];

    public static function boot()
    {
        parent::boot();
        static::deleting(function ($obj) {
            if (count((array)$obj->photos)) {
                foreach ($obj->photos as $file_path) {
                    if (is_string($file_path)) {
                        \Storage::disk('uploads')->delete($file_path);
                    }
                }
            }
        });
    }

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public function getImagePath($width = null, $height = null)
    {
        // @todo: put files to different folders
        if (is_array($this->photos)) {
            $photos = $this->photos;
            $photo = reset($photos);
            if ( ! empty($photo) && is_string($photo)) {
                return ($width && $height) ? asset("{$width}x{$height}/uploads/" . $photo) :
                    asset('uploads/' . $photo);
            }
        }
        return asset('img/coming-soon.jpg');
    }
    public function bidIsAboveMinAmount($amount = 0)
    {
        return round($this->min_bid_amount) <= $amount;
    }

    public function bidIsBelowPriceTo($amount = 0)
    {
        return round($this->range_to) >= $amount;
    }

    public function bidIsAutoAccept($amount = 0)
    {
        return $this->auto_accept_price ? $this->auto_accept_price <= $amount : false;
    }

    public function getExpired()
    {
        return $this->active()
            ->where('end_date', '<', Carbon::now())->get();
    }

    public function expire()
    {
        $this->status = self::STATUS_EXPIRED;
        return $this->save();
    }

    public function sold($amount, $buyerId)
    {
        $this->status = self::STATUS_SOLD;
        $this->sold_amount = $amount;
        $this->buyer_id = $buyerId;
        $this->sold_date = $this->last_bid_date = Carbon::now();
        return $this->save();
    }

    public function close($diclineBids = true)
    {
        $this->status = self::STATUS_CLOSED;
        $this->save();
        if ($diclineBids) {
            $bidRepo = resolve('App\Repositories\BidRepository');
            foreach ($this->bids()->pending()->get() as $bid) {
                $bidRepo->declineBySystem($bid);
            }
        }
        return $this;
    }

    public function auctionFeeKoef()
    {
        return 1 + $this->feeKoef();
    }

    public function feeKoef()
    {
        return $this->auction_house_fee / 100;
    }

    public function bidWithoutFee($amount = 0)
    {
        return $amount - $this->bidFee($amount);
    }

    public function bidWithFee($amount = 0)
    {
        return $amount + $this->bidFee($amount);
    }

    public function bidFee($amount = 0)
    {
        return $amount * $this->auction_house_fee / 100;
    }

    public function hasEnoughtAttempts($userId) {
        if ($bid = $this->bids()->where('user_id', $userId)->first()) {
            if ($bid->attempts()->where('user_id', $userId)->count() >= env('BIDS_LIMIT')) {
                return false;
            }
        }
        return true;
    }

    public function genMetaDescription()
    {
        $titleLen = mb_strlen($this->title);
        $description = str_replace("&nbsp;", ' ', $this->description);
        return $this->title .' - '. mb_strimwidth( strip_tags($description), 0, 150-$titleLen);
    }
    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function category()
    {
        return $this->hasOne(Category::class, 'id', 'category_id');
    }

    public function bids()
    {
        return $this->hasMany(Bid::class, 'lot_id', 'id');
    }

    public function buyer()
    {
        return $this->hasOne(User::class, 'id', 'buyer_id');
    }

    // @todo: check if needed
    public function partner()
    {
        return $this->hasOne(Partner::class, 'id', 'partner_id');
    }

    // @todo: cache counter
    public function newBids()
    {
        return $this->bids()
            ->pending()
            ->byBuyer();
    }


    public function seller()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function specifics()
    {
        return $this->hasMany(Specific::class, 'category_id', 'category_id')->orderBy('order', 'DESC');
    }

    public function lot_specifics_options()
    {
        return $this->hasMany(LotSpecificOptions::class, 'lot_id', 'id');
    }

    public function messages()
    {
        return $this->hasMany(Message::class, 'lot_id', 'id');
    }
    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /**
     * Scope a query to only active lots.
     *
     * @param \Illuminate\Database\Eloquent\Builder $builder
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where('status', self::STATUS_ACTIVE);
    }

    public function scopeSystem($query)
    {
        return $query->where('system', 1);
    }

    public function scopeOriginal($query)
    {
        return $query->where('system', null)->orWhere('system', 0);
    }

    public function scopeSold($query)
    {
        return $query->where('lots.status', self::STATUS_SOLD);
    }

    public function scopeActiveOrSold($query)
    {
        return $query->whereIn('status', [self::STATUS_ACTIVE, self::STATUS_SOLD]);
    }

    public function scopeSoldOrSoldOutside($query)
    {
        return $query->whereIn('status', [self::STATUS_SOLD, self::STATUS_SOLD_OUTSIDE]);
    }

    public function scopeClosedOrExpired($query)
    {
        return $query->whereIn('status', [self::STATUS_CLOSED, self::STATUS_EXPIRED]);
    }


    public function scopeSearch($query, $q)
    {
        return $query->where(function($query) use ($q) {
            $q = $this->filterSearchString($q);

            collect([' ',',','.',''])->each(function ($v, $k) use (&$query, $q) {
                if ($k == 0) {
                    $query->where('title', 'like', '%' . $v . $q . $v . '%')
                        ->orWhere('title', 'like', '%' . $v . $q)
                        ->orWhere('title', 'like', $q . $v . '%')
                        ->orWhere('title', 'like', $q . '%')
                        ->orWhere('description', 'like', '%' . $v . $q . $v . '%')
                        ->orWhere('description', 'like', '%' . $v . $q)
                        ->orWhere('description', 'like', $q . $v . '%')
                        ->orWhere('description', 'like', $q . '%');
                } else {
                    $query->orWhere('title', 'like', '%' . $v . $q . $v . '%')
                        ->orWhere('title', 'like', '%' . $v . $q. '%')
                        ->orWhere('title', 'like', '%' .$q . $v . '%')
                        ->orWhere('description', 'like', '%' . $v . $q . $v . '%')
                        ->orWhere('description', 'like', '%' . $v . $q. '%')
                        ->orWhere('description', 'like', '%' .$q . $v . '%');
                }
            });
            $query->orWhere('id', 'like', $this->prepareQuery($q));
        });
    }

    protected function filterSearchString($string)
    {
        return str_replace('%', '\%', $string);
    }

//    public function scopeSearch($query, $q)
//    {
//        return $query->where(function($query) use ($q) {
//            $query->where(DB::raw($this->prepareTitleQuery($q)))
//                ->orWhere('id', 'like', $this->prepareQuery($q));
//        });
//    }
//
//    protected function prepareTitleQuery($query)
//    {
//        return 'title REGEXP "(?:^|\W)((?i)'.preg_replace('/[^0-9a-zA-Z]/', '', $query).')(?:$|\W)"';
//    }

    protected function prepareQuery($query)
    {
        return '%' . $this->filterSearchString($query) . '%';
    }

    public function scopePopular($query)
    {
        return $query->where('popular', 1);
    }

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /**
     * Generates an URL for lot.
     *
     * @return string
     */
    public function getUrlAttribute()
    {
        //temporary disabled
        /*return route('lot', [
            'id' => $this->id,
            'parents' => $this->getFullUri($this->category),
            'slug' => $this->slug
        ]);*/
        return route('lot', ['id' => $this->id, 'slug' => $this->slug]);
    }

    /**
     * Get full url to category contain parent categories url's
     * @param null $item
     * @return string
     */
    public function getFullUri($item = null)
    {
        if (!$item->parent) {
            return $item->slug;
        } else {
            $segments = [];
            $segments[] = $this->getFullUri($item->parent);
            $segments[] = $item->slug;
            return implode('/', $segments);
        }
    }

    public function getImagePathAttribute()
    {
        if (is_array($this->photos)) {
            $photo = reset($this->photos);
            if ( ! empty($photo) && is_string($photo)) {
                return asset('uploads/' . $photo);
            }
        }
        return asset('img/coming-soon.jpg');
    }

    public function getBuyerInvoicePathAttribute()
    {
        return storage_path(self::BUYER_INVOICE_PATH."{$this->id}.pdf");
    }

    public function getBuyerInvoiceNameAttribute()
    {
        return "invoice_{$this->id}.pdf";
    }

    public function getIsActiveAttribute()
    {
        return $this->status == self::STATUS_ACTIVE;
    }

    public function getIsSoldAttribute()
    {
        return $this->status == self::STATUS_SOLD;
    }

    public function getIsClosedAttribute()
    {
        return $this->status == self::STATUS_CLOSED;
    }

    public function getIsSoldOutsideAttribute()
    {
        return $this->status == self::STATUS_SOLD_OUTSIDE;
    }

    public function getIsExpiredAttribute()
    {
        return $this->status == self::STATUS_EXPIRED;
    }

    public function getMinBidAmountWithFeeAttribute()
    {
        return $this->min_bid_amount * $this->auctionFeeKoef();
    }

    public function getRangeFromWithFeeAttribute()
    {
        return $this->range_from * $this->auctionFeeKoef();
    }

    public function getRangeToWithFeeAttribute()
    {
        return $this->range_to * $this->auctionFeeKoef();
    }

    public function getRangeFromFeeAttribute()
    {
        return $this->range_from * $this->feeKoef();
    }

    public function getRangeToFeeAttribute()
    {
        return $this->range_to * $this->feeKoef();
    }

    public function getAuctionHouseFeeAmountAttribute()
    {
        return $this->is_sold? $this->sold_amount * $this->auction_house_fee / 100 : 0;
    }

    public function getLastbidFeeAmountAttribute()
    {
        return $this->is_sold ? $this->auction_house_fee_amount*env('LAST_BID_FEE')/100 : 0;
    }

    public function getBuyerFullNameAttribute()
    {
        return $this->is_sold? $this->buyer->full_name:'';
    }

    public function getSpecificsDataAttribute()
    {
        $this->load('lot_specifics_options', 'specifics', 'specifics.specific_options');
        $lot_options = $this->lot_specifics_options->toArray();
        $cat_options = $this->specifics
            ->map(function ($el) use ($lot_options) {
                $el->is_text ? $el->selected = '' : $el->selected = [];
                $el->edited = false;
                $el->requiredNotSelected = null;
                $el->selectedIds = [];
                return $el;
            })->toArray();
        $cat_options = array_map(function ($el) use ($lot_options) {
            $selected = array_filter($lot_options, function ($v) use ($el) {
                return $v['specific_id'] == $el['id'];
            });
            if (is_array($selected) && count($selected)) {
                foreach ($selected as $opt) {
                    if ($el['is_text']){
                        $el['selected'] = $opt['value'];
                    }
                    if ($el['is_select']){
                        $el['selected'] = $opt['specific_option_id'];
                    }
                    if ($el['is_multi_select']){
                        $el['selected'][] = $opt['specific_option_id'];
                    }
                    $el['selectedIds'][] = $opt['id'];
                }
                $el['requiredNotSelected'] = false;
            } else {
                $el['required']? $el['requiredNotSelected'] = true : $el['requiredNotSelected'] = false;
            }
            unset($el['order'], $el['type'], $el['use_in_filter'], $el['filter_type']
                , $el['created_at'], $el['updated_at']);
            return $el;
        }, $cat_options );
        $data = [
            'cat_opts' => $cat_options
        ];
        return json_encode($data);
    }

    public function getMetaDescriptionAttribute($desc)
    {
        if($desc) return $desc;

        return $this->genMetaDescription();
    }

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */

    public function setMetaDescriptionAttribute($desc)
    {
        if($desc){
            $this->attributes['meta_description'] = $desc;
        }else{
            $this->attributes['meta_description'] = $this->genMetaDescription();
        }
    }

    public function setSpecificsDataAttribute($value)
    {
        $specifics = json_decode($value, true);
        foreach ($specifics as $specific) {
            if (($specific['is_multi_select'] && count($specific['selected']) == 0)
                || ($specific['is_text'] && $specific['selected'] == ''))
            {
                /*delete all options and exit*/
                LotSpecificOptions::where(['lot_id' =>$this->id, 'specific_id' => $specific['id']])->delete();
                continue;
            }
            if ($specific['is_text']) {
                if (count($specific['selectedIds'])) {
                    $this->_editLotOption($specific['selectedIds'][0], null, $specific['selected']);
                } else {
                    $this->_newLotOption($specific['id'], null, $specific['selected']);
                }
            }
            if ($specific['is_select']) {
                if (count($specific['selectedIds'])) {
                    $this->_editLotOption($specific['selectedIds'][0], $specific['selected']);
                } else {
                    $this->_newLotOption($specific['id'], $specific['selected']);
                }
            }
            if ($specific['is_multi_select']) {
                if (count($specific['selectedIds'])) {
                    LotSpecificOptions::whereIn('id', $specific['selectedIds'])->delete();
                }
                foreach ($specific['selected'] as $opt) {
                    $this->_newLotOption($specific['id'], $opt);
                }
            }
        }
        return true;
    }

    /**
     * Save and set photos
     * @TODO resize
     * @param $value
     */
    public function setPhotosAttribute($value)
    {
        // Save from import
        if (is_array($value) && isset($value[0]) && is_string($value[0])) {
            $this->attributes['photos'] = json_encode($value);
            return;
        }

        // Upload files
        $destination_path = "lots";
        if($this->id) {
            $destination_path = "$destination_path/{$this->id}";
        }
        $this->uploadMultipleFilesToDisk($value, 'photos', 'uploads', $destination_path);
    }

    /*
    |--------------------------------------------------------------------------
    | INTERNAL FUNCTIONS
    |--------------------------------------------------------------------------
    */
    private function _newLotOption($specific, $specific_option, $value=null)
    {
        $lot_option = LotSpecificOptions::create([
                        'lot_id' => $this->id,
                        'specific_id' => $specific,
                        'specific_option_id' => $specific_option,
                        'value'=> $value
                    ]);
        return $lot_option;
    }

    private function _editLotOption($id, $specific_option=null, $value = null)
    {
        $lotSpecificOption = LotSpecificOptions::findOrFail($id);
        if ($specific_option) {
            $lotSpecificOption->specific_option_id = $specific_option;
        } else {
            $lotSpecificOption->value = $value;
        }
        return $lotSpecificOption->save();
    }

    public function getSelectedSpecifics()
    {
        if ( ! ($this->lot_specifics_options || $this->specifics)) {
            return [];
        }
        $opt = $this->lot_specifics_options;
        return $this->specifics->map(function ($spec) use ($opt) {
            if ( ! $spec->is_text) {
                $specOpts = $spec->specific_options->filter(function ($value) use ($opt) {
                    return $opt->contains('specific_option_id', $value->id);
                });
                $specOpts = implode(', ', $specOpts->pluck('name')->toArray());
            } else {
                if ($opt->contains('specific_id', $spec->id)) {
                    $specOpts = $opt->first(function ($value) use ($spec) {
                        return $value->specific_id == $spec->id;
                    });
                    $specOpts = $specOpts->value;
                }
            }
            if (isset($specOpts) && $specOpts) {
                $spec->options = $specOpts;
                return $spec;
            }
        })->reject(function ($spec) {
            return empty($spec);
        });
    }

    public function generateBuyerInvoice()
    {
        $seller = $this->seller;
        $sellerAccount = $seller->accounts()->first();
        $buyer = $this->buyer;
        $lot = $this;
        $pdf = PDF::loadView('admin.invoices.buyer_lot_invoice',
            compact(['lot', 'seller', 'sellerAccount', 'buyer']))
            ->save($lot->buyer_invoice_path);
        unset($pdf);
        return file_exists($this->buyer_invoice_path);
    }

    public function getPartnerAlias()
    {
        try{
            $parsed_url = explode('.', parse_url($this->partner->link)['host']);
            if(in_array('www', $parsed_url)){
                return $parsed_url[1];
            } else{
                return $parsed_url[0];
            }

        }
        catch (\Exception $exception){
            return '';
        }
    }

    /**
     * Virtual field for move lots functionality.
     *
     * @return string
     */
    public function getMoveCheckbox()
    {
        return '<input name=to_move[] type="checkbox" value="'.$this->id.'">';
    }
}
