<?php

use Illuminate\Database\Seeder;

class AboutUsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(DB::table('about_us')->count() == 0) {
            DB::table('about_us')->insert([
                'id' => 1,
                'text' => nl2br('<p>A 100% sell-through rate at auction is extremely rare and, typically,
                    up to 30% of all items offered by auction houses around the world remain unsold.</p>
                <p>LastBid has been founded by a team of highly experienced, senior,
                    ex-auction-house professionals to provide a complementary service to auction houses globally,
                    by promoting these unsold items to a new audience of potential buyers.</p>
                <p>Our many years of experience in the auction world have taught us that it’s virtually
                    impossible to curate a 100% sale rate. The sheer number of auctions taking place every day,
                    each with a full catalogue of lots, makes it inevitable that thousands of potential buyers
                    will miss auction lots the first time round – for any number of reasons. LastBid is a convenient,
                    one-stop platform, listing these unsold lots in one place, giving each item additional exposure
                    and a second chance of finding a new home.</p>'),
                'team_member_1_name' => 'Peter Wallman',
                'team_member_1_text' => nl2br('Selling the ex-Park Lane showroom McLaren F1 in 2008 for a record sum, and consigning the $28m ex-Fangio 1956 Ferrari 290 MM to auction…

…are just two career highlights that demonstrate Peter Wallman is no stranger to the world’s top salerooms. British-born, but with spells in New York, London, and Milan during almost two decades in advertising, he is fluent in Italian. Previously MD of the European arm of an international auction house, he was responsible for conducting sales in Paris, London, Monaco and Milan, setting world records and forging strategic relationships. Away from work, Peter can be found enjoying his Ferrari 365 GT 2+2, early Jaguar E-type coupé or – a recent acquisition – his 1961 Riva Ariston motor launch.'),
                'team_member_1_image' => '',
                'team_member_2_name' => 'Simon Kidston',
                'team_member_2_text' => nl2br('Simon Kidston is known around the world as a multilingual commentator and columnist, historic car dealmaker and entrepreneur. Born into a fast motoring family — he’s the nephew of 1920s ‘Bentley Boy’ Commander Glen Kidston — Simon developed his business knowledge through an early working life in the auction world, co-founding Brooks Europe, latterly Bonhams Europe.

In 2006, he founded Kidston SA, the consultancy that has since become synonymous with Private Treaty sales of rare and exceptional motor cars, and he also devised and launched K500 — an online resource and price index that gives subscribers unique data and unbiased insight.

LastBid benefits from Simon Kidston’s breadth of knowledge and vast experience at the cutting edge of auctions as buyer, seller and principal of an international saleroom.'),
                'team_member_2_image' => '',
                'team_member_3_name' => 'Matt Woods',
                'team_member_3_text' => nl2br('Matt’s first love was the bike, racing throughout his time at university and touring the USA. Competition required attention to detail and an inquisitive nature to find the best solution, and these skills have already equipped him well in the tech sector: Matt enjoys digging into the detail and working in a team to achieve the high standards LastBid sets itself.

An experienced analyst, Matt focuses on the website and marketing campaigns. Previously writing analytical programs and consulting with a variety of businesses, the diverse input helps LastBid to offer a product that the auction world is missing.

When not in the office, Matt is probably out on the bike, building a Lego set or tucking into a craft beer with the other Matt.'),
                'team_member_3_image' => '',
                'team_member_4_name' => 'Anthony Maclean',
                'team_member_4_text' => nl2br('Called to the Bar in 1971, Oxford-educated and multilingual lawyer Anthony Maclean has enjoyed over four decades at the forefront of international law, specialising—since 1988 when he became a director of Robert Brooks (Auctioneers) Ltd on formation—in auction businesses, mergers and acquisitions and high value classic cars. He remained a key figure in Brooks, then Bonhams, until 2016.

His private legal practice is known for its expertise in international commercial, shipping and financial transactions, investment in classic cars and dispute resolution.

British born — he’s distantly related to Sir Fitzroy Maclean, the ex-Special Forces legend often considered Ian Fleming’s inspiration for 007 — Anthony has lived in Switzerland for many years and is fluent in French and Italian.'),
                'team_member_4_image' => '',
                'team_member_5_name' => 'Matt Fowler',
                'team_member_5_text' => nl2br('Born to a Somerset family of academics, Matt took a Masters in Mechanical and Motorsports Engineering. After a stint as lead guitarist in a band, he traded music for the race circuit, working for a prestigious historic Porsche restoration and race preparation company. Here he gained technical insight into the classic car world, especially ‘50s and ‘60s sportscars/prototypes.

A race engineer at Le Mans Classic and other international events, and Team Principal of Kingston University’s Formula Student team, Matt’s real talent is in his logical problem-solving and innovative product development approach. Already building LastBid’s analysis tools, Matt’s inquisitive nature allows the team to change course quickly to build better solutions.

When not in the office, Matt has a guitar around his neck, or is perhaps scoring a quick garden cricket 50 off the other Matt’s bowling.'),
                'team_member_5_image' => '',
                'team_member_6_name' => 'Emanuele Collo',
                'team_member_6_text' => nl2br('Born in Milan, Emanuele moved from Milan to Geneva in 2008 to start his career at Kidston SA as a junior classic car specialist.

He has since learned the business inside out, built up his own client network with particular emphasis on Italian, English and German enthusiasts and collectors, and established a reputation for a professional, close-liaising relationship with clients. He oversees Kidston’s price and history database, closely follows client restoration projects, enjoys supervising Kidston’s magazine and video shoots and bravely accompanies Simon Kidston as co-driver on frequent adventures. His own pride and joy is a 1973 Porsche 911 with original \'Milano\' licence plates and, although fascinated by both pre- and post-War cars, is a particular expert on ‘Youngtimers’ (with apologies to the Oxford English Dictionary).'),
                'team_member_6_image' => '',
            ]);
        }
    }
}
