<div class="popup policy-popup" id="modal-id">
    <div class="modal-content">
        <div class="title">Privacy Policy</div>
        <div class="wrapper-sign-up-agreement wrapper-private-policy">
            <div class="inner-wrapper-sign-up-agreement">
                @include('inc.policy')
            </div>
        </div>
        <div class="btn-wrap">
            <a class="btn" href="javascript:void 0;" onclick="closeModal()">close</a>
        </div>
    </div>
</div>
