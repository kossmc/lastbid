<?php

namespace App\Events\Lot;

use App\Models\Lot;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;

class LotExpired
{
    use Dispatchable, SerializesModels;

    public $lot;

    /**
     * Create a new event instance.
     * @param Lot $lot
     * @return void
     */
    public function __construct(Lot $lot)
    {
        $this->lot = $lot;
    }
}
