@extends('layouts.main')

@include('inc.page_metas', ['defTitle' => 'Main page'])

@section('top-links')
    @include('inc.top_links')
@stop

@section('user_css')
<link rel="stylesheet" href=" {{asset('css/owl.carousel.min.css')}} ">
<link rel="stylesheet" href=" {{asset('css/owl.theme.default.min.css')}} ">
@endsection

@section('content')
    @include('inc.discover_more_modal')
    @include('inc.main_slider')
    <div class="wrap">
        <div class="category-preview-wrap clearfix">
            @foreach ($categories as $category)
                @php
                $icon_default = '';
                $icon_hover = '';
                @endphp
                @foreach($categories_icons as $icons)
                    @if($icons->id == $category->categories_icons_id)
                        @php
                            $icon_default = $icons->image_default;
                            $icon_hover = $icons->image_hover;
                        @endphp
                    @endif
                @endforeach
                <a href="{{ $category->url }}" class="item category-icon">
                    @if($icon_default == '' || $icon_hover == '')
                        <span class="icon"><span class="vertical-middle"><em class="{{ $category->icon }}"></em></span></span>
                    @else
                    <span class="icon"><span class="vertical-middle"><img width="130px" src="{!! $icon_default !!}" data-src="{!! $icon_default !!}" data-hover="{!! $icon_hover !!}" alt=""></span></span>
                    @endif
                    <span class="name">{{ $category->title }}</span>
                </a>
            @endforeach
            <div class="clear"></div>
            <div class="btn-wrap">
                <a href="{{url('catalog')}}" class="btn brown">view all categories</a>
            </div>
        </div>

        <div class="most-popular clearfix">
            <div class="title-wrap-preview">
                <h2>Most popular</h2>
                <a href="{{url('catalog')}}" class="view">view all lots</a>
            </div>
            <div class="row" id="most-popular-slider">
                @foreach ($most_popular_lots as $lot)
                    @include('inc.latest_lot')
                @endforeach
            </div>
        </div>

        <div class="upcoming-event">
            <div class="title-wrap-preview">
                <h2>News</h2>
                <a href="{{ url('/news') }}" class="view">view all news</a>
            </div>
            <div class="row" id="upcoming-event-clider">
                @foreach ($blog_posts as $blog_post)
                <div class="item">
                    <div class="image">
                        <a href="{{ $blog_post->url }}">
                            <img src="{{ $blog_post->getImagePath(326,184) }}" alt="{{ $blog_post->title }}">
                        </a>
                    </div>
                    <div class="desc"><a href="{{ $blog_post->url }}">{{ $blog_post->title }}</a></div>
                    @if ($blog_post->post_date)
                        <div class="date">{{ $blog_post->post_date->format('j F Y') }}</div>
                    @endif
                    <br />
{{--                    <a class="btn discoverMoreBtn">discover more</a>--}}
                </div>
                @endforeach
            </div>
        </div>

        @if (!empty($recentlyLots) && $recentlyLots->isNotEmpty())
        <div class="won-deals clearfix">
            <div class="title-wrap-preview">
                <h2>Recently viewed lots</h2>
                <a href="{{url('catalog')}}" class="view">view all lots</a>
            </div>
            <div class="row" id="latest-lots-slider">
                @foreach ($recentlyLots as $lot)
                    @include('inc.latest_lot')
                @endforeach
            </div>
        </div>
        @endif
    </div>

    <script>
      // var sourceSwap = function () {
      //   var $this = $(this);
      //   var newSource = $this.data('alt-src');
      //   $this.data('alt-src', $this.attr('src'));
      //   $this.attr('src', newSource);
      // }
      //
      // $(function() {
      //   $('img[data-alt-src]').each(function() {
      //     new Image().src = $(this).data('alt-src');
      //   }).hover(sourceSwap, sourceSwap);
      // });

    </script>
@endsection

@section('scripts')
<script src="{{asset('js/owl.carousel.min.js')}}"></script>
<script>
    let homeSliders = {
        owlClass : 'owl-carousel',
        carousels : ['#latest-lots-slider', '#upcoming-event-clider', '#most-popular-slider'],
        init : () => {
            if ( $(window).width() < 768 ) {
                homeSliders.startCarousel();
            } else {
                homeSliders.stopCarousel();
            }
        },
        startCarousel : () => {
            homeSliders.carousels.forEach((carousel, index) => {
                $(carousel).addClass(homeSliders.owlClass);

                $('.'+homeSliders.owlClass+carousel).owlCarousel({
                    items:2,
                    nav: true,
                    touchDrag: true,
                    navText: ['<span class="prev slider-arrow" style=""><em class="icon-right_arrow"></em></span>',
                        '<span class="next slider-arrow" style=""><em class="icon-right_arrow"></em></span>']
                });
            });
        },
        stopCarousel : () => {
            homeSliders.carousels.forEach((carousel, index) => {
                $(carousel).removeClass(homeSliders.owlClass);

                let owl = $('.'+homeSliders.owlClass+carousel);
                owl.trigger('destroy.owl.carousel');
            });
        }
    };

    $(window).resize(homeSliders.init());
</script>
@endsection