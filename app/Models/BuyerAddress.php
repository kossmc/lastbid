<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BuyerAddress extends Model
{
    use SoftDeletes;
    protected $table = 'buyer_addresses';
    public $timestamps = true;
    protected $guarded = [];
}
