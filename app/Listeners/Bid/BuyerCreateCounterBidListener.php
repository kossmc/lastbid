<?php

namespace App\Listeners\Bid;

use App\Events\Bid\BuyerCreateCounterBid;

class BuyerCreateCounterBidListener extends SendGridAbstractBidListener
{
    /**
     * Buyer Email subject.
     */
    private $buyerSubject = 'You have countered an offer';

    /**
     * Buyer Email template.
     */
    private $buyerTemplate = '3f2a557f-698c-477b-af6e-7b85f485106b';

    /**
     * Seller Email subject.
     */
    private $sellerSubject = 'You have received a counterbid';

    /**
     * Seller Email template.
     */
    private $sellerTemplate = 'e4a62299-0b71-4539-921f-0875f27be18d';
    /**
     * Handle the event.
     *
     * @param  BuyerCreateCounterBid  $event
     * @return void
     */
    public function handle(BuyerCreateCounterBid $event)
    {
        $this->setData($event);
        /*notification to seller*/
        $this->sendMail($this->seller, $this->getData($this->sellerTemplate), $this->sellerSubject);
        /*notification to buyer*/
        $this->sendMail($this->buyer, $this->getData($this->buyerTemplate), $this->buyerSubject);
    }
}
