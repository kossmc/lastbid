@extends('parser.layout')
@section('content')
    <div class="container">
        @foreach($parsers as $type => $parser)
            <div class="panel panel-default">
                <div class="panel-heading">
                    @isset($parser['link'])
                        <a target="_blank" href="{{$parser['link']}}">{{ $parser['heading'] }}</a>
                    @else
                        {{ $parser['heading'] }}
                    @endisset
                </div>
                <div class="panel-body">
                    @if( !empty($parser['batch']) && $parser['batch']->isWorking)
                        <p>Parser working.....</p>
                    @else
                        {{ Form::open(array('url' => 'parser/process')) }}
                        {{ Form::hidden('type',$type) }}
                        <div class="form-inline row" style="padding-bottom: 20px">
                            <div class="form-group col-sm-4" style="text-align:left">
                                {{ Form::label('seller_id', 'Choose lot seller',
                                 ['class'=>'col-sm-8 control-label']) }}
                                {{ Form::select('seller_id', $sellers, $parser['seller'],
                                    ['required' => 'required','class' => 'form-control col-sm-5','placeholder' => 'Pick a lot seller...']) }}
                            </div>
                            <div class="form-group col-sm-4" style="text-align:left">
                                {{ Form::label('partner_id', 'Choose lot partner',
                                 ['class'=>'col-sm-8 control-label']) }}
                                {{ Form::select('partner_id', $partners, $parser['partner'],
                                    ['required' => 'required','class' => 'form-control col-sm-5','placeholder' => 'Pick a lot partner...']) }}
                            </div>
                        </div>
                        @isset($parser['input'])
                            <div class="form-group{{ $errors->has('parser-input') ? ' has-error' : '' }}" style="text-align:left">
                                {{ Form::text('parser-input', null,
                                ['class' => 'form-control', 'placeholder' => $parser['input']['placeholder']]) }}
                                <span class="help-block">{{ $errors->first('parser-input') }}</span>
                            </div>
                        @endisset
                        <button type="submit" class="btn btn-success bt-lg">Start parser</button>
                        {{ Form::close() }}
                    @endif
                </div>
                @if( (!empty($parser['batch']) && !$parser['batch']->isWorking) )
                    <div class="panel-footer">
                        <p>
                            @if($parser['batch']->isPending)
                                Parser pending ....
                            @endif

                            @if($parser['batch']->isFailed)
                                Parser failed ....
                            @endif

                            @if($parser['batch']->isFinished)

                                    Parser finished:

                                @if($parser['batch']->filename)
                                    <a href="{{ $parser['batch']->url }}" target="_blank">{{ $parser['batch']->filename }}</a>
                                @else
                                    no items listed for auction
                                @endif

                            @endif
                        </p>
                    </div>
                @endif
            </div>
        @endforeach
    </div>
@endsection
@push('after_scripts')
    <script>
        $(document).ready(function () {
            $('button.start-download').on('click', function() {
                new PNotify({
                    title: "Download start",
                    text: "Please wait..",
                    type: "success",
                    delay: 3000
                });
                return true;
            })
        });
    </script>
@endpush
