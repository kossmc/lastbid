<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BidsTableChanges extends Migration
{
    public function __construct()
    {
        DB::getDoctrineSchemaManager()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');
    }

    protected $statusesMap = [
        0 => 'declined',
        1 => 'pending',
        2 => 'accepted',
        3 => 'countered',
        4 => 'expired'
    ];
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bids', function (Blueprint $table) {
            $table->renameColumn('counter', 'counter_user_id');
            $table->renameColumn('product_id', 'lot_id');
            $table->renameColumn('bid_sum', 'amount');
            $table->renameColumn('status', 'status_int');
            $table->dropColumn(['offer_sum', 'counter_viewed']);
        });
        Schema::table('bids', function (Blueprint $table) {
            $table->enum('status', $this->statusesMap)->default($this->statusesMap[1]);
        });
        foreach ($this->statusesMap as $statusInt => $status) {
            $sql = 'UPDATE `bids` SET `status`=\'' . $status . '\' WHERE `status_int`=' . $statusInt;
            DB::connection()->getPdo()->exec($sql);
        }
        Schema::table('bids', function (Blueprint $table) {
            $table->dropColumn('status_int');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bids', function (Blueprint $table) {
            $table->renameColumn('counter_user_id', 'counter');
            $table->renameColumn('lot_id', 'product_id');
            $table->renameColumn('amount', 'bid_sum');
            $table->renameColumn('status', 'status_enum');
            $table->decimal('offer_sum', 16, 2)->nullable();
            $table->integer('counter_viewed')->default(0);
        });
        Schema::table('bids', function (Blueprint $table) {
            $table->boolean('status')->default(1);
        });
        foreach ($this->statusesMap as $statusInt => $status) {
            $sql = 'UPDATE `bids` SET `status`=' . $statusInt . ' WHERE `status_enum`=\'' . $status . '\'';
            DB::connection()->getPdo()->exec($sql);
        }
        Schema::table('bids', function (Blueprint $table) {
            $table->dropColumn('status_enum');
        });
    }
}
