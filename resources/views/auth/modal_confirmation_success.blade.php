<div class="popup" id="modal-id">
    <div class="modal-content">
        <div class="title">Email Verification</div>
        <p>Your email was successfully verified</p>
        <div class="btn-wrap">
            <a class="btn" href="javascript:void 0;" onclick="closeModal()">OK</a>
        </div>
    </div>
</div>
