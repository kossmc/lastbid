<div class="popup-wrap">
    <div class="popup popup--confirm-deletele-msgs">
        <div class="title">confirm deleting</div>
        <p>This action will affect selected conversations in Archive</p>
        <p>Are you sure you want to continue?</p>
        <div class="btn-wrap">
            <a class="popup-btn-cancel" href="javascript:void 0;">Ok</a>
            <a class="popup-btn-accept" href="javascript:void 0;">Cancel</a>
        </div>
    </div>
</div>
<div id="popup-overlay"></div>
