<?php

namespace App\Http\Controllers\Parser;

use App\Jobs\CrawlerJob;
use App\Http\Controllers\Controller;
use App\Http\Requests\Parser\ProcessRequest;
use App\Models\ParserBatch;
use App\Models\Partner;
use App\Models\User;
use App\Services\ArtNetDirectoriesCrawler;
use App\Services\DreweattsCrawler;
use App\Services\ForumAuctionsCrawler;
use App\Services\LyonTurnbullCrawler;
use App\Services\ArtNetGalleriesCrawler;
use App\Services\GrahamBudCrawler;
use App\Services\MaaklondonCrawler;
use App\Services\MacdouglasCrawler;
use App\Services\RmSothebysCrawler;
use App\Services\SterlingVaultCrawler;
use App\Services\SothebysCrawler;
use Illuminate\Support\Facades\DB;

class BatchController extends Controller
{
    private $parsers = [
        'graham' => [
            'parser' => GrahamBudCrawler::class,
            'heading' =>'Graham Bud Parser',
            'input' => ['placeholder'=>'Archive ID'],
            'link' =>'https://www.grahambuddauctions.co.uk/archives',
        ],
        'art-directories' => [
            'parser' => ArtNetDirectoriesCrawler::class,
            'link' =>'http://www.artnet.com/auction-houses/directory/',
            'heading' =>'Artnet Auction House Directories Parser',
        ],
        'lyonandturnbull' => [
            'parser' => LyonTurnbullCrawler::class,
            'heading' =>'Lyon and Turnbull Parser',
            'link' =>'https://www.lyonandturnbull.com/search/past',
            'input' => ['placeholder' => 'Auction Number']
        ],
        'art-galleries' => [
            'parser' => ArtNetGalleriesCrawler::class,
            'heading' =>'Artnet Art Galleries Parser',
            'link' =>'http://www.artnet.com/galleries/fine-art/',
        ],
        'dreweatts' => [
            'parser' => DreweattsCrawler::class,
            'heading' =>'www.dreweatts.com Parser',
            'link' =>'https://www.dreweatts.com/auctions/auction-results/',
            'input' => ['placeholder' => 'Enter saleId from https://www.dreweatts.com/auctions/auction-results/']
        ],
        'rmsothebys' => [
            'parser' => RmSothebysCrawler::class,
            'heading' =>'RM Sotheby\'s Parser',
            'link' =>'https://rmsothebys.com/en/home/lots/',
            'input' => ['placeholder' => 'Auction Number']
        ],
        'sterlingvault' => [
            'parser' => SterlingVaultCrawler::class,
            'heading' =>'Sterling Vault Parser',
            'link' =>'https://www.sterlingvault.co.uk/',
            'input' => ['placeholder' => 'Auction Number']
        ],
        'sothebys' => [
            'parser' => SothebysCrawler::class,
            'heading' =>'Sotheby\'s Parser',
            'link' =>'http://www.sothebys.com/',
            'input' => ['placeholder' => 'Enter example http://www.sothebys.com/en/auctions/2018/photographs-n09919.html']
        ],
        'macdouglas' => [
            'parser' => MacdouglasCrawler::class,
            'heading' =>'Macdouglas Parser',
            'link' =>'http://www.macdougallauction.com'
        ],
        'maaklondon' => [
            'parser' => MaaklondonCrawler::class,
            'heading' =>'Maaklondon Parser',
            'link' =>'http://www.maaklondon.com'
        ],
        'forumauctions' => [
            'parser' => ForumAuctionsCrawler::class,
            'heading' =>'Forum Auctions Parser',
            'link' =>'http://www.forumauctions.co.uk',
            'input' => ['placeholder' => 'Enter example https://www.forumauctions.co.uk/Forum-Auctions-Artsy-Urban-Jungle-III/01-04-2019']

        ],
    ];

    private function getCrawler($type) {
        return array_key_exists($type, $this->parsers) ? $this->parsers[$type]['parser'] : false;
    }

    /**
     * Select all parser
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function select()
    {
        // Try find working parsers or parser results
        $results = DB::select(
                    'select l.*
                    from parser_batches l
                    inner join (
                       select
                         type, max(updated_at) as latest
                       from parser_batches
                       group by type
                     ) r
                    on l.updated_at = r.latest and l.type = r.type
                    order by updated_at desc'
        );

        $results = collect($results)->map(function ($el) {
            $batch = new ParserBatch();
            $batch->setRawAttributes((array)$el, true);
            return $batch;
        });

        $parsers = collect($this->parsers)->map(function ($parser, $type) use ($results) {
            $result = $results->where('type', $type);
            if ($result && $result->isNotEmpty()) {
                $parser['batch'] = $result->first();
            }
            $parser['seller'] = env(strtoupper($type).'_SELLER_ID', null);
            $parser['partner'] = env(strtoupper($type).'_PARTNER_ID', null);
            return $parser;
        });

        $sellers = User::role('seller')->get()->mapWithKeys(function ($item) {
            return [$item['id'] => $item['company_name']];
        });

        $partners = Partner::all()->mapWithKeys(function ($item) {
            return [$item['id'] => $item['title']];
        });

        return view('parser.select',['parsers'=>$parsers, 'sellers'=> $sellers, 'partners'=>$partners]);
    }

    /**
     * Parse data and save to file
     * @param ProcessRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function process(ProcessRequest $request)
    {
        $type = $request->type;
        if ($crawler = $this->getCrawler($type)) {

            $batch = ParserBatch::create([
                'type' => $type,
                'input' => $request->get('parser-input', ''),
                'partner_id' => $request->get('partner_id', 0),
                'seller_id' => $request->get('seller_id', 0),
            ]);

            $job = new CrawlerJob($crawler, $batch);
            //$job->handle();
            dispatch($job);

            \Alert::error('Please wait, parser is working...!')->flash();
        } else {
            \Alert::error('Error! Parser not found!')->flash();
        }
        return redirect()->route('parser-home');
    }

    /**
     * Download parsed file
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function download($id)
    {
        $batch = ParserBatch::find($id);
        if ($batch) {
            $filePath = storage_path('app/parser/'. $batch->filename);
            if (file_exists($filePath)) {
                $headers = ['Content-Type' => 'text/csv'];
                return response()->download($filePath, $batch->filename, $headers);
            }
        }

        \Alert::error('Error! File not found!')->flash();

        return redirect()->route('parser-home');
    }
}
