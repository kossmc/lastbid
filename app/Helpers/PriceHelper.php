<?php

if (!function_exists('format_price')) {

    /**
     * format price
     *
     * @param $amountFrom float
     * @param $amountTo float
     * @return string
     */
    function format_price($amountFrom, $amountTo = null, $currency = 'usd')
    {
        $result = number_format($amountFrom, 0, '', '&nbsp;');
        if (!is_null($amountTo)) {
            $result .= '&nbsp;&ndash;&nbsp;' . number_format($amountTo, 0, '', '&nbsp;');
        }
        return $result . '&nbsp;<span class="currency">'.strtoupper($currency).'</span>';
    }
}

if ( ! function_exists('format_money')) {

    /**
     * format price
     *
     * @param $amount
     * @return string
     */
    function format_money($amount)
    {
        return number_format($amount, 0, '', ',') . '.00';
    }
}

if ( ! function_exists('format_usd')) {

    /**
     * format price
     *
     * @param $amount
     * @return string
     */
    function format_usd($amountFrom, $amountTo = null)
    {
        $result = '$&nbsp;' . number_format($amountFrom, 0, '', '&nbsp;');
        if (!is_null($amountTo)) {
            $result .= '&nbsp;&ndash;&nbsp;$&nbsp;' . number_format($amountTo, 0, '', '&nbsp;');
        }
        return $result;
    }
}
