<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStatusToLots extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = 'ALTER TABLE `lots` CHANGE `status` `status` ENUM(\'active\',\'expired\',\'sold\',\'closed\',\'sold_outside\') NOT NULL DEFAULT \'active\';';
        DB::connection()->getPdo()->exec($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $sql = 'ALTER TABLE `lots` CHANGE `status` `status` ENUM(\'active\',\'expired\',\'sold\',\'closed\') NOT NULL DEFAULT \'active\';';
        DB::connection()->getPdo()->exec($sql);
    }
}
