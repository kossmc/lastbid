<?php

namespace App\Listeners\User;

use App\Events\User\BuyerAddNewCard;
use Illuminate\Contracts\Queue\ShouldQueue;

class BuyerAddNewCardListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  BuyerAddNewCard  $event
     * @return void
     */
    public function handle(BuyerAddNewCard $event)
    {
        //
    }
}
