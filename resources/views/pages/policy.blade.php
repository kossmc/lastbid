@extends('layouts.main')

@include('inc.page_metas', ['defTitle' => 'Privacy policy'])

@section('top-links')
    @include('inc.top_links')
@stop

@section('content')
    <div class="page-wrap page-terms">
        <div class="wrap">
            <div class="clearfix">
                <div class="content">
                    <div class="title-evets">
                        <h1>Privacy policy</h1>
                    </div>
                    <div class="info">
                        @include('inc.policy')
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection