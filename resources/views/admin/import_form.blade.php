@extends('backpack::layout')
@section('header')
    <section class="content-header">
        <h1>Import CSV</h1>
        <ol class="breadcrumb">
            <li>
                <a href="{{ url(config('backpack.base.route_prefix'), 'dashboard') }}">{{ trans('backpack::crud.admin') }}</a>
            </li>
            <li><a href="{{ url($crud->route) }}" class="text-capitalize">{{ $crud->entity_name_plural }}</a></li>
            <li class="active">Import CSV</li>
        </ol>
    </section>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <!-- Default box -->
            @if ($crud->hasAccess('list'))
                <a href="{{ url($crud->route) }}"><i
                            class="fa fa-angle-double-left"></i> {{ trans('backpack::crud.back_to_all') }}
                    <span>{{ $crud->entity_name_plural }}</span></a><br><br>
            @endif

            {{--{!! Form::open(array('url' =>url($crud->route).'/import', 'method' => 'post',
            'files'=> true)) !!}
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Import CSV</h3>
                </div>
                <div class="box-body row">
                    <!-- text input -->
                    <div class="form-group col-md-12{{ $errors->has('csv_file') && $errors->get('upload_type')[0] == 'simple' ? ' has-error' : '' }}">
                        <label>File</label>

                        --}}{{-- Show the file picker on CREATE form. --}}{{--
                        <input
                            type="file"
                            id="csv_file_input"
                            name="csv_file"
                            class="form-control"
                        >
                            @if ($errors->has('csv_file') && $errors->get('upload_type')[0] == 'simple')
                                <p class="help-block">{{ $errors->first('csv_file') }}</p>
                            @endif
                        <input type="hidden" name="upload_type" value="simple">
                    </div>

                </div><!-- /.box-body -->
                <div class="box-footer">
                    <div id="saveActions" class="form-group">


                        <div class="btn-group">
                            <button type="submit" class="btn btn-success">
                                <span class="fa fa-save" role="presentation" aria-hidden="true"></span> &nbsp;
                                <span>Upload</span>
                            </button>

                        </div>

                        <a href="{{ url($crud->route) }}" class="btn btn-default"><span class="fa fa-ban"></span> &nbsp;{{ trans('backpack::crud.cancel') }}</a>
                    </div>

                </div><!-- /.box-footer-->

            </div><!-- /.box -->
            {!! Form::close() !!}

            <br/><br/>--}}

            {!! Form::open(array('url' =>url($crud->route).'/import', 'method' => 'post',
            'files'=> true)) !!}
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Import lots </h3> <a href="/examples/base_upload_example.csv" target="_blank" style="float: right;">csv example</a>
                </div>
                <div class="box-body row">
                    <div class="form-group col-md-12{{ $errors->has('csv_file') && $errors->get('upload_type')[0] == 'base' ? ' has-error' : '' }}">
                        <label>*.csv File</label>
                        <input
                                type="file"
                                id="csv_file_input"
                                name="csv_file"
                                class="form-control"
                        >
                        @if ($errors->has('csv_file') && $errors->get('upload_type')[0] == 'base')
                            <p class="help-block">{{ $errors->first('csv_file') }}</p>
                        @endif
                    </div>
                    <div class="form-group col-md-12{{ $errors->has('zip_file') && $errors->get('upload_type')[0] == 'base' ? ' has-error' : '' }}">
                        <label>*.zip File (photos)</label>
                        <input
                                type="file"
                                id="zip_file_input"
                                name="zip_file"
                                class="form-control"
                        >
                        @if ($errors->has('zip_file') && $errors->get('upload_type')[0] == 'base')
                            <p class="help-block">{{ $errors->first('zip_file') }}</p>
                        @endif
                    </div>
                    <div class="form-group col-md-6">
                        <label>Seller</label>
                        <select name="user_id" class="form-control">
                            @foreach($sellers as $seller)
                                <option value="{{ $seller->id }}">{{ $seller->first_name }} {{ $seller->last_name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-md-6">
                        <label>Partner</label>
                        <select name="partner_id" class="form-control">
                            @foreach($partners as $partner)
                                <option value="{{ $partner->id }}">{{ $partner->title }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-md-12">
                        <label>Category</label>
                        <select name="category_id" class="form-control">
                            <option value="0" selected>no category</option>
                            @foreach($categories as $category)
                                <option value="{{ $category->id }}">{{ $category->title }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-md-3">
                        <label>Buyer premium (%)</label>
                        <input type="number" min="0" max="100" step="0.01"  required id="buyer_premium" name="buyer_premium" class="form-control">
                    </div>
                    <div class="form-group col-md-3">
                        <label>Duration (days)</label>
                        <input type="number" min="1" id="duration" required name="duration" class="form-control">
                    </div>
                    <div class="form-group col-md-3">
                        <label>Auto decline (%)</label>
                        <input type="number" min="1" max="99" id="min_bid_amount" name="min_bid_amount" class="form-control">
                    </div>
                    <div class="form-group col-md-3">
                        <label>Auto accept (%)</label>
                        <input type="number" min="1" id="auto_accept_price" name="auto_accept_price" class="form-control">
                    </div>
                    <div class="form-group col-md-3">
                        <label>Currency</label>
                        <select name="currency" class="form-control">
                            <option value="usd" selected>USD</option>
                            <option value="gbp">GBP</option>
                            <option value="eur">EUR</option>
                        </select>
                    </div>
                    <input type="hidden" name="upload_type" value="base">
                </div><!-- /.box-body -->
                <div class="box-footer">
                    <div id="saveActions" class="form-group">
                        <div class="btn-group">
                            <button type="submit" class="btn btn-success">
                                <span class="fa fa-save" role="presentation" aria-hidden="true"></span> &nbsp;
                                <span>Upload</span>
                            </button>
                        </div>
                        <a href="{{ url($crud->route) }}" class="btn btn-default"><span class="fa fa-ban"></span> &nbsp;{{ trans('backpack::crud.cancel') }}</a>
                    </div>

                </div><!-- /.box-footer-->

            </div><!-- /.box -->
            {!! Form::close() !!}
            <br /><br />

            {!! Form::open(array('url' =>url($crud->route).'/import', 'method' => 'post',
            'files'=> true)) !!}
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">FORUM AUCTIONS Import lots</h3>  <a href="/examples/forum_upload_example.csv" target="_blank" style="float: right;">csv example</a>
                </div>
                <div class="box-body row">
                    <!-- text input -->
                    <div class="form-group col-md-12{{ $errors->has('csv_file') && $errors->get('upload_type')[0] == 'forum' ? ' has-error' : '' }}">
                        <label>*.csv File</label>
                        <input
                                type="file"
                                id="csv_file_input"
                                name="csv_file"
                                class="form-control"
                        >
                        @if ($errors->has('csv_file') && $errors->get('upload_type')[0] == 'forum')
                            <p class="help-block">{{ $errors->first('csv_file') }}</p>
                        @endif
                    </div>
                    <div class="form-group col-md-12{{ $errors->has('zip_file') && $errors->get('upload_type')[0] == 'forum' ? ' has-error' : '' }}">
                        <label>*.zip File (photos)</label>
                        <input
                                type="file"
                                id="zip_file_input"
                                name="zip_file"
                                class="form-control"
                        >
                        @if ($errors->has('zip_file') && $errors->get('upload_type')[0] == 'forum')
                            <p class="help-block">{{ $errors->first('zip_file') }}</p>
                        @endif
                    </div>
                    <div class="form-group col-md-12">
                        <label>Seller</label>
                        <select name="user_id" class="form-control">
                            @foreach($sellers as $seller)
                            <option value="{{ $seller->id }}">{{ $seller->first_name }} {{ $seller->last_name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-md-12">
                        <label>Category</label>
                        <select name="category_id" class="form-control">
                            <option value="0" selected>no category</option>
                            @foreach($categories as $category)
                                <option value="{{ $category->id }}">{{ $category->title }}</option>
                            @endforeach
                        </select>
                    </div>
                    <input type="hidden" name="upload_type" value="forum">
                </div><!-- /.box-body -->
                <div class="box-footer">
                    <div id="saveActions" class="form-group">


                        <div class="btn-group">
                            <button type="submit" class="btn btn-success">
                                <span class="fa fa-save" role="presentation" aria-hidden="true"></span> &nbsp;
                                <span>Upload</span>
                            </button>

                        </div>

                        <a href="{{ url($crud->route) }}" class="btn btn-default"><span class="fa fa-ban"></span> &nbsp;{{ trans('backpack::crud.cancel') }}</a>
                    </div>

                </div><!-- /.box-footer-->

            </div><!-- /.box -->
            {!! Form::close() !!}

            <br><br>

            {!! Form::open(array('url' =>url($crud->route).'/import', 'method' => 'post',
            'files'=> true)) !!}
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">ASTE BOLAFFI Import lots</h3>  <a href="/examples/bolaff_upload_example.csv" target="_blank" style="float: right;">csv example</a>
                </div>
                <div class="box-body row">
                    <!-- text input -->
                    <div class="form-group col-md-12{{ $errors->has('csv_file') && $errors->get('upload_type')[0] == 'bolaffi' ? ' has-error' : '' }}">
                        <label>*.csv File</label>
                        <input
                                type="file"
                                id="csv_file_input"
                                name="csv_file"
                                class="form-control"
                        >
                        @if ($errors->has('csv_file') && $errors->get('upload_type')[0] == 'bolaffi')
                            <p class="help-block">{{ $errors->first('csv_file') }}</p>
                        @endif
                    </div>
                    <div class="form-group col-md-12">
                        <label>Seller</label>
                        <select name="user_id" class="form-control">
                            @foreach($sellers as $seller)
                                <option value="{{ $seller->id }}">{{ $seller->first_name }} {{ $seller->last_name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-md-12">
                        <label>Category</label>
                        <select name="category_id" class="form-control">
                            <option value="0" selected>no category</option>
                            @foreach($categories as $category)
                                <option value="{{ $category->id }}">{{ $category->title }}</option>
                            @endforeach
                        </select>
                    </div>
                    <input type="hidden" name="upload_type" value="bolaffi">
                </div><!-- /.box-body -->
                <div class="box-footer">
                    <div id="saveActions" class="form-group">
                        <div class="btn-group">
                            <button type="submit" class="btn btn-success">
                                <span class="fa fa-save" role="presentation" aria-hidden="true"></span> &nbsp;
                                <span>Upload</span>
                            </button>

                        </div>

                        <a href="{{ url($crud->route) }}" class="btn btn-default"><span class="fa fa-ban"></span> &nbsp;{{ trans('backpack::crud.cancel') }}</a>
                    </div>

                </div><!-- /.box-footer-->

            </div><!-- /.box -->
            {!! Form::close() !!}

            <br/><br/>

            {!! Form::open(array('url' =>url($crud->route).'/import', 'method' => 'post',
            'files'=> true)) !!}
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">FINARTE Import lots</h3> <a href="/examples/finarte_upload_example.csv" target="_blank" style="float: right;">csv example</a>
                </div>
                <div class="box-body row">
                    <!-- text input -->
                    <div class="form-group col-md-12{{ $errors->has('csv_file') && $errors->get('upload_type')[0] == 'finarte' ? ' has-error' : '' }}">
                        <label>*.csv File</label>
                        <input
                                type="file"
                                id="csv_file_input"
                                name="csv_file"
                                class="form-control"
                        >
                        @if ($errors->has('csv_file') && $errors->get('upload_type')[0] == 'finarte')
                            <p class="help-block">{{ $errors->first('csv_file') }}</p>
                        @endif
                    </div>
                    <div class="form-group col-md-12{{ $errors->has('zip_file') && $errors->get('upload_type')[0] == 'finarte' ? ' has-error' : '' }}">
                        <label>*.zip File (photos)</label>
                        <input
                                type="file"
                                id="zip_file_input"
                                name="zip_file"
                                class="form-control"
                        >
                        @if ($errors->has('zip_file') && $errors->get('upload_type')[0] == 'finarte')
                            <p class="help-block">{{ $errors->first('zip_file') }}</p>
                        @endif
                    </div>
                    <div class="form-group col-md-12">
                        <label>Seller</label>
                        <select name="user_id" class="form-control">
                            @foreach($sellers as $seller)
                                <option value="{{ $seller->id }}">{{ $seller->first_name }} {{ $seller->last_name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-md-12">
                        <label>Category</label>
                        <select name="category_id" class="form-control">
                            <option value="0" selected>no category</option>
                            @foreach($categories as $category)
                                <option value="{{ $category->id }}">{{ $category->title }}</option>
                            @endforeach
                        </select>
                    </div>
                    <input type="hidden" name="upload_type" value="finarte">
                </div><!-- /.box-body -->
                <div class="box-footer">
                    <div id="saveActions" class="form-group">
                        <div class="btn-group">
                            <button type="submit" class="btn btn-success">
                                <span class="fa fa-save" role="presentation" aria-hidden="true"></span> &nbsp;
                                <span>Upload</span>
                            </button>
                        </div>
                        <a href="{{ url($crud->route) }}" class="btn btn-default"><span class="fa fa-ban"></span> &nbsp;{{ trans('backpack::crud.cancel') }}</a>
                    </div>

                </div><!-- /.box-footer-->

            </div><!-- /.box -->
            {!! Form::close() !!}
        </div>
    </div>
@endsection
