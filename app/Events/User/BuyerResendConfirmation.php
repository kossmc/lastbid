<?php

namespace App\Events\User;

use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use App\Models\User;

class BuyerResendConfirmation
{
    use Dispatchable, SerializesModels;

    public $user;
    public $referrer;

    /**
     * Create a new event instance.
     *
     * @param User $user
     * @param string $referrer
     * @return void
     */
    public function __construct(User $user, $referrer)
    {
        $this->user = $user;
        $this->referrer = $referrer;
    }
}
