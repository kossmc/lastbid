@extends('seller.profile')
@section('title', 'Payment Methods')
@php(!isset($edit)?$edit=false:'')
@section('profile_content')
    <div class="buyer-account">
        <section class="payment {{$edit?'edit-bank-account':'add-bank-account'}}">
            <h3>{{$edit?'Edit bank account':'Add new bank account'}}</h3>
            @include('inc.top_errors_list')
            <form id="add-bank-account" method="POST"
                  action="{{$edit ? route('seller-payment-methods-bank-edit-save', $account->id)
                  :route('seller-payment-methods-bank-add-new')}}">
                <div class="form-controll form-controll--medium">
                    <label>Account holder's name</label>
                    <input type="text" name="holder_name" placeholder="Name"
                           value="{{old('holder_name', $account->holder_name)}}"
                           class="{{ $errors->has('holder_name') ? 'has-error--bgcolor' : '' }}">
                </div>
                <div class="form-controll form-controll--newline form-controll--small">
                    <label>Account number</label>
                    <input type="text" name="account_number" placeholder="Number"
                           value="{{old('account_number', $account->account_number)}}"
                           class="{{ $errors->has('account_number') ? 'has-error--bgcolor' : '' }}">
                </div>
                <div class="form-controll form-controll--small">
                    <label>ABA Number/Routing number </label>
                    <input type="text" name="routing_number" placeholder="Number"
                           value="{{old('routing_number', $account->routing_number)}}"
                           class="{{ $errors->has('routing_number') ? 'has-error--bgcolor' : '' }}">
                </div>
                <div class="form-controll form-controll--newline form-controll--medium">
                    <label>Bank name</label>
                    <input type="text" name="bank_name" placeholder="Bank name"
                           value="{{old('bank_name', $account->bank_name)}}"
                           class="{{ $errors->has('bank_name') ? 'has-error--bgcolor' : '' }}">
                </div>
                <div class="form-controll form-controll--newline form-controll--medium">
                    <label>SWIFT/BIC code (optional)</label>
                    <input type="text" name="swift_code" placeholder="SWIFT/BIC Code"
                           value="{{old('swift_code', $account->swift_code)}}"
                           class="{{ $errors->has('swift_code') ? 'has-error--bgcolor' : '' }}">
                </div>
                <div class="form-controll form-controll--newline">
                    <h3>Bank address </h3>
                </div>
                <div class="form-controll form-controll--newline form-controll--medium">
                    <label>Address</label>
                    <input type="text" name="address" placeholder="Address"
                           value="{{old('address', $account->address)}}"
                           class="{{ $errors->has('address') ? 'has-error--bgcolor' : '' }}">
                </div>
                <div class="form-controll form-controll--newline form-controll--small">
                    <label>City</label>
                    <input type="text" name="city" placeholder="City"
                           value="{{old('city', $account->city)}}"
                           class="{{ $errors->has('city') ? 'has-error--bgcolor' : '' }}">
                </div>
                <div class="form-controll form-controll--small">
                    <label>State / province</label>
                    <input type="text" name="state" placeholder="State"
                           value="{{old('state', $account->state)}}"
                           class="{{ $errors->has('state') ? 'has-error--bgcolor' : '' }}">
                </div>
                <div class="form-controll form-controll--small form-controll--newline">
                    <label>Country</label>
                    <input type="text" name="country" placeholder="Country"
                           value="{{old('country', $account->country)}}"
                           class="{{ $errors->has('country') ? 'has-error--bgcolor' : '' }}">
                </div>
                <div class="form-controll form-controll--small">
                    <label>Post code / zip</label>
                    <input type="text" name="zip" placeholder="Code"
                           value="{{old('zip', $account->zip)}}"
                           class="{{ $errors->has('zip') ? 'has-error--bgcolor' : '' }}">
                </div>
                <div class="form-controll form-controll--newline form-controll--large">
                    <div class="btn-wrap">
                        <button class="btn" type="submit">
                            {{$edit?'Edit account':'Add bank account'}}
                        </button>
                        <a class="btn2" href="{{route('seller-payment-methods')}}">Cancel</a>
                    </div>
                </div>
                {{csrf_field()}}
            </form>
        </section>
    </div>
@endsection
