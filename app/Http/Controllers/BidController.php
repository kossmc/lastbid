<?php

namespace App\Http\Controllers;

use App\Models\BidAttempt;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\PlaceBidRequest;
use App\Repositories\BidRepository;
use App\Repositories\LotRepository;
use App\Repositories\MessageRepository;

/**
 * Class BidController
 * @package App\Http\Controllers
 */
class BidController extends Controller
{

    /** @var BidRepository The bid repository implementation */
    private $bid;
    /** @var LotRepository The lot repository implementation. */
    private $lot;
    /** @var MessageRepository The lot repository implementation. */
    private $message;

    /**
     * Create a new controller instance.
     * @param BidRepository $bid
     * @param LotRepository $lot
     * @param MessageRepository $message
     */
    public function __construct(BidRepository $bid, LotRepository $lot, MessageRepository $message)
    {
        $this->bid = $bid;
        $this->lot = $lot;
        $this->message = $message;
    }

    /**
     * Add bid to lot
     * @param Request $request
     * @param PlaceBidRequest $formRequest
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(Request $request, PlaceBidRequest $formRequest)
    {
        $lot = $request->lot;

        $data = [
            'user_id' => Auth::id(),
            'lot_id' => $lot->id,
            'amount' => $formRequest->amount,
            'message' => $formRequest->message
        ];

        if ($bid = $this->bid->createByBuyer($data)) {

            // if need message with bid
            $this->message->create([
                'lot_id' => $lot->id,
                'text'=> $formRequest->message,
                'not_send' => true
            ]);

            $lot->increment('bids_count');
            $lot->last_bid_date = $bid->created_at;
            $lot->save();

            $autoAccept = $lot->bidIsAutoAccept($data['amount']);

            return response()->json([
                'status' => true,
                'congWindow'=> view()->make('inc.modal_congratulations',
                    compact('bid', 'lot', 'autoAccept'))->render()
            ]);
        }

        abort(404);
    }
}
