<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeFieldTypesInHowItWorksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('how_it_works', function (Blueprint $table) {
            $table->text('section_1_text')->change();
            $table->text('section_2_text')->change();
            $table->text('section_3_text')->change();
            $table->text('section_4_text')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('how_it_works', function (Blueprint $table) {
            //
        });
    }
}
