@if($items)
    <div id="popup-overlay"></div>
    @foreach($items as $item)
        <div class="popup buyer-account---confirm-deleting"
             id="conf_modal_{{$item->id}}">
            <div class="title">Confirm deletion</div>
            <p>This action will delete your {{$name}} shipping address from the system.<br>Are you sure you want to continue?</p>
            <div class="btn-wrap"><a class="btn" href="{{ route($route,$item->id) }}">ok</a><a
                        class="btn2" href="javascript: void 0;" data-dismiss="modal">cancel</a></div>
        </div>
    @endforeach
@endif
