@extends('layouts.inner')
@section('title', 'Account info')
@php
    $account_routes = ['account-info','account-info-edit'];
    $payments_routes = ['payment-methods','payment-methods-accounts','payment-methods-card-edit',
    'payment-methods-card-add','payment-methods-bank-add','payment-methods-card-add','payment-methods-bank-edit'];
    $shipping = ['shipping-info-new','shipping-info-edit','shipping-info'];
@endphp
@section('inner_content')
    @include('inc.flash_message')
    <ul class="nav nav-tabs profile-tabs-nav lots-tabs-nav">
        <li{!! areActiveRoutes($account_routes) ?' class="active"' : '' !!}>
            <a href="{{ route('account-info') }}">account info</a></li>
        {{--<li{!! areActiveRoutes($payments_routes) ? ' class="active"' : '' !!}>
            <a href="{{ route('payment-methods') }}">payment methods</a></li>--}}
        <li{!! areActiveRoutes($shipping) ? ' class="active"' : '' !!}>
            <a href="{{ route('shipping-info') }}">shipping info</a></li>
        {{--<li{!! isActiveRoute('subscriptions') ? ' class="active"' : '' !!}>--}}
            {{--<a href="{{ route('subscriptions') }}">subscriptions</a></li>--}}
    </ul>
    <div class="tab-content profile-tab-content">
        <div class="tab-pane fade in active">
            @yield('profile_content')
        </div>
    </div>
@endsection
