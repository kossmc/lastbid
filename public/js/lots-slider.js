/**
 * sliderId - id slider block, required
 * options - not required
 // * options.showAdjacentsSlides - false if show only active slide
 * options.responsive - example 'responsive' : [
 {to : 768, {}},
 {from : 769, {}},
 ]
 *
 */

var categoryLotsSlider = class {
    constructor(sliderId, options){
        this.setSliderId(sliderId);
        this.setDefaultVars(options);

        if(!this.isItemsEnoughtForSlider()) return;

        // if(this.options.responsive !== undefined){
        //     window.onresize = function(){
        //         $.each(sliders, function(i, v){
        //             v.setResponsive();
        //         });
        //     }
        //     this.setResponsive();
        // }else{
        this.initSlider(sliderId);
        // }
    }

    setDefaultVars(options) {
        this.activeItem = 'active';
        this.leftItem = 'left-item';
        this.rightItem = 'right-item';
        this.classItem = '.item';
        this.classViewport = '.viewport';
        this.classArrow = '.slider-arrow';
        this.cloneItem = 'item-clone';
        this.cloneItemPrev = 'clone-prev';
        this.cloneItemNext = 'clone-next';
        this.slideId = 'slide-';
        this.dataSlideId = 'slideid';
        this.countFocusedElems = 2;

        this.slideWrapStart = options.slideWrapStart === undefined ? '<div class="slide">': options.slideWrapStart;
        this.slideWrapEnd =  options.slideWrapEnd === undefined ? '</div>': options.slideWrapStart;


        this.itemCount = $(this.sliderId + ' ' + this.classItem).length;
        this.options = options !== undefined ? options : {} ;
        this.itemPerSlide = options.itemPerSlide ? options.itemPerSlide : 1;

        this.sliderWidth = $(this.sliderId).outerWidth();
        this.itemWidth = this.sliderWidth / this.itemPerSlide;

        this.distanceMoveOneItem = options.distanceMoveOneItem === undefined ? this.itemWidth/2 : options.distanceMoveOneItem;
        this.distanceMoveEndViewport = options.distanceMoveEndViewport === undefined ? (this.itemWidth + (this.itemWidth/4)) : options.distanceMoveEndViewport;
    }

    setSliderId(siderId) {
        if(siderId == null || siderId.length < 1) {
            throw "Slider initialized without slider id!";
        }

        this.sliderId = '#' + siderId;
    }

    initSlider(sliderId, navId) {
        this.setItemsWidth();
        this.setActiveImg();

        if(this.options.createClones !== false) this.createClones();

        this.setNextImage();
        this.setPrevImage();

        this.moveViewport();

        this.arrowsShow();
        this.touch();
        $(this.sliderId + ' .next').click((e) => {this.next(e)});
        $(this.sliderId + ' .prev').click((e) => {this.prev(e)});
    }

    touch() {
        $(this.sliderId + ' .item').swipe( {
            swipeStatus:(event, phase, direction, distance) => {
                if(phase === 'end'){

                    if(distance < this.distanceMoveOneItem) {
                        this.scrollToNearest();
                    }else if(distance > this.distanceMoveOneItem && distance < this.distanceMoveEndViewport){
                        if(direction === 'left') this.next(event);
                        if(direction === 'right') this.prev(event);
                    }else if (distance > this.distanceMoveEndViewport){
                        if(direction === 'left') this.nextEnd(event);
                        if(direction === 'right') this.prevEnd(event);
                    }

                }else if(phase === 'move'){

                    if(direction === 'left'){
                        $(this.sliderId +' '+this.classViewport).css('left', this.startPostition - distance + 'px' );
                    }else if(direction === 'right'){
                        $(this.sliderId +' '+this.classViewport).css('left', this.startPostition + distance + 'px' );
                    }

                }else if(phase === 'start'){
                    $( this.sliderId+' '+this.classViewport ).stop();

                    this.startPostition = parseInt(
                        $(this.sliderId +' '+this.classViewport).css('left'),
                        10
                    );
                }
            },

            threshold:0
        });
    }

    next(e){
        e.preventDefault();

        let delay = this.checkLoop('right') ? 100 : 0 ;

        setTimeout(() => {
            this.prevImage.removeClass(this.leftItem);
            this.activeImage.removeClass(this.activeItem).addClass(this.leftItem);
            this.nextImage.removeClass(this.rightItem).addClass(this.activeItem);

            this.setNextImage();

            this.moveViewport();
        }, delay);
    }
    nextEnd(e){

        let nextAllElems = this.activeImage.nextAll();
        if(nextAllElems.length < this.countFocusedElems) {
            this.scrollToStartTouch();
            return;
        }

        let scrollTarget = (nextAllElems.length - this.countFocusedElems +1) * this.itemWidth;
        let scrollDistance = -scrollTarget + this.startPostition;

        let targetActiveElem = $(nextAllElems).eq(nextAllElems.length - this.countFocusedElems);

        this.scrollToPosition(scrollDistance);

        this.cleanActiveItems();
        targetActiveElem.addClass(this.activeItem);
        this.setNextImage();
        this.setPrevImage();

        // this.checkLoop('right');
    }

    prev(e) {
        e.preventDefault();

        let delay = this.checkLoop('left') ? 100 : 0 ;

        setTimeout(() => {
            this.nextImage.removeClass(this.rightItem);
            this.activeImage.removeClass(this.activeItem).addClass(this.rightItem);
            this.prevImage.removeClass(this.leftItem).addClass(this.activeItem);

            this.setPrevImage();

            this.moveViewport();
        }, delay);

        // this.toggleNavItem();
    }
    prevEnd(e){

        let prevAllElems = this.activeImage.prevAll();
        if(prevAllElems.length === 0) {
            this.scrollToStartTouch();
            return;
        }

        let scrollTarget = (prevAllElems.length - this.countFocusedElems) * this.itemWidth;
        let scrollDistance = -scrollTarget - this.startPostition - (this.itemWidth*this.countFocusedElems);

        let targetActiveElem = $(prevAllElems).last();

        this.scrollToPosition(scrollDistance);

        this.cleanActiveItems();
        targetActiveElem.addClass(this.activeItem);
        this.setNextImage();
    }

    get activeImage() {
        return $(this.sliderId).find('.'+this.activeItem).first();
    }

    get prevImage() {
        return $(this.sliderId).find('.'+this.leftItem).first();
    }

    get nextImage() {
        return $(this.sliderId).find('.'+this.rightItem).first() || '.'+this.activeImage.next();
    }

    setActiveImg(targetItem) {
        if(targetItem !== undefined){
            $(targetItem).addClass(this.activeItem);
        }else if($(this.sliderId + ' .' + this.activeItem).length < 1){
            $(this.sliderId + ' ' + this.classItem).first().addClass(this.activeItem);
        }
    }

    setNextImage() {
        $(this.sliderId).find('.'+this.rightItem).removeClass(this.rightItem);

        $(this.activeImage).next().addClass(this.rightItem);
    }

    setPrevImage() {
        $(this.sliderId).find('.'+this.leftItem).removeClass(this.leftItem);

        $(this.activeImage).prev().addClass(this.leftItem);
    }

    checkLoop(direct) {
        if(this.ifActiveImgLast(direct)){
            if(this.options.createClones !== false && this.options.loop !== false){
                this.transition('add');

                this.moveImgs(direct);
                this.moveViewport();

                setTimeout(() => {
                    this.transition();
                }, 10);

                return true;
            }
        }
    }

    createClones() {
        $(this.sliderId +' '+ this.classItem + ':not(.'+ this.cloneItemPrev +')')
            .clone()
            .prependTo(this.sliderId +' '+this.classViewport)
            .removeClass(this.activeItem)
            .removeClass(this.leftItem)
            .removeClass(this.rightItem)
            .addClass(this.cloneItemPrev)
            .addClass(this.cloneItem);

        $(this.sliderId +' '+ this.classItem + ':not(.'+ this.cloneItemPrev +')')
            .clone()
            .appendTo(this.sliderId +' '+this.classViewport)
            .removeClass(this.activeItem)
            .removeClass(this.leftItem)
            .removeClass(this.rightItem)
            .addClass(this.cloneItemNext)
            .addClass(this.cloneItem);

        $(this.sliderId + ' .' + this.leftItem).remove();
    }

    moveImgs(direct) {
        let currentImgsClass = this.classItem;

        if($(this.activeImage).hasClass(this.cloneItemNext)){
            currentImgsClass += '.' + this.cloneItemPrev;
        }else{
            currentImgsClass += '.' + this.cloneItemNext;
        }

        if(direct === 'left'){
            $(this.sliderId + ' ' + this.classViewport).prepend($(this.sliderId + ' ' + currentImgsClass));
        }else{
            $(this.sliderId + ' ' + this.classViewport).append($(this.sliderId + ' ' + currentImgsClass));
        }
    }

    setItemsWidth(){
        $(this.sliderId +' '+ this.classItem).css("width", this.itemWidth);
    }

    moveViewport(){
        let posActive = $(this.sliderId +' '+ this.classItem).index($(this.sliderId +' .'+ this.activeItem).first());
        let targetPosition = -(posActive*this.itemWidth);

        $(this.sliderId +' '+ this.classViewport).css('left', targetPosition);
    }

    ifActiveImgLast(direct) {
        if(direct === 'left') {
            return $(this.activeImage).prevAll().length <= this.countFocusedElems
        }else{
            return $(this.activeImage).nextAll().length <= this.countFocusedElems
        }
    }

    isItemsEnoughtForSlider() {
        if(this.itemCount >= this.itemPerSlide*2){
            return true;
        }
        return false;
    }

    // ------ responsive methods

    setResponsive() {
        if(this.options.responsive === undefined) return;

        if(!this.responsiveInit()){
            this.unwrapItems();
        }
    }

    responsiveInit() {
        // let isInit = 0;
        //
        // Object.keys(this.options.responsive).forEach((resolution) => {
        //     if(resolution > document.body.clientWidth){
        //         this.unwrapItems();
        //
        //         this.setResponsiveVars();
        //         this.wrapItems();
        //
        //         return isInit = 1;
        //     }
        // });
        //
        // return isInit;
    }

    unwrapItems() {
        // if($(this.sliderId+' .viewport .slide').length > 0) {
        //     $(this.sliderId + ' .viewport').append($('#slider-category-26 .viewport .item'));
        //     $(this.sliderId+' .viewport .slide').remove();
        // }
    }

    wrapItems() {
        let viewport = $(this.sliderId+' .viewport');

        let items = Array.from(document.querySelectorAll('#slider-category-26 .viewport>.item'));

        let i,j=items.length, itemChuks = [];

        for (i=0; i<j; i+=this.itemPerSlide) {
            let chunk = items.slice(i,i+this.itemPerSlide);

            $(viewport).append(this.slideWrapStart + this.slideWrapEnd);

            chunk.forEach((value, index) => {
                $(this.sliderId+' .viewport .slide').last().append(value);
            });
        }
    }

    setResponsiveVars() {

    }

    transition(action) {
        if(action === undefined) {
            $(this.sliderId + ' ' + this.classViewport).removeClass('transition-0');
        }else{
            $(this.sliderId + ' ' + this.classViewport).addClass('transition-0');
        }
    }

    //for scrolling viewport

    scrollToNearest() {
        let scrollTo = (this.getPositionViewport() / this.itemWidth |0) * this.itemWidth;
        scrollTo = scrollTo < 0 ? scrollTo : 0;
        $( this.sliderId+' '+this.classViewport).css('left', scrollTo);

        this.setActiveItemsByViewportPosition();
    }

    stopViewport() {
        $(this.slideId+' '+this.classViewport).stop();
        this.scrollToNearest();
    }

    scrollToStartTouch() {
        if(this.startPostition === undefined) return false;

        $( this.sliderId+' '+this.classViewport ).animate({
                left: this.startPostition
            }, 500,
            this.scrollToNearest()
        );
    }

    setActiveItemsByViewportPosition() {
        this.cleanActiveItems();

        let countItems = Math.abs(this.getPositionViewport() / this.itemWidth | 0);

        this.setActiveImg($( this.sliderId+' '+this.classViewport +' '+ this.classItem).eq(countItems));
        this.setPrevImage();
        this.setNextImage();
    }

    cleanActiveItems() {
        this.prevImage.removeClass(this.leftItem);
        this.activeImage.removeClass(this.activeItem);
        this.nextImage.removeClass(this.rightItem);
    }

    scrollToPosition(scrollTarget) {

        $( this.sliderId+' '+this.classViewport ).animate({
            left: scrollTarget
        }, {
            duration: 500,
            always: this.scrollToNearest()
        });
    }

    getPositionViewport() {
        return parseInt($( this.sliderId+' '+this.classViewport ).css('left'), 10);
    }

    arrowsShow() {
        $(this.sliderId+' '+this.classArrow).show();
    }
}