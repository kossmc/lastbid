<?php

namespace App\Listeners\Bid;

use App\Events\Bid\SellerDeclineBid;

class SellerDeclineBidListener extends SendGridAbstractBidListener
{
    /**
     * Buyer Email subject.
     */
    private $buyerSubject = 'Your bid has been declined';

    /**
     * Buyer Email template.
     */
    private $buyerTemplate = '5a5d3540-1906-4648-a17b-ecb5e0d9c3f8';

    /**
     * Seller Email subject.
     */
    private $sellerSubject = 'You have declined a bid';

    /**
     * Seller Email template.
     */
    private $sellerTemplate = 'ac179963-be83-4425-9759-cc9738d08bd2';
    /**
     * Handle the event.
     *
     * @param  SellerDeclineBid  $event
     * @return void
     */
    public function handle(SellerDeclineBid $event)
    {
        $this->setData($event);
        /*notification to seller*/
        $this->sendMail($this->seller, $this->getData($this->sellerTemplate), $this->sellerSubject);
        /*notification to buyer*/
        $this->sendMail($this->buyer, $this->getData($this->buyerTemplate), $this->buyerSubject);
    }
}
