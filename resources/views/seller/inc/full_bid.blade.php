<div id="tab-person{!! $bid->user_id !!}" class="tab-pane fade in bid-tab-by-person">
    <span class="btn-close-exapanded-bid icon-esc_icon"></span>
    @foreach ($bid->attempts as $attempt)
        <div class="bid-action new">
            <div class="date">
                @if ($attempt->is_countered)
                    {!! $attempt->created_at->format('j F Y, h:i A') !!} EST
                @else
                    {!! $attempt->updated_at->format('j F Y, h:i A') !!} EST
                @endif
            </div>
            <div class="title">
                @if ($attempt->user_id != Auth::id())
                    @if ($attempt->is_pending)
                        Please respond to this @if ($attempt->is_counter) counter @endif bid from <span class="from">{{ $attempt->user->short_name }}</span>
                    @elseif ($attempt->is_accepted)
                        You accepted @if ($attempt->is_counter) counter @endif bid from <span class="from">{{ $attempt->user->short_name }}</span>
                    @elseif ($attempt->is_declined)
                        You declined @if ($attempt->is_counter) counter @endif bid from <span class="from">{{ $attempt->user->short_name }}</span>
                    @elseif ($attempt->is_countered)
                        You received @if ($attempt->is_counter) counter @endif bid from <span class="from">{{ $attempt->user->short_name }}</span>
                    @elseif ($attempt->is_expired)
                        @if ($attempt->is_counter) Counter bid @else Bid @endif from <span class="from">{{ $attempt->user->short_name }}</span> was expired
                    @endif
                @else
                    @if ($attempt->is_pending)
                        Your counter bid was sent to <span class="from">{{ $bid->buyer->short_name }}</span>
                    @elseif ($attempt->is_accepted)
                        Your @if ($attempt->is_counter) counter @endif bid was accepted by <span class="from">{{ $bid->buyer->short_name }}</span>
                    @elseif ($attempt->is_declined)
                        Your @if ($attempt->is_counter) counter @endif bid was declined by <span class="from">{{ $bid->buyer->short_name }}</span>
                    @elseif ($attempt->is_countered)
                        You sent counter bid to <span class="from">{{ $bid->buyer->short_name }}</span>
                    @elseif ($attempt->is_expired)
                        Your @if ($attempt->is_counter) counter @endif bid to <span class="from">{{ $bid->buyer->short_name }}</span> was expired
                    @endif
                @endif
            </div>
            <ul class="bid-body">
                <li class="bid-body-row">
                    <div class="first-col">Bid price:</div>
                    <div class="second-col">
                        <div class="price{{ !($attempt->is_pending || $attempt->is_accepted) ? ' crossed' : '' }}">{!! format_price($attempt->amount, null, $lot->currency) !!}</div>
                        <div class="bid-consists">
                            <span class="title">The bid price consists of:</span>
                            <ul class="consist-list">
                                <li class="consist-list-i">
                                    <span class="left">Bid price:</span>
                                    <span class="right price">{!! format_price($attempt->amount, null, $lot->currency) !!}</span>
                                </li>
                                <li class="consist-list-i">
                                    <span class="left">Auction house premium:</span>
                                    <span class="right price">{!! format_price($lot->bidFee($attempt->amount), null, $lot->currency) !!}</span>
                                </li>
                                <li class="consist-list-i">
                                    <span class="left">Total bid price:</span>
                                    <span class="right price">{!! format_price($lot->bidWithFee($attempt->amount), null, $lot->currency) !!}</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </li>
                @if ($attempt->is_pending)
                    <li class="bid-body-row">
                        <div class="first-col">Bid expires in:</div>
                        <div class="second-col"><b>{{ format_time_left($attempt->end_date) }}</b></div>
                    </li>
                @endif
                <li class="bid-body-row">
                    <div class="first-col">{{ $attempt->user_id != Auth::id() ? 'Buyer’s' : 'Your' }} message:</div>
                    <div class="second-col">{{ !empty($attempt->message) ? $attempt->message : '--' }}</div>
                </li>
                @if ($attempt->is_pending && $bid->by_buyer)
                    <li class="bid-body-row">
                        <div class="buttons">
                            <a href="javascript:void 0;" data-toggle="modal" data-target="#conf_modal_{{$bid->id}}" class="btn">Accept</a>
                            <button data-href="{{ route('seller_counter_bid', ['id' => $bid->id]) }}" class="btn btn2  counter-bid-modal">Counter bid</button>
                            <a href="{{ route('seller_switch_bid_status', ['id' => $bid->id, 'method' => 'decline']) }}" class="btn btn2">Decline</a>
                        </div>
                    </li>
                @endif
            </ul>
        </div>
    @endforeach
</div>
