<?php

namespace App\Http\Controllers;

use App\Models\FaqCategory;
use Illuminate\Http\Request;

class FaqController extends Controller
{
    private function getCategories($faqQuery)
    {
        if ($faqQuery) {
            $categories = FaqCategory::whereHas('questions', function ($q) use ($faqQuery) {
                $q->search($faqQuery);
            })->with(['questions' => function ($q) use ($faqQuery) {
                $q->search($faqQuery)->active()->ordered();
            }]);
        } else {
            $categories = FaqCategory::with(['questions' => function ($q) {
                $q->active()->ordered();
            }]);
        }
        return $categories->active()->ordered()->get();
    }

    /**
     * Display a listing of the faq.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $faqQuery = $request->input('faq_query', null);
        $meta = \App\Models\PageMeta::byPage('news');
        $categories = $this->getCategories($faqQuery);

        return view('pages.faq', compact('categories','faqQuery', 'meta'));
    }

    /**
     * View faq landing
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index_landing(Request $request)
    {
        $faqQuery = $request->input('faq_query', null);

        return view('landing.faq', ['categories' => $this->getCategories($faqQuery),'faqQuery' => $faqQuery]);
    }
}
