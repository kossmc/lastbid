<?php

namespace App\Services;

use Carbon\Carbon;
use Symfony\Component\DomCrawler\Crawler;

/**
 * Class SothebysCrawler
 * @package App\Services
 */
class SothebysCrawler extends BaseCrawler
{
    public $baseUrl = 'http://www.sothebys.com/';

    public $crowler;

    /**
     * SothebysCrawler constructor.
     * @param $batch
     */
    public function __construct($batch)
    {
        $this->crowler = new Crawler();

        $name = substr($batch->input, strrpos($batch->input, '/') + 1);

        $name = str_replace(strstr($name, '.html'), '', $name);

        $this->filename = 'Sothebys_' . $name . '_' . date('Y-m-d_H-i-s') . '.csv';

        parent::__construct($batch);
    }

    /*
     *  Start parse data
     */
    public function startCrawling()
    {
        $headers = ['Cookie' => 'ak_bmsc=parse;'];

//        $client = new \GuzzleHttp\Client(['headers' => $headers]);
//        $response = $client->request('GET', $this->baseUrl . 'en/auctions/list/_jcr_content.currencyRates.json/' . date('d-m-Y'));
//
//        if ($response->getStatusCode() != 200) {
//            return false;
//        }
//
//        $content = $this->processContent($response->getBody()->getContents());
//
//        if (!$jsonDateRate = json_decode($content, true)) {
//            return false;
//        }
//
//        $rates = $jsonDateRate['rates'];

        $client = new \GuzzleHttp\Client(['headers' => $headers]);

        $response = $client->request('GET', $this->getUrl());
        if ($response->getStatusCode() != 200) {
            \Log::info(print_r($response, true));
            return false;
        }

        $content = $this->processContent(
            $response->getBody()->getContents()
        );

        $this->crowler->add($content);

        $auctionDate = $this->getStartDate();
        $startDate = $auctionDate->format('Y-m-d H:i:s');
        $endDate = $auctionDate->addMonth()->format('Y-m-d H:i:s');

        $lots = [];
        if (preg_match_all('/ECAT.lot\[(.+?)\]=(.*?)};/u', $content, $matches)) {

            foreach ($matches[2] as $i => $lot) {

                $dataLot = str_replace('"','\"', $lot);

                $dataLot = trim(str_replace('\'', '"', $dataLot));

                $textLot = str_replace('"hasBidLive":false,','', $dataLot);

                $dataLot = json_decode($textLot . '}', true);

                if(!is_array($dataLot)){
                    \Log::info(var_export($lot, true));
                    continue;
                }

                if ($dataLot['isSold'] == 1/** || $dataLot['isPast'] == 0**/)
                    continue;

                $client = new \GuzzleHttp\Client(['headers' => $headers]);
                $response = $client->request('GET', $this->baseUrl . $dataLot['condRep']);

                if ($response->getStatusCode() != 200) {
                    \Log::info(print_r($response, true));
                    return false;
                }

                $content = $this->processContent(
                    $response->getBody()->getContents()
                );

                $extraLot = $this->getLotInfo($content, $dataLot);

                if (!is_array($extraLot)){
                    continue;
                }

                $dataLot = array_merge($dataLot, $extraLot);

                if(isset($rates)){
                    foreach ($rates as $rate){

                        if($dataLot['currency'] == $rate['currency_from'] && $rate['currency_to'] == 'USD'){

                            $dataLot['currency'] = 'usd';
                            $dataLot['lowEst'] = $dataLot['lowEst'] * $rate['rate'];
                            $dataLot['highEst'] = $dataLot['highEst'] * $rate['rate'];
                            $dataLot['salePrice'] = $dataLot['salePrice'] * $rate['rate'];

                            break;
                        }
                    }
                }else{
                    $dataLot['currency'] = strtolower($dataLot['currency']);
                }

                $lots[] = [
                    'Lot Id' => $dataLot['id'],
                    'Title' => $dataLot['guaranteeLine'],
                    'Description' => isset($dataLot['description']) ? $dataLot['description'] : '',
                    'Category' => $dataLot['saleName'],
                    'Sub Category' => '',
                    'Partner' => $this->partner_id,
                    'Shipping description' => '',
                    'Auction House Fee' => 20,
                    'Minimum bid amount' => ($dataLot['lowEst']) ? (int)$dataLot['lowEst'] * 0.5 : 0,
                    'Price range from' => ($dataLot['lowEst']) ? (int)$dataLot['lowEst'] : 0,
                    'Price range to' => ($dataLot['highEst']) ? (int)$dataLot['highEst'] : 0,
                    'Price to auto-accept bid' => ($dataLot['salePrice']) ? (int)$dataLot['salePrice'] : 0,
                    'Start date' => $startDate,
                    'End date' => $endDate,
                    'Seller' => $this->seller_id,
                    'Currency' => $dataLot['currency'],
                    'Lot photos' => implode(',', $dataLot['photos'])
                ];
            }
        }

        if ($lots) {
            $this->headers = array_keys($lots[0]);
        }

        return $lots;
    }

    /**
     * Get main full url
     * @return string
     */
    public function getUrl()
    {
        if(strpos($this->batch->input , $this->baseUrl . 'en/auctions/') === false){
            return false;
        }

        return $this->batch->input;
    }

    /**
     * Get start auction
     * @return Carbon
     */
    public function getStartDate()
    {

        $dateHtml = $this->crowler->filter('#x-event-date')->first();

        if (!$dateHtml)
            return Carbon::now();

        return Carbon::parse(trim($dateHtml->attr('datetime')));
    }

    /**
     * Get lot info
     * @param $content
     * @param $dataLot
     * @return array
     */
    public function getLotInfo($content, $dataLot)
    {
        $lot = [];

        try {
            $crawler = new Crawler();
            $crawler->add($content);

            $count = $crawler->filter('.lotdetail-guarantee')->count();
            if($count > 0){
                $title = $crawler->filter('.lotdetail-guarantee')->first();
                $lot['guaranteeLine'] = trim($title->text());
            }

            $lot['description'] = '';
            $count = $crawler->filter('.lotdetail-description-text')->count();
            if($count > 0){
                $detailDescData = $crawler->filter('.lotdetail-description-text')->first();
                $lot['description'] .= '<p>' . trim($detailDescData->text()) . '</p>';
            }

            $count = $crawler->filter('.lotdetail-details-content .readmore-content')->count();
            if($count > 0){
                $descData = $crawler->filter('.lotdetail-details-content .readmore-content')->first();
                $lot['description'] .= '<p>' . trim($descData->text()) . '</p>';
            }

            $count = $crawler->filter('.lotdetail-catalogue-notes .readmore-content')->count();
            if($count > 0){
                $readData = $crawler->filter('.lotdetail-catalogue-notes .readmore-content')->first();
                $lot['description'] .= '<p>' . trim($readData->text()) . '</p>';
            }

            $photos = $crawler->filter('#lotDetail-carousel li img')->each(function (Crawler $node, $i) {
                return $node->attr('data-image-path');
            });

            $arrRequestHeaders = array(
                'http' => array(
                    'method' => 'GET',
                    'protocol_version' => 1.1,
                    'follow_location' => 1,
                    'header' => "Cookie:ak_bmsc=parse;"
                )
            );

            $lot['photos'] = [];
            if (!empty($photos)) {
                foreach ($photos as $photo) {

                    if(strpos($photo , $this->baseUrl) === false){
                        $photo = $this->baseUrl . $photo;
                    }

                    $lot['photos'][] = $this->loadImage($photo, $arrRequestHeaders);
                }
            } else {

//                if(strpos($dataLot['image'] , $this->baseUrl) === false){
//                    $dataLot['image'] = $this->baseUrl . $dataLot['image'];
//                }

                $lot['photos'][] = $this->loadImage($this->baseUrl . $dataLot['image'], $arrRequestHeaders);
            }

        } catch (\InvalidArgumentException $e) {
            \Log::error($e->getMessage() . ' invalid argument');

            //dd($e->getMessage(), $dataLot);
            return false;
        } catch (\Exception $e) {
            \Log::error($e->getMessage() . 'invalid error');

            //dd($e->getMessage(), $dataLot);
            return false;
        }

        return $lot;
    }
}