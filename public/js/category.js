if(document.documentElement.clientWidth){
    $('.sidebar-category .cat-icon-expanded').toggleClass('cat-icon-collapsed cat-icon-expanded');
}

$('.reset-filters').on('click', function (e) {
    return document.location = document.location.pathname;
});

$('#sort_by').on('change', function () {
    if ($(this).val() != 0) {
        $('.sorting a#sort_up').click();
    } else {
        updateUrl([['sort_by'], ['sort_direction']]);
    }
});

$('.filter .item .sorting a').off('click').on('click', function(){
    var $input = $('#sort_direction'),
        $selected = $('select#sort_by').val();
    if ($selected != 0) {
        $(this).hasClass('down') ? $input.val('desc'):$input.val('asc');
        filterLots();
    }
});


function filterLots() {

    var previousValue = $(this).attr('previous');
    var name = $(this).attr('name');

    if (previousValue == "true")
    {
        $(this).removeAttr('checked');
        $(this).attr('previous', false);
    }

    var $filters = $('#category_filters input, .range-wrap input, form#top_sorting');

    document.location = document.location.pathname + '?' + $filters.serialize();
}

function removeSeeMore(el) {
    el.children('li:has(span.see-more-cat-items)').remove();
    el.children('li.hidden').removeClass('hidden');
}

$('ul:has(li.hidden:has(input:checked))').each(function () {
    removeSeeMore($(this));
});

$('span.see-more-cat-items').on('click', function (e) {
    removeSeeMore($(this).parent('li').parent('ul'));
});

$(document).on("click",'#category_filters input, .range-wrap button', filterLots);

function initRangeSlider(){
    $("#category-range-price").slider({
        range: true,
        min: sliderVars.min,
        max: sliderVars.max,
        step: sliderVars.step,
        values: [
            sliderVars.values[0],
            sliderVars.values[1]
        ],
        slide: function (event, ui) {
            for (var i = 0; i < ui.values.length; ++i) {
                $("input.range_price_value[data-index=" + i + "]").val(ui.values[i]);
            }
        }
    });
};

$("input.range_price_value").change(function() {
    var $this = $(this);
    $("#category-range-price").slider("values", $this.data("index"), $this.val());
});

$(document).on('change', '#t-agreement', function (e) {
    if (e.target.checked) {
        $(this).closest('form').find('button[type="submit"]').attr('disabled', false);
    }
    if (!e.target.checked) {
        $(this).closest('form').find('button[type="submit"]').attr('disabled', true);
    }
})
var isSending = false;
$(document).on("submit", "#form-talk", function (e) {
    var form = $(this);
    e.preventDefault();

    if (isSending) {
        return false;
    }
    isSending = true;

    var data = $(form).serialize();
    var url = "/" + form.attr('action');
    $.ajax({
        type: "POST",
        data: data,
        dataType: "json",
        url: url,

        success: function (data) {
            $('#lets-talk-header').text('');
            $('#lets-talk-notice').text('');

            if (data.status) {
                $(modal).hide();

                $("#lets-talk-header").text(data.message);
                $("#lets-talk-notice").text(data.note);

                $("#letsTalk").show();

            }
        },
        error: function (errors) {
            //var errorBlock = this.find(".error-notification")
        },
        complete: function () {
            isSending = false;

            $("#form-talk input,textarea").val("");
        }
    });
});
var letsTalk = document.getElementById("letsTalk");
var modal = document.getElementById("discoverMore");
var close_letsTalk = document.getElementById("letsTalk_close")
close_letsTalk.onclick = function () {
    letsTalk.style.display = "none";
};
window.onclick = function (event) {
    if (event.target == modal) {
        modal.style.display = "none";
    } else if (event.target == letsTalk) {
        letsTalk.style.display = "none";
    }
};