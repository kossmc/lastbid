<?php

namespace App\Listeners\Bid;

use App\Events\Bid\SellerCreateCounterBid;

class SellerCreateCounterBidListener extends SendGridAbstractBidListener
{
    /**
     * Buyer Email subject.
     */
    private $buyerSubject = 'You have received a counter offer from seller';

    /**
     * Buyer Email template.
     */
    private $buyerTemplate = 'de74c2b0-e4d1-43e9-85f8-4a5a1e46b09e';

    /**
     * Seller Email subject.
     */
    private $sellerSubject = 'You have made a counter offer';

    /**
     * Seller Email template.
     */
    private $sellerTemplate = '7d1b3586-e43b-40fa-8765-bcab4fb7b117';
    /**
     * Handle the event.
     *
     * @param  SellerCreateCounterBid  $event
     * @return void
     */
    public function handle(SellerCreateCounterBid $event)
    {
        $this->setData($event);
        /*notification to seller*/
        $this->sendMail($this->seller, $this->getData($this->sellerTemplate), $this->sellerSubject);
        /*notification to buyer*/
        $this->sendMail($this->buyer, $this->getData($this->buyerTemplate), $this->buyerSubject);
    }
}
