<?php
namespace App\Repositories;

use App\Models\BlogPost;
use Bosnadev\Repositories\Eloquent\Repository;

/**
 * Class BlogRepository
 * @package App\Repositories
 */
class BlogRepository extends Repository
{
    /**
     * Get model
     * @return mixed
     */
    public function model()
    {
        return BlogPost::class;
    }

    /**
     * Active blog
     * @return mixed
     */
    public function active()
    {
        return $this->model->active();
    }

    /**
     * Order blog
     * @return mixed
     */
    public function order()
    {
        return $this->model->orderBy('post_date', 'desc');
    }

    /**
     * Get latest active blog
     * @param int $limit
     * @param array $exclude
     * @return mixed
     */
    public function latest($limit = 3, $exclude=[])
    {
        return $this->model->whereNotIn('id', $exclude)->active()->latest()->take($limit)->get();
    }

    /**
     * Find active item by slug
     * @param $slug
     * @return mixed
     */
    public function findBySlug($slug)
    {
        return $this->model->where('slug', $slug)->active()->first();
    }
}
