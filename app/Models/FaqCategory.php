<?php

namespace App\Models;
use Backpack\CRUD\CrudTrait;

use Illuminate\Database\Eloquent\Model;

class FaqCategory extends Model
{
    use CrudTrait;

    public $timestamps = true;
    protected $guarded = [];

    public function questions()
    {
        return $this->hasMany('App\Models\FaqQuestion', 'faq_category_id', 'id');
    }

    public function scopeActive($q)
    {
        return $q->where('active',1);
    }

    public function scopeOrdered($q)
    {
        return $q->orderBy('order');
    }
}
