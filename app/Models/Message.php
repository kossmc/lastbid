<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class Message extends Model
{
    /*messages state*/
    const STATE_READ = 'read';
    const STATE_UNREAD = 'unread';
    /*messages status*/
    const STATUS_ACTIVE = 'active';
    const STATUS_ARCHIVED = 'archived';
    const STATUS_DELETED = 'deleted';
    /*last sent message by*/
    const LAST_BY_SELLER = 'seller';
    const LAST_BY_BUYER = 'buyer';

    protected $dates = ['last_message_at'];
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'lot_id', 'buyer_id', 'seller_id', 'buyer_state','buyer_status',
        'seller_state', 'seller_status', 'last_message_by', 'last_message_at'
    ];
    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['latest'];
    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function messageBuyer()
    {
        return $this->belongsTo(User::class, 'buyer_id', 'id');
    }

    public function messageSeller()
    {
        return $this->belongsTo(User::class, 'seller_id', 'id');
    }

    public function lot()
    {
        return $this->belongsTo(Lot::class, 'lot_id', 'id');
    }

    public function history()
    {
        return $this->hasMany(MessageText::class, 'message_id', 'id')
            ->orderBy('created_at', 'ASC');
    }

    public function latestMessage()
    {
        return $this->hasMany(MessageText::class, 'message_id', 'id')
            ->orderBy('created_at', 'DESC')->first();
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /**
     * Scope a query to only include seller or buyer messages.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param integer                               $id
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeByUser($query, $id)
    {
        $role = $this->_getUserRole();
        return $query->where("{$role}_id", $id);
    }

    /**
     * Scope a query to only include received messages.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeReceived($query)
    {
        $role = $this->_getUserRole();
        return $query->where('last_message_by',
            $role == self::LAST_BY_BUYER ? self::LAST_BY_SELLER : self::LAST_BY_BUYER);
    }

    /**
     * Scope a query to only include sent messages.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param string                                $type
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSent($query)
    {
        $role = $this->_getUserRole();
        return $query->where('last_message_by',
            $role == self::LAST_BY_BUYER ? self::LAST_BY_BUYER : self::LAST_BY_SELLER);
    }

    /**
     * Scope a query to only include active messages.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        $role = $this->_getUserRole();
        return $query->where("{$role}_status", self::STATUS_ACTIVE);
    }

    /**
     * Scope a query to only include archived messages.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param string                                $status
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeArchived($query)
    {
        $role = $this->_getUserRole();
        return $query->where("{$role}_status", self::STATUS_ARCHIVED);
    }

    /**
     * Scope a query to only include read\unread messages.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param string                                $state
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeUnread($query)
    {
        $role = $this->_getUserRole();
        return $query->where("{$role}_state", self::STATE_UNREAD);
    }

    /**
     * Scope a query to get last updated messages.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeLatestUpdated($query)
    {
        return $query->orderBy('last_message_at', 'DESC');
    }

    /**
     * Scope a query to sorting by lot end date.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeLotEndDate($query)
    {
        return $query->select('messages.*')->join('lots', 'lots.id', '=', 'messages.lot_id')
                ->orderBy('lots.end_date', 'ASC');
    }

    /**
     * Scope a query to search in messages body, lot id, lot name.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param string                                $q
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeMessagesSearch($query, $q)
    {
        return $query->whereHas('lot',
            function ($query) use ($q) {
                $query->where('title', 'LIKE', "%{$q}%");
            })
            ->orWhereHas('history',
                function ($query) use ($q) {
                    $query->where('text', 'LIKE', "%{$q}%");
                })
            ->orWhere('messages.lot_id', 'LIKE', "%{$q}%");
    }

    /**
     * Scope a query to search in users first name and last name.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param string                                $q
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeUsersSearch($query, $q)
    {
        $relation = 'message' . ucfirst($this->_getInterlocutorRole());
        return $query->orWhereHas($relation,
            function ($query) use ($q) {
                $query->where('first_name', 'LIKE', "%{$q}%")
                    ->orWhere('last_name', 'LIKE', "%{$q}%");
            });
    }
    /**
     * Scope a query to exclude deleted messages.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeWithOutDeleted($query)
    {
        $role = $this->_getUserRole();
        return $query->where("{$role}_status",'<>', self::STATUS_DELETED);
    }
    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /**
     * Check if message read by user
     * @return bool
     */
    public function getIsReadAttribute()
    {
        $role = $this->_getUserRole();
        return $this->{$role.'_state'} == self::STATE_READ;
    }

    /**
     * Check if message sent by user
     * @return bool
     */
    public function getIsSentAttribute()
    {
        return $this->last_message_by == $this->_getUserRole();
    }

    /**
     * Check if message is archived by user
     * @return bool
     */
    public function getIsArchivedAttribute()
    {
        $role = $this->_getUserRole();
        return $this->{$role . '_status'} == self::STATUS_ARCHIVED;
    }

    public function getSenderAttribute()
    {
        return $this->last_message_by == self::LAST_BY_BUYER ?
            $this->messageBuyer : $this->messageSeller;
    }

    public function getRecipientAttribute()
    {
        return $this->last_message_by == self::LAST_BY_BUYER ?
            $this->messageSeller : $this->messageBuyer;
    }

    public function getByBuyerAttribute()
    {
        return $this->last_message_by == self::LAST_BY_BUYER;
    }

    public function getBySellerAttribute()
    {
        return $this->last_message_by == self::LAST_BY_SELLER;
    }

    public function getLatestAttribute()
    {
        if (!isset($this->attributes['latest'])) {
            $this->attributes['latest'] = $this->latestMessage();
        }
        return $this->attributes['latest'];
    }

    /**
     * Generates an URL for message.
     *
     * @return string
     */
    public function getUrlAttribute()
    {
        return route('message_view', ['id' => $this->id]);
    }

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /**
     * Switch messages status for user
     * @param string $method
     * @param array $ids
     * @return void
     */
    public function switchStatus($method, $ids)
    {
        $role = $this->_getUserRole();
        switch ($method) {
            case 'read':
                $data = ["{$role}_state" => self::STATE_READ];
                Session::flash('message', 'Message(s) was set read');
                break;
            case 'unread':
                $data = ["{$role}_state" => self::STATE_UNREAD];
                Session::flash('message', 'Message(s) was set unread');
                break;
            case 'archive':
                $data = ["{$role}_status" => self::STATUS_ARCHIVED];
                Session::flash('message', 'Message(s) was archived');
                break;
            case 'delete':
                $data = ["{$role}_status" => self::STATUS_DELETED];
                Session::flash('message', 'Message(s) was deleted');
                break;
            case 'restore':
                $data = ["{$role}_status" => self::STATUS_ACTIVE];
                Session::flash('message', 'Your message was restored');
                break;
        }
        if (is_array($ids) && count($ids) && isset($data)) {
            $this->whereIn('id', $ids)->update($data);
        }
    }

    /**
     * Get role for current logged user
     * @return string
     */
    public function _getUserRole()
    {
        $user = Auth::user();
        if ($user->hasRole('seller')) {
            return 'seller';
        }
        return 'buyer';
    }
    /**
     * Get role for user interlocutor
     * @return string
     */
    public function _getInterlocutorRole()
    {
        if ($this->_getUserRole() == 'seller') {
            return 'buyer';
        }
        return 'seller';
    }

    /**
     * Show FROM field for history messages
     * @param integer $id
     * @return string
     */
    public function fromName($id)
    {
        $user = Auth::user();
        if ($id == $user->id) {
            return 'Me';
        }
        /*try find user*/
        if ($this->messageSeller->id == $id) {
            return $this->messageSeller->short_name;
        }

        return $this->messageBuyer ? $this->messageBuyer->short_name : '';
    }

    /**
     * Show TO field for history messages
     * @param integer $id
     * @return string
     */
    public function toName($id)
    {
        $user = Auth::user();
        if ($id != $user->id) {
            return 'Me';
        }
        /*try find user*/
        if ($this->messageSeller->id == $id) {
            return $this->messageBuyer->short_name;
        }
        return $this->messageSeller->short_name;
    }
    /**
     * Save user reply
     * @param string $text
     * @return string
     */
    public function reply($text)
    {
        $messText = MessageText::create([
            'message_id' => $this->id,
            'user_id' => Auth::id(),
            'text' => $text
        ]);
        $this->last_message_at = $messText->created_at;
        $role = $this->_getUserRole();
        $this->last_message_by = $role;
        $this->{$role . '_state'} = self::STATE_READ;
        $this->{$role . '_status'} = self::STATUS_ACTIVE;
        $interlocutor = $this->_getInterlocutorRole();
        $this->{$interlocutor . '_state'} = self::STATE_UNREAD;
        $this->{$interlocutor . '_status'} = self::STATUS_ACTIVE;
        return $this->save();
    }

    public function newChain($data)
    {
        $lot = Lot::with('seller')->findOrFail($data['lot_id']);
        $mess = $this->create([
            'lot_id' => $lot->id,
            'buyer_id' => $data['buyer_id'],
            'seller_id' => $lot->seller->id,
            'buyer_state' => self::STATE_READ,
            'last_message_by' => self::LAST_BY_BUYER
        ]);
        $messText = MessageText::create([
            'message_id' => $mess->id,
            'user_id' => $data['buyer_id'],
            'text' => $data['text']
        ]);
        $mess->last_message_at = $messText->created_at;
        return $mess->save() ? $mess : null;
    }
}
