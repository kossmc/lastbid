<?php

use Illuminate\Database\Seeder;

class ArticleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*$faker = Faker\Factory::create();
        for ($i = 0; $i < 10; $i ++) {
            $p = new \App\Models\Article([
                'title'=> '',
                'location' => '',
                'brief' => '',
                'body' => '',
                'event_date' => '',
                'image'=>''
            ]);
            $p->save();
        }*/
//        $p = new \App\Models\Article([
//            'title' => 'Bonhams’ £5.2m Aston Martin Works Sale 2017',
//            'brief' => 'Aston Martin Works had an almost cathedral-like look to it today, the 18th annual all-Aston auction to be held at the spiritual home of the marque. The congregation in the saleroom might well have been true believers, but it took all of auctioneer James Knight’s powers of persuasion to sell them 60% of the motor car catalogue – up from 52% in 2016…',
//            'body' => 'Aston Martin Works had an almost cathedral-like look to it today, the 18th annual all-Aston auction to be held at the spiritual home of the marque. The congregation in the saleroom might well have been true believers, but it took all of auctioneer James Knight’s powers of persuasion to sell them 60% of the motor car catalogue – up from 52% in 2016…',
//            'image' => 'bonhams-aston-1.jpg'
//        ]);
//        $p->save();
//
//        $p = new \App\Models\Article([
//            'title' => 'Emperor Haile Selassie’s Patek Philippe Ref. 2497 Sells For $2.9 Million',
//            'brief' => 'Christie’s sells Selassie’s watch at the second asking in an old-school battle of watch dealer royalty...',
//            'body' => 'Christie’s sells Selassie’s watch at the second asking in an old-school battle of watch dealer royalty...',
//            'image' => 'watch-pp.jpg'
//        ]);
//        $p->save();
//
//
//        $p = new \App\Models\Article([
//            'title' => 'Utra-Rare Barn Find Vincent White Shadow Leads Bonhams Spring Stafford Sale',
//            'brief' => 'Bonhams motorcycle department saw success across the board at their Spring Stafford Sale today (23 April 2017), with a 90% sell through rate and an outstanding total of £2,132,257 achieved. Leading the sale was the 1949 Vincent HRD 988cc White Shadow Series-C Project, which realised a staggering £163,900 against a pre-sale estimate of £50,000-60,000…',
//            'body' => 'Bonhams motorcycle department saw success across the board at their Spring Stafford Sale today (23 April 2017), with a 90% sell through rate and an outstanding total of £2,132,257 achieved. Leading the sale was the 1949 Vincent HRD 988cc White Shadow Series-C Project, which realised a staggering £163,900 against a pre-sale estimate of £50,000-60,000…',
//            'image' => 'bike.jpeg'
//        ]);
//        $p->save();


        //        $p = new \App\Models\Article([
        //            'title' => 'Christies Wine Sale 17 May 2017',
        //            'brief' => 'An impressive collection of Burgundy, Bordeaux and Rhne Christies May wine sale with Domaine de la Romane-Conti in pole position…',
        //            'body' => 'An impressive collection of Burgundy, Bordeaux and Rhne Christies May wine sale with Domaine de la Romane-Conti in pole position…',
        //            'event_date' => '17.05.2017',
        //            'image' => 'l17705_500_1-wine.jpg'
        //        ]);
        //            $p->save();


//        $p = new \App\Models\Article([
//            'title' => 'When is a driving watch not a driving watch? – Vacheron Constantin ‘American 21’',
//            'brief' => 'This is a rare and quirky watch, popularised through its 2008 re-issue as part of Vacheron Constantin’s ‘Historiques Collection’. The modern watch differs from the earlier models in the position of the sub-seconds dial, the use of a modern wristwatch movement re-positioning the seconds indicator at right angles to the crown rather than opposite as in the original...',
//            'body' => 'This is a rare and quirky watch, popularised through its 2008 re-issue as part of Vacheron Constantin’s ‘Historiques Collection’. The modern watch differs from the earlier models in the position of the sub-seconds dial, the use of a modern wristwatch movement re-positioning the seconds indicator at right angles to the crown rather than opposite as in the original...',
//            'image' => 'elgin-etc.jpg'
//        ]);
//        $p->save();
//
//
//        $p = new \App\Models\Article([
//            'title' => 'More Cash for Ash? Investing in Cigars',
//            'brief' => "'Tabacalera Cubana was the name of the American Trust Company in the 1940s - 1950s . Cigars were likely made by them at the la Corona factory exclusively for Robert Lewis…",
//            'body' => "'Tabacalera Cubana was the name of the American Trust Company in the 1940s - 1950s . Cigars were likely made by them at the la Corona factory exclusively for Robert Lewis…",
//            'image'=>'472x472_341-PRE1739-NOV2014.JPG'
//]);
//        $p->save();
    }
}
