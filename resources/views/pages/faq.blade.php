@extends('layouts.main')

@include('inc.page_metas', ['defTitle' => 'Frequently asked questions'])

@section('top-links')
    @include('inc.top_links')
@stop
@section('content')
    @include('pages.faq_content', ['search' => true])
@endsection
