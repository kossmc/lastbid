<?php

if (!function_exists('isActiveRoute')) {
    /*
    |--------------------------------------------------------------------------
    | Detect Active Route
    |--------------------------------------------------------------------------
    |
    | Compare given route with current route and return output if they match.
    | Very useful for navigation, marking if the link is active.
    |
    */
    function isActiveRoute($route)
    {
        if (Route::currentRouteName() == $route) {
            return true;
        }
        return false;
    }
}

if (!function_exists('areActiveRoutes')) {
    /*
    |--------------------------------------------------------------------------
    | Detect Active Routes
    |--------------------------------------------------------------------------
    |
    | Compare given routes with current route and return output if they match.
    | Very useful for navigation, marking if the link is active.
    |
    */
    function areActiveRoutes(Array $routes)
    {
        foreach ($routes as $route)
        {
            if (Route::currentRouteName() == $route) {
                return true;
            }
        }
        return false;
    }
}


if ( ! function_exists('isReferrerRoute')) {
    /*
    |--------------------------------------------------------------------------
    | Detect Active Route
    |--------------------------------------------------------------------------
    |
    | Compare given route with current route and return output if they match.
    | Very useful for navigation, marking if the link is active.
    |
    */
    function isReferrerRoute($route)
    {
        $uri = request()->server('HTTP_REFERER');
        $refRouteName = app('router')->getRoutes()
            ->match(app('request')->create($uri))->getName();
        return $route == $refRouteName;
    }
}
