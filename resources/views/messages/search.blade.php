@extends('layouts.inner')
@section('title', 'My received messages')
@section('inner_content')
    <div class="page-buyer page-buyer--msg">
        <div class="search-results-box">
            <p>@if (count($messages)){{count($messages)}} @else 0 @endif search results</p>
        </div>
        <form class="search-message-box" action="{{route('messages_search')}}" method="get">
            <input pattern=".{3,}" required title="3 characters minimum" id="search_input"
                    type="text" name="q" placeholder="Search" @isset($q) value="{{$q}}" @endisset/>
            <span class="btn-search-message icon-search_icon"></span>
        </form>

        <!-- end of Top tabs-->
        @if (count($messages))
            <!-- Content of tabs-->
            <div class="tab-content profile-tab-content">
                <div class="tab-pane fade in active">
                    <div class="tab-inner">
                        <ul class="message-list">
                                <li class="message-item message-item--placeholder">
                                    <div class="message-item--short">
                                        <div class="select-option">
                                        </div>
                                        <div class="controllers">
                                        </div>
                                        <div class="placeholders">
                                            <div class="person">
                                                From/To
                                            </div>
                                            <div class="id-lot">LastBid Lot No</div>
                                            <div class="subject">Subject</div>
                                            <div class="lot-expires-date lot-expires-sort">Lot Expires
                                            <svg id='svg-arrow-down' width="6px" height="4px" viewBox="0 0 6 4" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                            <g id="Accounts" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                <g id="Seller_messages_1_3" transform="translate(-1236.000000, -294.000000)" fill="#DCDCDC">
                                                    <g id="table_header" transform="translate(405.000000, 285.000000)">
                                                        <g id="sorting" transform="translate(831.000000, 3.000000)">
                                                            <polygon id="Rectangle" points="6 6 3 10 -1.07184979e-13 6"></polygon>
                                                        </g>
                                                    </g>
                                                </g>
                                            </g>
                                        </svg> 
                                        <svg id='svg-arrow-up' width="6px" height="4px" viewBox="0 0 6 4" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                            <g id="Accounts" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                <g id="Seller_messages_1_3" transform="translate(-1236.000000, -288.000000)" fill="#DCDCDC">
                                                    <g id="table_header" transform="translate(405.000000, 285.000000)">
                                                        <g id="sorting" transform="translate(831.000000, 3.000000)">
                                                            <polygon id="Rectangle-Copy-3" transform="translate(3.000000, 2.000000) scale(1, -1) translate(-3.000000, -2.000000) " points="6 0 3 4 -1.07184979e-13 0"></polygon>
                                                        </g>
                                                    </g>
                                                </g>
                                            </g>
                                        </svg>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                @foreach($messages as $message)
                                    <li class="message-item @if ($message->is_read) message-item--read @else message-item--unread @endif"
                                    data-href="{{ $message->url }}">
                                        <div class="message-item--short">
                                            <div class="select-option">
                                            </div>
                                            <div class="person">
                                                @if ($message->is_sent)
                                                    To: {{$message->recipient->short_name}}
                                                @else
                                                    From: {{$message->sender->short_name}}
                                                @endif
                                            </div>
                                            <div class="id-lot">{{$message->lot_id}}</div>
                                            <div class="subject">{{$message->lot->title}}
                                            </div>
                                            <div class="lot-expires-date">{{$message->lot->end_date->toFormattedDateString()}}</div>
                                        </div>
                                    </li>
                                @endforeach
                        </ul>
                    </div>
                </div>
            </div>
            <!-- End of content of tabs-->
            <!-- Pagination-->
            @include('inc.paginator_with_page_selector', ['items'=>$messages])
        @else
            <p class="notification-no-content">No results for your query!</p>
        @endif
    </div>
    <!-- end of Pagination-->
@endsection
@section('user_script')
    //click on message
    $('.message-item:not(.message-item--placeholder)').on('click', function(e){
    if($(e.currentTarget).is('li')){
    window.location = $(e.currentTarget).attr('data-href');
    }
    });
    $('.btn-search-message').on('click', function (e) {
    var el = $(e.target);
    if (el.siblings('input')[0].value.length > 2){
    el.parents('form').submit();
    } else {
        $('#search_input').tooltip('show');
    }
    });
@endsection
