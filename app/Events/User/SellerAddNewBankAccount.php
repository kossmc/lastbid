<?php

namespace App\Events\User;

use App\Models\BuyerAccount;
use App\Models\User;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;

class SellerAddNewBankAccount
{
    use Dispatchable, SerializesModels;

    private $user;

    private $account;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(BuyerAccount $account,User $user)
    {
        $this->account = $account;
        $this->user = $user;
    }
}
