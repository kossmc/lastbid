<?php

namespace App\Listeners\Bid;

use App\Events\Bid\BuyerCreateBid;

class BuyerCreateBidListener extends SendGridAbstractBidListener
{
    /**
     * Buyer Email subject.
     */
    private $buyerSubject = 'You have placed a bid';

    /**
     * Buyer Email template.
     */
    private $buyerTemplate = 'f1ce9a99-8b0b-46d8-8948-643e2377498e';

    /**
     * Seller Email subject.
     */
    private $sellerSubject = 'Buyer has placed a bid';

    /**
     * Seller Email template.
     */
    private $sellerTemplate = '015f9e90-bf1f-49ef-b941-6865d8aeea8b';

    /**
     * Handle the event.
     *
     * @param  BuyerCreateBid  $event
     * @return void
     */
    public function handle(BuyerCreateBid $event)
    {
        $this->setData($event);
        /*notification to seller*/
        $this->sendMail($this->seller, $this->getData($this->sellerTemplate), $this->sellerSubject);
        /*notification to buyer*/
        $this->sendMail($this->buyer, $this->getData($this->buyerTemplate), $this->buyerSubject);
    }
}
