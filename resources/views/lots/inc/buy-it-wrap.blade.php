<div id="buy-it-wrap" class="buy-it-wrap bui-it-wrap">
    @if (
        $lotState->lotIsActive()->isSeller()->isNotLotOwner()->getState() ||
        $lotState->lotIsActive()->isSeller()->isLotOwner()->lotHasNoBids()->getState()
    )
        @include('lots.inc.lot_info')
    @elseif ($lotState->lotIsActive()->isSeller()->isLotOwner()->lotHasBids()->getState())
        @include('lots.inc.lot_info_bids')
    @elseif (
        $lotState->lotIsExpired()->getState() ||
        $lotState->lotIsClosed()->getState()
    )
        @include('lots.inc.lot_closed')
    @elseif (
        $lotState->lotIsSoldOuside()->getState() ||
        $lotState->lotIsSold()->isSeller()->isNotLotOwner()->getState() ||
        $lotState->lotIsSold()->isGuest()->getState() ||
        $lotState->lotIsSold()->isBuyer()->isNotWinner()->getState()
    )
        @include('lots.inc.lot_sold')
    @elseif ($lotState->lotIsSold()->isBuyer()->isWinner()->getState())
        @include('lots.inc.you_are_winner')
    @elseif ($lotState->lotIsActive()->isBuyer()->isYourBid()->getState())
        @include('lots.inc.your_bid')
    @elseif ($lotState->lotIsActive()->isBuyer()->isCounterBid()->getState())
        @include('lots.inc.your_counter_bid')
    @elseif ($lotState->lotIsSold()->isSeller()->isLotOwner()->getState())
        @include('lots.inc.bid_accepted')
    @else
        @if ($canPlaceBid)
            @include('lots.inc.place_bid')
        @else
            @include('lots.inc.lot_info')
        @endif
    @endif
</div>