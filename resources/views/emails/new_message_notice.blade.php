<p>Lot name: <a href="{{ $lot->url }}">{{ $lot->title }}</a></p>
<p>Lot#: {{ $lot->id }}</p>
<p>Sender name: {{ $sender->short_name }}</p>
<p>Message created: {{ $msg->latest->created_at }}</p>
<p><a href="{{ $msg->url }}">Read message at the site</a></p>
<p>Message:</p>
<p>{{ $msg->latest->text }}</p>

