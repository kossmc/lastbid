<?php

namespace App\Services;

use Carbon\Carbon;
use Symfony\Component\DomCrawler\Crawler;

class RmSothebysCrawler extends BaseCrawler
{
    /**
     * temp file
     *
     * @var string
     */
    public $tempFile;

    /**
     * base url
     *
     * @var string
     */
    public $baseUrl;

    /**
     * parse url
     *
     * @var string
     */
    public $parseUrl;

    /**
     * Items list
     *
     * @var array
     */
    public $items;

    /**
     * Auction items count
     *
     * @var int
     */
    public $totalItemsCount = null;

    /**
     * RmSothebysCrawler constructor.
     * @param $batch
     */
    public function __construct($batch)
    {
        $this->filename = 'Rm_Sothebys_' . $batch->input . '_' . date('Y-m-d_H-i-s') . '.csv';

        $this->tempFile = $this->tempFolder . $this->filename;

        $this->baseUrl = 'https://rmsothebys.com';
        $this->parseUrl = 'https://rmsothebys.com/umbraco/Surface/SearchPageSurface/SearchLotsJson';

        $this->items = [];

        parent::__construct($batch);
    }

    /**
     * @return array|bool
     */
    public function startCrawling()
    {
        $data = $this->getLots();
        if ($data) {
            $this->headers = array_keys($data[0]);
        }
        return $data;
    }

    /**
     * Generate search data
     * @param int $page
     * @return array
     */
    public function getPayload($page = 1) {
        $search = [
            "SortBy" => "Default",
            "SearchTerm" => "",
            "Category" => "Automobiles",
            "FromYear" => null,
            "ToYear" => null,
            "IncludeWithdrawnLots" => false,
            "Auction" => $this->batch->input,
            "OfferStatus" => "All availability",
            "AuctionYear" => "",
            "Model" => "Model",
            "Make" => "Make",
            "FeaturedOnly" => false,
            "StillForSaleOnly" => false,
            "Collection" => "All Lots",
            "WithoutReserveOnly" => false,
            "Tags" => null
        ];

        $params = [
            "page" => $page,
            "pageSize" => 40,
            "title" => "ALL LOTS"
        ];

        $payload = array_merge($params, [
            "search" => $search
        ]);

        return $payload;
    }

    /**
     * Get start auction
     * @return array|bool
     */
    public function getDates() {

        $response = $this->client->request('GET', $this->baseUrl . '/en/auctions/' . $this->batch->input);
        if ($response->getStatusCode() != 200) {
            \Log::info(print_r($response, true));
            return false;
        }

        $content = $this->processContent($response->getBody()->getContents());

        $crawler = new Crawler();
        $crawler->add($content);

        $dates = $crawler->filter('.info__detail .detail__date')->each(function (Crawler $node, $i) {
            return Carbon::parse(trim($node->filter('a')->text()));
        });

        return $dates;
    }

    /**
     * Get all lots and format data
     * @return array|bool
     */
    public function getLots()
    {
        $startDate = Carbon::now();
        $endDate = $startDate->addMonths(1);

        $dates = $this->getDates();
        if($dates){
            $startDate = $dates[0];
            $endDate = $dates[count($dates) - 1];
        }

        $count = 0;
        $page = 1;

        do {
            $response = $this->client->request(
                'POST',
                $this->parseUrl,
                [\GuzzleHttp\RequestOptions::JSON => $this->getPayload($page)]
            );

            if ($response->getStatusCode() != 200) {
                \Log::info(print_r($response, true));
                return false;
                break;
            }
            if (!$json = json_decode($response->getBody()->getContents())) {
                break;
            }

            if (count($json->Items) <= 0) {
                break;
            }

            if (is_null($this->totalItemsCount)) {
                $this->totalItemsCount = $json->Pager->TotalItems;
            }
            $count += count($json->Items);

            foreach ($json->Items as $item) {

                $content = $this->getLotInfo($item->Link);

                $photos = [];
                foreach ($content['photos'] as $photoUrl) {
                    $photos[] = $this->loadImage($photoUrl);
                }

                $estimate = $item->PriceForWeb;

                try{
                    if(!empty($estimate) && strpos($estimate, '-') !== false){
                        list($from, $to) = explode('-', trim($estimate));

                        $from = preg_replace('/[^0-9]/', '', $from);
                        $to = preg_replace('/[^0-9]/', '', $to);

                        $content['price_from'] = ($from) ? $from / 1000: 0;
                        $content['price_to'] = ($to) ? $to / 1000: 0;
                    }
                }catch (\Exception $e){
                    \Log::info($e->getMessage());
                }

                $this->items[] = [
                    'lot_id' => preg_replace("/[^0-9]/", '', $item->Header),
                    'product_id' => $item->Id,
                    'Title' => $item->PublicName,
                    'Description' => '<p>' . $content['description'] . '</p>',
                    'Category' => 'Automobiles',
                    'Sub Category' => '',
                    'Partner' => $this->partner_id,
                    'Shipping description' => '',
                    'Auction House Fee' => 20,
                    'Minimum bid amount' => $content['price_from'] * 0.5,
                    'Price range from' => $content['price_from'],
                    'Price range to' => $content['price_to'],
                    'Price to auto-accept bid' => $content['price_from'] * 0.7,
                    'Start date' => $startDate->format('Y-m-d H:i:s'),
                    'End date' => $endDate->format('Y-m-d H:i:s'),
                    'Seller' => $this->seller_id,
                    'Currency' => 'usd',
                    'Lot photos' => implode(',', $photos)
                ];
            }

            $page ++;
        } while (!is_null($this->totalItemsCount) && $this->totalItemsCount > $count);


        \Log::info($this->totalItemsCount);
        \Log::info($count);
        \Log::info(count($this->items));

        return count($this->items) > 0 ? $this->items : false;
    }

    /**
     * @param $link
     * @return array
     */
    public function getLotInfo($link) {

        $result = ['description' => '', 'photos' => [], 'price_from' => 0, 'price_to' => 0];

        $response = $this->client->request('GET', $this->baseUrl . $link);

        if ($response->getStatusCode() != 200) {
            return $result;
        }

        if (!$content = $response->getBody()->getContents()) {
            return $result;
        }

        if (preg_match('/<section class="lot__specs">(.*)<\/section>/Usi', $content, $matches)) {
            $description = $matches[0];
            if (preg_match_all('/<li class="list-check__item">(.*)<\/li>/Usi', $description, $desc)) {
                $result['description'] = implode('<br>', $desc[1]);
            }

            if (preg_match_all('/<picture>(.*)src="(.*)"(.*)<\/picture>/Usi', $content, $pic)) {
                $result['photos'] = $pic[2];
            }
        }

        return $result;
    }
}