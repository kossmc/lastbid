@foreach($lots as $lot)
    <div class="item-cat">
        <div class="image">
            <div class="inner">
                <div class="vertical-middle">
                    <a href="{{ $lot->url }}">
                        <img src="{{ $lot->getImagePath(120, 160) }}" alt="">
                    </a>
                </div>
            </div>
        </div>
        <div class="desc-wrap">
            <div class="price">{!! format_price($lot->range_from, $lot->range_to, $lot->currency) !!}</div>
            <div class="desc">
                <a href="{{ $lot->url }}">{{ $lot->title }}</a>
            </div>
            @if ($lot->is_active)
                <div class="info-bottom time-wrap-line">
                    <div class="time-line">
                        <div class="time-line-left" style="width:{{ percent_time_left($lot->start_date, $lot->end_date) }}%"></div>
                    </div>

                    @if(\Carbon\Carbon::createFromTimeString($lot->end_date)->timestamp < \Carbon\Carbon::now()->timestamp)
                        Sorry, lot has already expired.
                    @else
                        {{ format_time_left($lot->end_date) }} left
                    @endif
                </div>
            @endif
            <div class="info-product">
                {{--<div class="item-info">{{ $lot->bids_count }} <span>bids</span></div>--}}
                <div class="item-info">{{ $lot->views_count}} <span>views</span></div>
            </div>
        </div>
    </div>
@endforeach
