// function CardValidation(formSelector, hasAdditionalFields){
//
//     console.log(123)
//         var CURRENT_YEAR = new Date().getFullYear();
//         var CURRENT_MONTH = new Date().getMonth();
//
//         var hasAdditionalFields;
//         if(arguments[1] != undefined && arguments[1] == true){
//             hasAdditionalFields = true;
//         }
//
//
//         var form = $(formSelector);
//         var controlls = {
//             number: form.find('input[name="cc"]'),
//             cvv: form.find('input[name="card_cvv"]'),
//             selectedMonth: form.find('span.card_month'),
//             month: form.find('ul.select-month li'),
//             year: form.find('ul.select-year li'),
//             selectedYear: form.find('span.card_year'),
//             sumbitBtn: form.find('[type="submit"]')
//         };
//
//
//         // order: American Express, Master Card, Visa
//         var cards = {
//             length: [15, 16, 16],
//             cvv: [4, 3, 3],
//         };
//
//
//         var state = {
//             'number': false,
//             'cardType': -1,
//             'cvv': false,
//             'month': -1,
//             'year': -1,
//             'date': false
//         };
//
//         if(hasAdditionalFields){
//             controlls.name = form.find('input[name="card_name"]');
//             state.name = false;
//         }
//
//
//
//
//         setEvents();
//
//         function setEvents(){
//
//            checkOnWindowLoad();
//
//             if(hasAdditionalFields){
//                 controlls.name.on('input', inputHandlerName);
//             }
//             controlls.number.on('input', Module.monitorCardNumber.bind(null, controlls.number, state));
//             controlls.number.on('input', focusHandlerNumber);
//             controlls.number.on('change', changeHandlerNumber);
//             controlls.cvv.on('input', inputHandlerCvv);
//             controlls.month.on('click', clickhandlerMonth);
//             controlls.year.on('click', clickHandlerYear);
//         }
//
//         //---------------------------- Event Handlers -------------------------------------//
//
//         function checkOnWindowLoad(){
//
//             if(controlls.name.val() != ''){
//                 notification(controlls.name, true);
//                 state.name = true;
//             }
//
//             if(controlls.number.val() != ''){
//                 Module.monitorCardNumber(controlls.number, state);
//             }
//
//             if(controlls.cvv.val() != ''){
//                 inputHandlerCvv(controlls.cvv.val());
//             }
//
//             if(controlls.selectedMonth.text() != 'Exp. month' && controlls.selectedYear.text() != 'Exp. year'){
//                 notification([controlls.selectedYear, controlls.selectedMonth], true);
//                 state.date = true;
//             }
//
//             verification(state);
//         }
//
//         function inputHandlerName(){
//
//             var val = $(this).val();
//             if(val.trim() != ''){
//                 $(this).addClass('validated').removeClass('not-validated');
//                 state.name = true;
//             }
//             else{
//                 $(this).addClass('not-validated').removeClass('validated');
//                 state.name = false;
//             }
//
//             verification(state);
//         }
//
//
//         function focusHandlerNumber(){
//             if(state.cvv){
//                 state.cvv = false;
//                 controlls.cvv.val('');
//                 notification(controlls.cvv, state.cvv);
//             }
//             verification(state);
//         }
//
//         function changeHandlerNumber(){
//
//             Module.monitorCardNumber(controlls.number, state);
//             verification(state);
//
//         }
//
//         function inputHandlerCvv(e){
//
//             var $el = typeof e != 'string' ? $(this) : e;
//             var length = typeof e != 'string' ? $el.val().length : $el.length;
//             var value = typeof e != 'string' ? $el.val() : e;
//
//             state.cvv = cvv_verification(value, state.cardType);
//             notification(controlls.cvv, state.cvv && length == cards.cvv[state.cardType]);
//             verification(state);
//
//         }
//
//         function clickhandlerMonth(){
//             checkMonth($(this));
//         }
//
//         function checkMonth(el){
//
//             var val = el.text();
//             state.month = getMonthIndex(val);
//
//             if(state.year != -1){
//                 state.date = date_verification(state.month, state.year);
//                 notification([controlls.selectedYear, controlls.selectedMonth], state.date);
//             }
//             verification(state);
//
//         }
//
//
//         function clickHandlerYear(){
//             checkYear($(this));
//         }
//
//         function checkYear(el){
//
//             var val = el.text();
//             state.year = parseInt(val);
//             state.date = date_verification(state.month, state.year);
//             notification([controlls.selectedYear, controlls.selectedMonth], state.date);
//             verification(state);
//
//         }
//
//
//
//         //-------------------- Verification functions -----------------------------------------//
//
//
//         function cvv_verification(str, cardIndex){
//             return cardIndex == -1 ? false : str.length == cards.cvv[cardIndex] ? true : false;
//         }
//
//
//         function date_verification(month, year){
//
//             if(year > CURRENT_YEAR) return true;
//             if(year < CURRENT_YEAR) return false;
//             if(year == CURRENT_YEAR && month < CURRENT_MONTH){
//                 return false;
//             }
//             if(year == CURRENT_YEAR && month >= CURRENT_MONTH){
//                 return true;
//             }
//         }
//
//         function verification(state){
//
//             if(state.number && state.cvv && state.date){
//                 controlls.sumbitBtn.removeAttr("disabled");
//             }
//             else{
//                 controlls.sumbitBtn.attr("disabled", true);
//             }
//             if(hasAdditionalFields == true){
//
//                 if(state.name == true && state.number && state.cvv && state.date){
//                     controlls.sumbitBtn.removeAttr("disabled");
//                 }
//                 else{
//                     controlls.sumbitBtn.attr("disabled", true);
//                 }
//
//             }
//         }
//
//
//         function notification(elements, isPassed){
//
//             if(isPassed){
//                 $.each(elements, function(index, element){
//                     $(this).addClass('validated').removeClass('not-validated');
//                 });
//             }
//             else{
//                 $.each(elements, function(index, element){
//                     $(this).addClass('not-validated').removeClass('validated');
//                 });
//             }
//         }
//
//         function getMonthIndex(name){
//             var monthes = ['January', 'February', 'March', 'April','May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
//             return monthes.indexOf(name);
//         }
//
//     }
/************************************************************************************************************* */
function CardValidation(formSelector, hasAdditionalFields){
    var form = $(formSelector), controls = {};
    var domControls = form.find('input[type=text], input[type=password], select');

    form.find('.select-style li').click(function(){
        $(this).parent().siblings('select').val($(this).text()).trigger('change');
    });

    domControls.each(function(){
        controls[$(this).attr('name')] = $(this);
    }).on('input change', function(){
        (validators[$(this).attr('name')] || validators['default'] || $.noop)(this, $(this).val());
    });

    form.find('input[name=cc], input[name=card_cvv]').keydown(function(e) {
        var key = e.charCode || e.keyCode || 0;
        return (key == 8 || key == 9 || key == 13 || key == 46 || key == 110 || key == 116 || (key >= 35 && key <= 40) || (key >= 48 && key <= 57) || (key >= 96 && key <= 105));
    });

    var validators = {
        cc: function(el, value) {
            if(!value) {
                setValidationState(el, false);
            } else {
                var numberValue = value.replace(/ /g, '');
                var type = getCardType(numberValue);

                if(type) {
                    if(numberValue.length == type.cc_length) {
                        $(el).attr('class', 'cc_type_'+type.code);
                        setValidationState(el, true);
                        if(controls.card_cvv.val()) setValidationState(controls.card_cvv, controls.card_cvv.val() == type.cvv_length);
                    } else {
                        $(el).attr('class', '');
                        setValidationState(el, false);
                    }

                    for(var i = 0; i < Math.min(type.format.length, numberValue.length); i++) {
                        if (type.format.charAt(i) !== 'x') {
                            numberValue = numberValue.slice(0, i) + type.format.charAt(i) + numberValue.slice(i);
                        }
                    }
                }

                $(el).val(numberValue);
                // controls.card_cvv.val('');
                //setValidationState(controls.card_cvv, false);
            }
        },
        card_cvv: function(el, value) {
            var cardNumber = controls.cc.val().replace(/ /g, '');
            var type = getCardType(cardNumber);
            setValidationState(el, type && value && value.length == type.cvv_length);
        },
        month: function(elMonth, valueMonth){
            var options = $(elMonth).siblings('ul').children('li');
            valueMonth = options.index(options.filter(':contains("'+valueMonth+'")'))+1;
            var elYear = controls.year;
            var valueYear = +elYear.val();

            if(valueMonth && valueYear) {
                var valid = valueYear > new Date().getFullYear() || valueYear == new Date().getFullYear() && valueMonth > new Date().getMonth();
            } else {
                valid = null;
            }

            setValidationState(elMonth, valid);
            setValidationState(elYear, valid);
        },
        year: function() {
            validators.month(controls.month, controls.month.val());
        },
        state: function(el, value) {
            if(!value) {
                setValidationState(el, false);
            }else {
                setValidationState(el, true);
            }
        },
        country: function(el, value) {
            if(!value || !el) return;

            let selectedState = document.getElementsByName('state')[0];

            validators.state(selectedState, selectedState.value);
        },
        default: function(el, value) {
            setValidationState(el, !!value);
        }
    };

    function setValidationState(el, valid, errorMessage) {
        $(el).data('valid', valid);
        $(el).removeClass('has-error--bgcolor validated').siblings('span:not(.help-block)').removeClass('has-error--bgcolor validated');
        $(el).siblings('span.help-block').remove();
        // country = document.getElementById('country');

        if(valid) {
            $(el).addClass('validated').siblings('span:not(.help-block)').addClass('validated');
            $("#country").addClass('validated');
        } else if(valid === null) {
            //do nothing
        } else {
            $(el).addClass('has-error--bgcolor').siblings('span:not(.help-block)').addClass('has-error--bgcolor');
            $('#country').removeClass('validated');
            var mesg = errorMessage || messages[$(el).attr('name')] || '';
            $(el).parent().append('<span class="help-block"><strong>'+mesg+'</strong></span>');
        }

        setSubmitState();
    }

        function setSubmitState() {
        for(var name in controls) {
            if(controls[name].data('valid') !== true && name != 'country') {
                return form.find('[type="submit"]').attr('disabled', true);
            }
        }

        form.find('[type="submit"]').attr('disabled', false);
    }

    var messages = {
        'card_name': '*Please enter your name as listed on your credit card. First and Last Name must matches the credit card',
        'cc': '*Please enter your credit card number',
        'month': '*Please check the front of your card for the correct expiration date',
        'card_cvv': '*Please enter a valid CVV/CVC. You can find it on the back of your card',
        'city': '*Please enter your credit card\'s City',
        'zip': '*Please enter your credit card\'s Zip/Postal Code',
        'state': '*Please enter your card\'s State/Region/Province. Case sensitive',
        'no_state': '*No such State/Region/Province. Please enter a valid State/Region/Province. Case sensitive',
        'country': '*Please enter your card\'s Country',
        'address': '*Please enter your card\'s Address'
    };

    function getCardType(card) {
        for(var i in card_types) {
            if(card.match(card_types[i].pattern)) return card_types[i];
        }
        return null;
    };

    var card_types = [{
        name: 'American Express',
        code: 'ax',
        pattern: /^3[47]/,
        cc_length: 15,
        cvv_length: 4,
        format: 'xxxx xxxxxxx xxxx'
    }, {
        name: 'Diners Club',
        code: 'dc',
        pattern: /^3[68]/,
        cc_length: 14,
        cvv_length: 3,
        format: 'xxxx xxxx xxxx xx'
    }, {
        name: 'JCB',
        code: 'jc',
        pattern: /^35/,
        cc_length: 16,
        cvv_length: 3,
        format: 'xxxx xxxx xxxx xxxx'
    }, {
        name: 'Visa',
        code: 'vs',
        pattern: /^4/,
        cc_length: 16,
        cvv_length: 3,
        format: 'xxxx xxxx xxxx xxxx'
    }, {
        name: 'Mastercard',
        code: 'mc',
        pattern: /^5[1-5]/,
        cc_length: 16,
        cvv_length: 3,
        format: 'xxxx xxxx xxxx xxxx'
    }
    ];
}

var Module = (function(){


    var card_types = [
        {
            name: 'American Express',
            code: 'ax',
            pattern: /^3[47]/,
            valid_length: [15],
            formats : [
                {
                    length: 15,
                    format: 'xxxx xxxxxxx xxxx'
                }
            ]
        }, {
            name: 'Diners Club',
            code: 'dc',
            pattern: /^3[68]/,
            valid_length: [14],
            formats : [
                {
                    length: 14,
                    format: 'xxxx xxxx xxxx xx'
                }
            ]
        }, {
            name: 'JCB',
            code: 'jc',
            pattern: /^35/,
            valid_length: [16],
            formats : [
                {
                    length: 16,
                    format: 'xxxx xxxx xxxx xxxx'
                }
            ]
        }, {
            name: 'Visa',
            code: 'vs',
            pattern: /^4/,
            valid_length: [16],
            formats : [
                {
                    length: 16,
                    format: 'xxxx xxxx xxxx xxxx'
                }
            ]
        }, {
            name: 'Mastercard',
            code: 'mc',
            pattern: /^5[1-5]/,
            valid_length: [16],
            formats : [
                {
                    length: 16,
                    format: 'xxxx xxxx xxxx xxxx'
                }
            ]
        }
    ];


    var isValidLength = function (cc_num, card_type) {
		for(var i in card_type.valid_length) {
			if (cc_num.length <= card_type.valid_length[i]) {
				return true;
			}
		}
		return false;
	};
	
	var getCardType = function (cc_num) {
		for(var i in card_types) {
			var card_type = card_types[i];
			if (cc_num.match(card_type.pattern) && isValidLength(cc_num, card_type)) {
				return card_type;
			}
		}
	};
	
	var getCardFormatString = function (cc_num, card_type) {
		for(var i in card_type.formats) {
			var format = card_type.formats[i];
			if (cc_num.length <= format.length) {
				return format.format;
			}
		}
	};
	
	var formatCardNumber = function (cc_num, card_type) {
		var numAppendedChars = 0;
		var formattedNumber = '';
		
		if (!card_type) {
			return cc_num;
		}
		
		var cardFormatString = getCardFormatString(cc_num, card_type);
		for(var i = 0; i < cc_num.length; i++) {
			cardFormatIndex = i + numAppendedChars;
			if (!cardFormatString || cardFormatIndex >= cardFormatString.length) {
				return cc_num;
			}
			
			if (cardFormatString.charAt(cardFormatIndex) !== 'x') {
				numAppendedChars++;
				formattedNumber += cardFormatString.charAt(cardFormatIndex) + cc_num.charAt(i);
			} else {
				formattedNumber += cc_num.charAt(i);
			}
		}
		
		return formattedNumber;
    };  
	

    var addCardClassIdentifier = function ($elem, card_type, cc_num, _state) {
		var classIdentifier = 'cc_type_unknown';
		if (card_type && card_type.valid_length[0] === cc_num.length) {
			classIdentifier = 'cc_type_' + card_type.code;
		}		
		if (!$elem.hasClass(classIdentifier)) {

			var classes = '';
			for(var i in card_types) {
				classes += 'cc_type_' + card_types[i].code + ' ';
			}
			$elem.removeClass(classes + 'cc_type_unknown');
            $elem.addClass(classIdentifier);
            
            if(classIdentifier == 'cc_type_vs'){
                _state.number = true;
                _state.cardType = 2;
            }
            if(classIdentifier == 'cc_type_mc'){
                _state.number = true;
                _state.cardType = 1;
            }
            if(classIdentifier =='cc_type_ax'){
                _state.number = true;
                _state.cardType = 0;
            }

		}
    };

    
    var monitorCardNumber = function($elem, _state){

        var cc_num = $elem.val().replace(/\D/g,'');
        var card_type = getCardType(cc_num);
        $elem.val(formatCardNumber(cc_num, card_type));
        addCardClassIdentifier($elem, card_type, cc_num, _state);  

    };
 
    return {
        monitorCardNumber: monitorCardNumber
    }

})();