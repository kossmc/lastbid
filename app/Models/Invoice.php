<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    protected $table = 'invoices';
    public $timestamps = true;
    protected $guarded = [];
    public $appends = [];
    const PATH = 'app/invoices/seller/';

    public function getStoragePathAttribute()
    {
        return storage_path(self::PATH."{$this->id}.pdf");
    }
}
