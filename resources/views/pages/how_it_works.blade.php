@extends('layouts.main')

@include('inc.page_metas', ['defTitle' => 'How it works'])

@section('top-links')
    @include('inc.top_links')
@stop
@section('content')
   @include('pages.how_it_works_content')
@endsection
