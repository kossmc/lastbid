<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

/**
 * Class LotFile
 *
 * @property $id
 * @property $name
 * @property $status
 * @property $created_at
 * @property $updated_at
 *
 * @package App\Models
 */
class LotFile extends Model
{
    const STATUS_ROLLBACK = 0;
    const STATUS_DONE = 1;

    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'lots_files';
    protected $primaryKey = 'id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
    protected $fillable = [
        'name',
        'status',
        'created_at',
        'updated_at'
    ];
    // protected $hidden = [];
    // protected $dates = [];

    /**
     * Get count lots in file
     * @return int
     */
    public function getCountTextAttribute()
    {
        $lots = LotVersion::where('lot_file_id', $this->attributes['id'])->withTrashed()->get();

        if($this->attributes['status'] == self::STATUS_DONE){

            $lotsBid = [];
            foreach ($lots as $lot){
                if(!$lot->deleted_at){
                    array_push($lotsBid, $lot->lot_id);
                }
            }

            if($lotsBid){
                return count($lots) . ' lots. ' . 'Not delete lots: ' . implode(',', $lotsBid);
            }
        }

        return count($lots) . ' lots';
    }

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public function getStatusButtonHtml()
    {
        return view('admin.components.rollback_link', ['model' => $this])->render();
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function versions()
    {
        return $this->hasMany(LotVersion::class, 'lot_file_id', 'id');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
