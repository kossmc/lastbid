<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CanUserSendMessage
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->ajax() && $request->has('can_user_send_message')) {
            if ( Auth::guest()) {
                return response()->json([
                    'auth' => true,
                    'status' => false
                ]);
            } else {
                return response()->json([
                    'status' => true,
                ]);
            }
        }
        return $next($request);
    }
}
