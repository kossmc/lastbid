<div class="filter-wrapper">
    <div class="filter items-per-page-filter filter-default">
        <div class="item">
            <div class="name">Items per page:</div>
            <div class="select">
                <select onchange="window.location.search='per_page='+this.value">
                    <option @if ($per_page == 25) selected @endif value="25">25</option>
                    <option @if ($per_page == 50) selected @endif value="50">50</option>
                    <option @if ($per_page == 100) selected @endif value="100">100</option>
                </select>
            </div>
        </div>
    </div>
</div>