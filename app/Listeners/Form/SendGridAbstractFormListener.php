<?php

namespace App\Listeners\Form;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class SendGridAbstractFormListener implements ShouldQueue
{
    protected $email;
    protected $name;
    protected $message;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function setData($event)
    {
        $this->email = $event->email;
        $this->name = $event->name;
        $this->message = $event->message;
    }

    protected function getData($id)
    {
        $data = [
            "sub"=>[
                "-siteLink-" => [route('home')]
                ,"-emailPreferencesLink-" => [route('subscriptions')]
                ,"-name-"=>[$this->name]
                ,"-currentYear-"=>[date('Y')]
                //,"-message-"=>[$this->message]
            ],
            "filters"=>[
                "templates"=>[
                    "settings"=>[
                        "enable"=>1,
                        "template_id"=>$id
                    ]
                ],
                "subscriptiontrack"=>[
                    "settings"=>[
                        "enable"=>1,
                        "replace"=>'[unsubscribe]'
                    ]
                ]
            ]
        ];
        return asJSON($data);
    }

    protected function sendMail($data, $subject)
    {
        Mail::send('emails.empty', [], function ($message) use($subject, $data) {
            $message->getSwiftMessage()->getHeaders()->addTextHeader('X-SMTPAPI', $data);
            $message->to($this->email, $this->name)->subject($subject);
        });
    }
}
