<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Buyer invoice</title>

    <!-- Bootstrap core CSS -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <style>
        .table > thead > tr > th,
        .table > tbody > tr > td,
        .table > tfoot > tr > td {
            text-align: left;
            line-height: 1;
            font-size: 10px;
        }

        .table > tfoot > tr > td {
            font-weight: bold;
        }

        .logo {
            padding: 20px;
            margin-top: 20px;
        }

        hr {
            color: #E4E4E4;
            margin-bottom: 20px;
        }
    </style>
</head>

<body>

<div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center logo">
            <img src="{{asset('img/logo.svg')}}" alt="LastBid">
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <hr>
        </div>
    </div>

    <div class="row" style="margin-top: 20px;margin-bottom: 20px">
        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
            @yield('top_left_col')
        </div>
        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 text-right
                    col-xs-offset-4 col-sm-offset-4 col-md-offset-4 col-lg-offset-4">
            @yield('top_right_col')
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <hr>
        </div>
    </div>
    @yield('message')
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            @yield('table')
        </div>
    </div>
    <div class="row">
        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
            @yield('bottom_left_col')
        </div>
    </div>
</div><!-- /.container -->
</body>
</html>
