<?php

namespace App\Services;

use Carbon\Carbon;
use Symfony\Component\DomCrawler\Crawler;

class MaaklondonCrawler extends BaseCrawler
{
    /**
     * @var string
     */
    public $baseUrl = 'https://maaklondon.irostrum.com/AuctionIndex/10090';

    /**
     * MaaklondonCrawler constructor.
     * @param $batch
     */
    public function __construct($batch)
    {
        $this->filename = 'Maaklondon_auction' . date('Y-m-d_H-i-s') . '.csv';
        parent::__construct($batch);
    }
    /**
     * Parsing logic go here
     * array with results
     * @return array
     */
    public function startCrawling()
    {
        $pagination_links = [];
        $lots = [];
        $response = $this->client->request('GET', $this->baseUrl);
        if ($response->getStatusCode() == 200) {
            $page = new Crawler($response->getBody()->getContents(), $this->baseUrl);
            $pagination_links[] = 'https://maaklondon.irostrum.com/AuctionIndex/10090?PageNumber=1';
            $page->filter('body .lot-matrix .row')
                ->first()
                ->filter('a')
                ->each(function($sub_item) use (&$pagination_links){
                    if($sub_item->text() != 'next page »'){
                        $pagination_links[] = $sub_item->link()->getUri();
                    }
                });
            foreach ($pagination_links as $pg_link){
                $current_page_links = [];
                $response =  $this->client->request('GET', $pg_link);
                if ($response->getStatusCode() != 200) {
                    \Log::info(print_r($response, true));
                    return false;
                }
                $content = $this->processContent(
                    $response->getBody()->getContents()
                );
                $page = new Crawler($content, $pg_link);

                $page->filter('body .lot-block div a')->each(function ($item) use(&$current_page_links) {
                    $current_page_links[] = $item->link()->getUri();
                });
                foreach ($current_page_links as $cp_link){
                    $response =  $this->client->request('GET', $cp_link);
                    if ($response->getStatusCode() != 200) {
                        \Log::info(print_r($response, true));
                        return false;
                    }
                    $content = $this->processContent(
                        $response->getBody()->getContents()
                    );

                    $crawler = new Crawler($content, $cp_link);
                    try{
                        $res = $this->getLotInfo($crawler, $cp_link);
                        if(!$res){
                            $failed[] = $cp_link;
                            //dump("failed: ".count($failed)."");
                            continue;
                        }
                        $lots[] = $res;

                        //dump("success: ".count($lots)."");
                    } catch(\Exception $exception){
                        \Log::info(print_r($exception->getMessage(), true));
                    }
                }
            }
        }
        if ($lots) {
            $this->headers = array_keys($lots[0]);
        }
        return $lots;
    }

    /**
     * @param $crawler
     * @param $link
     * @return array
     *
     * Get lot information
     */
    public function getLotInfo($crawler, $link){
        $lot = [];
        $price = $this->getLotPrice($crawler);
        $photos = $this->getLotPhotos($crawler);
        $auctionDate = $this->getStartDate($crawler);
        $endDate = $auctionDate->format('Y-m-d H:i:s');
        $startDate = $auctionDate->subDays(3)->format('Y-m-d H:i:s');
        $lot['id'] = $this->getLotId($crawler);
        $lot['Title'] = $this->getLotTitle($crawler);
        $lot['Description'] = $this->getDescription($crawler);
        $lot['Category'] = 'Modern, Contemporary Ceramics,';
        $lot['Sub Category'] = '';
        $lot['Partner'] = $this->partner_id;
        $lot['Shipping description'] = '';
        $lot['Auction House Fee'] = 20;
        $lot['Minimum bid amount'] = isset($price['price_from']) ? $price['price_from'] * 0.5 : 0;
        $lot['Price range from'] = isset($price['price_from']) ? $price['price_from'] : 0;
        $lot['Price range to'] = isset($price['price_to']) ? $price['price_to'] : 0;
        $lot['Price to auto-accept bid'] = isset($price['price_from']) ? $price['price_from'] * 0.7 : 0;
        $lot['Start date'] = $startDate;
        $lot['End date'] = $endDate;
        $lot['Seller'] = $this->seller_id;
        $lot['Currency'] = isset($price['currency']) ? $price['currency'] : 'usd';
        $lot['Lot photos'] = implode(',', $photos);

        return $lot;
    }

    /**
     * @param $crawler
     * @return string|string[]|null
     * Get ID of lot
     */
    public function getLotId($crawler){
        return preg_replace(
            '/[^0-9]/',
            '',
            $crawler->filter('body .lotDetails .lot-numb')->text()
        );
    }

    /**
     * @param $crawler
     * @return string
     * get lot title
     */
    public function getLotTitle($crawler){
        return trim(
            preg_replace(
                '/^[0-9]*/',
                '',
                $crawler->filter('body .lot-breadcrumb span')->last()->text()
            )
        );
    }

    /**
     * @param $crawler
     * @return mixed
     *
     * get lot description
     */
    public function getDescription($crawler){
        return $this->processContent(
            trim(mb_convert_encoding($crawler->filter('body .lot-content')->text(), 'UTF-8'))
        );
    }

    /**
     * @param $crawler
     * @return array
     * get lot price (mix, max, currency)
     */
    public function getLotPrice($crawler){
        $price = explode('-', $crawler->filter('body .lot-text strong')->text());
        return [
            'currency' => 'gbp',
            'price_from' => preg_replace(
                '/[^0-9]/',
                '',
                $price[0]
            ),
            'price_to' => preg_replace(
                '/[^0-9]/',
                '',
                $price[1]
            ),
        ];
    }

    /**
     * @param $crawler
     * @return Carbon
     *
     * get lot auction date
     */
    public function getStartDate($crawler)
    {
        $dateHtml = $crawler->filter('body #AuctionEnds')->first();

        if (!$dateHtml)
            return Carbon::now();

        return Carbon::parse(trim($dateHtml->text()));
    }

    /**
     * @param $content
     * @return mixed
     *
     * remove special characters
     */
    public function processContent($content) {
        return str_replace(["\n\r", "\0", "\x0B", "\n", "\r", "\t", "£", "   ", PHP_EOL], '', $content);
    }

    /**
     * @param $crawler
     * @return array
     *
     * find, load and save lot photos
     */
    public function getLotPhotos($crawler){
        $photos = [];
        $photos[] = $this->loadImage($crawler->filter('body #LeftColumn img')->image()->getUri());
        $crawler->filter('body .lotThumbs img')->each(function($item) use(&$photos){
            $photos[] = $this->loadImage($item->image()->getUri());
        });
        return $photos;
    }

}