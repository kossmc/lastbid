<?php
namespace App\Repositories;

use App\Events\Bid\BuyerAutoAcceptBid;
use App\Events\Bid\SystemAutoAcceptBid;
use Bosnadev\Repositories\Eloquent\Repository;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Auth;
use App\Events\Bid\SellerAcceptBid;
use App\Events\Bid\SellerDeclineBid;
use App\Events\Bid\SellerCreateCounterBid;
use App\Events\Bid\BuyerAcceptCounterBid;
use App\Events\Bid\BuyerDeclineCounterBid;
use App\Events\Bid\BuyerCreateCounterBid;
use App\Events\Bid\BuyerCreateBid;
use App\Events\Bid\BidExpired;
use App\Events\Bid\SystemDeclineBid;
use App\Models\Bid;

class BidRepository extends Repository
{
    public function model()
    {
        return Bid::class;
    }

    public function depositAmount($bidAmount = 0)
    {
        return $bidAmount * env('BID_DEPOSIT');
    }

    public function createByBuyer(array $data)
    {
        if ($bid = $this->model->createByBuyer($data)) {
            if ($bid->lot->bidIsAutoAccept($data['amount'])) {
                Event::fire(new BuyerAutoAcceptBid($bid));
            }else{
                Event::fire(new BuyerCreateBid($bid));
            }
            return $bid;
        }
        return false;
    }

    // get bids by lot for seller
    public function lotBidsByStatus($lotId, $status = 'received') {
        $bids = $this->model->where('lot_id', $lotId)->withHistory();
        switch ($status) {
            case 'received':
                $bids = $bids->byBuyer()->acceptedOrPending();
                break;
            case 'sent':
                $bids = $bids->bySeller()->acceptedOrPending();
                break;
            case 'declined':
                $bids = $bids->declinedOrExpired();
                break;
        }
        return $bids->latest('updated_at')->get();
    }

    public function isMine($id)
    {
        // @todo: check if I can modify bid
        return $this->model->pending()->findOrFail($id);
    }

    public function acceptBySeller($bid) {
        if ($bid->acceptBySeller()) {
            Event::fire(new SellerAcceptBid($bid));
        }
    }

    public function declineBySeller($bid) {
        if ($bid->declineBySeller()) {
            Event::fire(new SellerDeclineBid($bid));
        }
    }

    public function counterBySeller($bid, array $data) {
        if ($bid->counterBySeller($data)) {
            Event::fire(new SellerCreateCounterBid($bid));
        }
    }

    public function acceptByBuyer($bid) {
        if ($bid->acceptByBuyer()) {
            Event::fire(new BuyerAcceptCounterBid($bid));
        }
    }

    public function declineByBuyer($bid) {
        if ($bid->declineByBuyer()) {
            Event::fire(new BuyerDeclineCounterBid($bid));
        }
    }

    public function counterByBuyer($bid, array $data) {
        if ($bid->counterByBuyer($data)) {
            Event::fire(new BuyerCreateCounterBid($bid));
            if ( $bid->lot->bidIsAutoAccept($data['amount'])) {
                Event::fire(new SystemAutoAcceptBid($bid));
            }
        }
    }

    public function declineBySystem($bid) {
        if ($bid->decline()) {
            Event::fire(new SystemDeclineBid($bid));
        }
    }

    public function markExpired() {
        $expiredBids = $this->model->getExpired();
        foreach ($expiredBids as $bid) {
            if ($bid->expire()) {
                Event::fire(new BidExpired($bid));
            }
        }
    }

    public function paginateReceived()
    {
        $userId = Auth::id();
        $query = $this->model->where('user_id', $userId)
            ->bySeller()
            ->acceptedOrPending()
            ->with('lot');
        $query = $this->_setSorting($query);
        return $this->_paginate($query);
    }

    public function paginateSent()
    {
        $userId = Auth::id();
        $query = $this->model->where('user_id', $userId)
            ->latest('updated_at')
            ->byBuyer()
            ->acceptedOrPending()
            ->with('lot')
            ->withCount(['attempts' => function($query) use ($userId) {
                $query->where('user_id', $userId);
            }]);
        $query = $this->_setSorting($query);
        return $this->_paginate($query);
    }

    public function paginateDeclined()
    {
        $userId = Auth::id();
        $query = $this->model->where('user_id', $userId)
            ->declinedOrExpired()
            ->with('lot')
            ->withCount(['attempts' => function($query) use ($userId) {
                $query->where('user_id', $userId);
            }]);
        $query = $this->_setSorting($query);
        return $this->_paginate($query);
    }

    public function getMyBid($lotId) {
        $userId = Auth::id();
        return $this->model
            ->where('lot_id', $lotId)
            ->where('user_id', $userId)
            ->withHistory()
            ->withCount(['attempts' => function($query) use ($userId) {
                $query->where('user_id', $userId);
            }])
            ->first();
    }

    /**
     * Sort bid collection
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    private function _setSorting($query)
    {
        if (request('sort_latest', 'DESC') == 'ASC') {
            return $query->oldest('updated_at');
        }
        return $query->latest('updated_at');
    }

    /**
     * Paginate bid collection
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    private function _paginate($query)
    {
        return $query->has('lot')
            ->paginate(request('per_page', env('PAGINATE_BUYER_BIDS', 25)));
    }
}
