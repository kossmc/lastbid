<?php

namespace App\Events\User;

use App\Models\BuyerCard;
use App\Models\User;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;

class BuyerAddNewCard
{
    use Dispatchable, SerializesModels;

    public $card;
    public $user;

    /**
     * Create a new event instance.
     * @param  BuyerCard $card
     * @param  User $user
     * @return void
     */
    public function __construct(BuyerCard $card, User $user)
    {
        $this->card = $card;
        $this->user = $user;
    }
}
