@extends('landing.content')

@section('content_page')

            <div class="some-text">
                <p>Only auction houses with a 100% sell-through - that is, not a single unsold item - have nothing to gain from LastBid.</p>
                <p>Everyone else can add extra earnings straight to their bottom line.</p>
            </div>

            <div id="discoverMore" class="modal">
                <div class="modal-content">
                    <span class="close">&times;</span>
                    <h2>discover more</h2>
                    <form method="post" action="subscribe" id="form-discover">
                        <div class="notification-sent-msg">
                            <p></p>
                        </div>
                        <span class="error-notification"></span>
                        <div class="item">
                            <input type="email" placeholder="Email" name="email" required
                                   class="{{ $errors->has('email') ? 'has-error--bgcolor' : '' }}">
                        </div>
                        <div class="wrap-check-policy">
                            <input type="checkbox" id="t-agreement-discover" name="terms" value="yes">
                            <label class="" for="t-agreement-discover">
                                I accept LastBid's <a href="{{ url('/terms') }}" class="terms-agreement-link">Terms & Conditions</a> and <a href="{{ url('/privacy') }}" class="terms-agreement-link">Privacy Policy</a>
                            </label>
                        </div>
                        <div class="item">
                            <button class="btn popup-btn" type="submit" disabled="disabled">send</button>
                        </div>
                    </form>
                </div>
            </div>

            @include('notifications/lets_talk')

            <div class="wrap wrap-main-fix">

                <div class="row-image-text clearfix">
                    <div class="wrap image-left">
                        <div class="image">
                            <img id="placeholderVideo" src="{{ asset('img/Intro600px.jpg')}}" width="600" alt="">
                            <video width="600" controls="" id="lastBidVideo" style="display: none">
                                <source src="{{ asset('uploads/LastBid_long_version.mp4')}}" type="video/mp4">
                                <p>Your browser doesn't support HTML5 video. Here is
                                    a <a href="{{ asset('uploads/LastBid_long_version.mp4')}}">link to the video</a> instead.
                                </p>
                            </video>
                        </div>
                        <div class="landing-form">
                            <div class="title">let's talk</div>
                            <p>We’re keen to start a dialogue with those
                                in the auction world to discuss how we could help,
                                so please do get in touch.
                            </p>

                            <form method="post" action="landing" id="form-landing">
                                <div class="notification-sent-msg">
                                </div>
                                <span class="error-notification">
                            </span>
                                <div class="item item__fullWidth">
                                    <input type="text" placeholder="Name *" name="name" required
                                           class="{{ $errors->has('name') ? 'has-error--bgcolor' : '' }}">
                                </div>
                                <div class="item">
                                    <input type="text" placeholder="Company *" name="company" required
                                           class="{{ $errors->has('company') ? 'has-error--bgcolor' : '' }}">
                                </div>
                                <div class="item">
                                    <input type="text" placeholder="Job title *" name="job_title" required
                                           class="{{ $errors->has('job_title') ? 'has-error--bgcolor' : '' }}">
                                </div>
                                <div class="item">
                                    <input type="email" placeholder="Email *" name="email" required
                                           class="{{ $errors->has('email') ? 'has-error--bgcolor' : '' }}">
                                </div>
                                <div class="item">
                                    <input type="text" placeholder="Telephone" name="telephone"
                                           class="{{ $errors->has('telephone') ? 'has-error--bgcolor' : '' }}">
                                </div>
                                <div class="item item__fullWidth">
                                <textarea placeholder="Message" name="message"
                                      class="{{ $errors->has('message') ? 'has-error--bgcolor' : '' }}"></textarea>
                                </div>
                                <div class="wrap-check-policy">
                                    <input type="checkbox" id="t-agreement" name="terms" value="1">
                                    <label class="" for="t-agreement">
                                        I accept Lastbid's <a href="{{ url('/terms') }}" class="terms-agreement-link">Terms & Conditions</a> and <a href="{{ url('/privacy') }}" class="terms-agreement-link">Privacy policy</a>
                                    </label>
                                </div>
                                <div class="item">
                                    <button class="btn popup-btn" type="submit" id="sendBtn" disabled="disabled">send</button>
                                </div>
                            </form>

                        </div>
                    </div>

                    <div class="text-block">
                        <div class="quotes">&#8220;</div>
                        <div class="line"></div>
                        <p>
                            “LastBid brings auction houses and potential buyers together after an auction has finished,
                            offering
                            unsold lots to a worldwide audience of interested bidders.
                        </p>
                        <p>
                            For auction houses, it’s a straightforward way of giving unsold lots a second chance, this time
                            to
                            many more clients worldwide. For buyers, it’s a simple, discreet method of making that 'last
                            bid' to
                            seal a deal, hence&nbsp;our&nbsp;name.
                        </p>
                        <p>
                            The contract between buyer and seller is strictly via the auction house. There is no extra cost
                            to
                            the buyer.
                        </p>
                        <p>
                            LastBid has negotiated arrangements with the leading auction houses of the world. Categories of
                            items offered by LastBid auction partners range from Watches to Wine, Jewellery to Jaguars.
                            Regular email newsletters and our insider’s blog keep LastBid users in the know on what’s
                            happening
                            in the market.
                        </p>
                        <p>
                            It’s as easy as one, two, three: Choose, Bid, Win.”
                        </p>
                        <p class="signature">
                            Peter Wallman
                        </p>
                        <p class="signature-footer">
                            Managing Director LastBid UK (LTD)
                        </p>
                    </div>

                </div>

            </div>

            <div id="videoPopup" class="modal">
                <div class="modal-content modal-content__video">
                    <span class="close"></span>

                    <div class="video-block">

                    </div>

                </div>
            </div>

            <style>

                .btn-main-center{
                    margin: 0 auto;
                    width: 210px;
                    display: block;
                }
            </style>

            <div class="wrap">

                <a class="btn btn-main btn-main-center" id="discoverMoreBtn">discover more</a>

            </div>

    <div id="modal-win"></div>
    <div id="modal-win2"></div>
@endsection

@section('remote_js_scripts')
    <script src="{{ asset('js/landing.js?ver='.time())}}"></script>
@endsection