<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Category;
use App\Models\Partner;
use App\Models\Lot;

class LotsTableSeeder extends Seeder
{
    public $categories = [
        '20th century' => '20th Century Design',
        'old master paintings' => 'Old Master Paintings',
        'asian art' => 'Asian Art',
        'automobiles' => 'Motor Cars',
        'contemporary art' => 'Contemporary Art',
        'furniture' => 'English Furniture',
        'impressionist & modern art' => 'Impressionist & Modern Art',
        'jewellery' => 'Jewellery',
        'motorcycles' => 'Motorcycles',
        'watches' => 'Wrist watches',
        'wine' => 'Wine',
        'ceramics' => 'Ceramics'
    ];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [];
        $faker = Faker\Factory::create();
        $seller = User::role('seller')->first();
        $partners = Partner::all()->pluck('id')->toArray();
        $lotsDirectoryPath = database_path('seeds/lots/*/');
        foreach (glob($lotsDirectoryPath . '*.csv') as $filename) {
            $data = $lot = [];
            if (($handle = fopen($filename, 'r')) !== false) {
                while (($row = fgetcsv($handle, 1000, ';')) !== false) {
                    $key = trim($row[0]);
                    $value = trim($row[1]);
                    if (!empty($value)) {
                        if ($key == 'photos') {
                            if (!isset($data[$key])) {
                                $data[$key] = [$value];
                            }
                            else {
                                $data[$key][] = $value;
                            }
                        }
                        else {
                            $data[$key] = $value;
                        }
                    }
                    if (!empty($row[2]) && !empty($row[3])) {
                        $key = trim($row[2]);
                        $value = trim($row[3]);
                        if (!empty($value)) {
                            if ($key == 'photos') {
                                if (!isset($data[$key])) {
                                    $data[$key] = [$value];
                                }
                                else {
                                    $data[$key][] = $value;
                                }
                            }
                            else {
                                $data[$key] = $value;
                            }
                        }
                    }
                }
                fclose($handle);
            }
            $title = $data['title'];
            $titleParts = explode("\r\n", $title);
            if (isset($titleParts[1])) {
                $title = $titleParts[1];
                $author = $titleParts[0];
            }
            $lot = [
                'title' => $title,
                'partner_id' => $faker->randomElement($partners),
                'user_id' => $seller->id,
                'slug' => \Cviebrock\EloquentSluggable\Services\SlugService::createSlug(Category::class, 'slug', $title),
                'start_date' => \Carbon\Carbon::now(),
                'end_date' => \Carbon\Carbon::now()->addDays($faker->numberBetween(60, 90)),
                'status' => Lot::STATUS_ACTIVE,
                'range_from' => $data['price_from'],
                'range_to' => $data['price_to'],
            ];
            if (isset($data['min_bid_amount'])) {
                $lot['min_bid_amount'] = $data['min_bid_amount'];
            }
            else {
                $lot['min_bid_amount'] = 0.7 * $data['price_from'];
            }
            if (isset($data['description'])) {
                $lot['description'] = '<p>' . str_replace(["\n", "\r"], ['<br>', ''], $data['description']) . '</p>';
            }
            // description
            if (isset($data['category'])) {
                $categoryName = strtolower($data['category']);
                if ($category = Category::where('title', $this->categories[$categoryName])->first()) {
                    $lot['category_id'] = $category->id;
                }
            }
            $lot['photos'] = [];
            if (isset($data['photos'])) {
                $photosCount = 0;
                foreach ($data['photos'] as $photo) {
                    if ($photosCount == 10) {
                        break;
                    }
                    $basename = basename($photo);
                    $filename = database_path('seeds/lots/photos/' . $basename);
                    if (is_file($filename)) {
                        $hash = md5($filename);
                        if (copy($filename, public_path('uploads/lots/' . $hash . '.jpg'))) {
                            $lot['photos'][] = 'lots/' . $hash . '.jpg';
                            $photosCount ++;
                        }
                    }
                }
            }
            $newLot = Lot::create($lot);
            if (!$newLot->save()) {
                dd($lot);
            }
            else {
                DB::table('lots')
                    ->where('id', $newLot->id)
                    ->update(['photos' => json_encode($lot['photos'])]);
            }
        }
    }
}
