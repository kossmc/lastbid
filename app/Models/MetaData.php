<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MetaData extends Model
{
    protected $table = 'meta_datas';

    public function metable()
    {
        return $this->morphTo();
    }
}