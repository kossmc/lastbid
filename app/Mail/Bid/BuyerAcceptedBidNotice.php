<?php

namespace App\Mail\Bid;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

use App\Traits\BidNoticeTrait;

class BuyerAcceptedBidNotice extends Mailable
{
    use Queueable, SerializesModels, BidNoticeTrait;

    /**
     * Email subject.
     *
     */
    protected $bid_notice_subject = 'Congratulations! Your bid was accepted by seller';

    /**
     * Email template.
     *
     */
    protected $bid_notice_template = 'emails.buyer_accepted_bid_notice';
}
