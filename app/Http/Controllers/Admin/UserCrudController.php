<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\NewSellerRequest;
use App\Http\Requests\UpdateUserRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Backpack\PermissionManager\app\Models\Role;

// VALIDATION: change the requests to match your own file names if you need form validation
//use App\Http\Requests\UserRequest as StoreRequest;
//use App\Http\Requests\UserRequest as UpdateRequest;

class UserCrudController extends CrudController
{
    public function setUp()
    {

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\User');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/user');
        $this->crud->setEntityNameStrings('user', 'users');

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */

        //        $this->crud->setFromDb();

        // ------ CRUD FIELDS
        // $this->crud->addField($options, 'update/create/both');
        $this->crud->addFields([
             [
                 'name' => 'first_name',
                 'label' => 'First name'
             ]
             ,
             [
                 'name' => 'last_name',
                 'label' => 'Last name'
             ]
             ,[
                 'name' => 'email',
                 'label' => 'User Email'
             ],
             [       // Select2Multiple = n-n relationship (with pivot table)
                 'label' => 'Roles',
                 'type' => 'select2_multiple',
                 'name' => 'roles', // the method that defines the relationship in your Model
                 'entity' => 'roles', // the method that defines the relationship in your Model
                 'attribute' => 'name', // foreign key attribute that is shown to user
                 'model' => config('laravel-permission.models.role'), // foreign key model
                 'pivot' => true, // on create&update, do you need to add/delete pivot table entries?
             ]
         ], 'update');
        $this->crud->addFields([
             [
                 'name' => 'first_name',
                 'label' => 'First name'
             ]
             ,[
                 'name' => 'last_name',
                 'label' => 'Last name'
             ]
             ,[
                 'name' => 'email',
                 'label' => 'User Email'
             ]
             ,[
                 'name' => 'password',
                 'label' => 'User password',
                 'type' => 'password'
             ],
             [       // Select2Multiple = n-n relationship (with pivot table)
                 'label' => 'Roles',
                 'type' => 'select2_multiple',
                 'name' => 'roles', // the method that defines the relationship in your Model
                 'entity' => 'roles', // the method that defines the relationship in your Model
                 'attribute' => 'name', // foreign key attribute that is shown to user
                 'model' => config('laravel-permission.models.role'), // foreign key model
                 'pivot' => true, // on create&update, do you need to add/delete pivot table entries?
             ]
         ], 'create');
        // $this->crud->removeField('name', 'update/create/both');
        // $this->crud->removeFields($array_of_names, 'update/create/both');

        // ------ CRUD COLUMNS
        $this->crud->setColumns(['first_name','last_name','email']);
        // $this->crud->addColumn(); // add a single column, at the end of the stack
        $this->crud->addColumn([
            // n-n relationship (with pivot table)
            'label' => 'Role', // Table column heading
            'type' => 'select_multiple',
            'name' => 'roles', // the method that defines the relationship in your Model
            'entity' => 'roles', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'model' => 'Backpack\PermissionManager\app\Models\Role', // foreign key model
        ]);
        // $this->crud->addColumns(); // add multiple columns, at the end of the stack
        // $this->crud->removeColumn('column_name'); // remove a column from the stack
        // $this->crud->removeColumns(['column_name_1', 'column_name_2']); // remove an array of columns from the stack
        // $this->crud->setColumnDetails('column_name', ['attribute' => 'value']); // adjusts the properties of the passed in column (by name)
        // $this->crud->setColumnsDetails(['column_1', 'column_2'], ['attribute' => 'value']);

        // ------ CRUD BUTTONS
        // possible positions: 'beginning' and 'end'; defaults to 'beginning' for the 'line' stack, 'end' for the others;
        // $this->crud->addButton($stack, $name, $type, $content, $position); // add a button; possible types are: view, model_function
        // $this->crud->addButtonFromModelFunction($stack, $name, $model_function_name, $position); // add a button whose HTML is returned by a method in the CRUD model
        // $this->crud->addButtonFromView($stack, $name, $view, $position); // add a button whose HTML is in a view placed at resources\views\vendor\backpack\crud\buttons
        $this->crud->addButtonFromView('top', 'add_seller', 'add_seller');
        $this->crud->removeButton('create');
        // $this->crud->removeButtonFromStack($name, $stack);

        // ------ CRUD ACCESS
        $this->crud->allowAccess(['list', 'create', 'update', 'delete']);
        // $this->crud->allowAccess(['list', 'create', 'update', 'reorder', 'delete']);
        // $this->crud->denyAccess(['list', 'create', 'update', 'reorder', 'delete']);

        // ------ CRUD REORDER
        // $this->crud->enableReorder('label_name', MAX_TREE_LEVEL);
        // NOTE: you also need to do allow access to the right users: $this->crud->allowAccess('reorder');

        // ------ CRUD DETAILS ROW
        // $this->crud->enableDetailsRow();
        // NOTE: you also need to do allow access to the right users: $this->crud->allowAccess('details_row');
        // NOTE: you also need to do overwrite the showDetailsRow($id) method in your EntityCrudController to show whatever you'd like in the details row OR overwrite the views/backpack/crud/details_row.blade.php

        // ------ REVISIONS
        // You also need to use \Venturecraft\Revisionable\RevisionableTrait;
        // Please check out: https://laravel-backpack.readme.io/docs/crud#revisions
        // $this->crud->allowAccess('revisions');

        // ------ AJAX TABLE VIEW
        // Please note the drawbacks of this though:
        // - 1-n and n-n columns are not searchable
        // - date and datetime columns won't be sortable anymore
        // $this->crud->enableAjaxTable();

        // ------ DATATABLE EXPORT BUTTONS
        // Show export to PDF, CSV, XLS and Print buttons on the table view.
        // Does not work well with AJAX datatables.
        // $this->crud->enableExportButtons();

        // ------ ADVANCED QUERIES
        // $this->crud->addClause('active');
        // $this->crud->addClause('type', 'car');
        // $this->crud->addClause('where', 'name', '==', 'car');
        // $this->crud->addClause('whereName', 'car');
        // $this->crud->addClause('whereHas', 'posts', function($query) {
        //     $query->activePosts();
        // });
        // $this->crud->addClause('withoutGlobalScopes');
        // $this->crud->addClause('withoutGlobalScope', VisibleScope::class);
        // $this->crud->with(); // eager load relationships
        // $this->crud->orderBy();
        // $this->crud->groupBy();
        // $this->crud->limit();
    }

    public function store(Request $request)
    {
        // fallback to global request instance
        if (is_null($request)) {
            $request = \Request::instance();
        }
        // your additional operations before save here
        $redirect_location = parent::storeCrud();
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateUserRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud();
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function addSellerForm()
    {
        $this->_setSellerFields();
        $this->data['crud'] = $this->crud;
        $this->data['saveAction'] = $this->getSaveAction();
        $this->data['fields'] = $this->crud->getCreateFields();
        return view('admin.add_seller_form', $this->data);
    }

    private function _setSellerFields()
    {
        $this->crud->addFields([
            [
                'name' => 'first_name',
                'label' => 'First name'
            ]
            , [
                'name' => 'last_name',
                'label' => 'Last name'
            ]
            , [
                'name' => 'email',
                'label' => 'Email'
            ]
            , [
                'name' => 'password',
                'label' => 'Password',
                'type' => 'password'
            ],
            [
                'name' => 'form_type',
                'value' => 'seller_form',
                'type' => 'hidden'
            ]

        ], 'create');
        $this->crud->addField([
            'name' => 'company_name',
            'label' => 'Company name'
        ], 'create')->beforeField('first_name');
        $this->crud->removeField('roles', 'update/create/both');
    }

    public function saveSellerForm(NewSellerRequest $request)
    {
        $request = \Request::instance();
        /*add seller role to user*/
        $seller = Role::findByName('seller');
        $request->request->set('roles', [$seller->id]);
        // your additional operations before save here
        $redirect_location = parent::storeCrud();
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
