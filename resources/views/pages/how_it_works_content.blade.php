<div class="page-wrap how-it-works">
    <div class="full-width-page clearfix">
        <div class="image-head-page small" style="background-image:url(img/about_us_wallpaper_210821018.png);">
            <div class="wrap clearfix">
                <div class="desc-wrap">
                    <h1>How it works</h1>
                    <div class="text">As easy as 'one, two, three'</div>
                </div>
            </div>
        </div>

        <div class="content">
            <div class="row-image-text clearfix">
                <div class="wrap image-left">
                    <div class="image">
                        {{--<img id="placeholderVideo" src="{{ asset('img/Intro600px.jpg')}}" width="520" alt="">--}}
                        <video width="100%" controls="" id="lastBidVideo" style="display: block">
                            <source src="{!! asset('uploads/'.$data->video) !!} " type="video/mp4">
                            <p>Your browser doesn't support HTML5 video. Here is
                                a <a href="{{ asset('uploads/'.$data->video)}}">link to the video</a>
                                instead.
                            </p>
                        </video>
                    </div>
                    <div class="text">
                        <div class="title-page">
                            <h3>OUR OFFERING</h3>
                        </div>
{{--                        <p>Through the LastBid website, auction houses offer unsold auction lots to a worldwide audience of interested bidders.</p>--}}
{{--                        <p>There’s no extra cost to the buyer; they pay exactly the same as if they’d bought at the original sale.</p>--}}
{{--                        <p>Everyone will find something of interest... there are so many categories of unsold items to browse: from art to wine, from jewellery to watches, from motor cars to manuscripts.</p>--}}
{{--                        <p>Found something you like? Then it’s as easy as one, two, three: Choose, Bid, Win.</p>--}}
                        {!! $data->section_1_text !!}
                    </div>
                </div>
            </div>

            <div class="row-image-text clearfix">
                <div class="wrap image-right">
                    <div class="image"><img src="uploads/{!! $data->image_section_2 !!}" alt=""></div>
                    <div class="text">
                        <div class="title-page">
                            <h3>CHOOSE:</h3>
                        </div>
                        <p>{!! $data->section_2_text !!}
                        </p>
                    </div>
                </div>
            </div>

            <div class="row-image-text clearfix">
                <div class="wrap image-left">
                    <div class="image"><img src="uploads/{!! $data->image_section_3 !!}" alt=""></div>
                    <div class="text">
                        <div class="title-page">
                            <h3>BID:</h3>
                        </div>
                        <p>{!! $data->section_3_text !!}</p>
                    </div>
                </div>
            </div>

            <div class="row-image-text clearfix">
                <div class="wrap image-right">
                    <div class="image"><img src="uploads/{!! $data->image_section_4 !!}" alt=""></div>
                    <div class="text">
                        <div class="title-page">
                            <h3>WIN:</h3>
                        </div>
                        <p>{!! $data->section_4_text !!}
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
   @section('user_script')

    var video = document.getElementById("lastBidVideo");

    if (video != null) {

        var playScrollVideo = true;

        function StartPauseHandler(e) {
            if (e.type === "pause") {
                playScrollVideo = false;
            } else if (e.type === "playing") {
                console.log("playing");
            } else {
                console.log('any');
            }
        }

        video.addEventListener('click touchstart', StartPauseHandler);
        video.addEventListener("pause", StartPauseHandler);
        video.addEventListener("playing", StartPauseHandler);

        function playVid() {
            video.muted = true;
            video.play();
        }

        function stopVid() {
            video.pause();
        }

        $(window).scroll(function () {
            if (playScrollVideo == true) {
                if ($(this).scrollTop() > 100) {
                    $('#lastBidVideo').show();
                    $('#placeholderVideo').hide();
                    playVid();
                }
            }
        });
    }


    @endsection
</script>