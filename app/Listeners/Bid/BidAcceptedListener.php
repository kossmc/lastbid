<?php

namespace App\Listeners\Bid;
use App\Repositories\BidRepository;
use Illuminate\Support\Facades\Mail;

class BidAcceptedListener
{
    /**
     * The bid repository implementation.
     *
     * @var BidRepository
     */
    private $bid;


    /**
     * Create a new listener instance.
     *
     * @param BidRepository $bid
     * @return void
     */
    public function __construct(BidRepository $bid)
    {
        $this->bid = $bid;
    }

    /**
     * Handle the event.
     *
     * @param $event
     * @return void
     */
    public function handle($event)
    {
        $bid = $event->bid;
        $lot = $bid->lot;
        $buyer = $bid->buyer;
        $attempt = $bid->latestAttempt();
        if ($lot->sold($attempt->amount,$buyer->id)) {
            // decline all pending bids for sold lot
            foreach ($lot->bids()->pending()->get() as $bid) {
                $this->bid->declineBySystem($bid);
            }
            //generate and save pdf invoice
            $lot->generateBuyerInvoice();
        }
    }
}
