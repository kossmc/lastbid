<?php

namespace App\Http\Controllers;

use App\Events\User\BuyerResendConfirmation;
use App\Events\User\SellerAddNewBankAccount;
//use App\Events\User\BuyerAddNewCard;
use App\Events\User\SellerDeleteBankAccount;
//use App\Events\User\BuyerDeleteCard;
use App\Http\Requests\BuyerBankAccountRequest;
use App\Http\Requests\BuyerShippingAddressRequest;
//use App\Http\Requests\EditBuyerCardRequest;
//use App\Http\Requests\NewBueyrCardRequest;
use App\Http\Requests\UserSubscriptionsStoreRequest;
use App\Mail\User\UserUpdateEmailNotice;
use App\Models\BuyerAccount;
use App\Models\BuyerAddress;
//use App\Models\BuyerCard;
use App\Models\Subscription;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\BuyerProfileUpdateRequest;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use App\Http\Requests\UpdateUserPasswordRequest;

class UserController extends Controller
{
    public function account()
    {
        $user = Auth::user();
        if ($user->hasRole('seller')) {
            return view('seller.personal_info', ['user' => $user]);
        }
        return view('buyer.profile.personal_info', ['user'=> $user]);
    }
    public function sellerPaymentBankAccounts()
    {
        $accounts = Auth::user()->accounts;
        return view('seller.payment-methods-bank-accounts', ['accounts' => $accounts]);
    }
    public function accountEdit()
    {
        return view('buyer.profile.personal_info_edit', ['user'=>Auth::user()]);
    }
    public function accountEditSave(BuyerProfileUpdateRequest $request)
    {
        $send = false;
        $message = 'All changes saved!';
        $input = $request->only(['first_name', 'middle_name', 'last_name', 'email']);
        $user = Auth::user();
        if ( ! empty($input['email']) && $input['email'] !== $user->email) {
            if ($user->email_confirmed) {
                $input['new_email'] = $input['email'];
                $input['email_token'] = str_random(30);
                $message .= ' New email will be set after you confirm it!';
                $send = true;
                unset($input['email']);
            } else {
                $message .= ' Email needs to be confirmed, please check your inbox!';
                $user->email = $input['email'];
                $user->save();
                Event::fire(new BuyerResendConfirmation($user, request()->server('HTTP_REFERER')));
            }
        } else {
            if (!$user->email_confirmed) {
                $message .= ' Email needs to be confirmed, please check your inbox!';
                $user->email = $input['email'];
                $user->save();
                Event::fire(new BuyerResendConfirmation($user, request()->server('HTTP_REFERER')));
            }
        }
        $user->fill($input)->save();
        if ($send) {
            Mail::send(new UserUpdateEmailNotice($user));
        }
        Session::put('message', $message);
        return redirect()->route('account-info');
    }

    public function accountConfirmEmail($token)
    {
        $user = User::where('email_token', $token)->first();
        if ($user && $user->new_email) {
            $user->email = $user->new_email;
            $user->new_email = $user->email_token = null;
            $user->email_confirmed = 1;
            $user->save();
            Auth::logout();
            Session::setPreviousUrl(route('account-info'));
            Session::put('email_changed', 'Your email changed successfully. Please, log in with new email!');
        }
        return redirect()->route('account-info');
    }

    public function accountEditPassword(UpdateUserPasswordRequest $request)
    {
        $user = Auth::user();
        $user->password = $request->get('password');
        $user->save();
        Session::put('password_changed', 'Your password changed!');
        return redirect()->route('account-info');
    }

    public function paymentBankAccountForm()
    {
        return view('seller.payment-methods-add-bank-account',['account'=> new BuyerAccount()]);
    }

    public function paymentBankAccountNew(BuyerBankAccountRequest $request)
    {
        $account = Auth::user()->accounts()->save( new BuyerAccount(
            $request->only(['holder_name', 'account_number', 'routing_number', 'bank_name', 'swift_code',
                'address', 'city', 'state', 'country', 'zip'])
        ));
        Event::fire(new SellerAddNewBankAccount($account, Auth::user()));
        Session::put('message', 'New bank account added!');
        return redirect()->route('seller-payment-methods');
    }

    public function paymentBankAccountEditForm($id)
    {
        $account = BuyerAccount::where(['id'=>$id, 'user_id'=>Auth::id()])->firstOrFail();
        $edit = true;
        return view('seller.payment-methods-add-bank-account', compact(['account','edit']));
    }

    public function paymentBankAccountEditSave(BuyerBankAccountRequest $request, $id)
    {
        $account = BuyerAccount::where(['id' => $id, 'user_id' => Auth::id()])->firstOrFail();
        $account->fill(
            $request->only(['holder_name', 'account_number', 'routing_number', 'bank_name', 'swift_code',
            'address', 'city', 'state', 'country', 'zip'])
        )->save();
        Session::put('message', 'Bank account edited successfully!');
        return redirect()->route('seller-payment-methods');
    }

    public function paymentBankAccountSetDefault($id)
    {
        $account = BuyerAccount::where(['id' => $id, 'user_id' => Auth::id()])->firstOrFail();
        Auth::user()->accounts()->where('default', 1)->update(['default' => 0]);
        $account->default = 1;
        $account->save();
        Session::put('message', 'Bank account set as default!');
        return redirect()->route('seller-payment-methods');
    }

    public function paymentBankAccountDelete($id)
    {
        $account = BuyerAccount::where(['id' => $id, 'user_id' => Auth::id()])->firstOrFail();
        $account->delete();
        Event::fire(new SellerDeleteBankAccount($account, Auth::user()));
        Session::put('message', 'Bank account deleted successfully!');
        return redirect()->route('seller-payment-methods');
    }

    public function shipping()
    {
        $addresses = Auth::user()->addresses;
        return view('buyer.profile.shipping-info',['addresses'=> $addresses]);
    }

    public function shippingNewForm()
    {
        return view('buyer.profile.shipping-info-add-edit', ['address' => new BuyerAddress()]);
    }

    public function shippingNewSave(BuyerShippingAddressRequest $request)
    {
        $address = Auth::user()->addresses()->save(new BuyerAddress(
            $request->only(['address_name','address', 'city', 'state', 'country', 'zip'])
        ));
        Session::put('message', 'New shipping address added!');
        return redirect()->route('shipping-info');
    }

    public function shippingEditForm($id)
    {
        $address = Auth::user()->addresses()->where('id',$id)->firstOrFail();
        return view('buyer.profile.shipping-info-add-edit', ['address' => $address, 'edit'=>true]);
    }

    public function shippingEditSave(BuyerShippingAddressRequest $request, $id)
    {
        $address = Auth::user()->addresses()->where('id', $id)->firstOrFail();
        $address->fill($request->only(['address_name', 'address', 'city', 'state', 'country', 'zip']))->save();
        Session::put('message', 'Shipping address edited!');
        return redirect()->route('shipping-info');
    }

    public function shippingSetDefault($id)
    {
        $address = Auth::user()->addresses()->where('id', $id)->firstOrFail();
        Auth::user()->addresses()->where('default', 1)->update(['default' => 0]);
        $address->default = 1;
        $address->save();
        Session::put('message', 'Shipping address set as default!');
        return redirect()->route('shipping-info');
    }

    public function shippingDelete($id)
    {
        $address = Auth::user()->addresses()->where('id', $id)->firstOrFail();
        $address->delete();
        Session::put('message', 'Shipping address deleted!');
        return redirect()->route('shipping-info');
    }
    
    public function subscriptions()
    {
        $subscriptions = Subscription::getSelectedByUser(Auth::user());
        return view('buyer.profile.subscriptions', compact(['subscriptions']));
    }

    public function subscriptionsSave(UserSubscriptionsStoreRequest $request)
    {
        if ($request->has('un_subscribe') || ! $request->has('subscriptions') ) {
            //remove all subscriptions
            Auth::user()->subscriptions()->detach();
            $message = 'All subscriptions canceled!';
        } elseif (count($subscriptions = $request->input('subscriptions',[]))) {
            //attach subscriptions to user
            Auth::user()->subscriptions()->sync($subscriptions);
            $message = 'All data saved!';
        }
        if (isset($message)) {
            Session::put('message', $message);
        }
        return redirect()->route('subscriptions');
    }
}
