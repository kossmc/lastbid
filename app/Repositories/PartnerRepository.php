<?php
namespace App\Repositories;

use Bosnadev\Repositories\Eloquent\Repository;
use App\Models\Partner;

class PartnerRepository extends Repository
{
    public function model()
    {
        return Partner::class;
    }

    /**
     * Paginate partner collection
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function paginateSort()
    {
        return $this->model->orderBy('title')
            ->paginate(request('per_page', env('PAGINATE_PARTNERS')));
    }
}
