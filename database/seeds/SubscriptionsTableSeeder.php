<?php

use App\Models\Subscription;
use Illuminate\Database\Seeder;

class SubscriptionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     * php artisan db:seed --class=SubscriptionsTableSeeder
     * @return void
     */
    public function run()
    {
        $subscriptions = ['Sale date announcements', 'Catalogue online announcement', 'Exhibition announcements',
            'Auction results announcement','Selling exhibitions announcements','Newsletters',
            'General updates and promotions','Invitations'];
        foreach ($subscriptions as $subscription) {
            $s = Subscription::create(['name' => $subscription]);
        }
    }
}
