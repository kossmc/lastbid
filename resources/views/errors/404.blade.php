@php $canonical = url('404'); @endphp
@php $h1_tag = env('APP_NAME').' 404 page';@endphp

@extends('layouts.main')
@section('title', 'Main page')
@section('top-links')
  @include('inc.top_links')
@stop

@section('user_css')
  <style>
    body {
      margin: 0;
      padding: 0;
      width: 100%;
      height: 100%;
      color: #B0BEC5;
      display: table;
      font-weight: 100;
      font-family: 'Lato';
    }

    .container {
      text-align: center;
      vertical-align: middle;
    }

    .content {
      text-align: center;
      display: inline-block;
    }

    .title {
      font-size: 156px;
    }

    .quote {
      font-size: 36px;
    }

    .explanation {
      font-size: 24px;
    }
  </style>
@endsection

@section('content')
  <div class="container">
    <div class="content">
      <div class="title">404</div>
      <div class="quote">Page not found.</div>
      <div class="explanation">
        <br>
        <small>
            <?php
            $default_error_message = "Please return to <a href='".url('')."'>our homepage</a>.";
            ?>
          {!! isset($exception)? ($exception->getMessage()?$exception->getMessage():$default_error_message): $default_error_message !!}
        </small>
      </div>
    </div>
  </div>
@endsection