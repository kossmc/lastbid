<?php

namespace App\Services;

use stdClass;

/**
 * Class SliderService
 * @package App\Services
 */
class SliderService
{
    /**
     * @param StdClass $loop
     * @return string
     */
    public function getItemClassName(StdClass $loop)
    {
        $classes = ['item'];

        $iterationClassMapping = [
            1 => 'active',
            2 => 'right-item',
        ];

        $additionalClass = array_get($iterationClassMapping, $loop->iteration, '');

        array_push($classes, $additionalClass);

        return implode(' ', $classes);
    }
}
