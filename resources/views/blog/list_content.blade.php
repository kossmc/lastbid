@include('inc.discover_more_modal')
<div class="page-wrap event-page">
    <div class="wrap">
        <div class="title-evets">
            <h1>News</h1>
        </div>
        <div class="all-evets-wrap">
            @foreach($blog_posts as $blog_post)
                <div class="item clearfix">
                    <div class="image"><a
                                href="{{ $blog_post->url }}">
                            <img src="{{$blog_post->getImagePath(326,244)}}" alt="{{ $blog_post->title }}">
                        </a>
                    </div>
                    <div class="desc-wrap">
                        <div class="title"><h3><a
                                        href="{{ $blog_post->url }}">{{ $blog_post->title }}</a>
                            </h3></div>
                        <div class="status-wrap clearfix">
                            @if ($blog_post->post_date)
                                <div class="item-status">{{ $blog_post->post_date->format('j F Y') }}</div>
                            @endif
                        </div>
                        <div class="content">
                            {{$blog_post->brief}}
                        </div>
                        {{--<br />--}}
                        {{--<button disabled class="btn" href="{{ $blog_post->url }}">discover More</button>--}}
                    </div>
                </div>
            @endforeach
        </div>

        @include('inc.paginator_with_page_selector', ['items'=>$blog_posts])

        <div class="evets-btn">
            {{--<a class="btn discoverMoreBtn">subscribe</a>--}}
        </div>

    </div>
</div>