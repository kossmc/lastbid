<?php

namespace App\Listeners\User;

use App\Events\User\ResetPassword;
use Illuminate\Support\Facades\Mail;

class ResetPasswordListener
{
    /**
     * Email subject.
     */
    private $subject = 'Reset password';

    /**
     * Email template.
     */
    private $template = 'bc820fff-ee09-44b0-9530-c73d95fefec0';

    protected $user;
    protected $name;
    protected $email;
    protected $token;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function setData($event)
    {
        $this->user = $event->user;
        $this->name = $event->user->full_name;
        $this->email = $event->user->email;
        $this->token = $event->token;
    }

    protected function getData($id)
    {
        $data = [
            "sub" => [
                "-siteLink-" => [route('home')]
                , "-emailPreferencesLink-" => [route('subscriptions')]
                , "-currentYear-" => [date('Y')]
                , "-name-" => [$this->name]
                , '-verifyLink-' =>[url(route('password.reset', $this->token, false))]
            ],
            "filters" => [
                "templates" => [
                    "settings" => [
                        "enable" => 1,
                        "template_id" => $id
                    ]
                ],
                "subscriptiontrack" => [
                    "settings" => [
                        "enable" => 1,
                        "replace" => '[unsubscribe]'
                    ]
                ]
            ]
        ];
        return asJSON($data);
    }
    /**
     * Handle the event.
     *
     * @param  ResetPassword  $event
     * @return void
     */
    public function handle(ResetPassword $event)
    {
        $this->setData($event);
        $this->sendMail($this->getData($this->template), $this->subject);
    }


    protected function sendMail($data, $subject)
    {
        Mail::send('emails.empty', [], function ($message) use ($subject, $data) {
            $message->getSwiftMessage()->getHeaders()->addTextHeader('X-SMTPAPI', $data);
            $message->to($this->email, $this->name)->subject($subject);
        });
    }
}
