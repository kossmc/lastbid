@extends('backpack::layout')
@section('header')
    <section class="content-header">
        <h1>Move lots</h1>
        <ol class="breadcrumb">
            <li>
                <a href="{{ url(config('backpack.base.route_prefix'), 'dashboard') }}">{{ trans('backpack::crud.admin') }}</a>
            </li>
            <li><a href="{{ url($crud->route) }}" class="text-capitalize">{{ $crud->entity_name_plural }}</a></li>
            <li class="active">Move lots</li>
        </ol>
    </section>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <!-- Default box -->
            @if ($crud->hasAccess('list'))
                <a href="{{ url($crud->route) }}"><i
                            class="fa fa-angle-double-left"></i> {{ trans('backpack::crud.back_to_all') }}
                    <span>{{ $crud->entity_name_plural }}</span></a><br><br>
            @endif

            {!! Form::open(array('url' =>route('category.move_lots_to'), 'method' => 'post')) !!}
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Move lots from one category to another</h3>
                </div>
                <div class="box-body row">
                    <!-- text input -->
                    <div class="form-group col-md-12">
                        <label for="to_cat" class="col-sm-2 control-label">Move lots to category</label>
                        <div class="col-sm-10">
                            <select name="to_cat" id="to_cat" class="form-control">
                                @foreach($cats_to as $k=>$title)
                                    <option value="{{$k}}">{{$title}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <!-- text input -->
                    <div class="form-group col-md-12">
                        <label>Confirm category/categories from which lots will be moved</label>
                        @foreach($cats_from as $k=>$title)
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <input type="hidden" name="from_cat[]" value="{{$k}}">
                                    <h4>{{$title}}</h4>
                                </div>
                            </div>
                        @endforeach
                    </div>

                </div><!-- /.box-body -->
                <div class="box-footer">
                    <div id="saveActions" class="form-group">


                        <div class="btn-group">
                            <button type="submit" class="btn btn-success">
                                <span class="fa fa-save" role="presentation" aria-hidden="true"></span> &nbsp;
                                <span>Move</span>
                            </button>

                        </div>

                        <a href="{{ url($crud->route) }}" class="btn btn-default"><span class="fa fa-ban"></span> &nbsp;{{ trans('backpack::crud.cancel') }}</a>
                    </div>

                </div><!-- /.box-footer-->

            </div><!-- /.box -->
            {!! Form::close() !!}
        </div>
    </div>
@endsection
