<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ProductsTableChanges extends Migration
{
    public function __construct()
    {
        DB::getDoctrineSchemaManager()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');
    }

    protected $statusesMap = [
        0 => 'active',
        1 => 'expired',
        2 => 'sold'
    ];
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::rename('products', 'lots');
        Schema::table('lots', function (Blueprint $table) {
            $table->renameColumn('name', 'title');
            $table->renameColumn('sub_title', 'subtitle');
            $table->renameColumn('shipping', 'shipping_description');
            $table->renameColumn('status', 'status_int');
            $table->renameColumn('min_price', 'min_bid_amount');
            $table->renameColumn('min_range', 'range_from');
            $table->renameColumn('max_range', 'range_to');
            $table->renameColumn('start_date', 'start_date_old');
            $table->renameColumn('end_date', 'end_date_old');
            $table->dropColumn(['reserve_price', 'shipping_price', 'auc_house_premium']);
            $sql = 'UPDATE `lots` SET `photos`=REPLACE(`photos`, \'products\', \'lots\')';
            DB::connection()->getPdo()->exec($sql);
        });
        Schema::table('lots', function (Blueprint $table) {
            $table->enum('status', $this->statusesMap)->default($this->statusesMap[0]);
            $table->timestamp('start_date')->useCurrent();
            $table->timestamp('end_date')->nullable();
        });
        $sql = 'UPDATE `lots` SET `start_date`=(NOW() - INTERVAL FLOOR(RAND() * 60) DAY), `end_date`=(NOW() + INTERVAL FLOOR(RAND() * 60) DAY)';
        DB::connection()->getPdo()->exec($sql);
        foreach ($this->statusesMap as $statusInt => $status) {
            $sql = 'UPDATE `lots` SET `status`=\'' . $status . '\' WHERE `status_int`=' . $statusInt;
            DB::connection()->getPdo()->exec($sql);
        }
        Schema::table('lots', function (Blueprint $table) {
            $table->dropColumn(['status_int', 'start_date_old', 'end_date_old']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::rename('lots', 'products');
        Schema::table('products', function (Blueprint $table) {
            $table->renameColumn('title', 'name');
            $table->renameColumn('subtitle', 'sub_title');
            $table->renameColumn('shipping_description', 'shipping');
            $table->renameColumn('status', 'status_enum');
            $table->renameColumn('min_bid_amount', 'min_price');
            $table->renameColumn('range_from', 'min_range');
            $table->renameColumn('range_to', 'max_range');
            $table->renameColumn('start_date', 'start_date_old');
            $table->renameColumn('end_date', 'end_date_old');
            $table->float('reserve_price', 20, 2)->default(0.00);
            $table->float('shipping_price', 20, 2)->default(0.00);
            $table->float('auc_house_premium', 20, 2)->default(0.00);
            $sql = 'UPDATE `products` SET `photos`=REPLACE(`photos`, \'lots\', \'products\')';
            DB::connection()->getPdo()->exec($sql);
            $sql = 'UPDATE `products` SET `photos`=REPLACE(`photos`, \'lots\', \'products\')';
        });
        Schema::table('products', function (Blueprint $table) {
            $table->integer('status')->default(0);
            $table->dateTime('start_date')->default(\Carbon\Carbon::now());
            $table->dateTime('end_date')->default(\Carbon\Carbon::now());
        });
        $sql = 'UPDATE `products` SET `start_date`=`start_date_old`, `end_date`=`end_date_old`';
        foreach ($this->statusesMap as $statusInt => $status) {
            $sql = 'UPDATE `products` SET `status`=' . $statusInt . ' WHERE `status_enum`=\'' . $status . '\'';
            DB::connection()->getPdo()->exec($sql);
        }
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn(['status_enum', 'start_date_old', 'end_date_old']);
        });
    }
}
