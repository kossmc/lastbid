@extends('layouts.inner')
@section('title', 'My lots')
@section('inner_content')
    <ul class="nav nav-tabs profile-tabs-nav lots-tabs-nav">
        <li{!! isActiveRoute('seller_active_lots') ? ' class="active"' : '' !!}><a href="{{ route('seller_active_lots') }}">Active lots</a></li>
        <li{!! isActiveRoute('seller_closed_lots') ? ' class="active"' : '' !!}><a href="{{ route('seller_closed_lots') }}">Closed lots</a></li>
        <li{!! isActiveRoute('seller_sold_lots') ? ' class="active"' : '' !!}><a href="{{ route('seller_sold_lots') }}">Sold lots</a></li>
    </ul>
    <div class="tab-content profile-tab-content">
        <div class="tab-pane tab-lots fade in active">
            <div class="tab-inner">
                <div class="filter-wrapper">
                    @include('inc.top_sorting_price_purchase_date')
                </div>
                <div class="lots-list">
                    <ul class="purchased-lots-list purchased-lots-list--seller">
                    @foreach ($lots as $lot)
                            <li>
                                <div class="image-wrapper"><a href="{{ route('seller_lot', ['id' => $lot->id]) }}"><img
                                                src="{{ $lot->getImagePath(180, 190) }}" alt=""></a></div>
                                <div class="purchase-card-wrapper">
                                    <div class="price">{!! format_price($lot->sold_amount, null, $lot->currency) !!} </div>
                                    <ul class="lot-info">
                                        <li>
                                            <p>Auction house premium:</p>
                                            <div class="price">{!! format_price($lot->bidFee($lot->sold_amount), null, $lot->currency) !!}</div>
                                        </li>
                                        <li>
                                            <p>Total lot price:</p>
                                            <div class="price">{!! format_price($lot->bidWithFee($lot->sold_amount), null, $lot->currency) !!}</div>
                                        </li>
                                    </ul>
                                    <ul class="actions-wrapper">
                                        <li class="action-step active"><span class="icon-invoice active"></span><span
                                                    class="text">Invoice</span></li>
                                        <li class="action-step active"><span class="icon-paid"></span><span
                                                    class="text">Paid</span></li>
                                        <li class="action-step"><span class="icon-send"></span><span
                                                    class="text">Send</span></li>
                                        <li class="action-step"><span class="icon-recieved"></span><span
                                                    class="text">Received</span></li>
                                    </ul>
                                </div>
                                <div class="description-wrapper">
                                    <h3><a href="{{ route('seller_lot', ['id' => $lot->id]) }}">{{ $lot->title }}</a></h3>
                                    <div class="lot-number"><span>LastBid Lot No</span><span>{{$lot->id}}</span></div>
                                    <ul class="buyer-info">
                                        @if ($lot->is_sold_outside)
                                            <li><span>Lot was sold outside</span></li>
                                        @else
                                            <li><span>Buyer name: </span>{{$lot->buyer->full_name}}</li>
                                            <li><span>Sale date: </span>{!! $lot->sold_date->format('j F Y, h:i A') !!}
                                                EST
                                            </li>
                                        @endif
                                    </ul>
                                </div>
                            </li>
                    @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
    @php request()->has('sort_sold_date') ? $append = ['sold_date'=>request('sold_date')]:$append = []; @endphp
    @include('inc.paginator_with_page_selector',
            ['items'=>$lots, 'append'=>$append])
@endsection
@section('user_script')
    $('#sort_by').off().on('change', function () {
    if ($(this).val() != 0) {
    $('.sorting a#sort_up').click();
    } else {
    updateUrl([['sort_by'], ['sort_direction']]);
    }
    });

    $('.filter .item .sorting a').off('click').on('click', function () {
    var $input = $('#sort_direction'),
    $selected = $('select#sort_by').val();
    if ($selected != 0) {
    $(this).hasClass('down') ? $input.val('desc') : $input.val('asc');
    document.location = document.location.pathname + '?' + $('form#top_sorting').serialize();
    }
    });
@stop
