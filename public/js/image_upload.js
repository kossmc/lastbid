var imageUploader = {
    classImageBlock : 'uploaded-lot-images-block',
    classImageUploader : 'image-uploader',
    classImageItem : 'uploaded-lot-image',
    classImageItemDel : 'del-uploaded-image',
    classImageItemOld : 'old-image',
    classImagesContentBlock : 'images-block-content',
    modalId : 'modal-win2',
    delClass : 'to-delete',
    init : function(){
        $('.'+imageUploader.classImageUploader).change(imageUploader.show);
        $('.'+imageUploader.classImageItemDel).click(imageUploader.delete);
        $('.'+imageUploader.classImageItem + ' img').click(imageUploader.show_popup);
        imageUploader.checkContentsImagesBlock();
    },
    initNew: function () {
        $('.new-photo.'+imageUploader.classImageItem + ' img').click(imageUploader.show_popup);
        imageUploader.checkContentsImagesBlock();
    },
    show : function(e){
        let images_area = $('div[data-target="'+e.target.id+'"] .' + imageUploader.classImagesContentBlock);
        var files = e.target.files;

        for(var i = 0; i < files.length; i++) {
            let imgName = e.target.files[i];
            let imgId = i;
            let imgSrc = URL.createObjectURL(e.target.files[i]);

            images_area.prepend(imageUploader.template(imgId, imgName, imgSrc));
        }

        imageUploader.initNew();
    },
    delete : function(e) {
        let parent = $(e.target.parentElement);

        if(parent.hasClass(imageUploader.delClass)){
            parent.find('input').remove();
            parent.toggleClass(imageUploader.delClass);
        }else{
            let photoToDel = parent.find('img').attr('src');
            parent.prepend('<input type="hidden" name="photosDel[]" value="'+photoToDel+'">');
            parent.toggleClass(imageUploader.delClass);
        }
    },
    show_popup : function(e) {
        let src = $(e.target).attr('src');
        let popup_content = imageUploader.popup_template(src);

        $('#'+imageUploader.modalId).html(popup_content).modal('toggle');

        $('.modal-backdrop.in').click(function(modal_block){
            $('#'+imageUploader.modalId).modal('toggle');
        });
    },
    template : function(imgId, fileName, src, ) {
        return '<div class="'+ imageUploader.classImageItem +' item_' + imgId  +' new-photo" data-imgId="' + imgId + '" data-uploaderClass="'+ imageUploader.classImageUploader +'">' +
                '<img src="'+ src +'" alt="' + fileName + '">' +
            '</div>';
    },
    popup_template: function(src){
        return '<div class="uploaded-image-modal"> \
                <img src="'+ src +'">\
            </div>';
    },
    checkContentsImagesBlock : function(){
        $('.'+imageUploader.classImageBlock).each(function(){
            let countItems = $(this).find('.'+imageUploader.classImageItem).length;

            countItems > 0 ? $(this).show() : $(this).hide();
        });
    }

};

$(document).ready(imageUploader.init());