<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\MoveLotsRequest;
use App\Http\Requests\MoveSelectedLotsRequest;
use App\Models\Category;
use App\Models\Lot;
use App\Models\LotSpecificOptions;
use App\Models\Partner;
use App\Models\Seller;
use App\Models\Specific;
use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\LotRequest;
use App\Http\Requests\LotUpdateRequest;
use App\Http\Requests\LotImportRequest;
use App\Http\Requests\LotSpecificSaveRequest;
use App\Repositories\ImportRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Queue;
use LiveControl\EloquentDataTable\DataTable;

class LotCrudController extends CrudController
{
    public function setUp()
    {
        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Lot');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/lot');
        $this->crud->setEntityNameStrings('lot', 'lots');

        if(request()->has('system')){
            $this->crud->addClause('where', 'system', '=', '1');
        } else {
            $this->crud->addClause('where', function($query){
                $query->where('system', '=', 0)
                    ->orWhereNull('system');
            });
        }

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */

        //        $this->crud->setFromDb();

        // ------ CRUD FIELDS
        // $this->crud->addField($options, 'update/create/both');

        $this->crud->addFields([
            [
                'name' => 'status',
                'label' => 'Status',
                'default' => Lot::STATUS_ACTIVE,
                'type' => 'select_from_array',
                'options' => [
                    Lot::STATUS_ACTIVE => 'Active',
                    Lot::STATUS_CLOSED => 'Closed',
                    Lot::STATUS_SOLD_OUTSIDE => 'Sold elsewhere'
                ],
                'allows_null' => false
            ],
            [
                'name' => 'popular',
                'label'=> 'Include in most popular list?',
                'type' => 'checkbox'
            ]
            ,[
                'label' => 'Category',
                'type' => 'select',
                'name' => 'category_id', // the db column for the foreign key
                'entity' => 'category', // the method that defines the relationship in your Model
                'attribute' => 'title', // foreign key attribute that is shown to user
                'model' => 'App\Models\Category' // foreign key model
            ]
            ,[
                'label' => 'Partner',
                'type' => 'select',
                'name' => 'partner_id', // the db column for the foreign key
                'entity' => 'partner', // the method that defines the relationship in your Model
                'attribute' => 'title', // foreign key attribute that is shown to user
                'model' => 'App\Models\Partner' // foreign key model
            ]
            ,[
                'name' => 'title',
                'label' => 'Lot name'
            ],[
                'name' => 'subtitle',
                'label' => 'Sub header'
            ],[
                'name' => 'description',
                'label' => 'Lot description',
                'type' => 'wysiwyg'
            ],[
                'name' => 'shipping_description',
                'label' => 'Shipping description',
                'type' => 'wysiwyg'
            ],[
                'name' => 'auction_house_fee',
                'label' => 'Auction House Fee',
                'type' => 'number',
                'attributes' => ['step' => 'any', 'min' => 0],
                'prefix' => '%',
                'default' => 0
            ],[
                'name' => 'currency',
                'label' => 'Lot currency',
                'type' => 'select2_from_array',
                'options' => ['usd' => 'USD', 'gbp' => 'GBP', 'eur' => 'EUR'],
                'allows_null' => false,
                'default' => 'usd'
            ],[
                'name' => 'min_bid_amount',
                'label' => 'Minimum bid amount',
                'type' => 'number',
                'attributes' => ['step' => 'any', 'min' => 1],
            ],[
                'name' => 'range_from',
                'label' => 'Price range from',
                'type' => 'number',
                'attributes' => ['step' => 'any', 'min' => 1],
            ],[
                'name' => 'range_to',
                'label' => 'Price range to',
                'type' => 'number',
                'attributes' => ['step' => 'any', 'min' => 1],
            ],[
                'name' => 'auto_accept_price',
                'label' => 'Price to auto-accept bid',
                'type' => 'number',
                'attributes' => ['step' => 'any', 'min' => 1],
            ],[
                'name' => 'start_date',
                'label' => 'Start date',
                'type' => 'datetime_picker',
                'datetime_picker_options' => [
                    'format' => 'DD/MM/YYYY HH:mm',
                    'language' => 'en'
                ]
            ],[
                'name' => 'end_date',
                'label' => 'End date',
                'type' => 'datetime_picker',
                'datetime_picker_options' => [
                    'format' => 'DD/MM/YYYY HH:mm',
                    'language' => 'en'
                ]
            ],[
                'name' => 'user_id',
                'label' => 'Seller',
                'type' => 'select',
                'model' => 'App\Models\Seller',
                'attribute' => 'full_name'
            ],[   // Upload
                'name' => 'photos',
                'label' => 'Lot photos',
                'type' => 'upload_multiple',
                'upload' => true,
            //'disk' => 'uploads'  if you store files in the /public folder, please ommit this; if you store them in /storage or S3, please specify it;
            ],[
                'name' => 'system',
                'value' => $this->request->get('system', 0),
                'type' => 'hidden'
            ]
         ], 'create');

        $this->crud->addFields([
            [
                'name' => 'status',
                'label' => 'Status',
                'default' => Lot::STATUS_ACTIVE,
                'type' => 'select_from_array',
                'options' => [
                    Lot::STATUS_ACTIVE => 'Active',
                    Lot::STATUS_CLOSED => 'Closed',
                    Lot::STATUS_SOLD_OUTSIDE => 'Sold elsewhere'
                ],
                'allows_null' => false
            ],
            [
                'name' => 'popular',
                'label' => 'Include in most popular list?',
                'type' => 'checkbox'
            ]
            ,[
                'label' => 'Category',
                'type' => 'select',
                'name' => 'category_id', // the db column for the foreign key
                'entity' => 'category', // the method that defines the relationship in your Model
                'attribute' => 'title', // foreign key attribute that is shown to user
                'model' => 'App\Models\Category', // foreign key model
                'attributes' => [
                    'id' => 'category_select',
                    //'disabled' => 'disabled'
                ]
            ],
            [
                'name' => 'specifics_data',
                'label' => 'Lot specifics:',
                'type' => 'vue_lot_specifics2',
            ],
            [
                'label' => 'Partner',
                'type' => 'select',
                'name' => 'partner_id', // the db column for the foreign key
                'entity' => 'partner', // the method that defines the relationship in your Model
                'attribute' => 'title', // foreign key attribute that is shown to user
                'model' => 'App\Models\Partner' // foreign key model
            ],[
                'name' => 'title',
                'label' => 'Lot name'
            ],[
                'name' => 'subtitle',
                'label' => 'Sub header'
            ],[
                'name' => 'description',
                'label' => 'Lot description',
                'type' => 'wysiwyg'
            ],[
                'name' => 'shipping_description',
                'label' => 'Shipping description',
                'type' => 'wysiwyg'
            ],[
                'name' => 'auction_house_fee',
                'label' => 'Auction House Fee',
                'type' => 'number',
                'attributes' => ['step' => 'any', 'min' => 0],
                'prefix' => '%'
            ],[
                'name' => 'currency',
                'label' => 'Lot currency',
                'type' => 'select2_from_array',
                'options' => ['usd' => 'USD', 'gbp' => 'GBP', 'eur' => 'EUR'],
                'allows_null' => false,
                'default' => 'usd'
            ],[
                'name' => 'min_bid_amount',
                'label' => 'Minimum bid amount',
                'type' => 'number',
                'attributes' => ['step' => 'any', 'min' => 1],
            ],[
                'name' => 'range_from',
                'label' => 'Price range from',
                'type' => 'number',
                'attributes' => ['step' => 'any', 'min' => 1],
                ],[
                'name' => 'range_to',
                'label' => 'Price range to',
                'type' => 'number',
                'attributes' => ['step' => 'any', 'min' => 1],
            ],[
                'name' => 'auto_accept_price',
                'label' => 'Price to auto-accept bid',
                'type' => 'number',
                'attributes' => ['step' => 'any', 'min' => 1],
            ],[
                'name' => 'start_date',
                'type' => 'hidden'
            ],[
                'name' => 'end_date',
                'label' => 'End date',
                'type' => 'datetime_picker',
                'datetime_picker_options' => [
                    'format' => 'DD/MM/YYYY HH:mm',
                    'language' => 'en'
                ]
            ],[
                'name' => 'user_id',
                'label' => 'Seller',
                'type' => 'select',
                'model' => 'App\Models\Seller',
                'attribute' => 'full_name',

            ],[   // Upload
                'name' => 'photos',
                'label' => 'Lot photos',
                'type' => 'upload_multiple',
                'upload' => true,
                //'disk' => 'uploads'  if you store files in the /public folder, please ommit this; if you store them in /storage or S3, please specify it;
            ],[
                'name' => 'system',
                'type' => 'hidden'
            ]
         ], 'update');

        $this->crud->addFields([
            [
                'name' => 'meta_description',
                'type' => 'textarea',
                'label' => 'Meta description'
            ],
        ], 'both');

        // $this->crud->removeField('name', 'update/create/both');
        // $this->crud->removeFields($array_of_names, 'update/create/both');

        // ------ CRUD COLUMNS
        $this->crud->setColumns([
            [
                'name' => 'move',
                'type' => "model_function",
                'function_name' => 'getMoveCheckbox'
            ],
            [
                'label' => 'Lot Id',
                'name' => 'id',
            ]
            , [
                'label' => 'Category',
                'type' => 'select',
                'name' => 'category_id', // the db column for the foreign key
                'entity' => 'category', // the method that defines the relationship in your Model
                'attribute' => 'title', // foreign key attribute that is shown to user
                'model' => 'App\Models\Category' // foreign key model
            ]
            , [
                'label' => 'Partner',
                'type' => 'select',
                'name' => 'partner_id', // the db column for the foreign key
                'entity' => 'partner', // the method that defines the relationship in your Model
                'attribute' => 'title', // foreign key attribute that is shown to user
                'model' => 'App\Models\Partner' // foreign key model
            ]
            ,[
                'label' => 'Name',
                'name' => 'title'
            ]
            ,[
                'label' => 'Status',
                'name' => 'status'
            ]
            ,[
                'label' => 'Start date',
                'name' => 'start_date'
            ]
            ,[
                'label' => 'End date',
                'name' => 'end_date'
            ]
            ,[
                'label' => 'Range from',
                'name' => 'range_from',
                'type' => 'dollar'
            ],[
                'label' => 'Range to',
                'name' => 'range_to',
                'type' => 'dollar'
            ]
            ,[
                'label' => 'Min bid amount',
                'name' => 'min_bid_amount',
                'type' => 'dollar'
            ]
            , [
                'label' => 'Bids',
                'name' => 'bids_count'
            ]
            ,[
                'label' => 'Views',
                'name' => 'views_count'
            ]
        ]);
        // $this->crud->addColumn(); // add a single column, at the end of the stack
        // $this->crud->addColumns(); // add multiple columns, at the end of the stack
        // $this->crud->removeColumn('column_name'); // remove a column from the stack
        // $this->crud->removeColumns(['column_name_1', 'column_name_2']); // remove an array of columns from the stack
        // $this->crud->setColumnDetails('column_name', ['attribute' => 'value']); // adjusts the properties of the passed in column (by name)
        // $this->crud->setColumnsDetails(['column_1', 'column_2'], ['attribute' => 'value']);

        // ------ CRUD BUTTONS
        // possible positions: 'beginning' and 'end'; defaults to 'beginning' for the 'line' stack, 'end' for the others;
        // $this->crud->addButton($stack, $name, $type, $content, $position); // add a button; possible types are: view, model_function
        // $this->crud->addButtonFromModelFunction($stack, $name, $model_function_name, $position); // add a button whose HTML is returned by a method in the CRUD model
        // $this->crud->addButtonFromView($stack, $name, $view, $position); // add a button whose HTML is in a view placed at resources\views\vendor\backpack\crud\buttons
        // $this->crud->removeButton($name);
        // $this->crud->removeButtonFromStack($name, $stack);

        $this->crud->addButtonFromView('top', 'import_lots', 'import_lots');
        $this->crud->addButtonFromView('top', 'move_lots', 'move_lots', 'end');
        $this->crud->addButtonFromView('top', 'add_system_lot', 'add_system_lot', 'end');
        //$this->crud->addButton('top', 'rollback_lots', 'rollback_lots', 'admin.components.rollback_button'); // add a button; possible types are: view, model_function

        // ------ CRUD VIEWS
        $this->crud->setListView('admin.lots.list');

        // ------ CRUD ACCESS
        $this->crud->allowAccess(['list', 'create', 'update', 'delete']);
        // $this->crud->denyAccess(['list', 'create', 'update', 'reorder', 'delete']);

        // ------ CRUD REORDER
        // $this->crud->enableReorder('label_name', MAX_TREE_LEVEL);
        // NOTE: you also need to do allow access to the right users: $this->crud->allowAccess('reorder');

        // ------ CRUD DETAILS ROW
        $this->crud->enableDetailsRow();
        $this->crud->allowAccess('details_row');
        // NOTE: you also need to do allow access to the right users: $this->crud->allowAccess('details_row');
        // NOTE: you also need to do overwrite the showDetailsRow($id) method in your EntityCrudController to show whatever you'd like in the details row OR overwrite the views/backpack/crud/details_row.blade.php

        // ------ REVISIONS
        // You also need to use \Venturecraft\Revisionable\RevisionableTrait;
        // Please check out: https://laravel-backpack.readme.io/docs/crud#revisions
        // $this->crud->allowAccess('revisions');

        // ------ AJAX TABLE VIEW
        // Please note the drawbacks of this though:
        // - 1-n and n-n columns are not searchable
        // - date and datetime columns won't be sortable anymore
        $this->crud->enableAjaxTable();

        // ------ DATATABLE EXPORT BUTTONS
        // Show export to PDF, CSV, XLS and Print buttons on the table view.
        // Does not work well with AJAX datatables.
        // $this->crud->enableExportButtons();

        // ------ ADVANCED QUERIES
        // $this->crud->addClause('active');
        // $this->crud->addClause('type', 'car');
        // $this->crud->addClause('where', 'name', '==', 'car');
        // $this->crud->addClause('whereName', 'car');
        // $this->crud->addClause('whereHas', 'posts', function($query) {
        //     $query->activePosts();
        // });
        // $this->crud->addClause('withoutGlobalScopes');
        // $this->crud->addClause('withoutGlobalScope', VisibleScope::class);
        // $this->crud->orderBy();
        // $this->crud->groupBy();
        // $this->crud->limit();
    }

    public function store(LotRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud();
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(LotUpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud();
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        /*on save check if lot have required specifics, and if true return back to lot edit page*/
        $lot = $this->data['entry'];
        $lot_specifics = json_decode($lot->specifics_data);
        if ($lot_specifics->cat_opts) {
            $requiredSpecific = array_filter($lot_specifics->cat_opts, function ($v) {
               return $v->requiredNotSelected;
            });
            if ($requiredSpecific) {
                return redirect($this->crud->route . '/' . $lot->id .'/edit');
            }
        }
        return $redirect_location;
    }

    /**
     * Used with AJAX in the list view (datatables) to show extra information about that row that didn't fit in the table.
     * It defaults to showing some dummy text.
     *
     * It's enabled by:
     * - setting: $crud->details_row = true;
     * - adding the details route for the entity; ex: Route::get('page/{id}/details', 'PageCrudController@showDetailsRow');
     * - adding a view with the following name to change what the row actually contains: app/resources/views/vendor/backpack/crud/details_row.blade.php
     */
    public function showDetailsRow($id)
    {
        $lot = Lot::where('id', $id)->with(['bids'])->first();
        return view('admin.inc.lot_detail_row', ['lot' => $lot]);
    }

    /**
     * Import view
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function importForm()
    {
        return view('admin.import_form', [
            'crud' => $this->crud,
            'sellers' => Seller::all(),
            'categories' => Category::all(),
            'partners' => Partner::all()
        ]);
    }

    /**
     * Import process
     * @param LotImportRequest $request
     * @param ImportRepository $import
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function importProcess(LotImportRequest $request, ImportRepository $import)
    {
        $path = $request->csv_file->store('csv');
        $name = $request->csv_file->getClientOriginalName();
        if ($totalLots = $import->processFile(storage_path('app/' . $path), $name)) {
            \Alert::success($totalLots . ' lots has been imported successfully')->flash();
        }
        else {
            \Alert::error('Couldn\'t process CSV file')->flash();
        }
        return redirect($this->crud->route);
    }

    public function moveLotsForm(){
        if (request()->has('to_move')) {
            $lotsIds = collect(request()->only('to_move'))->flatten();
            if ($lotsIds && $lotsIds->isNotEmpty()) {
                $categories = Category::all(['id','title'])->pluck('title', 'id');
                $lots  = Lot::whereIn('id', $lotsIds)->pluck('title', 'id');
                return view('admin.lots.move_selected_lots', ['crud' => $this->crud,
                    'categories' => $categories, 'lots' => $lots]);
            }
        }
        return redirect()->back();
    }

    public function moveLots(MoveSelectedLotsRequest $request){
        $to = Category::find($request->to_cat);
        $lotsMoved = Lot::whereIn('id', $request->lots)->update(['category_id' => $to->id]);
        $message = $lotsMoved.' lots moved!';
        \Alert::info($message)->flash();
        return redirect(url($this->crud->route));
    }
}
