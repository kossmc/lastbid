@extends('landing.content')
@section('title','Events')

@section('content_page')
    @include('events.content_list', ['landing' => true])
@endsection


@section('remote_js_scripts')
    <script src="{{asset('js/zabuto_calendar.min.js')}}"></script>
    <script>
        $(document).ready(function () {

            $('#calendar').zabuto_calendar({
                data: {!! $calendar !!},
                year: '{{$date->format('Y')}}',
                month: '{{$date->format('m')}}',
                cell_border: true,
                action: function () {
                    return this.title ? window.open(this.title, '_self') : false;
                }
                ,nav_icon: {
                    prev: '<span class="arrow-icon arrow-icon-back"></span>',
                    next: '<span class="arrow-icon"></span>'
                }
            });
        })
    </script>
@stop