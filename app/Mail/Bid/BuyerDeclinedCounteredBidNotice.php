<?php

namespace App\Mail\Bid;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

use App\Traits\BidNoticeTrait;

class BuyerDeclinedCounteredBidNotice extends Mailable
{
    use Queueable, SerializesModels, BidNoticeTrait;

    /**
     * Email subject.
     *
     */
    protected $bid_notice_subject = 'You declined a bid';

    /**
     * Email template.
     *
     */
    protected $bid_notice_template = 'emails.buyer_declined_countered_bid_notice';
}
