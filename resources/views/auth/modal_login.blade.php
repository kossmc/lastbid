<div class="modal fade" id="modal-id">
<div class="container-form-wrap container-login-form">
    <a href="#" class="esc" data-dismiss="modal" aria-hidden="true">
        <em class="icon-esc_icon"></em>
        esc
    </a>
    <div class="container-forms">
        <div class="container-info">
            <div class="info-item">
                <div class="table">
                    <div class="table-cell">
                        <div class="text">If you already have an account, click the button below to enter your account.
                        </div>
                        <div class="btn">
                            Login
                        </div>
                    </div>
                </div>
            </div>
            <div class="info-item">
                <div class="table">
                    <div class="table-cell">
                        <div class="text">If you don't already have an account, click the button below to create your
                            account.
                        </div>
                        <div class="btn" onclick="auth('register_form')">
                            Register
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <form action="" id="reg-form">
            <div class="container-form">
            <div class="form-item log-in">
                <div class="table">
                        <div class="table-cell">
                            <div class="title" style="margin: 8px 0 40px;">login
                                @if (!empty($mess))
                                    <div class="row">
                                            <span class="error-notification"
                                                  style="width: 400px; margin: 20px auto 0;">{{$mess}}</span>
                                    </div>
                                @endif
                            </div>
                            <div class="row">
                                <div class="inp-wrap">
                                    <input type="text" placeholder="Your email" name="email" value="{{ old('email') }}"
                                           required autofocus
                                           class="{{ $errors->has('email') ? 'has-error--bgcolor' : '' }}">
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="inp-wrap">
                                    <input type="password" placeholder="Your password" name="password" required
                                           class="{{ $errors->has('password') ? 'has-error--bgcolor' : '' }}">
                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="remember-wrap">
                                <div class="item">
                                    <a href="javascript:void 0;" onclick="auth('forgot_form')">Forgotten your password? &nbsp;  Reset password</a>
                                </div>
                                <div class="item">
                                    <input id="field1" class="sort-box-cb" name="remember" type="checkbox" checked>
                                    <label for="field1">Remember me</label>
                                </div>
                            </div>
                            <div class="btn-wrap">
                                <a href="javascript:void 0;" class="btn" onclick="auth('login')">Log in</a>
                            </div>
                            {{--<div class="lognin-divider">--}}
                            {{--</div>--}}
                            {{--<div class="row wrap-login-with-account">--}}
                                {{--<a href="{{ route('socialAuth',['provider'=>'facebook']) }}"><span--}}
                                            {{--class="btn-login-with login-with-fb">facebook</span></a>--}}
                                {{--<a href="{{ route('socialAuth',['provider'=>'google']) }}">--}}
                                    {{--<span class="btn-login-with login-with-google">google</span>--}}
                                {{--</a>--}}
                            {{--</div>--}}
                        </div>
                </div>
            </div>
        </div>
        <input type="hidden" name="ip" value="" id="ip">
        </form>
    </div>
</div>
</div>
