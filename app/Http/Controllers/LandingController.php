<?php

namespace App\Http\Controllers;

use App\Http\Requests\KeepInTouchRequest;
use App\Http\Requests\SubscribeToNewsletterRequest;
use App\Services\Contracts\EmailMarketingServiceInterface;
use Illuminate\Support\Facades\Mail;

/**
 * Class LandingController
 * @package App\Http\Controllers
 */
class LandingController extends Controller
{
    /**
     * Main landing page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        \URL::forceRootUrl(env('APP_LANDING_URL'));

        return view('landing.index');
    }

    /**
     * Policy page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function policy()
    {
        \URL::forceRootUrl(env('APP_LANDING_URL'));

        return view('landing.policy');
    }

    /**
     * Policy page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function terms()
    {
        \URL::forceRootUrl(env('APP_LANDING_URL'));

        return view('landing.terms');
    }

    /**
     * Policy page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function about()
    {
        \URL::forceRootUrl(env('APP_LANDING_URL'));

        return view('landing.about');
    }

    /**
     * Feedback form
     *
     * @param KeepInTouchRequest $request
     * @param EmailMarketingServiceInterface $emailMarketingService
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(KeepInTouchRequest $request, EmailMarketingServiceInterface $emailMarketingService)
    {
        $subscriptionListId = config('services.campaignmonitor.lists.general');

        $emailMarketingService->subscribe($request->get('email'), $subscriptionListId);

        $subject = trans('emails.admin_lets_talk_notice.subject');

        $to = [
            config('mail.auction_email'),
        ];

        $this->notify('emails.admin_lets_talk_notice', $subject, $request->all(), $to);

        return response()->json([
            'status'  => true,
            'message' => trans('newsletter.lets_talk'),
            'note'    => trans('newsletter.thanks'),
        ]);
    }

    /**
     * Subscription form
     *
     * @param SubscribeToNewsletterRequest $request
     * @param EmailMarketingServiceInterface $emailMarketingService
     * @return \Illuminate\Http\JsonResponse
     */
    public function subscribe(SubscribeToNewsletterRequest $request, EmailMarketingServiceInterface $emailMarketingService)
    {
        $subscriptionListId = config('services.campaignmonitor.lists.newsletter');

        if ($emailMarketingService->isSubscribed($request->get('email'), $subscriptionListId)) {
            return response()->json([
                'status'  => true,
                'message' => trans('newsletter.alreadySubscribed'),
                'note'    => '',
            ]);
        }

        $emailMarketingService->subscribe($request->get('email'), $subscriptionListId);

        $subject = 'NL' === $request->get('type')
            ? trans('emails.admin_landing_discover_more_notice.subject.newsletter')
            : trans('emails.admin_landing_discover_more_notice.subject.discover');

        $to = [
            config('mail.auction_email'),
            config('mail.contact_us_email'),
            config('mail.test_email'),
        ];

        $this->notify('emails.admin_landing_discover_more_notice', $subject, $request->all(), $to);

        return response()->json([
            'status'  => true,
            'message' => trans('newsletter.successfulSubscription'),
            'note'    => trans('newsletter.confirmEmail'),
        ]);
    }

    /**
     * @param string $emailView
     * @param string $subject
     * @param array $data
     * @param array $to
     */
    private function notify($emailView, $subject, $data, $to)
    {
        Mail::send($emailView, $data, function ($message) use ($subject, $to) {
            $message->subject($subject);
            $message->to($to);
        });
    }
}
