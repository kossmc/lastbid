<p>Congratultaions! Your bid on lot <a href="{{ $lot->url }}">{{ $lot->title }}</a> was accepted by seller for {!! format_price($bid->attempt->amount, null, $lot->currency) !!}.</p>
<p>Our managers will contact you to discuss details!</p>
