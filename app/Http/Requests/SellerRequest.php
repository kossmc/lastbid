<?php

namespace App\Http\Requests;

use Backpack\CRUD\app\Http\Requests\CrudRequest as FormRequest;
use Illuminate\Validation\Rule;

class SellerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        /*try to get edit user id in order to ignore current email*/
        $request = \Request::instance();
        $user_id = $request->input('id', 0);
        if ($user_id) {
            return [
                'first_name' => 'required|string|max:190',
                'last_name' => 'required|string|max:190',
                'fees' => 'required|array',
                'email' => [
                    'required', 'email', 'max:190', Rule::unique('users')->ignore($user_id)
                ]
            ];
        } else {
            return [
                'first_name' => 'required|string|max:190',
                'last_name' => 'required|string|max:190',
                'fees' => 'required|array',
                'email' => [
                    'required', 'email', 'max:190', Rule::unique('users')
                ]
            ];
        }
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            //
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            //
        ];
    }
}
