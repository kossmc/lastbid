<?php

namespace App\Mail\Bid;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

use App\Traits\BidNoticeTrait;

class SellerAcceptedBidNotice extends Mailable
{
    use Queueable, SerializesModels, BidNoticeTrait;

    /**
     * Email subject.
     *
     */
    protected $bid_notice_subject = 'You accepted a bid';

    /**
     * Email template.
     *
     */
    protected $bid_notice_template = 'emails.seller_accepted_bid_notice';
}
