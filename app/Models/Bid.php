<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Bid extends Model
{

    const STATUS_PENDING = 'pending';
    const STATUS_ACCEPTED = 'accepted';
    const STATUS_DECLINED = 'declined';
    const STATUS_EXPIRED = 'expired';

    const BY_SELLER = 'seller';
    const BY_BUYER = 'buyer';

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $dates = ['created_at', 'updated_at'];

    protected $table = 'bids';
    //protected $primaryKey = 'id';
    public $timestamps = true;
    // protected $guarded = ['id'];
    protected $fillable = ['user_id', 'lot_id', 'last_bid_by', 'status'];
    // protected $hidden = [];
    // protected $dates = [];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
    ];
    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['attempt'];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public function createByBuyer(array $data) {
        $userId = $data['user_id'];
        $attributes = [
            'user_id' => $userId,
            'lot_id' => $data['lot_id']
        ];
        $data['last_bid_by'] = self::BY_BUYER;
        $data['status'] = self::STATUS_PENDING;
        if ($bid = $this->updateOrCreate($attributes, $data)) {
            $bid->attempts()->create($data);
            return self::withCount(['attempts' => function($query) use ($userId) {
                $query->where('user_id', $userId);
            }])->where('id', $bid->id)->first();
        }
        return false;
    }

    public function isCounter() {
        return $this->latestAttempt()->is_counter;
    }

    public function acceptBySeller()
    {
        $this->last_bid_by = self::BY_SELLER;
        return $this->accept();
    }

    public function acceptByBuyer()
    {
        $this->last_bid_by = self::BY_BUYER;
        return $this->accept();

    }

    public function accept()
    {
        $this->status = self::STATUS_ACCEPTED;
        if ($this->save()) {
            $this->latestAttempt()->accept();
            return true;
        }
        return false;
    }

    public function declineBySeller()
    {
        $this->last_bid_by = self::BY_SELLER;
        return $this->decline();
    }

    public function declineByBuyer()
    {
        $this->last_bid_by = self::BY_BUYER;
        return $this->decline();

    }
    
    public function decline()
    {
        $this->status = self::STATUS_DECLINED;
        if ($this->save()) {
            $this->latestAttempt()->decline();
            return true;
        }
        return false;
    }

    public function counterBySeller(array $data)
    {
        $this->last_bid_by = self::BY_SELLER;
        return $this->counter($data);
    }

    public function counterByBuyer(array $data)
    {
        $this->last_bid_by = self::BY_BUYER;
        return $this->counter($data);

    }

    protected function counter(array $data) {
        $this->status = self::STATUS_PENDING;
        if ($this->save($data)) {
            $this->latestAttempt()->counter();
            $data['is_counter'] = 1;
            $bid = $this->attempts()->create($data);
            $this->lot->last_bid_date = $bid->created_at;
            $this->lot->save();
            return true;
        }
        return false;
    }
    public function expire()
    {
        $this->status = self::STATUS_EXPIRED;
        if ($this->save()) {
            $this->latestAttempt()->expire();
            return true;
        }
        return false;
    }

    public function getExpired() {
        return $this->pending()->has('expiredAttempts')->get();
    }
    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function lot()
    {
        return $this->belongsTo(Lot::class, 'lot_id', 'id');
    }

    public function buyer()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function attempts()
    {
        return $this->hasMany(BidAttempt::class, 'bid_id')->latest();
    }

    public function expiredAttempts() {
        return $this->hasMany(BidAttempt::class, 'bid_id')->expired();
    }

    public function latestAttempt()
    {
        return $this->attempts()->first();
    }


    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    public function scopePending($query)
    {
        return $query->where('status', self::STATUS_PENDING);
    }

    public function scopeDeclinedOrExpired($query)
    {
        return $query->whereIn('status', [self::STATUS_DECLINED, self::STATUS_EXPIRED]);
    }

    public function scopeWithHistory($query)
    {
        return $query->with(['attempts', 'buyer']);
    }

    public function scopeAcceptedOrPending($query)
    {
        return $query->whereIn('status', [self::STATUS_PENDING, self::STATUS_ACCEPTED]);
    }

    public function scopeAccepted($query)
    {
        return $query->where('status', self::STATUS_ACCEPTED);
    }

    public function scopeByBuyer($query)
    {
        return $query->where('last_bid_by', self::BY_BUYER);
    }

    public function scopeBySeller($query)
    {
        return $query->where('last_bid_by', self::BY_SELLER);
    }

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    public function getAttemptAttribute()
    {
        if (!isset($this->attributes['attempt'])) {
            $this->attributes['attempt'] = $this->latestAttempt();
        }
        return $this->attributes['attempt'];
    }

    public function getBySellerAttribute()
    {
        return $this->last_bid_by == self::BY_SELLER;
    }

    public function getByBuyerAttribute()
    {
        return $this->last_bid_by == self::BY_BUYER;
    }

    public function getIsPendingAttribute()
    {
        return $this->status == self::STATUS_PENDING;
    }

    public function getIsAcceptedAttribute()
    {
        return $this->status == self::STATUS_ACCEPTED;
    }

    public function getIsDeclinedAttribute()
    {
        return $this->status == self::STATUS_DECLINED;
    }

    public function getIsExpiredAttribute()
    {
        return $this->status == self::STATUS_EXPIRED;
    }

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
