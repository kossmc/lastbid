@extends('layouts.inner')
@section('title', $lot->title)
@section('inner_content')
    @if ($bid->is_expired || $bid->is_declined)
        @section('back_link', route('buyer_declined_bids'))
    @elseif ($bid->by_buyer)
        @section('back_link', route('buyer_sent_bids'))
    @else
        @section('back_link', route('buyer_received_bids'))
    @endif
    <div class="tab-content profile-tab-content">
        <div class="tab-pane tab-bids fade in active">
            <div class="tab-inner">
                <div class="bids-list lots-list">
                    <div class="lots-list-i buyer-card--received buyer-card--expanded">
                        @include('buyer.inc.short_bid_v2', ['lotLink' => false])
                        <div class="expanded-description">
                            <a href="@yield('back_link')" class="btn-close-exapanded-bid icon-esc_icon"></a>
                            @foreach ($bid->attempts as $attempt)
                                @include('buyer.inc.attempt')
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('custom_modal')
    @include('inc.modal_confirm_accept_bid',['item'=>$bid, 'route'=>'buyer_switch_bid_status'])
@endsection
