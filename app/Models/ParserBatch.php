<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ParserBatch extends Model
{
    /*batch status*/
    const STATUS_PENDING = 'pending';
    const STATUS_PROCESSING = 'processing';
    const STATUS_FAILED = 'failed';
    const STATUS_FINISHED = 'finished';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'job_id', 'status', 'filename', 'input', 'type', 'partner_id', 'seller_id'
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */

    protected $appends = ['url'];
    public function process() {
        $this->setStatus(self::STATUS_PROCESSING);
    }
    
    public function fail() {
        $this->setStatus(self::STATUS_FAILED);
    }

    public function finish($filename) {
        $this->filename = $filename;
        $this->setStatus(self::STATUS_FINISHED);
    }

    private function setStatus($status) {
        $this->status = $status;
        $this->save();
    }

    /**
     * Generates an URL for download.
     *
     * @return string
     */
    public function getUrlAttribute()
    {
        return route('download-parser-csv', ['id' => $this->id]);
    }

    public function getIsWorkingAttribute()
    {
        return $this->status == self::STATUS_PROCESSING;
    }

    public function getIsPendingAttribute()
    {
        return $this->status == self::STATUS_PENDING;
    }

    public function getIsFailedAttribute()
    {
        return $this->status == self::STATUS_FAILED;
    }

    public function getIsFinishedAttribute()
    {
        return $this->status == self::STATUS_FINISHED;
    }
}
