<?php
namespace App\Repositories;


use App\Jobs\FetchLotPhoto;
use App\Models\LotFile;
use App\Models\LotVersion;
use Bosnadev\Repositories\Eloquent\Repository;
use App\Models\Lot;
use App\Models\User;
use App\Models\Category;
use App\Models\Partner;
use Illuminate\Support\Collection;
use Illuminate\Container\Container as App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Carbon\Carbon;
use App\Traits\CsvReaderTrait;
use ZipArchive;

class ImportRepository extends Repository
{
    use CsvReaderTrait;

    private $sellers = [];
    private $partners = [];
    private $uploadType;
    private $user_id;
    private $zip_file;
    private $zip_dir;
    private $photoDriver;
    private $fields;
    private $category_id;
    static $defaults = [
        'title' => '',
        'description' => '',
        'category_id' => 0,
        'partner_id' => 0,
        'shipping_description' => '',
        'auction_house_fee' => '',
        'min_bid_amount' => '',
        'range_from' => '',
        'range_to' => '',
        'auto_accept_price' => '',
        'start_date' => '',
        'end_date' => '',
        'user_id' => '',
        'currency' => '',
        'photos' => [],
        'status' => ''
    ];

    public function model()
    {
        return Lot::class;
    }

    /**
     * @param App $app
     * @param Collection $collection
     * @throws \Bosnadev\Repositories\Exceptions\RepositoryException
     */
    public function __construct(App $app, Collection $collection)
    {
        $this->sellers      = User::role('seller')->get()->keyBy('id');
        $this->partners     = Partner::all()->keyBy('id');
        $this->uploadType   = request('upload_type');
        $this->user_id      = request('user_id', 0);
        $this->zip_file     = request('zip_file', null);
        $this->category_id  = request('category_id', null);
        parent::__construct($app, $collection);
    }

    /**
     * Get lots from file
     * Add lots to db
     * @param $path
     * @param $name
     * @return int|null
     * @throws \Exception
     */
    public function processFile($path, $name)
    {
        $arrayOfIds = [];

        $results = $this->getCsvIterator($path);

        if($this->zip_file){
            $this->zip_dir = $this->processZip();
        }

        DB::beginTransaction();
        try {

            /** @var LotFile $lotFile */
            $lotFile = new LotFile();
            $lotFile->name = $name;
            $lotFile->save();

            foreach ($results as $i => $row) {

                if($i == 0)
                    continue;

                $data = $this->normalizeData($this->getData($row));
                /** @var Lot $model */
                $model = new Lot($data);
                if(!$model->save()){
                    DB::rollBack();
                    return null;
                }

                /** @var LotVersion $version */
                $version = new LotVersion($data);
                $version->photos = json_encode($version->photos);
                $version->setAttribute('lot_file_id', $lotFile->id);
                $version->setAttribute('lot_id', $model->id);
                $version->setAttribute('version', 1);
                if(!$version->save()){
                    DB::rollBack();
                    return null;
                }

                array_push($arrayOfIds, $model->id);
            }

            if($this->zip_file){
                $this->cleanZip(storage_path('app/zip/'.$this->zip_dir));
            }

            DB::commit();
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            if($this->zip_file){
                $this->cleanZip(storage_path('app/zip/'.$this->zip_dir));
            }
            DB::rollBack();
            return null;
        }
        return count($arrayOfIds);
    }

    /**
     * Rollback import file
     * @param $id
     * @return bool|int
     * @throws \Exception
     */
    public function rollbackFile($id)
    {
        $lotFile = LotFile::whereId($id)->first();
        if(!$lotFile){
            return false;
        }

        DB::beginTransaction();

        try {

            $arrayOfIds = LotVersion::where('lot_file_id', $lotFile->id)->pluck('lot_id')->all();

            $lots = Lot::whereIn('id', $arrayOfIds)->with('bids')->get();

            $lotsWithBids = [];
            foreach ($lots as $lot){

                // Lots with bids (Not delete this lot, Only message)
                if(count($lot->bids) > 0){

                    $arrayOfIds = array_diff($arrayOfIds, [$lot->id]);

                    array_push($lotsWithBids, $lot);
                }
            }

            $res = Lot::whereIn('id', $arrayOfIds)->delete();
            if($res){
                // Delete version
                LotVersion::whereIn('lot_id', $arrayOfIds)->delete();
                // Change status
                $lotFile->status = LotFile::STATUS_DONE;
                $lotFile->save();
            }

            foreach ($lotsWithBids as $lot){
                \Alert::error('Lot not delete because has bid #' . $lot->id)->flash();
            }

            DB::commit();
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            DB::rollBack();
            return false;
        }
        return count($arrayOfIds);
    }

    /**
     *
     * @return bool
     */
    protected function isFileValid()
    {
        $diff = array_diff(array_keys($this->fields), $this->headers);
        return empty($diff);
    }

    /**
     * @param $row
     * @return array
     */
    protected function getData($row)
    {
        $this->setDriver();
        $data = [];
        foreach ($this->fields as $field => $key) {
            $index = array_search($field, $this->headers);
            isset($row[$index]) ? $value = $row[$index]: $value = '';
            switch ($key) {
                case 'start_date':
                case 'end_date':
                    $data[$key] = $this->getDate($value);
                    break;
                case 'category':
                    if ($categoryId = $this->getCategory($value)) {
                        $data['category_id'] = $categoryId;
                    } else {
                        continue;
                    }
                    break;
                case 'subcategory':
                    if ( ! empty($value)) {
                        if ($categoryId = $this->getCategory($value, isset($data['category_id']) ? $data['category_id'] : null)) {
                            $data['category_id'] = $categoryId;
                        } elseif ( ! isset($data['category_id'])) {
                            continue;
                        }
                    }
                    break;
                case 'seller':
                    $data['user_id'] = $this->getSeller($value);
                    break;
                case 'partner_id':
                    $data[$key] = $this->getPartner($value);
                    break;
                case 'photos':
                    $data[$key] = $this->getPhotos($value);
                    break;
                case 'currency':
                    $data[$key] = $this->getCurrency($value);
                    break;
                default:
                    $data[$key] = $value;
            }
        }
        $data['status'] = isset($data['end_date']) ? $this->getStatus($data['end_date']) : $this->getStatus(null);

        return $data;
    }

    /**
     * @param $title
     * @param null $parent
     * @return bool|mixed
     */
    protected function getCategory($title, $parent = null) {
        $title = trim($title);
        $category = Category::where('title', $title)
            ->where('depth', empty($parent) ? 1 : 2)
            ->when($parent, function ($q) use ($parent) {
                $q->where('parent_id', $parent);
            })
            ->orWherehas('aliases', function ($q) use ($title) {
                $q->where('alias', $title);
            })
            ->get();
        if ($category && $category->isNotEmpty()) {
            return $category->first()->id;
        }
        $data = [
            'title' => $title,
            'parent_id' => $parent,
            'depth' => empty($parent) ? 1 : 2
        ];
        if ($category = Category::create($data)) {
            return $category->id;
        }
        return false;
    }

    /**
     * @param $lotPhotos
     * @param null $id
     * @return array|bool
     */
    protected function getPhotos($lotPhotos, $id = null) {
        $photos = [];

        switch ($this->photoDriver) {
            case 'storage':
                foreach (explode(',', $lotPhotos) as $photoPath) {
                    $path = storage_path('app/parser/photos/' . $photoPath);
                    $pathParts = pathinfo($path);
                    if (file_exists($path) && is_file($path)) {
                        $to = public_path('uploads/lots/' . $pathParts['basename']);
                        if (File::copy($path, $to)) {
                            $photos[] = 'lots/' . $pathParts['basename'];
                        }
                    }
                }
                break;
            case 'zip':
                $photoFiles = [];
                $folders = File::directories(storage_path('app/zip/'.$this->zip_dir));
                $files = File::files(storage_path('app/zip/'.$this->zip_dir));
                if(count($folders)){
                    foreach ($folders as $folder){
                       if(explode('/', $folder)[count(explode('/',$folder))-1] != '__MACOSX' &&
                        is_dir($folder)
                       )
                       {
                           foreach (File::files($folder) as $photoPath){
                               $photoFiles[] = $photoPath;
                           }
                       }
                    }
                }
                if(count($files)){
                    foreach ($files as $file){
                        $photoFiles[] = $file;
                    }
                }


                foreach ($photoFiles as $photoPath){
                    $pathParts = pathinfo($photoPath);
                    if (
                        file_exists($photoPath) &&
                        is_file($photoPath) &&
                        strtolower($pathParts['extension']) == 'jpg' ||
                        strtolower($pathParts['extension']) == 'png'
                    )
                    {
                        if(
                            strpos($pathParts['filename'], '_') !== false &&
                            explode('_',$pathParts['filename'])[0] == $id
                        )
                        {
                            $newName = md5($pathParts['basename'] . time() . rand(1, 1000000)).'.'.$pathParts['extension'];
                            $to = public_path('uploads/lots/' . $newName);
                            if (File::copy($photoPath, $to)) {
                                $photos[] = 'lots/' . $newName;
                            }
                        }
                        if(
                            strpos($pathParts['filename'], '-') !== false &&
                            explode('-',$pathParts['filename'])[0] == $id
                        )
                        {
                            $newName = md5($pathParts['basename'] . time() . rand(1, 1000000)).'.'.$pathParts['extension'];
                            $to = public_path('uploads/lots/' . $newName);
                            if (File::copy($photoPath, $to)) {
                                $photos[] = 'lots/' . $newName;
                            }
                        }
                        if(
                            strpos($pathParts['filename'], '_') === false &&
                            strpos($pathParts['filename'], '-') === false &&
                            $pathParts['filename'] == $id
                        )
                        {
                            $newName = md5($pathParts['basename'] . time() . rand(1, 1000000)).'.'.$pathParts['extension'];
                            $to = public_path('uploads/lots/' . $newName);
                            if (File::copy($photoPath, $to)) {
                                $photos[] = 'lots/' . $newName;
                            }
                        }
                    }
                }
                break;
            case 'web':
                if(!$id) return $lotPhotos;
                foreach (explode(',', $lotPhotos) as $photoPath) {
                    $pathParts = pathinfo($photoPath);
                    $newName = md5($pathParts['basename'] . time() . rand(1, 1000000)).'.'.$pathParts['extension'];
                    $to = public_path('uploads/lots/' . $newName);
                    dispatch((new FetchLotPhoto($photoPath, $to, $newName))->onQueue('fetchLotPhoto'));
                    $photos[] = 'lots/' . $newName;
                }
                break;
            default:
                return false;
        }

        if (!empty($photos)) {
            return $photos;
        }
        return false;
    }

    /**
     * @param $date
     * @return string
     */
    protected function getDate($date) {
        return Carbon::parse($date);
    }

    /**
     * @param $id
     * @return null
     */
    protected function getSeller($id) {
        return isset($this->sellers[$id]) ? $id : null;
    }

    /**
     * @param $id
     * @return null
     */
    protected function getPartner($id) {
        return isset($this->partners[$id]) ? $id : null;
    }

    /**
     * @param $end_date
     * @return string
     */
    protected function getStatus($end_date){
        if($end_date && $end_date->lt(Carbon::now())) return 'expired';
        if($end_date && $end_date->gt(Carbon::now())) return 'active';
        return 'active';
    }

    /**
     * @param $currency
     * @return string
     */
    protected function getCurrency($currency){
        return strtolower($currency);
    }

    /**
     * @param array $data
     * @return array
     */
    protected function normalizeData($data = []){
        if($this->uploadType == 'simple') return $data;
        return $this->{'normalize'.ucfirst($this->uploadType)}(array_merge(static::$defaults , $data));
    }

    /**
     * @param $row
     * @return mixed
     */
    protected function normalizeForum($row){
        $findPartner = $this->partners->filter(function($item, $key){
            if(
                preg_match('/forum/', strtolower($item->title)) ||
                $item->link == 'https://www.forumauctions.co.uk'
            ){
                return $key;
            }
        });
        $row['start_date'] = Carbon::now();
        $row['end_date'] = Carbon::now()->addDays(7);
        $row['auction_house_fee'] = 25;
        $row['user_id'] = $this->user_id;
        $row['min_bid_amount'] = $row['range_from'] * 80 / 100;
        $row['auto_accept_price'] = $row['range_from'];
        $row['partner_id'] = $findPartner->count() ? $findPartner->first()->id : 0;
        $row['category_id'] = ($this->category_id) ? $this->category_id : $row['category_id'];
        $row['photos'] = $this->getPhotos([], $row['lot_id']);
        $row['status'] = $this->getStatus($row['end_date']);
        unset($row['lot_id']);
        return $row;
    }

    /**
     * @param $row
     * @return mixed
     */
    protected function normalizeBolaffi($row){
        $findPartner = $this->partners->filter(function($item, $key){
            if(
                preg_match('/Aste Bolaffi/', strtolower($item->title)) ||
                $item->link == 'https://www.astebolaffi.it/en'
            ){
                return $key;
            }
        });
        $row['start_date'] = Carbon::now();
        $row['end_date'] = Carbon::now()->addDays(30);
        $row['auction_house_fee'] = 25;
        $row['user_id'] = $this->user_id;
        $row['min_bid_amount'] = $row['range_from'] * 99 / 100;
        $row['auto_accept_price'] = $row['range_from'] * 2;
        $row['range_to'] = ($row['range_from'] * 20 / 100) + $row['range_from'];
        $row['currency'] = 'gbp';
        $row['title'] = preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $row['title']);
        $row['description'] = preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $row['description']);
        $row['category_id'] = ($this->category_id) ? $this->category_id : $row['category_id'];
        $row['partner_id'] = $findPartner->count() ? $findPartner->first()->id : 0;
        $row['photos'] = $this->getPhotos($row['photos'], $row['lot_id']);
        unset($row['lot_id']);
        return $row;
    }

    protected function normalizeFinarte($row){
        $findPartner = $this->partners->filter(function($item, $key){
            if(
                preg_match('/finarte/', strtolower($item->title)) ||
                $item->link == 'https://www.finarte.it/'
            ){
                return $key;
            }
        });
        $row['start_date'] = Carbon::now();
        $row['end_date'] = Carbon::now()->addMonth(1);
        $row['auction_house_fee'] = 25;
        $row['user_id'] = $this->user_id;
        $row['min_bid_amount'] = $row['range_from'] * 50 / 100;
        $row['auto_accept_price'] = $row['range_from'] * 95 / 100;
        $row['partner_id'] = $findPartner->count() ? $findPartner->first()->id : 0;
        $row['category_id'] = ($this->category_id) ? $this->category_id : $row['category_id'];
        $row['photos'] = $this->getPhotos([], $row['lot_id']);
        $row['status'] = $this->getStatus($row['end_date']);
        unset($row['lot_id']);
        return $row;
    }

    protected function normalizeBase($row){

        $row['partner_id'] = request()->get('partner_id');
        $row['currency'] = request()->get('currency');
        $row['start_date'] = Carbon::now();
        $row['end_date'] = Carbon::now()->addDays(request()->get('duration', 7));
        $row['auction_house_fee'] = request()->get('buyer_premium', 1);
        $row['user_id'] = request()->get('user_id');
        $row['min_bid_amount'] = $row['range_from'] * request()->get('min_bid_amount', 1) / 100;
        $row['auto_accept_price'] = $row['range_from'] * request()->get('auto_accept_price', 1) / 100;
        $row['category_id'] = request()->get('category_id',0);
        $row['photos'] = $this->getPhotos([], $row['lot_id']);
        $row['status'] = 'active';
        unset($row['lot_id']);
        return $row;
    }



    public function processZip(){
        $filesystem = new ZipArchive();
        $filesystem->open($this->zip_file);
        $root = 'zip_'.time();
        $filesystem->extractTo(storage_path("/app/zip/$root"));
        return $root;
    }

    public function cleanZip($dir) {
        if (is_dir($dir)) {
            $objects = scandir($dir);
            foreach ($objects as $object) {
                if ($object != "." && $object != "..") {
                    if (is_dir($dir."/".$object))
                        $this->cleanZip($dir."/".$object);
                    else
                        unlink($dir."/".$object);
                }
            }
            rmdir($dir);
        }
    }

    public function setDriver(){
        switch ($this->uploadType) {
            case 'forum':
                $this->photoDriver = 'zip';
                $this->fields = [
                    'Lot Number' => 'lot_id',
                    'Lot Ext' => 'end_date',
                    'Lot Title' => 'title',
                    'Lot Description' => 'description',
                    'Curr Code' => 'currency',
                    'Reserve' => 'range_from',
                    'Hi Est' => 'range_to',
                ];
                break;
            case 'bolaffi':
                $this->photoDriver = 'web';
                $this->fields = [
                    'Lot Number' => 'lot_id',
                    'Lot Title' => 'title',
                    'Lot Description' => 'description',
                    'Starting Bid' => 'range_from',
                    'Image' => 'photos'
                ];
                break;
            case 'finarte':
                $this->photoDriver = 'zip';
                $this->fields = [
                    'Lot number' => 'lot_id',
                    'Title' => 'title',
                    'Description' => 'description',
                    'Starting bid' => 'min_bid_amount',
                    'Minimum estimate' => 'range_from',
                    'Maximum estimate' => 'range_to',
                    'Reserve price' => 'auto_accept_price',
                    'Buyers premium' => 'auction_house_fee',
                    'Currency' => 'currency'
                ];
                break;
            case 'base':
                $this->photoDriver = 'zip';
                $this->fields = [
                    'Lot number' => 'lot_id',
                    'Title' => 'title',
                    'Description' => 'description',
                    'Minimum estimate' => 'range_from',
                    'Maximum estimate' => 'range_to',
                ];
                break;
            default:
                $this->photoDriver = 'storage';
                $this->fields = [
                    'Title' => 'title',
                    'Description' => 'description',
                    'Category' => 'category',
                    'Sub Category' => 'subcategory',
                    'Partner' => 'partner_id',
                    'Shipping description' => 'shipping_description',
                    'Auction House Fee' => 'auction_house_fee',
                    'Minimum bid amount' => 'min_bid_amount',
                    'Price range from' => 'range_from',
                    'Price range to' => 'range_to',
                    'Price to auto-accept bid' => 'auto_accept_price',
                    'Start date' => 'start_date',
                    'End date' => 'end_date',
                    'Seller' => 'seller',
                    'Currency' => 'currency',
                    'Lot photos' => 'photos'
                ];
        }
    }
}
