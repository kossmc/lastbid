<?php

namespace App\Services;


class ArtNetGalleriesCrawler extends BaseCrawler
{

    public $baseUrl = 'http://www.artnet.com';

    public function __construct($batch)
    {
        $this->filename = 'Artnet_Art_Galleries_' . date('Y-m-d_H-i-s') . '.csv';
        parent::__construct($batch);
    }
    /**
     * Parsing logic go here
     *
     * array with results
     *
     * @return array
     */
    public function startCrawling()
    {
        $galleries = [];
        $count = null;
        $page = 1;
        do {
            $response = $this->client->request('GET', $this->getListUrl($page));
            if ($response->getStatusCode() == 200) {
                $content = $this->processContent(
                    $response->getBody()->getContents()
                );
                if (is_null($count)) {
                    $count = $this->getPagesCount($content);
                }
                $iframeLinks = $this->getIframeLinks($content);
                $directLinks = $this->getDirectLinks($content);
                foreach (array_merge($directLinks, $iframeLinks) as $link) {
                    $response = $this->client->request('GET', $this->getGalleryUrl($link['link']));
                    if ($response->getStatusCode() == 200) {
                        $content = $this->processContent(
                            $response->getBody()->getContents()
                        );
                        if ($data = $this->getData($content, $link['type'])) {
                            $galleries[] = [
                                'Name' => $link['title'],
                                'Location 1 - Adress' => isset($data['location1_address1']) && isset($data['location1_address2']) ? $data['location1_address1'] . "\n" . $data['location1_address2'] : '',
                                'Location 1 - Phone' => isset($data['location1_tel']) ? $data['location1_tel'] : '',
                                'Location 1 - Fax' => isset($data['location1_fax']) ? $data['location1_fax'] : '',
                                'Location 2 - Adress' => isset($data['location2_address1']) && isset($data['location2_address2']) ? $data['location2_address1'] . "\n" . $data['location2_address2'] : '',
                                'Location 2 - Phone' => isset($data['location2_tel']) ? $data['location2_tel'] : '',
                                'Location 2 - Fax' => isset($data['location2_fax']) ? $data['location2_fax'] : ''
                            ];
                        }
                    }
                }
            }
            $page ++;
        } while ($count > $page);
        if (!empty($galleries)) {
            $this->headers = array_keys($galleries[0]);
        }
        return $galleries;
    }

    public function getListUrl($page = 1) {
        return $this->baseUrl . '/galleries/fine-art/' . $page;
    }

    public function getGalleryUrl($link = '') {
        return $this->baseUrl . $link;
    }
    
    public function getPagesCount($content) {
        if (preg_match('/<font color="#333333" size="1">([0-9]+)<\/font>/ui', $content, $matches)) {
            return $matches[1];
        }
        return null;
    }

    public function getIframeLinks($content) {
        $links = [];
        if (preg_match_all("/<a\s+onClick=\"if\(isOpen\('[0-9]+'\)\)\{closeElement\([0-9]+\);\}else\{iframe\.location='([^']+?)'\};return false;\" name=\".+?\" href=\".+?\">([^<]+?)<\/a>/ui", $content, $matches)) {
            foreach ($matches[1] as $i => $link) {
                $links[] = [
                    'link' => $link,
                    'title' => trim($matches[2][$i]),
                    'type' => 'Iframe'
                ];
            }
        }
        return $links;
    }

    public function getDirectLinks($content) {
        $links = [];
        if (preg_match_all('/<a name=".+?" href="([^"]+?)">([^<]+?)<\/a>/ui', $content, $matches)) {
            foreach ($matches[1] as $i => $link) {
                $links[] = [
                    'link' => $link,
                    'title' => trim($matches[2][$i]),
                    'type' => 'Direct'
                ];
            }
        }
        return $links;
    }

    public function getData($content, $type) {
        return $this->{'get' . $type . 'Data'}($content);
    }

    public function getIframeData($content) {
        $data = [];
        if (preg_match('/<table style="margin-left:10px;" border="0" cellspacing="0" cellpadding="0" width="0"><tr><td colspan="2">([^<]+?)<\/td><\/tr><tr><td colspan="2">([^<]+?)<\/td>/ui', $content, $matches)) {
            $data['location1_address1'] = $matches[1];
            $data['location1_address2'] = $matches[2];
        }
        if (preg_match('/<td width="40">Tel:&nbsp;<\/td><td>([^<]+?)</ui', $content, $matches)) {
            $data['location1_tel'] = $matches[1];
        }
        if (preg_match('/<td width="40">Fax:&nbsp;<\/td><td>([^<]+?)</ui', $content, $matches)) {
            $data['location1_fax'] = $matches[1];
        }        
        return $this->cleanData($data);
    }

    public function getDirectData($content) {
        $data = [];
        if (preg_match_all('/<ul itemprop="address"(.+?)<\/ul>/ui', $content, $matches)) {
            foreach ($matches[1] as $i => $addressContent) {
                if (preg_match('/itemprop="streetAddress">([^<]+?)</ui', $addressContent, $addressMatches)) {
                    $data['location' . ($i + 1) . '_address1'] = $addressMatches[1];
                }
                if (preg_match('/liStringAddress">([^<]+?)</ui', $addressContent, $addressMatches)) {
                    $data['location' . ($i + 1) . '_address2'] = $addressMatches[1];
                }
                if (preg_match('/itemprop="addressCountry">([^<]+?)</ui', $addressContent, $addressMatches)) {
                    if (!empty($data['location' . ($i + 1) . '_address2'])) {
                        $data['location' . ($i + 1) . '_address2'] .= ', ' . $addressMatches[1];
                    }
                    else {
                        $data['location' . ($i + 1) . '_address2'] = $addressMatches[1];
                    }
                }
                if (preg_match('/liPhone1">T: ([^<]+?)</ui', $addressContent, $addressMatches)) {
                    $data['location' . ($i + 1) . '_tel'] = $addressMatches[1];
                }
                if (preg_match('/liFax">F: ([^<]+?)</ui', $addressContent, $addressMatches)) {
                    $data['location' . ($i + 1) . '_fax'] = $addressMatches[1];
                }
            }
        }
        return $this->cleanData($data);
    }

    public function cleanData($data) {
        foreach ($data as &$value) {
            $value = html_entity_decode($value);
            $value = preg_replace('/\s+,/ui', ',', $value);
            $value = preg_replace('/\s+/ui', ' ', $value);
            $value = preg_replace('/^\s+/ui', '', $value);
            $value = preg_replace('/\s+$/ui', '', $value);
        }
        return $data;
    }
}
