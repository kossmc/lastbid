<?php

return [
    'alreadySubscribed'      => 'You are already subscribed!',
    'successfulSubscription' => 'Thank you for subscription!',
    'confirmEmail'           => 'Please confirm your email via link sent to you',
    'lets_talk'              => 'Let\'s talk',
    'thanks'                 => 'Thank you! Your message has been sent.',
];
