<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Repositories\LotRepository;

class ExpiredLot extends Command
{
    /**
     * The lot repository implementation.
     *
     * @var LotRepository
     */
    private $lot;

    /**
     * Create a new controller instance.
     *
     * @param LotRepository $lot
     * @return void
     */
    public function __construct(LotRepository $lot)
    {
        $this->lot = $lot;
        parent::__construct();
    }

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'lot:mark-expired';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Mark lot as expired after end date';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->lot->markExpired();
    }
}
