<?php
namespace App\Repositories;

use Bosnadev\Repositories\Eloquent\Repository;
use App\Models\Event;
use Carbon\Carbon;

class EventRepository extends Repository
{
    public function model()
    {
        return Event::class;
    }

    public function latest($limit = 3, $exclude = [])
    {
        return $this->model
            ->whereNotIn('id', $exclude)
            ->active()
            ->upcoming()
            ->oldest('event_date')
            ->take($limit)->get();
    }

    public function findBySlug($slug)
    {
        return $this->markAsArchive($this->model->with(['partners'])->where('slug', $slug)->active()->first());
    }

    public function getEvents()
    {
        return $this->modifyCollection($this->model
            ->with(['partners'])
            ->upcoming()
            ->active()
            ->oldest('event_date')
            ->paginate(request('per_page', env('PAGINATE_EVENTS'))));
    }

    public function getMonthEvents($start, $end)
    {
        return $this->modifyCollection($this->model
            ->where('event_date',">=", $start)
            ->where('event_date',"<=", $end)
            ->with(['partners'])
            //->upcoming()
            ->active()
            ->oldest('event_date')
            ->paginate(request('per_page', env('PAGINATE_EVENTS'))));
    }

    public function getEventsByDate($date)
    {
        return $this->modifyCollection($this->model
            ->with(['partners'])
            ->byDate($date)
            ->active()
            ->oldest('event_date')
            ->paginate(request('per_page', env('PAGINATE_EVENTS'))));
    }

    /**
     * modify existing collections for paginator
     * @param $collection
     * @return mixed
     */
    public function modifyCollection($collection){
        return $collection->setCollection($collection->getCollection()->each(function($item){
            return $this->markAsArchive($item);
        }));
    }

    /**
     * add mark "archive" mark to collection title (Lot title)
     * @param $item
     * @return mixed
     */
    public function markAsArchive($item){
        $item->title = Carbon::parse($item->event_date)->lt(Carbon::now()) ? "{$item->title} (archive)" : $item->title;
        return $item;

    }

    public function getCalendar()
    {
        $this->model->active()->get(['id', 'title', 'event_date']);
        $events = $this->model->active()->get(['id','title','event_date']);
        $events = $events->map(function ($event) {
           return (object)[
               'id' => $event->id,
               'date' => $event->event_date->format('Y-m-d'),
               'badge' => false,
               'classname' => 'bg-info',
               'title' => route('events',[
                   'year'=> $event->event_date->format('Y'),
                   'month'=> $event->event_date->format('m'),
                   'day'=> $event->event_date->format('d'),
               ])
           ];
        });
        return $events->toJson();
    }
}
