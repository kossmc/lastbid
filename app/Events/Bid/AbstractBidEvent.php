<?php

namespace App\Events\Bid;

use App\Models\Bid;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;

abstract class AbstractBidEvent
{
    use Dispatchable, SerializesModels;

    public $bid;

    /**
     * Create a new event instance.
     * @param Bid $bid
     * @return void
     */
    public function __construct(Bid $bid)
    {
        $this->bid = $bid;
    }
}
