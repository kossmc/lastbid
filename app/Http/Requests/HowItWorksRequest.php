<?php


namespace App\Http\Requests;

use Backpack\CRUD\app\Http\Requests\CrudRequest;

class HowItWorksRequest extends CrudRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
//            'video' => 'string|required',
            'section_1_text' => 'string|required',
            'section_2_text' => 'string|required',
            'section_3_text' => 'string|required',
            'section_4_text' => 'string|required',
//            'image_section_2' => 'string|required',
        ];
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            //
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            //
        ];
    }
}