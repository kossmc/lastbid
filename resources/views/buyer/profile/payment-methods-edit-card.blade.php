@extends('buyer.profile.profile')
@section('title', 'Payment Methods')
@section('profile_content')
    <div class="buyer-account">
        <section class="payment">
            <h3>Edit card</h3>
            <p>We take your security very seriously and use <a href="#">Special System</a> for payment processing. For security
                purposes, you may see a temporary authorization to validate your identity and payment information.</p>
            @include('inc.top_errors_list')
            <form id="add-payment-card" action="{{route('payment-methods-card-edit-save', $card->id)}}" method="POST">
                @include('buyer.profile._card_form', ['card' => $card, 'edit'=>true])
                <div class="form-controll form-controll--newline form-controll--large">
                    <div class="btn-wrap">
                        <button class="btn" type="submit">Save</button>
                        <a class="btn2" href="{{route('payment-methods')}}">Cancel</a>
                    </div>
                </div>
            </form>
        </section>
    </div>
@endsection
