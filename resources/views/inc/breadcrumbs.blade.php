@php( ! isset($isLastLink) ? $isLastLink = false : true)
<div class="cat-breadcrumbs">
        @foreach ($links as $link)
                @if ($loop->last)
                        @if ($isLastLink)
                                <a href="{!! $link['link'] !!}"><span>{{ $link['title'] }}</span></a>
                        @else
                                <span class="cat-breadcrumbs--last">{{ $link['title'] }}</span>
                        @endif
                @else
                        <a href="{!! $link['link'] !!}"><span>{{ $link['title'] }}</span></a><span
                                class="cat-breadcrumbs--devider">&gt;</span>
                @endif
        @endforeach
</div>
