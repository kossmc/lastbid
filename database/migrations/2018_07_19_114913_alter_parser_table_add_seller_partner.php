<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterParserTableAddSellerPartner extends Migration
{
    /**timezone
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('parser_batches', function (Blueprint $table) {
            $table->unsignedInteger('seller_id')->nullable();
            $table->unsignedInteger('partner_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('parser_batches', function (Blueprint $table) {
            $table->dropColumn('partner_id');
            $table->dropColumn('seller_id');
        });
    }
}
