@if (isset($slides) && $slides->isNotEmpty())
<div class="slider-head">
    <div class="slider">
        @foreach($slides as $slide)
        <div class="item">
            <div class="slide-bg-image"
                 style="background: url(../uploads/{!! $slide->image !!}) no-repeat 47% center;">
                <div class="desc-wrap">
                    <div class="title">
                        <h3>{!! $slide->title !!}</h3>
                    </div>
                    <div class="desc">
                        {!! $slide->text !!}
                    </div>
                    <div class="btn-wrap">
                        <a href="{!! $slide->button_link !!}" class="btn brown">{{$slide->button_text}}</a>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>
@endif
