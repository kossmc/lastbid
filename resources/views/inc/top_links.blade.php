@if (Auth::check())
	<div class="icons">
		<a href="{{ route('messages_received') }}" class="top-counter-links messages-counter{{ Auth::user()->new_messages_count > 0 ? ' active' : '' }}">
			<span class="counter">{{ Auth::user()->new_messages_count }}</span>
			<em class="icon-messages_icon"></em>
		</a>
		<a href="@role('buyer') {{ route('buyer_received_bids') }} @else {{ route('seller_bids') }} @endrole" class="top-counter-links bids-counter{{ Auth::user()->new_bids_count > 0 ? ' active' : '' }}">
			<span class="counter">{{ Auth::user()->new_bids_count }}</span>
			<em class="icon-bids_icon"></em>
		</a>
	</div>
	<div class="user-enter">
		<ul>
			<li>
				<a href="javascript:void 0;">{{ Auth::user()->full_name }}</a>
				<ul>
					@role('admin')
                        <li><a href="{{ url('admin') }}">Admin panel</a></li>
					@else
                        @role('buyer')
							<li><a href="{{ route('account-info') }}">Personal info</a></li>
                            <li><a href="{{ route('purchases') }}">My successful bids</a></li>
                            <li><a href="{{ route('buyer_received_bids') }}">Bids</a></li>
                        @else
							<li><a href="{{ route('seller-account-info') }}">Personal info</a></li>
                            @if (false)
                                <li><a href="{{ route('managers') }}">Managers</a></li>
                            @endif
                            <li><a href="{{ route('seller_active_lots') }}">My lots</a></li>
                            <li><a href="{{ route('seller_bids') }}">Bids</a></li>
    					@endrole
                        <li><a href="{{ route('messages_received') }}">Messages</a></li>
					@endrole
					<li>
						<a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
							Log out
						</a>
						<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
							{{ csrf_field() }}
						</form>
					</li>
				</ul>
			</li>
		</ul>
	</div>
@else
	@include('inc.auth_links')
@endif
