<?php

namespace App\Models;

use App\Traits\MetaRelationTrait;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Backpack\CRUD\CrudTrait;
use App\Traits\ModelImageTrait;

class Event extends Model
{

    use Sluggable;
    use CrudTrait;
    use ModelImageTrait;
    use MetaRelationTrait;

    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;
    /*
   |--------------------------------------------------------------------------
   | GLOBAL VARIABLES
   |--------------------------------------------------------------------------
   */
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'events';
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;
    public $dates = ['event_date'];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'slug', 'image', 'location', 'body', 'brief', 'event_date', 'status', 'header_image'];

    protected $appends = ['url'];

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title',
                'maxLength' => 255,
            ]
        ];
    }

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }
    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public static function boot()
    {
        parent::boot();
        static::deleting(function ($obj) {
            \Storage::disk('uploads')->delete($obj->image);
        });
    }

    public function genMetaDescription()
    {
        $body = str_replace("&nbsp;", ' ', $this->body);

        $titleLen = mb_strlen($this->title);
        $locationLen = mb_strlen($this->location);
        $descriptionLen = (150-$titleLen)-$locationLen;

        return $this->title .' - '. $this->location .' - '. mb_strimwidth( strip_tags($body), 0, $descriptionLen);
    }
    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function partners()
    {
        return $this->belongsToMany('App\Models\Partner');
    }
    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */
    /**
     * Scope a query to only active events.
     *
     * @param \Illuminate\Database\Eloquent\Builder $builder
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where('status', self::STATUS_ACTIVE);
    }
    /**
     * Scope a query to only upcoming events.
     *
     * @param \Illuminate\Database\Eloquent\Builder $builder
     * @param $date
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeUpcoming($query, $date=null)
    {
        $date = $date? :Carbon::now()->startOfDay();
        return $query->whereDate('event_date', '>=', $date);
    }
    /**
     * Scope a query to only events by specific date.
     *
     * @param \Illuminate\Database\Eloquent\Builder $builder
     * @param $date
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeByDate($query, $date)
    {
        return $query->whereDate('event_date', $date);
    }
    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */


    /**
     * Generates an URL for event.
     *
     * @return string
     */
    public function getUrlAttribute()
    {
        return route('event', ['slug' => $this->slug]);
    }
    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
    public function setImageAttribute($value)
    {
        $attribute_name = "image";
        $disk = "uploads";
        $destination_path = "events";

        // if the image was erased
        if ($value == null) {
            // delete the image from disk
            \Storage::disk($disk)->delete($this->image);

            // set null in the database column
            $this->attributes[$attribute_name] = null;
        }

        // if a base64 was sent, store it in the db
        if (starts_with($value, 'data:image')) {
            // 0. Make the image
            $image = \Image::make($value);
            // 1. Generate a filename.
            // @todo: why jpeg here?
            $filename = md5($value . time()) . '.jpg';
            // 2. Store the image on disk.
            \Storage::disk($disk)->put($destination_path . '/' . $filename, $image->stream());
            // 3. Save the path to the database
            $this->attributes[$attribute_name] = $destination_path . '/' . $filename;
        }
    }

    public function setHeaderImageAttribute($value)
    {
        $attribute_name = "header_image";
        $disk = "uploads";
        $destination_path = "events";

        // if the image was erased
        if ($value == null) {
            // delete the image from disk
            \Storage::disk($disk)->delete($this->image);

            // set null in the database column
            $this->attributes[$attribute_name] = null;
        }

        // if a base64 was sent, store it in the db
        if (starts_with($value, 'data:image')) {
            // 0. Make the image
            $image = \Image::make($value);
            // 1. Generate a filename.
            // @todo: why jpeg here?
            $filename = md5($value . time()) . '.jpg';
            // 2. Store the image on disk.
            \Storage::disk($disk)->put($destination_path . '/' . $filename, $image->stream());
            // 3. Save the path to the database
            $this->attributes[$attribute_name] = $destination_path . '/' . $filename;
        }
    }
}
