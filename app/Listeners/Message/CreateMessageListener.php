<?php

namespace App\Listeners\Message;

use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\Message\AbstractMessageEvent;

class CreateMessageListener extends SendGridAbstractMessageListener
{

    /**
     * Email subject.
     */
    private $subject = 'You received a new message';

    /**
     * Email template.
     */
    private $template = 'fd1c58f7-fa3c-4fa0-a704-00334dc15fa5';

    /**
     * Handle the event.
     *
     * @param  AbstractMessageEvent $event
     * @return void
     */
    public function handle(AbstractMessageEvent $event)
    {
        $this->setData($event);
        $this->sendMail($this->recipient, $this->getData($this->template), $this->subject);
    }
}