<p>Lot name: <a href="{{ $lot->url }}">{{ $lot->title }}</a></p>
<p>Lot#: {{ $lot->id }}</p>
<p><img width="200" src="{!! $lot->getImagePath(200, 200) !!}" alt=""></p>
<p>Lot price range: {!! format_price($lot->range_from_with_fee, $lot->range_to_with_fee,$lot->currency) !!}</p>
<p>Lot end date: {{ $lot->end_date }}</p>
