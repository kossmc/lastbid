@extends('layouts.inner')
@section('title', 'My lots')
@section('inner_content')
    @include('inc.flash_message')
    <ul class="nav nav-tabs profile-tabs-nav lots-tabs-nav">
        <li{!! isActiveRoute('seller_active_lots') ? ' class="active"' : '' !!}><a
                    href="{{ route('seller_active_lots') }}">Active lots</a></li>
        <li{!! isActiveRoute('seller_closed_lots') ? ' class="active"' : '' !!}><a
                    href="{{ route('seller_closed_lots') }}">Closed lots</a></li>
        <li{!! isActiveRoute('seller_sold_lots') ? ' class="active"' : '' !!}><a href="{{ route('seller_sold_lots') }}">Sold
                lots</a></li>
    </ul>
    <div class="tab-content profile-tab-content">
        <div class="tab-pane tab-lots fade in active">
            <div class="tab-inner">
                <div class="{!! isActiveRoute('seller_active_lots') ? ' filter-wrapper-lots' : 'filter-wrapper' !!}">

                    @if(isActiveRoute('seller_active_lots'))
                    <div class="buttons-block">
                        <span class="first-col">
                            <a href="{{ route('seller_create_lots_disabled') }}" class="btn2 btn-small">Add lot</a>
                        </span>

                        <span class="second-col">
                            <a href="{{ route('seller_bulk_upload_disabled') }}" class="btn2 btn-small">Add Bulk</a>
                        </span>
                    </div>
                    @endif

                    <div class="filter filter-default">
                        <div class="item">
                            <div class="name">Sort by</div>
                            <div class="select"><select>
                                    <option>Latest</option>
                                </select></div>
                            <div class="sorting">
                                <a href="javascript:void 0;" class="icon-sorting_up
                                @if(request('sort_latest', 'DESC') == 'ASC') icon-sorting--active @endif"></a>
                                <a href="javascript:void 0;" class="icon-sorting_up down
                                @if(request('sort_latest', 'DESC') == 'DESC') icon-sorting--active @endif"></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="lots-list">
                    @foreach ($lots as $lot)
                        <div class="lots-list-i">
                            <div class="image"><a href="{{ route('seller_lot', ['id' => $lot->id]) }}"><img src="{{
                            $lot->getImagePath(130, 130) }}" alt=""></a></div>
                            <div class="description left">
                                <div class="title"><h4><a
                                                href="{{ route('seller_lot', ['id' => $lot->id]) }}">{{ $lot->title }}</a>
                                    </h4></div>
                                <div class="lot-number-block">
                                    <span class="text-small">LastBid Lot No</span>
                                    <span class="text-normal lot-number">{{ $lot->id }}</span>
                                </div>
                                <div class="price-block">
                                    <span class="text-bigger price"> {!! format_price($lot->range_from, $lot->range_to, $lot->currency) !!}</span>
                                    <span class="text-small">Suggested bid range</span>
                                </div>
                                @if ($lot->is_active)
                                    <div class="info-bottom time-wrap-line">
                                        <div class="time-line">
                                            <div class="time-line-left"
                                                 style="width:{{ percent_time_left($lot->start_date, $lot->end_date) }}%"></div>
                                        </div>
                                        {{ format_time_left($lot->end_date) }} left
                                    </div>
                                @endif
                            </div>
                            <div class="description right">
                                <ul>
                                    <li>
                                        <span class="first-col">Manager:</span>
                                        <span class="second-col">{{ Auth::user()->full_name }}</span>
                                    </li>
                                    <li>
                                        <span class="first-col">Start:</span>
                                        <span class="second-col">{{$lot->start_date->setTimezone(session('time_zone'))->format('j F Y, h:i A')}}</span>
                                    </li>
                                    <li>
                                        <span class="first-col">End:</span>
                                        <span class="second-col">{{$lot->end_date->setTimezone(session('time_zone'))->format('j F Y, h:i A')}}</span>
                                    </li>
                                    <li>
                                        <span class="first-col">Bids:</span>
                                        <span class="second-col">{{ $lot->bids_count }} <a
                                                    href="{{ route('seller_lot', ['id' => $lot->id]) }}" class="view">view</a></span>
                                    </li>
                                    <li>
                                        <span class="first-col">Dialogs:</span>
                                        <span class="second-col">{{ $lot->messages_count }} <a href="{{route
                                        ('messages_received')}}" class="view">view</a></span>
                                    </li>
                                    <li>
                                        <span class="first-col">Views:</span>
                                        <span class="second-col">{{ $lot->views_count }}</span>
                                    </li>
                                </ul>
                                @if ($lot->is_active)
                                    <div style="padding-top: 30px" class="lots-action-btn">

                                        <span class="first-col">
                                            <a href="{{ route('seller_edit_lots', ['id' => $lot->id]) }}"
                                                    class="btn2">Edit lot</a></span>

                                        <span class="second-col">
                                            <a href="javascript:void 0;" data-toggle="modal"
                                               data-target="#conf_modal_{{$lot->id}}"
                                               class="btn2">Close lot</a></span>
                                    </div>
                                @endif
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    @php request()->has('sort_latest') ? $append = ['sort_latest'=>request('sort_latest')]:$append = []; @endphp
    @include('inc.paginator_with_page_selector',
            ['items'=>$lots, 'append'=>$append])
@endsection
@if (\Route::currentRouteName() == 'seller_active_lots')
@section('custom_modal')
    @include('seller.inc.confirm_close_lot_pop_up',
    ['items'=>$lots,'route'=>'seller_close_lot'])
@endsection
@endif
