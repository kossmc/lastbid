<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Repositories\BidRepository;

class ExpiredBid extends Command
{
    /**
     * The bid repository implementation.
     *
     * @var BidRepository
     */
    private $bid;

    /**
     * Create a new controller instance.
     *
     * @param BidRepository $bid
     * @return void
     */
    public function __construct(BidRepository $bid)
    {
        $this->bid = $bid;
        parent::__construct();
    }

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bid:mark-expired';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Mark bid as expired';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->bid->markExpired();
    }
}
