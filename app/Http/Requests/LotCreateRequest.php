<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LotCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'category' => 'required|min:1|max:255',
            'currency' => 'required|min:1|max:255',
            'auto_accept_price' => 'nullable|min:1|max:255',
            'title' => 'required|min:1|max:255',
            'description' => 'nullable|min:1|max:65534',
            'shipping_description' => 'nullable|min:1|max:65534',
            'auction_house_fee' => 'required|numeric|between:0,99.99|availableFee',
            'min_bid_amount' => 'required|numeric|min:1',
            'start_date' => 'required|date|after_or_equal:today',
            'end_date' => 'required|date|after_or_equal:start_date',
            'range_from' => 'required|numeric',
            'range_to' => 'required|numeric|greater_than_field:range_from',
            'photos.*' => 'mimes:jpeg,gif,png|nullable',
        ];
    }
}
