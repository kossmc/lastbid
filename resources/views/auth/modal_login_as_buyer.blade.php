<div class="modal fade" id="modal-id">
    <div class="modal-content">
        <div class="title">Buyer authentication failed</div>
        <p>Please login as buyer</p>
        <div class="btn-wrap">
            <a class="btn" href="javascript:void 0;" onclick="closeModal()">Ok</a>
        </div>
    </div>
</div>