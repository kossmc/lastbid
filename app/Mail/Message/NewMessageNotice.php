<?php

namespace App\Mail\Message;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NewMessageNotice extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The message instance.
     *
     * @var Message
     */
    public $msg;
    /**
     * The sender instance.
     *
     * @var User
     */
    public $sender;
    /**
     * The recipient instance.
     *
     * @var User
     */
    public $recipient;

    /**
     * The lot instance.
     *
     * @var Lot
     */
    public $lot;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->msg = $data['message'];
        $this->sender = $data['sender'];
        $this->recipient = $data['recipient'];
        $this->lot = $data['lot'];
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('New message at LastBid')->view('emails.new_message_notice');
    }
}
