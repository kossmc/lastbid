<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Services\StripeService;

class BuyerCard extends Model
{
    const TYPE_VISA = 'visa';
    const TYPE_MASTER = 'master';
    const TYPE_AMERICAN_EXPRESS = 'american_express';
    use SoftDeletes;
    protected $table = 'buyer_cards';
    public $timestamps = true;
    protected $guarded = [];
    public $appends = ['is_visa', 'is_master', 'is_american'];

    public function user()
    {
        return $this->belongsTo(User::class, 'id', 'user_id');
    }

    public function getIsVisaAttribute()
    {
        return $this->card_type === self::TYPE_VISA;
    }

    public function getIsMasterAttribute()
    {
        return $this->card_type === self::TYPE_MASTER;
    }

    public function getIsAmericanAttribute()
    {
        return $this->card_type === self::TYPE_AMERICAN_EXPRESS;
    }
    public static function validateCard($data)
    {
        $stripe = new StripeService();
        $stripeCardToken = $stripe->getCardToken($data['cc'], $data['month'], $data['year'], $data['card_cvv']);
        if (isset($stripeCardToken['error'])) {
            return false;
        }
        return true;
    }
    public static function detectCardType($number)
    {
        //visa
        $visa_regex = "/^4[0-9]{0,}$/";
        // MasterCard
        $mastercard_regex = "/^(5[1-5]|222[1-9]|22[3-9]|2[3-6]|27[01]|2720)[0-9]{0,}$/";
        // American Express
        $amex_regex = "/^3[47][0-9]{0,}$/";
        //ordering matter in detection, otherwise can give false results in rare cases
        if (preg_match($amex_regex, $number)) {
            return self::TYPE_AMERICAN_EXPRESS;
        }
        if (preg_match($visa_regex, $number)) {
            return self::TYPE_VISA;
        }
        if (preg_match($mastercard_regex, $number)) {
            return self::TYPE_MASTER;
        }
        return "unknown"; //unknown for this system
    }
}
