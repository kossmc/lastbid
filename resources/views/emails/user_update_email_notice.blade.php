<a href="{{url('/')}}"><img src="{{asset('img/Logo.svg')}}" alt="LastBid"></a>
<p>On LastBid site you ask to change email!</p>
<p>If you really want change email. Click on link bellow.</p>
<p>
    <a href="{{route('account-info-edit-confirm-email', ['token'=>$user->email_token])}}">
        Click to change email</a>
</p>

<p>This is an automated email from our team. </p>
