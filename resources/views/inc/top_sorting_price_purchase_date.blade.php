<form action="" id="top_sorting">
    <div class="filter filter-default">
        <div class="item">
            <div class="name">Sort by</div>
            <div class="select">
                <select name="sort_by" id="sort_by">
                    <option value=0>Choose...</option>
                    <option value="price"
                            {{(isset($sort['sort_by'])&&$sort['sort_by']=='price')?' selected ':''}} >
                        Price
                    </option>
                    <option value="purchase_date"
                            {{(isset($sort['sort_by'])&&$sort['sort_by']=='purchase_date')?' selected ':''}} >
                        Purchase Date
                    </option>
                </select>
            </div>
            <div class="sorting">
                <a id="sort_up" href="javascript:void 0;" class="icon-sorting_up
                                        {{(isset($sort['sort_direction'])&&$sort['sort_direction']=='asc')?
                                        ' icon-sorting--active':''}}"></a>
                <a id="sort_down" href="javascript:void 0;" class="icon-sorting_up down
                                        {{(isset($sort['sort_direction'])&&$sort['sort_direction']=='desc')?
                                        ' icon-sorting--active':''}}"></a>
                @php($sort_direction = isset($sort['sort_direction'])?$sort['sort_direction']:0)
                <input type="hidden" name="sort_direction"
                       id="sort_direction" value="{{$sort_direction}}">
            </div>
        </div>
    </div>
</form>
