<?php

return [
    'admin_lets_talk_notice'             => [
        'subject' => 'You have received a new message from LastBid',
    ],
    'admin_landing_discover_more_notice' => [
        'subject' => [
            'newsletter' => 'NEWSLETTER',
            'discover'   => 'DISCOVER MORE',
        ]
    ],
];
