<?php

namespace App\Models;

use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Specific extends Model
{
    const TYPE_SELECT = 'select';
    const TYPE_MULTI_SELECT = 'multi-select';
    const TYPE_TEXT = 'text';
    const FILTER_CHECKBOX = 'checkbox';
    const FILTER_RADIO = 'radio';
    use CrudTrait;
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'category_id', 'order', 'required', 'type', 'use_in_filter', 'filter_type', 'new_options'
    ];
    protected $appends = ['is_text','is_select','is_multi_select', 'is_filter_radio','is_filter_checkbox'];
    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function specific_options()
    {
        return $this->hasMany(SpecificOption::class);
    }
    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */
    public function getNewOptionsAttribute()
    {
       return $this->specific_options()->get()->toArray();
    }

    public function getIsTextAttribute()
    {
        return $this->type === self::TYPE_TEXT;
    }

    public function getIsSelectAttribute()
    {
        return $this->type === self::TYPE_SELECT;
    }

    public function getIsMultiSelectAttribute()
    {
        return $this->type === self::TYPE_MULTI_SELECT;
    }

    public function getIsFilterRadioAttribute()
    {
        return $this->filter_type === self::FILTER_RADIO;
    }

    public function getIsFilterCheckboxAttribute()
    {
        return $this->filter_type === self::FILTER_CHECKBOX;
    }
    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
    public function setNewOptionsAttribute($value)
    {
        $opts = json_decode($value, true);
        if (is_array($opts) && $this->id) {
            if (isset($opts['data'])) {
                $existing = array_filter($opts['data'], function ($v) {
                    return isset($v['id']) && $v['id']!=='';
                }, ARRAY_FILTER_USE_BOTH);
                if ($existing) {
                    /*update exist options*/
                    foreach ($existing as $item) {
                        $item['name'] = trim($item['name']);
                        if (isset($item['edited']) && $item['edited']) {
                            $option = SpecificOption::findOrFail($item['id']);
                            if ($item['name'] == '') {
                                $option->delete();
                            } else {
                                $option->name = $item['name'];
                                $option->save();
                            }
                        }
                    }
                }
                /*try find new elements and save it*/
                if ($new = array_diff_key($opts['data'], $existing)) {
                    $new = array_filter($new, function ($v) {
                        return isset($v['name']) && trim($v['name'])!=='';
                    }, ARRAY_FILTER_USE_BOTH);
                    $newParams = $this->specific_options()->createMany($new);
                }
            }
            if (isset($opts['deleted'])) {
                if ($deleted_ids = array_column($opts['deleted'], 'id')) {
                    /*remove deleted*/
                    DB::table('specific_options')
                        ->whereIn('id', $deleted_ids)
                        ->delete();
                }
            }
        }
    }
}
