<?php

namespace App\Jobs;

use App\Models\ParserBatch;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class CrawlerJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 1;

    /**
     * The number of seconds the job can run before timing out.
     *
     * @var int
     */
    public $timeout = 2400;

    /**
     * Active parser class
     *
     * @var string
     */
    protected $crawler;

    /**
     * Active batch data
     *
     * @var ParserBatch
     */
    protected $batch;

    /**
     * Parser config data
     *
     * @var array
     */
    protected $config;

    /**
     * Create a new job instance.
     *
     * @param $crawler
     * @param $batch
     */
    public function __construct($crawler, $batch)
    {
        $this->crawler = $crawler;
        $this->batch = $batch;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->crawler = new $this->crawler($this->batch);
        if ($this->crawler->parse() && $this->crawler->isFile) {

            \Mail::send('emails.admin_job_parser', [
                'type' => $this->batch->type,
                'input' => $this->batch->input,
                'isFile' => $this->crawler->isFile,
                'fileName' => $this->crawler->filename
            ], function ($message) {
                $message->to(env('AUCTIONS_EMAIL', 'auctions@lastbid.com'));
                $message->subject('Success - your file has been created');
                $message->attachData(file_get_contents($this->crawler->tempFolder . $this->crawler->filename),
                    $this->crawler->filename, ['mime' => 'text/csv']);
            });

            $this->batch->finish($this->crawler->filename);

        } else {

            \Mail::send('emails.admin_job_parser', [
                'type' => $this->batch->type,
                'input' => $this->batch->input,
                'isFile' => $this->crawler->isFile
            ], function ($message) {
                $message->to(env('AUCTIONS_EMAIL', 'auctions@lastbid.com'));
                $message->subject('Alert - your file hasn\'t been created');
            });

            if($this->crawler->parse() && !$this->crawler->isFile){
                $this->batch->finish(null);
            }else{
                $this->batch->fail();
            }
        }
    }
    /**
     * The job failed to process.
     *
     * @param  $exception
     * @return void
     */
    public function failed($exception)
    {
        $this->batch->fail();
    }
}
