<?php
namespace App\Repositories;

use App\Events\Lot\LotExpired;
use App\Models\Category;
use App\Models\Lot;
use Bosnadev\Repositories\Eloquent\Repository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Event;

class LotRepository extends Repository
{
    public function model()
    {
        return Lot::class;
    }

    public function latest($limit = 3)
    {
        return $this->model->latest()->take($limit)->get();
    }

    public function paginateByCategory($ids, $limit = 10)
    {
        return $this->model->whereIn('category_id', $ids)->active()->latest()->paginate($limit);
    }

    public function filterAndPaginateByCategory($ids, $selectedIds, $priceRange, $sort, $status, $q)
    {
        if ($q) {
            $query = $this->model->search($q)->whereIn('category_id', $ids);
        } else {
            $query = $this->model->whereIn('category_id', $ids);
        }
        $query = $this->applyPriceRange($query, $priceRange);
        if (count($selectedIds)) {
            $selectedIds->each(function ($i) use ($query) {
                $query = $query->whereHas('lot_specifics_options', function ($q) use ($i) {
                    $q->where('specific_option_id', $i);
                });
            });

        }
        $query = $this->applyStatus($query, $status);
        $query = $this->applySort($query, $sort);
        $query = $this->applyOriginal($query);

        return $this->_paginate($query);
    }

    public function paginateSearch($q, $priceRange, $sort, $status)
    {
        $query = $this->model->search($q);

        $query = $this->applyPriceRange($query, $priceRange);
        $query = $this->applyStatus($query, $status);
        $query = $this->applySort($query, $sort);
        $query = $this->applyOriginal($query);

        return $this->_paginate($query);
    }

    protected function applyPriceRange($query, $priceRange)
    {
        if (isset($priceRange['min']) && isset($priceRange['max'])) {
            $query = $query->where(function ($q) use ($priceRange) {
                $q->whereBetween('range_from', [$priceRange['min'], $priceRange['max']])
                    ->orWhereBetween('range_to', [$priceRange['min'], $priceRange['max']]);
            });
        }
        return $query;
    }

    protected function applySort($query, $sort)
    {
        if (count($sort)) {
            switch ($sort['sort_by']) {
                case 'price':
                    $sort['sort_direction'] == 'asc' ?
                        $query = $query->orderBy('range_from', 'ASC') :
                        $query = $query->orderBy('range_to', 'DESC');
                    break;
                case 'latest':
                    $sort['sort_direction'] == 'asc' ?
                        $query = $query->oldest() :
                        $query = $query->latest();
            }
        } else {
            $query = $query->latest();
        }
        return $query;
    }

    protected function applyStatus($query, $status)
    {
        if (!empty($status)) {
            switch ($status) {
                case 'active':
                    $query = $query->active();
                    break;
                case 'sold':
                    $query = $query->soldOrSoldOutside();
                    break;
                case 'closed':
                    $query = $query->closedOrExpired();
                    break;
            }
        }
        return $query;
    }

    protected function applyOriginal($query)
    {
        if(request()->has('system')){
            $query = $query->system();
        } else {
            $query = $query->original();
        }
        return $query;
    }

    public function getPriceRangeByCategory($ids)
    {
        $query = $this->model->whereIn('category_id', $ids);
        $min = $query->min('range_from');
        $max = $query->max('range_to');
        return [
            'min' => floor($min),
            'max' => ceil($max)
        ];
    }

    public function getPriceRangeByQuery($query)
    {
        $query = $this->model->search($query);
        $min = $query->min('range_from');
        $max = $query->max('range_to');
        return [
            'min' => floor($min),
            'max' => ceil($max)
        ];
    }

    public function getCategoriesWithCountByQuery($query, $priceRange, $status)
    {
        return Category::whereHas('lots', function ($q) use ($query, $priceRange, $status) {
            $this->applyPriceRange($this->applyStatus($q->search($query), $status), $priceRange);
        })->withCount(['lots' => function ($q) use ($query, $priceRange, $status) {
            $this->applyPriceRange($this->applyStatus($q->search($query), $status), $priceRange);
        }])->get();
    }

    public function related($lot, $limit = 4)
    {
        return $this->model
            ->where('category_id', $lot->category_id)
            ->where('id', '!=', $lot->id)
            ->active()
            ->latest()
            ->take($limit)
            ->get();
    }


    public function findById($id)
    {
        return $this->model->find($id);
    }

    // @todo: check if lot biddable
    public function isBiddable($id)
    {
        return $this->model->active()->findOrFail($id);
    }

    // @todo: check if lot discussable
    public function isDiscussable($id)
    {
        return $this->model->activeOrSold()->findOrFail($id);
    }

    public function random($limit = 4)
    {
        return $this->model->active()->inRandomOrder()->take($limit)->get();
    }

    public function popular($limit = 4)
    {
        return $this->model->active()->popular()->inRandomOrder()->take($limit)->get();
    }


    public function isMine($id)
    {
        return $this->model->where('user_id', Auth::id())->findOrFail($id);
    }

    public function hasMyBid($id)
    {
        return $this->model
            ->whereHas('bids', function ($query) {
                $query->where('user_id', Auth::id());
            })
            ->findOrFail($id);
    }

    public function paginateActive()
    {
        $query = $this->model->where('user_id', Auth::id())
            ->withCount(['messages' => function ($q) {
                $q->withOutDeleted();
            }])->active();
        $query = $this->_setSorting($query);
        return $this->_paginate($query);
    }

    public function paginateClosed()
    {
        $query = $this->model->where('user_id', Auth::id())->closedOrExpired()->latest('updated_at');
        return $this->_paginate($query);
    }


    public function paginateSold($sort = null)
    {
        $query = $this->model->where('lots.user_id', Auth::id())
            ->withCount(['messages' => function ($q) {
                $q->withOutDeleted();
            }])->with(['buyer'])->sold();
        $query = $this->_setSortingSoldDateOrPrice($query, $sort);
        return $this->_paginate($query);
    }

    public function buyerSoldLots($sort = null)
    {
        $query = $this->model->where('buyer_id', Auth::id())
            ->with(['seller'])->sold();
        $query = $this->_setSortingSoldDateOrPrice($query, $sort);
        return $this->_paginate($query);
    }

    // find all user lots with at least one bid and total and new bids counts
    public function paginateBids()
    {
        $userId = Auth::id();
        $query = $this->model
            ->where('user_id', $userId)
            ->where('bids_count', '>', 0)
            ->withCount('newBids');
        if (request()->has('sort_latest')) {
            $query = $this->_setSorting($query);
        } else {
            $query = $query->orderBy('new_bids_count', 'desc');
        }
        return $this->_paginate($query);
    }

    public function markExpired()
    {
        $expiredLots = $this->model->getExpired();
        foreach ($expiredLots as $lot) {
            if ($lot->expire()) {
                Event::fire(new LotExpired($lot));
            }
        }
    }

    public function getActualBid($lot)
    {
        $userId = Auth::id();
        if (!Auth::guest()) {
            if (Auth::user()->hasRole('buyer')) {
                return $lot->bids()
                    ->withCount(['attempts' => function ($query) use ($userId) {
                        $query->where('user_id', $userId);
                    }])
                    ->acceptedOrPending()
                    ->where('user_id', $userId)
                    ->first();
            } elseif (Auth::user()->hasRole('seller')) {
                return $lot->bids()
                    ->accepted()
                    ->first();
            }
        }
        return null;
    }

    public function canPlaceBid($lot)
    {
        if (!Auth::guest()) {
            $userId = Auth::id();
            if (!$lot->hasEnoughtAttempts($userId)) {
                return false;
            }
        }
        return true;
    }

    /**
     * @param array $relations
     * @return $this
     */
    public function withCount(array $relations)
    {
        $this->model = $this->model->withCount($relations);
        return $this;
    }

    /**
     * Sort messages collection
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    private function _setSorting($query)
    {
        if (request('sort_latest', 'DESC') == 'ASC') {
            return $query->latest('end_date');
        }
        return $query->oldest('end_date');
    }

    /**
     * Sort messages collection
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    private function _setSortingSoldDateOrPrice($query, $sort)
    {
        if (count($sort)) {
            switch ($sort['sort_by']) {
                case 'price':
                    $sort['sort_direction'] == 'asc' ?
                        $query = $query->orderBy('sold_amount', 'ASC') :
                        $query = $query->orderBy('sold_amount', 'DESC');
                    break;
                case 'purchase_date':
                    $sort['sort_direction'] == 'asc' ?
                        $query = $query->oldest('sold_date') :
                        $query = $query->latest('sold_date');
            }
        } else {
            $query = $query->latest('updated_at');
        }
        return $query;
    }

    /**
     * Paginate lot collection
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    private function _paginate($query)
    {
        return $query->paginate(request('per_page', env('PAGINATE_LOTS')));
    }

    public function getSelectedSpecifics($lot)
    {
        $lot->load('lot_specifics_options', 'specifics', 'specifics.specific_options');
        return $lot->getSelectedSpecifics();
    }

    public function getSellerMonthStatistic($id, $date)
    {
        return $this->model->where('user_id', $id)
            ->whereMonth('sold_date', '=', $date->month)
            ->whereYear('sold_date', '=', $date->year)
            ->sold()
            ->latest('sold_date')
            ->with(['buyer'])
            ->get();
    }

    public function getCreatedId()
    {
        return $this->model->id;
    }
}
