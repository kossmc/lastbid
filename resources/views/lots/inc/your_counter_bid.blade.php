<div class="block-card-lot block-card-lot--counter-bid">
    <p class="title">Counter bid</p>

    <div class="time-wrapper">
        <div class="time-wrap-line">
            <div class="time-line">
                <div class="time-line-left" style="width:{{ percent_time_left($bid->attempt->start_date, $bid->attempt->end_date) }}%"></div>
            </div>
            <span class="time-text">Bid expires in:<span class="expires-date">{{ format_time_left($bid->attempt->end_date) }}</span></span>
        </div>
    </div>

    <p class="bid-info">Please respond to this counter bid from <span>{{ $lot->seller->short_name }}</span></p>

    <div class="info">

        <div class="bid-price">
            {!! format_price($bid->attempt->amount, null, $lot->currency) !!}
            <p>Does not include auction house premium and shipping/delivery fees</p>
        </div>

        <div class="info-bid">       
            <p class="title">Total bid price consists of:</p>
            <div>
                <p>Bid price:</p>
                <div class="bid-price"> {!! format_price($bid->attempt->amount, null, $lot->currency) !!} </div>
            </div>
            <div>   
                <p>Auction house premium:</p>
                <div class="bid-price">{!! format_price($lot->bidFee($bid->attempt->amount), null, $lot->currency) !!}</div>
            </div>
            <div>   
                <p>Total bid price:</p>
                <div class="bid-price">{!! format_price($lot->bidWithFee($bid->attempt->amount), null, $lot->currency) !!}</div>
            </div>
        </div>

    </div>

    <a class="btn-lot btn-respond" href="#">respond</a>
    @if ($lot->is_active)
        <div class="time-wrapper">
            <div class="time-wrap-line">
                <div class="time-line">
                    <div class="time-line-left" style="width:{{ percent_time_left($lot->start_date, $lot->end_date) }}%"></div>
                </div>
                <span class="time-text">Lot expire in: <span class="expires-date">{{ format_time_left($lot->end_date) }}</span></span>
            </div>
        </div>
    @endif
</div>
