<?php

namespace App\Http\Requests;

use Illuminate\Http\JsonResponse;

class LotImportRequest extends \Backpack\CRUD\app\Http\Requests\CrudRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // only allow updates if the user is logged in
        return \Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'csv_file' => 'required|mimes:csv,txt',
            'zip_file' => 'required_if:upload_type,forum|required_if:upload_type,base|mimes:zip',
            'user_id'  => 'required_if:upload_type,forum|required_if:upload_type,base|integer',
            'min_bid_amount' => 'less_than_field:auto_accept_price|required|numeric|min:0|max:99',
            'partner_id' => 'required',
            'category_id' => 'required',
            'currency' => 'required',
            'buyer_premium' => 'required|numeric|min:0|max:100',
            'duration' => 'required|numeric|min:1',
            'auto_accept_price' => 'required|numeric|min:1'
        ];
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'upload_type' => request('upload_type', null)

        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [

        ];
    }

    public function response(array $errors){
        if ($this->expectsJson()) {
            return new JsonResponse($errors, 422);
        }
        $errors['upload_type'] = [$this->request->get('upload_type', 'simple')];
        return $this->redirector->to($this->getRedirectUrl())
            ->withInput($this->except($this->dontFlash))
            ->withErrors($errors, $this->errorBag);
    }
}
