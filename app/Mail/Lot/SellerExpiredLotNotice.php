<?php
namespace App\Mail\Lot;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SellerExpiredLotNotice extends Mailable
{
    use Queueable, SerializesModels;

    public $lot;

    /**
     * The seller instance.
     *
     * @var User
     */
    public $seller;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->lot = $data['lot'];
        $this->seller = $data['seller'];
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Your lot was expired')->view('emails.seller_expired_lot_notice');
    }
}
