<?php

namespace App\Listeners\Lot;

use App\Models\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class SendGridAbstractLotListener implements ShouldQueue
{
    protected $lot;
    protected $seller;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function setData($event)
    {
        $this->lot = $event->lot;
        $this->seller = $this->lot->seller;
    }

    protected function getData($id)
    {
        $data = [
            "sub"=>[
                "-sellerName-"=>[$this->seller->full_name]
                ,"-lotName-"=>[$this->lot->title]
                ,"-lotLink-"=>[$this->lot->url]
                ,"-siteLink-" => [route('home')]
                ,"-currentYear-"=>[date('Y')]
                ,"-emailPreferencesLink-" => [route('subscriptions')]
            ],
            "filters"=>[
                "templates"=>[
                    "settings"=>[
                        "enable"=>1,
                        "template_id"=>$id
                    ]
                ],
                "subscriptiontrack"=>[
                    "settings"=>[
                        "enable"=>1,
                        "replace"=>'[unsubscribe]'
                    ]
                ]
            ]
        ];
        return asJSON($data);
    }

    protected function sendMail(User $to, $data, $subject)
    {
        Mail::send('emails.empty',[], function ($message) use($to, $subject, $data) {
            $message->getSwiftMessage()->getHeaders()->addTextHeader('X-SMTPAPI', $data);
            $message->to($to->email, $to->full_name)->subject($subject);
        });
    }
}
