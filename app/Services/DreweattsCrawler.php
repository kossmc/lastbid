<?php

namespace App\Services;

use Carbon\Carbon;
use Symfony\Component\DomCrawler\Crawler;

class DreweattsCrawler extends BaseCrawler
{
    public $baseUrl = 'https://www.dreweatts.com/auctions/sale-details/';

    public $siteUrl = 'https://www.dreweatts.com';

    public $category;

    public $startDate;

    public $endDate;

    public $saleId;

    public $total;

    public $page;

    public function __construct($batch)
    {
        $this->filename = 'Dreweatts' .
            $batch->input . '_' . date('Y-m-d_H-i-s') . '.csv';
        $this->saleId = '?saleId='.$batch->input;
        parent::__construct($batch);
        $this->prepareCrawling();
    }

    public function prepareCrawling()
    {
        $this->getPage($this->getUrl());
        if ($this->page) {
            $this->category =  $this->page->filter('body .sale-main .sale-title')->first()->text();
            $paginatorLinks = $this->page->filter('body .pagination .pager a');
            $this->total = (int)$paginatorLinks->eq($paginatorLinks->count()-2)->text();
            $auctionDate = $this->page->filter('body .sale-date')->text();
            if ($auctionDate) {
                $auctionDate =  Carbon::parse(explode('|', $auctionDate)[0]);
                if ($auctionDate->getLastErrors()) {
                    $auctionDate =  Carbon::now();
                }
                $this->startDate = $auctionDate->format('Y-m-d H:i:s');
                $this->endDate = $auctionDate->addMonth()->format('Y-m-d H:i:s');
            }
        }
    }

    public function getPage($url)
    {
        $this->page = null;
        $response = $this->client->request('GET', $url);
        if ($response->getStatusCode() == 200) {
            $this->page = new Crawler($response->getBody()->getContents(), $this->siteUrl);
        }
    }

    public function getUnsoldLots()
    {
        $links = [];
        $this->page->filter('body #maincontent div.w-content.w--clickable')
            ->each(function (Crawler $node) use (&$links){
                $soldP = $node->filter('p.w-support-copy--secondary strong');
                if (!$soldP->count()) {
                    $links[] = $node->filter('a.w-image')->link()->getUri();
                }
            });
        return $links;
    }

    public function getLotInfo($uri)
    {
        $this->getPage($uri);
        if ($this->page) {
            try {
                $title = $this->page->filter('body h3.w-subheadline.product-subheadline')->text();
                $description = $this->page->filter('body .product-description-wrapper p')->last()->text();
                $estimate = $this->page->filter('body .product-description-wrapper .product-estimate')->text();
                list($from, $to) = explode('-', trim($estimate));
                $from = preg_replace('/[^0-9]/', '', $from);
                $to = preg_replace('/[^0-9]/', '', $to);
                $lotImages = [];
                $this->page->filter('body .product-gallery-wrapper .product-gallery-main img')
                    ->each(function (Crawler $node) use (&$lotImages){
                        $uri = $node->image()->getUri();
                        $lotImages[] = $this->loadImage($uri);
                    });
                return [
                    'Title' => $title,
                    'Description' => $description,
                    'Category' => $this->category,
                    'Sub Category' => '',
                    'Partner' => $this->partner_id,
                    'Shipping description' => '',
                    'Auction House Fee' => 20,
                    'Minimum bid amount' => isset($from) ? $from * 0.5 : 0,
                    'Price range from' => isset($from) ? $from : 0,
                    'Price range to' => isset($to) ? $to : 0,
                    'Price to auto-accept bid' => isset($from) ? $from * 0.7 : 0,
                    'Start date' => $this->startDate,
                    'End date' => $this->endDate,
                    'Seller' => $this->seller_id,
                    'Currency' => 'gbr',
                    'Lot photos' => implode(',', $lotImages)
                ];
            } catch (\Exception $e) {
                return false;
            }
        }
        return false;
    }

    public function startCrawling()
    {
        $lots = [];
        if ($this->category && $this->total) {
            for ($i = 1; $i <= $this->total; $i++) {
                if ($i > 1) $this->getPage($this->getUrl($i));
                if ($this->page) {
                    $unsoldLinks = $this->getUnsoldLots();
                    if (count($unsoldLinks)) {
                        foreach ($unsoldLinks as $link) {
                            $lot = $this->getLotInfo($link);
                            if ($lot) {
                                $lots[] = $lot;
                            }
                        }
                    }
                }
            }
        }
        if (!empty($lots)) {
            $this->headers = array_keys($lots[0]);
        }
        dump($lots);
        return $lots;
    }

    public function getUrl($page = null) {
        return $this->baseUrl.($page ? "page/{$page}/" : '').$this->saleId;
    }
}
