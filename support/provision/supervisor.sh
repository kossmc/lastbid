#!/usr/bin/env bash
echo "Install supervisor..."
sudo apt-get -y install supervisor > /dev/null 2>&1
service supervisor status
sudo groupadd supervisor
sudo usermod -a -G supervisor ubuntu
sudo cp /lastbid/support/nginx/supervisor/laravel-worker.conf /etc/supervisor/conf.d/laravel-worker.conf > /dev/null
supervisorctl reread
supervisorctl update
supervisorctl status
