<?php

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [];
        $categoryId = $title = $depth = null;
        $lft = 1;
        $filename = database_path('seeds/categories.csv');
        if (($handle = fopen($filename, 'r')) !== false) {
            while (($row = fgetcsv($handle, 1000, ';')) !== false) {
                $data[] = $row;
            }
            fclose($handle);
        }

        foreach ($data as $row) {
            if (!empty($row[0])) {
                $title = $row[0];
                $parentId = null;
                $depth = 1;
            }
            else {
                $title = $row[1];
                $parentId = $categoryId;
                $depth = 2;
            }
            $category = Category::create([
                'parent_id' => $parentId,
                'lft' => $lft,
                'title' => $title,
                'depth' => $depth,
                'slug' => \Cviebrock\EloquentSluggable\Services\SlugService::createSlug(Category::class, 'slug', $title),
                'icon' => ''
            ]);
            if ($category->save()) {
                if ($depth == 1) {
                    $categoryId = $category->id;
                }
                $lft ++;
            }
        }
    }
}
