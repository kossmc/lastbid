<?php
namespace App\Repositories;

use Bosnadev\Repositories\Eloquent\Repository;
use Illuminate\Support\Facades\Auth;
use App\Models\Lot;

class LotStateRepository extends Repository
{

    protected $state = true;

    protected $lot;

    protected $bid = null;

    protected function setState($state)
    {
        $this->state = $this->state && $state;
    }

    protected function continueChain()
    {
        return $this->state == true;
    }

    public function model()
    {
        return Lot::class;
    }

    public function setLot(Lot $lot)
    {
        $this->lot = $lot;
    }

    public function setBid($bid)
    {
        $this->bid = $bid;
    }

    public function getState()
    {
        $state = $this->state;
        $this->state = true;
        return $state;
    }

    public function lotIsActive()
    {
        if ($this->continueChain()) {
            $this->setState($this->lot->is_active);
        }
        return $this;
    }

    public function lotIsExpired()
    {
        if ($this->continueChain()) {
            $this->setState($this->lot->is_expired);
        }
        return $this;
    }

    public function lotIsSoldOuside()
    {
        if ($this->continueChain()) {
            $this->setState($this->lot->is_sold_outside);
        }
        return $this;
    }

    public function lotIsSold()
    {
        if ($this->continueChain()) {
            $this->setState($this->lot->is_sold);
        }
        return $this;
    }
   
    public function lotIsClosed()
    {
        if ($this->continueChain()) {
            $this->setState($this->lot->is_closed);
        }
        return $this;
    }
   

    public function isSeller()
    {
        if ($this->continueChain()) {
            $this->setState(!Auth::guest() && Auth::user()->hasRole('seller'));
        }
        return $this;
    }

    public function isBuyer()
    {
        if ($this->continueChain()) {
            $this->setState(!Auth::guest() && Auth::user()->hasRole('buyer'));
        }
        return $this;
    }

    public function isGuest()
    {
        if ($this->continueChain()) {
            $this->setState(Auth::guest());
        }
        return $this;
    }

    public function isLotOwner()
    {
        if ($this->continueChain()) {
            $this->setState($this->lot->user_id == Auth::id());
        }
        return $this;
    }

    public function isNotLotOwner()
    {
        if ($this->continueChain()) {
            $this->setState($this->lot->user_id != Auth::id());
        }
        return $this;
    }

    public function lotHasBids()
    {
        if ($this->continueChain()) {
            $this->setState($this->lot->bids_count > 0);
        }
        return $this;
    }

    public function lotHasNoBids()
    {
        if ($this->continueChain()) {
            $this->setState($this->lot->bids_count == 0);
        }
        return $this;
    }

    public function isNotWinner()
    {
        if ($this->continueChain()) {
            $this->setState(is_null($this->bid));
        }
        return $this;
    }

    public function isWinner()
    {
        if ($this->continueChain()) {
            $this->setState(!is_null($this->bid) && $this->bid->is_accepted);
        }
        return $this;
    }

    public function isYourBid()
    {
        if ($this->continueChain()) {
            $this->setState(!is_null($this->bid) && $this->bid->by_buyer && $this->bid->is_pending);
        }
        return $this;
    }

    public function isCounterBid()
    {
        if ($this->continueChain()) {
            $this->setState(!is_null($this->bid) && $this->bid->by_seller && $this->bid->is_pending);
        }
        return $this;
    }

}