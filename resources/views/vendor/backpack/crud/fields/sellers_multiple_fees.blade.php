
<div class="form-group col-md-12">
    <label for="auction_house_fee">Auction House Fee</label>
</div>

@if(isset($field['value']) && $field['value']->isNotEmpty())
    @foreach($field['value'] as $key => $fee)
        <div class="form-group col-md-12 auction-house-fee">
            <div class="input-group">
                <div class="input-group-addon">%</div>
                <input type="number" data-index="{{ $key }}" name="fees[{{ $key }}]" value="{{ $fee->fee }}" step="any" min="0" class="form-control">

                <span class="input-group-btn" @if(!$key) style="display:none"@endif>
                    <button type="button" class="btn btn-danger btn-flat delete-fee">
                        <i class="fa fa-fw fa-minus-square"></i>
                    </button>
                </span>
            </div>
        </div>
    @endforeach
@else
    <div class="form-group col-md-12 auction-house-fee">
        <div class="input-group"  id="auction_house_fee_0">
            <div class="input-group-addon">%</div>
            <input type="number" name="fees[0]" data-index="0" value="20" step="any" min="0" class="form-control">

            <span class="input-group-btn" style="display:none">
                    <button type="button" class="btn btn-danger btn-flat delete-fee">
                        <i class="fa fa-fw fa-minus-square"></i>
                    </button>
            </span>
        </div>
    </div>
@endif

<div class="form-group col-md-12" id="add_fee_btn">
    <div class="input-group">
        <button type="button" class="btn btn-light add-fee">Add More Fee</button>
    </div>
</div>

@if ($crud->checkIfFieldIsFirstOfItsType($field, $fields))
    @push('crud_fields_styles')
    @endpush

    @push('crud_fields_scripts')
        <script>
            $(document).ready(function(){
                initDelBtns();

                function initDelBtns(){
                    $('.delete-fee').click(function(e){
                        $(e.target).closest('.auction-house-fee').remove();
                    });
                }

                $('.add-fee').click(function(e){
                    let lastFee = $('.auction-house-fee').last();
                    index = $(lastFee).find('.form-control').last().data('index') + 1;

                    newFee = $(lastFee).clone();
                    $(newFee).find('.form-control').first().val('');
                    $(newFee).find('.form-control').first().attr('name', 'fees['+ index +']');
                    $(newFee).find('.form-control').first().data('index', index);
                    $(newFee).find('.input-group-btn').first().attr('style', '');

                    $('#add_fee_btn').before(newFee);

                    initDelBtns();
                });
            });
        </script>
    @endpush
@endif