@extends('layouts.inner')
@section('title', 'List of bids')
@section('inner_content')
    <div class="page-profile-content-header">
        <h3>Active bids</h3>
        <div class="filter-wrapper">
            <div class="filter filter-default">
                <div class="item">
                    <div class="name">Sort by</div>
                    <div class="select"><select>
                            <option>Latest</option>
                        </select></div>
                    <div class="sorting">
                        <a href="javascript:void 0;" class="icon-sorting_up
                                @if(request('sort_latest', '') == 'ASC') icon-sorting--active @endif"></a>
                        <a href="javascript:void 0;" class="icon-sorting_up down
                                @if(request('sort_latest', '') == 'DESC') icon-sorting--active @endif"></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="lots-list bids-list bids-list--seller">
        @each('seller.inc.lot', $lots, 'lot', 'raw|<h1 class="notification-no-content">You have no active bids</h1>')
    </div>
    @php request()->has('sort_latest') ? $append = ['sort_latest'=>request('sort_latest')]:$append = []; @endphp
    @include('inc.paginator_with_page_selector',
            ['items'=>$lots, 'append'=>$append])
@stop
