<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.css" />
<script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.js"></script>
<script>
    window.addEventListener("load", function(){
        window.cookieconsent.initialise({
            "palette": {
                "popup": {
                    "background": "#ffffff",
                    "text": "#333333"
                },
                "button": {
                    "background": "#caac8a",
                    "text": "#ffffff"
                }
            },
            "content": {
                "message": "This site uses cookies to improve your browsing experience, perform analytics and research, and conduct advertising. To change your preferences, see our Cookies & Tracking Notice. Otherwise, closing the banner or clicking Accept all Cookies indicates you agree to the use of cookies on your device.",
                "dismiss": "Accept all Cookies",
                "link": "Cookies &amp; Privacy Notice",
                "href": "/policy",
                "target": "_blank"
            }
        })});

        $(document).on('click', '.cc-link', function(e) {
            e.preventDefault();

            $('.privacy').click();
        });

</script>

<style>
    .cc-theme-block{
        height: 120px;
    }
    .cc-banner{
        border-top: 1px solid #E4E4E4;
    }
    .cc-compliance .cc-btn{
        display: inline-block;
        height: 44px;
        padding: 0 40px;
        border-radius: 22px;
        line-height: 44px;
        font-weight: bold;
        border: 0;
        font-size: 12px;
        color: #fff;
        text-transform: uppercase;
        background-color: #CAAC8A;
    }
    @media screen and (max-width: 480px) {
        .cc-compliance .cc-btn {
            display: block;
        }
    }
</style>