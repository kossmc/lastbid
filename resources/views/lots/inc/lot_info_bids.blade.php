<div class="block-card-lot block-card-lot--lot-info">
    <p class="title">Lot info</p>
    <div class="info">
        <div class="bid-counter">
            <span>new bids</span><span>{{ $lot->new_bids_count }}</span>
        </div>
        <div class="bid-price">
            {!! format_price($lot->range_from, $lot->range_to,$lot->currency) !!}
            <p>Suggested bid range</p>
        </div>
    </div>
    <div class="fee-info">
        <p>Does not include auction house premium and shipping/delivery fees</p>
    </div>

    <a class="btn-lot btn-manage-bids" href="{{ route('seller_lot', ['id' => $lot->id]) }}">manage bids</a>
    @if ($lot->is_active)
        <div class="time-wrapper">
            <div class="time-wrap-line">
                <div class="time-line">
                    <div class="time-line-left" style="width:{{ percent_time_left($lot->start_date, $lot->end_date) }}%"></div>
                </div>
                <span class="time-text">Lot expire in: <span class="expires-date">{{ format_time_left($lot->end_date) }}</span></span>
            </div>
        </div>
    @endif
</div>
