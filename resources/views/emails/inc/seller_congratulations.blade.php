<p>Congratultaions! Your counterbid on lot <a href="{{ $lot->url }}">{{ $lot->title }}</a> was accepted by buyer for {!! format_price($bid->attempt->amount, null, $lot->currency) !!}.</p>
<p>Our managers will contact you to discuss details!</p>
