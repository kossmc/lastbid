<div id="slider-product-nav" class="slider-nav lot-imgs-slider">
    @foreach ($items as $key => $img)

        <div class="slider-nav-item slide-{{ $key }} {{ $itemClass or '' }}" data-slideId="{{ $key }}">
            <img src="{{asset('uploads/'.$img )}}">
        </div>

    @endforeach
</div>