<div class="m-t-10 m-b-10 p-l-10 p-r-10 p-t-10 p-b-10">
    <div class="row">
        <div class="col-md-12">
            <!-- Default box -->
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Bids list for this lot:</h3>
                </div>
                <div class="box-body">
                    @if ($lot->bids)
                        <table class="table table-striped table-hover table-bordered">
                            <thead>
                            <tr>
                                <th>Bid date/time</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>Initiator</th>
                                <th>Status</th>
                                <th>Amount</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($lot->bids as $bid)
                                <tr>
                                    <td>{{ $bid->attempt->created_at }}</td>
                                    <td>{{ $bid->buyer->full_name }}</td>
                                    <td>{{ $bid->buyer->email }}</td>
                                    <td>{{ $bid->buyer->phone }}</td>
                                    <td>{{ $bid->last_bid_by }}</td>
                                    <td>{{ $bid->status }}</td>
                                    <td>{!! format_price($bid->attempt->amount) !!}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @else
                        Bids for this lot not found!
                    @endif
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div>
</div>
<div class="clearfix"></div>
