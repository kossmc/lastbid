<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use App\Models\Lot;

class RecentlyLot extends Model
{
    protected $table = 'lot_user';
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];
    public $timestamps = false;

    public static function recentlyLotsIds()
    {
        return Auth::guest() ? collect(request()->cookie('lots', [])) :
        Auth::user()->recentlyLots()->get()->pluck('id');
    }

    public static function recentlyLotsList()
    {
        if (Auth::guest()){
            $cookieLots = collect(request()->cookie('lots', []))->reverse()->take(4);
            if ($cookieLots->isNotEmpty()) {
                $lots = Lot::whereIn('id', $cookieLots)->get();
                return $lots->sort(function ($a, $b) use ($cookieLots) {
                    return ($cookieLots->search($a->id) > $cookieLots->search($b->id)) ? - 1 : 1;
                });
            }
        } else {
            $lots = Auth::user()->recentlyLots;
            if ($lots) {
                return $lots->reverse()->take(4);
            }
        }
    }
}
