<div class="modal fade" id="counter_bid_modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="popup place-bid-popup place-bid-wrapper">
        <div class="title">Counter bid</div>
        <div class="bid-amount-error" style="display: block">
            Does not include auction house premium and shipping/delivery fees
        </div>
        <form method="post" id="form-counter-bid" onsubmit="counter();return false;">
            {{ csrf_field() }}
            <div class="form-group">
                <input type="text" class="bid-amount-input" name="amount" id="amount" placeholder="Your bid" required="1" pattern="^[0-9\.,]+$" maxlength="14">
            </div>
            <div class="form-group">
                <textarea class="textarea-scrollbar" name="message" rows="3" placeholder="Message for bid" maxlength="200"></textarea>
            </div>
            <div class="btn-wrap">
                <button type="button" class="btn2" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn" onclick="counter();return false;">Place a Bid</button>
            </div>
        </form>
    </div>
</div>
