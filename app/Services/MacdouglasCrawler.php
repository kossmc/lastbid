<?php


namespace App\Services;

use Carbon\Carbon;
use Symfony\Component\DomCrawler\Crawler;

class MacdouglasCrawler extends BaseCrawler
{
    public $baseUrl = 'http://www.macdougallauction.com/Results.asp';

    public function __construct($batch)
    {
        $this->filename = 'Mac_Douglas_auction' . date('Y-m-d_H-i-s') . '.csv';
        parent::__construct($batch);
    }
    /**
     * Parsing logic go here
     * array with results
     * @return array
     */
    public function startCrawling()
    {
        $links = [];
        $lots = [];
        $failed = [];
        $response = $this->client->request('GET', $this->baseUrl);
        if ($response->getStatusCode() == 200) {
            $page = new Crawler($response->getBody()->getContents(), $this->baseUrl);
            $page->filter('body .STYLE24 a')->each(function ($item) use (&$links) {
                    if(preg_match('/Online Catalogue/',$item->text())){
                    $links[] = $item->link()->getUri();
                }
            });

            foreach ($links as $link){

                $response = $this->client->request('GET', $link);
                $paginations_links = [];
                $page = new Crawler($response->getBody()->getContents(), $link);
                if(! $page
                    ->filter('body table tbody tr')
                    ->eq($page->filter('body table tbody tr')->count() - 2)
                    ->filter('td a')
                    ->count()
                ){
                    $page
                        ->filter('body table')
                        ->last()
                        ->filter('tr td a')
                        ->each(function($item) use(&$paginations_links){
                            $paginations_links[] = $item->link()->getUri();
                        });
                } else {
                    $page
                        ->filter('body table tbody tr')
                        ->eq($page->filter('body table tbody tr')->count() - 2)
                        ->filter('td a')
                        ->each(function($item) use(&$paginations_links){
                            $paginations_links[] = $item->link()->getUri();
                        });
                }
                foreach ($paginations_links as $pg_link){
                    $current_page_links = [];
                    $response =  $this->client->request('GET', $pg_link);
                    if ($response->getStatusCode() != 200) {
                        \Log::info(print_r($response, true));
                        return false;
                    }
                    $content = $this->processContent(
                        $response->getBody()->getContents()
                    );

                    $page = new Crawler($content, $pg_link);


                    $selector = 'body table tbody tr td a';
                    if($page->filter('body table tbody tr td div a')->count()){
                        $selector = 'body table tbody tr td div a';
                    }

                    $page->filter($selector)->each(function ($item) use(&$current_page_links) {
                        $current_page_links[] = $item->link()->getUri();
                    });
                    foreach ($current_page_links as $cp_link){

                        $response =  $this->client->request('GET', $cp_link);
                        if ($response->getStatusCode() != 200) {
                            \Log::info(print_r($response, true));
                            return false;
                        }
                        $content = $this->processContent(
                            $response->getBody()->getContents()
                        );

                        $crawler = new Crawler($content, $cp_link);
                        try{
                            $res = $this->getLotInfo($crawler, $cp_link);
                            if(!$res){
                                $failed[] = $cp_link;
                                //dump("failed: ".count($failed)."");
                                continue;
                            }
                            $lots[] = $res;

                            //dump("success: ".count($lots)."");
                        } catch(\Exception $exception){
                            \Log::info(print_r($exception->getMessage(), true));
                        }
                    }
                }
            }
        }
        if ($lots) {
            $this->headers = array_keys($lots[0]);
        }
        file_put_contents(storage_path('app/parser/errors/macdouglas_parsing_errors.txt'), json_encode($failed));

        return $lots;
    }

    /**
     * Building main lot info
     * @param $crawler
     * @param $link
     * @return array|bool
     */
    public function getLotInfo($crawler, $link)
    {
        $lot = [];
        $price = $this->getLotPrice($crawler);
        if(!$price) return false;
        //$image = $this->loadImage($crawler->filter('body .MagicZoom img')->image()->getUri());
        $image = $crawler->filter('body .MagicZoom img')->image()->getUri();
        $auctionDate = $this->getStartDate($crawler);
        $startDate = $auctionDate->format('Y-m-d H:i:s');
        $endDate = $auctionDate->addMonth()->format('Y-m-d H:i:s');

        $lot['id'] = preg_replace('/[^0-9]/', '', $crawler->getUri());
        $lot['Title'] = $this->getTitle($crawler);
        $lot['Description'] = $this->getDescription($crawler);
        $lot['Category'] = 'Russian Works of Art';
        $lot['Sub category'] = '';
        $lot['Partner'] = $this->partner_id;
        $lot['Shipping description'] = '';
        $lot['Auction House Fee'] = 20;
        $lot['Minimum bid amount'] = isset($price['price_from']) ? $price['price_from'] * 0.5 : 0;
        $lot['Price range from'] = isset($price['price_from']) ? $price['price_from'] : 0;
        $lot['Price range to'] = isset($price['price_to']) ? $price['price_to'] : 0;
        $lot['Price to auto-accept bid'] = isset($price['price_from']) ? $price['price_from'] * 0.7 : 0;
        $lot['Start date'] = $startDate;
        $lot['End date'] = $endDate;
        $lot['Seller'] = $this->seller_id;
        $lot['Currency'] = isset($price['currency']) ? $price['currency'] : 'usd';
        $lot['Lot photos'] = $image;
        $lot['url'] = $link;
        return $lot;
    }

    /** Helper to find price
     * @param $delimiters
     * @param $string
     * @return array
     */

    public function multiexplode ($delimiters,$string) {

        $ready = str_replace($delimiters, $delimiters[0], $string);
        $launch = explode($delimiters[0], $ready);
        return  $launch;
    }

    /**
     * Get lot price
     * @param $crawler
     * @return array|bool
     */
    public function getLotPrice($crawler)
    {
        $content = false;
        if($crawler->filter('body span b')->last()->count()){
            $content = $crawler->filter('body span b')->last()->text();
        } elseif($crawler->filter('body table tr th h2')->last()->count()){
            $content = $crawler->filter('body table tr th h2')->last()->text();
        } elseif($crawler->filter('body table tr td h2')->last()->count()){
            $content = $crawler->filter('body table tr td h2')->last()->text();
        }
        if(!$content) return false;


        $parts = $this->multiexplode(['-', '—', ' ', '–'], trim($content));
        return [
            'currency' => isset($parts[2]) ? strtolower($parts[2]) : 'gbp',
            'price_from' => (
                isset($parts[0]) &&
                preg_replace('/[^0-9]/', '', $parts[0]) !== ""
            )
                ?
                    preg_replace('/[^0-9]/', '', $parts[0])
                :
                    1,
            'price_to' => (
                isset($parts[1]) &&
                preg_replace('/[^0-9]/', '', $parts[1]) != ""
            )
                ?
                    preg_replace('/[^0-9]/', '', $parts[1])
                :
                    1
        ];
    }

    /**
     * Finding title on page
     * @param $crawler
     * @return string
     */
    public function getTitle($crawler){
        if($crawler->filter('body .STYLE3')->count()){
            return $crawler->filter('body .STYLE3')->text();
        }
        if($crawler->filter('body span')->last()->count()){
            return $crawler->filter('body span')->last()->text();
        }
        if($crawler->filter('body table tr td h2')->first()->count()){
            return $crawler->filter('body table tr td h2')->first()->text();
        }
        return 'no founded';
    }

    /**
     * Finding description on page
     * @param $crawler
     * @return string
     */
    public function getDescription($crawler){
        if($crawler->filter('body span i')->last()->count()){
            return "<p>" . $crawler->filter('body span i')->last()->text() . "</p>";
        }
        if($crawler->filter('body span')->last()->count()){
            return "<p>".$crawler->filter('body span')->last()->text()."</p>";
        }
        if($crawler->filter('body table tr th h2 i')->first()->count()){
            return "<p>".$crawler->filter('body table tr th h2 i')->first()->text()."</p>";
        }
        return 'no founded';
    }

    /**
     * Get start auction
     * @return Carbon
     */
    public function getStartDate($crawler)
    {

        $dateHtml = null;

        if (!$dateHtml)
            return Carbon::now();

        return Carbon::parse(trim($dateHtml->text()));
    }
}