<?php

namespace App\Services;

use Carbon\Carbon;

class GrahamBudCrawler extends BaseCrawler
{
    /**
     * temp file.
     *
     * @var string
     */
    public $tempFile;

    /**
     * temp filename.
     *
     * @var string
     */
    public $filename;

    /**
     * from url.
     *
     * @var string
     */
    public $url;
    /**
     * site url.
     *
     * @var string
     */
    public $baseUrl;
    /**
     * Source site categories.
     *
     * @var array
     */
    public $categories;
    /**
     * Lots list.
     *
     * @var array
     */
    public $lots;

    /**
     * Auction id.
     *
     * @var int
     */
    public $archiveId;

    /**
     * Auction lots count.
     *
     * @var int
     */
    public $totalLotsCount = null;


    public function __construct($batch)
    {
        $this->archiveId = $batch->input;
        $this->filename = 'Graham_' . $this->archiveId . '_' . date('Y-m-d_H-i-s') . '.csv';
        $this->tempFile = $this->tempFolder . $this->filename;
        $this->baseUrl = 'https://www.grahambuddauctions.co.uk/';
        $this->url = $this->baseUrl . 'index.php?option=com_bidders&task=lots.getLots&format=json&lots_per_page=100&auction_id=' . $this->archiveId;
        $this->lots = collect([]);
        parent::__construct($batch);
    }

    public function startCrawling()
    {
        if ($this->archiveId) {
            $data = $this->getLots();
            if ($data) {
                $this->headers = array_keys($data->first());
            }
            return $data;
        }
        return [];
    }

    public function getUrl($page = 1) {
        return $this->url . '&page_no=' . $page;
    }
    public function getCategories($categories)
    {
        $this->categories = array_combine(
            collect($categories)->map(function ($item) {
                return $item->id;
            })->toArray(),
            collect($categories)->map(function ($item) {
                return $item->category;
            })->toArray()
        );
    }
    public function getLots()
    {
        $count = 0;
        $page = 1;
        do {
            $response = $this->client->request('POST', $this->getUrl($page));
            if ($response->getStatusCode() == 200) {
                if ($json = json_decode($response->getBody()->getContents())) {
                    if ($json->count > 0) {
                        if (is_null($this->totalLotsCount)) {
                            $this->totalLotsCount = $json->count;
                        }
                        $count += count($json->lots);
                        $this->getCategories($json->categories);
                        $this->lots = $this->lots->concat(
                            collect($json->lots)->reject(function ($item) {
                                /*remove sold items? or check sold for null*/
                                return (
                                    $item->hammer_price != '0.00' || empty($item->lot_name) ||
                                    $item->lot_name == 'No Lot' || empty($item->description) ||
                                    empty($item->low_estimate) || empty($item->high_estimate)
                                );
                            })->map(function ($item) {
                                $category = !empty($this->categories[$item->cat_id])?$this->categories[$item->cat_id]:'unknow';
                                /*grab info*/
                                $startDate = $item->auction_date . ' ' . $item->start_time;
                                $image = str_replace('medium', 'large', $this->baseUrl.$item->imageUrl);
                                return [
                                    'Title' => $item->lot_name,
                                    'Description' => $item->description,
                                    'Category' => 'Sporting Memorabilia',
                                    'Sub Category' => $category,
                                    'Partner' => $this->partner_id,
                                    'Shipping description' => '',
                                    'Auction House Fee' => 20,
                                    'Minimum bid amount' => $item->low_estimate * 0.5,
                                    'Price range from' => $item->low_estimate,
                                    'Price range to' => $item->high_estimate,
                                    'Price to auto-accept bid' => $item->low_estimate * 0.7,
                                    'Start date' => $startDate,
                                    'End date' => Carbon::parse($startDate)->addMonth()->format('Y-m-d H:i:s'),
                                    'Seller' => $this->seller_id,
                                    'Currency' => 'gbp',
                                    'Lot photos' => $this->loadImage($image)
                                ];
                            })
                        );
                    }
                    else {
                        break;
                    }
                }
                else {
                    break;
                }
            }
            $page ++;
        } while (!is_null($this->totalLotsCount) && $this->totalLotsCount > $count);
        \Log::info($this->totalLotsCount);
        \Log::info($count);
        \Log::info($this->lots->count());
        return $this->lots->count() > 0 ? $this->lots : false;
    }
}
