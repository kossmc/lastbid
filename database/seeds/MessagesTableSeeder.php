<?php

use App\Models\User;
use App\Models\Lot;
use App\Models\Message;
use App\Models\MessageText;
use Illuminate\Database\Seeder;

class MessagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     * php artisan db:seed --class=MessagesTableSeeder
     * (if class not found run composer dump-autoload)
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $buyers = User::role('buyer')->get()->pluck('id')->toArray();
        $lots = Lot::all()->pluck('id')->toArray();
        $seller = User::role('seller')->first();
        /*admin receive letters*/
        for ($k = 0; $k < 50; $k ++) {
            $buyer_id = $faker->randomElement($buyers);
            $lot_id = $faker->randomElement($lots);
            if ($buyer_id && $lot_id) {
                $mess = Message::create([
                    'lot_id' => $lot_id,
                    'buyer_id' => $buyer_id,
                    'seller_id' => $seller->id,
                ]);
                //message text
                /*buyer sent*/
                $messText = MessageText::create([
                    'message_id' => $mess->id,
                    'user_id' => $buyer_id,
                    'text' => $faker->sentence
                ]);
                $mess->last_message_at = $messText->created_at;
                $mess->save();
                sleep(2);
                /*seller replay*/
                $messText = MessageText::create([
                    'message_id' => $mess->id,
                    'user_id' => $seller->id,
                    'text' => $faker->sentence
                ]);
                $mess->last_message_at = $messText->created_at;
                $mess->last_message_by = 'seller';
                $mess->save();
                if ($faker->randomElement([true, false])) {
                    //buyer replay
                    sleep(2);
                    $messText = MessageText::create([
                        'message_id' => $mess->id,
                        'user_id' => $buyer_id,
                        'text' => $faker->sentence
                    ]);
                    $mess->last_message_at = $messText->created_at;
                    $mess->last_message_by = 'buyer';
                    $mess->save();
                }
            }
        }
    }
}
