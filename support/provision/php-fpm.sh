echo "Installing PHP and modules..."
sudo add-apt-repository -y ppa:ondrej/php > /dev/null 2>&1
sudo apt-get update > /dev/null 2>&1
sudo apt-get install -y php7.1 > /dev/null 2>&1
sudo apt-get install -y php-fpm php7.1-fpm php-pgsql php7.1-mcrypt php-xml php7.1-xml php-curl php7.1-curl php-mysql php7.1-mysql php-redis php-intl php-dev php-gd php7.1-gd php-mbstring php7.1-mbstring pkg-config php-pear libmagickwand-dev imagemagick php-imagick > /dev/null 2>&1

# Reload PHP service
sudo service php7.1-fpm reload > /dev/null 2>&1

# Installing Composer
echo "Installing Composer..."
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php composer-setup.php
php -r "unlink('composer-setup.php');"
mv composer.phar /usr/local/bin/composer

# Restart servers
sudo service nginx restart > /dev/null 2>&1
sudo cp /lastbid/support/php/php-cli.ini /etc/php/7.1/cli/php.ini
sudo cp /lastbid/support/php/php-fpm.ini /etc/php/7.1/fpm/php.ini
sudo service php7.1-fpm restart > /dev/null 2>&1

