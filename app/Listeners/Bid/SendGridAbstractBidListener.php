<?php

namespace App\Listeners\Bid;

use App\Models\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class SendGridAbstractBidListener implements ShouldQueue
{
    protected $bid;
    protected $buyer;
    protected $lot;
    protected $seller;
    protected $attempt;
    protected $prevAttempt;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function setData($event)
    {
        $this->bid = $event->bid;
        $this->bid->load(['lot', 'lot.seller', 'buyer','attempts']);
        $this->buyer = $this->bid->buyer;
        $this->lot = $this->bid->lot;
        $this->seller = $this->lot->seller;
        $this->prevAttempt = $this->bid->attempts->values()->get(1);
        $this->attempt = $this->bid->latestAttempt();
    }

    protected function getData($id)
    {
        $data = [
            "sub"=>[
                "-buyerName-"=>[$this->buyer->full_name]
                ,"-buyerEmail-"=>[$this->buyer->email]
                ,"-buyerBillingAddress-"=>[$this->buyer->getAddressFormatted()]
                ,"-buyerShortName-"=>[$this->buyer->short_name]
                ,"-sellerName-"=>[$this->seller->full_name]
                ,"-sellerShortName-"=>[$this->seller->short_name]
                ,"-lotName-"=>[$this->lot->title]
                ,"-lotNumber-"=>[$this->lot->id]
                ,"-bidAmount-"=>[format_price($this->attempt->amount, null, $this->lot->currency)]
                ,"-auctionHousePremium-"=>[format_price($this->lot->bidFee($this->attempt->amount), null, $this->lot->currency)]
                ,"-lotFullPrice-"=>[format_price($this->lot->bidWithFee($this->attempt->amount), null, $this->lot->currency)]
                ,"-prevBidAmount-"=>[$this->prevAttempt?format_price($this->prevAttempt->amount, null, $this->lot->currency):0]
                ,"-bidExpire-"=>[$this->attempt->end_date->setTimezone($this->buyer->timezone?$this->buyer->timezone:'America/New_York')->format('j F Y, h:i A')]
                ,"-bidCreated-"=>[$this->attempt->created_at->setTimezone($this->buyer->timezone?$this->buyer->timezone:'America/New_York')->format('j F Y, h:i A')]
                ,"-lotLink-"=>[$this->lot->url]
                ,"-lotEndDate-"=>[$this->lot->end_date->setTimezone($this->buyer->timezone?$this->buyer->timezone:'America/New_York')->format('j F Y, h:i A')]
                ,"-lotPriceRange-"=>[format_price($this->lot->range_from, $this->lot->range_to, $this->lot->currency)]
                ,"-bidMessage-"=>[$this->attempt->message? $this->attempt->message:'No message']
                ,"-lotImage-"=>[$this->lot->getImagePath(600, 600)]
                ,"-siteLink-" => [route('home')]
                ,"-buyerBidLink-" => [route('buyer_lot', ['id' => $this->lot->id])]
                ,"-sellerBidLink-" => [route('seller_bids')]
                ,"-emailPreferencesLink-" => [route('subscriptions')]
                ,"-currentYear-"=>[date('Y')]
                ,"-purchasesLink-" => [route('purchases')]
                ,"-soldLotsLink-" => [route('seller_sold_lots')]
                ,"-invoiceLink-" => [route('show-buyer-invoice', ['id' => $this->lot->id])]
                ,"-categoryLink-" => [route('category', ['slug' => $this->lot->category->slug])]
            ],
            "filters"=>[
                "templates"=>[
                    "settings"=>[
                        "enable"=>1,
                        "template_id"=>$id
                    ]
                ],
                "subscriptiontrack"=>[
                    "settings"=>[
                        "enable"=>1,
                        "replace"=>'[unsubscribe]'
                    ]
                ]
            ]
        ];
        return asJSON($data);
    }

    protected function sendMail(User $to, $data, $subject)
    {
        try {
            Mail::send('emails.empty', [], function ($message) use ($to, $subject, $data) {
                $message->getSwiftMessage()->getHeaders()->addTextHeader('X-SMTPAPI', $data);
                $message->to($to->email, $to->full_name)->subject($subject);
            });
        } catch (\Swift_TransportException $exception) {
        }
    }

    protected function sendMailToAdmin($data, $subject){
        $to = User::where('email', '=','admin@lastbid.com')->first();
        try {
            Mail::send('emails.empty', [], function ($message) use ($to, $subject, $data) {
                $message->getSwiftMessage()->getHeaders()->addTextHeader('X-SMTPAPI', $data);
                $message->to($to->email, $to->full_name)->subject($subject);
            });
        } catch (\Swift_TransportException $exception) {
        }
    }
}
