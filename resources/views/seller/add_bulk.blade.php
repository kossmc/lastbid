@extends('layouts.inner')
@section('title', 'Bulk upload')

@section('user_css')

    <link rel="stylesheet" type="text/css" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />

@endsection

@section('inner_content')

    @include('inc.flash_message')

    <div class="lot-edit lots-list-i current-bid-i">

        <form id="add-bank-account" method="POST" enctype="multipart/form-data"
              action="{{route('seller_bulk_upload_save')}}" >
            <input type="hidden" name="user_id" value="{{ Auth::id() }}">
            <input type="hidden" name="upload_type" value="base">
            <div class="form-controll form-controll--newline">
                <h3>Bulk upload </h3>
            </div>

            <div class="form-controll form-controll--bigger form-controll--newline">
                <label>*.csv File</label>
                <input
                        type="file"
                        id="csv_file_input"
                        name="csv_file"
                        class="{{ ($errors->has('csv_file') && $errors->get('upload_type')[0] == 'base') ? 'has-error--bgcolor' : '' }}"
                >
                @if ($errors->has('csv_file') && $errors->get('upload_type')[0] == 'base')
                    <p class="help-block">{{ $errors->first('csv_file') }}</p>
                @endif
            </div>

            <div class="form-controll form-controll--bigger form-controll--newline">
                <label>*.zip File (photos)</label>
                <input
                        type="file"
                        id="zip_file_input"
                        name="zip_file"
                        class="{{ ($errors->has('zip_file') && $errors->get('upload_type')[0] == 'base') ? 'has-error--bgcolor' : '' }}"
                >
                @if ($errors->has('zip_file') && $errors->get('upload_type')[0] == 'base')
                    <p class="help-block">{{ $errors->first('zip_file') }}</p>
                @endif
            </div>
            <div class="form-controll form-controll--newline form-controll--bigger">
                <label>Partner</label>

                <div class="select-style">
					<span class="card_month selected-suffix {{ $errors->has('partner') ? 'has-error--bgcolor' : '' }}">
					</span>
                    <ul class="select-month" style="display: none;">
                        @forelse($partners as $partner)
                            <li data-id="{{$partner->id}}">{{$partner->title}}</li>
                        @empty
                        @endforelse
                    </ul>

                    <select name="partner_id" style="display: none;">
                        <option value="{{old('partner', '')}}">
                            {{old('partner', '-')}}
                        </option>
                    </select>
                </div>
                @if ($errors->has('partner_id'))
                    <p class="help-block has-error--bgcolor">{{ $errors->first('partner_id') }}</p>
                @endif
            </div>
            <div class="form-controll form-controll--newline form-controll--bigger">
                <label>Category</label>
                <div class="select-style select-category">
                    <span class="selected-suffix"></span>
                    <ul class="select-category" style="display: none;">
                        @forelse($categories as $category)
                            <li data-id="{{$category->id}}">{{$category->title}}</li>
                        @empty
                        @endforelse
                    </ul>
                    <select name="category_id" style="display: none;">
                        <option value=""></option>
                    </select>
                </div>
                @if ($errors->has('category_id'))
                    <p class="help-block has-error--bgcolor">{{ $errors->first('category_id') }}</p>
                @endif
            </div>

            <div class="form-controll form-controll--small">
                <label>Buyer premium (%)</label>
                <input type="text" min="0" max="100" id="buyer_premium" class="{{ ($errors->has('buyer_premium')) ? 'has-error--bgcolor' : '' }}" name="buyer_premium">
                @if ($errors->has('buyer_premium'))
                    <p class="help-block">{{ $errors->first('buyer_premium') }}</p>
                @endif
            </div>
            <div class="form-controll form-controll--small">
                <label>Duration (days)</label>
                <input type="text" min="1" id="duration" class="{{ ($errors->has('duration')) ? 'has-error--bgcolor' : '' }}" name="duration">
                @if ($errors->has('duration'))
                    <p class="help-block">{{ $errors->first('duration') }}</p>
                @endif
            </div>
            <div class="form-controll form-controll--small">
                <label>Auto decline (%)</label>
                <input type="text" min="1" max="99" class="{{ ($errors->has('min_bid_amount')) ? 'has-error--bgcolor' : '' }}" id="min_bid_amount" name="min_bid_amount">
                @if ($errors->has('min_bid_amount'))
                    <p class="help-block">{{ $errors->first('min_bid_amount') }}</p>
                @endif
            </div>
            <div class="form-controll form-controll--small">
                <label>Auto accept (%)</label>
                <input type="text" min="1" id="auto_accept_price" class="{{ ($errors->has('auto_accept_price')) ? 'has-error--bgcolor' : '' }}" name="auto_accept_price">
                @if ($errors->has('auto_accept_price'))
                    <p class="help-block">{{ $errors->first('auto_accept_price') }}</p>
                @endif
            </div>

            <div class="form-controll form-controll--small">
                <label>Currency</label>
                <div class="select-style">
                    <span class="card_month selected-suffix"></span>
                    <ul class="select-month" style="display: none;">
                        <li>usd</li>
                        <li>gbp</li>
                    </ul>
                    <select name="currency" style="display: none;">
                        <option value=""></option>
                    </select>
                </div>
                @if ($errors->has('currency'))
                    <p class="help-block has-error--bgcolor">{{ $errors->first('currency') }}</p>
                @endif
            </div>

            <div class="form-controll form-controll--newline form-controll--large">
                <div class="btn-wrap">
                    <button class="btn" type="submit">
                        Upload
                    </button>
                    <a class="btn2" href="{{route('seller_active_lots')}}">Cancel</a>
                </div>
            </div>

            {{csrf_field()}}
        </form>

    </div>

@endsection