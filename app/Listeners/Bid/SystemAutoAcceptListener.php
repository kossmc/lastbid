<?php

namespace App\Listeners\Bid;

use App\Events\Bid\SystemAutoAcceptBid;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Repositories\BidRepository;

class SystemAutoAcceptListener implements ShouldQueue
{
    /**
     * The bid repository implementation.
     *
     * @var BidRepository
     */
    private $bid;


    /**
     * Create a new listener instance.
     *
     * @param BidRepository $bid
     * @return void
     */
    public function __construct(BidRepository $bid)
    {
        $this->bid = $bid;
    }

    /**
     * Handle the event.
     *
     * @param  SystemAutoAcceptBid  $event
     * @return void
     */
    public function handle(SystemAutoAcceptBid $event)
    {
        $this->bid->acceptBySeller($event->bid);
    }
}
