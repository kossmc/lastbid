<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;
use Illuminate\Support\HtmlString;

class Partner extends Model
{
    use CrudTrait;

    /*
   |--------------------------------------------------------------------------
   | GLOBAL VARIABLES
   |--------------------------------------------------------------------------
   */
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'partners';
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'description', 'link', 'terms_link', 'image'];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
    ];


    public static function boot()
    {
        parent::boot();
        static::deleting(function ($obj) {
            \Storage::disk('uploads')->delete($obj->image);
        });
    }

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function lots()
    {
        return $this->hasMany(Lot::class, 'partner_id', 'id');
    }
    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */
    /**
     * Check if image exist and return it path
     * If not return fake image
     * @return mixed|string

    public function getImageAttribute()
    {
        $disk = 'uploads';
        $path = $disk . '/' . 'coming_soon.jpg';
        if ( ! (is_null($this->image)) && \Storage::disk($disk)->has($this->image)) {
            $path = $disk . '/' . $this->attributes['image'];
        }
        return $path;
    }*/
    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
    public function setImageAttribute($value)
    {
        $attribute_name = 'image';
        $disk = 'uploads';
        $destination_path = 'partners';

        $this->uploadFileToDisk($value, $attribute_name, $disk, $destination_path);
    }
}
