<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Spatie\Permission\Traits\HasRoles;
use App\Scopes\SellerScope;

class Seller extends User
{
    use HasRoles;
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'users';

	public static function boot()
	{
		parent::boot();
		static::addGlobalScope(new SellerScope);
	}
    /**
     * A user may have multiple roles.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function roles()
    {
        return $this->belongsToMany(
            config('laravel-permission.models.role'),
            config('laravel-permission.table_names.user_has_roles'),
            'user_id',
            'role_id'
        );
    }

}
