<div class="block-card-lot block-card-lot--accepted-bid">
    <span class="icon-congrat"></span>
    <p class="title">
        @if ($bid->by_seller)
            You accepted bid
        @else
            Your bid was acceted
        @endif
    </p>
    <div class="info">
        <div class="info-person">
            <p>{{ $bid->buyer->short_name }}</p>
            <span>{{ $bid->buyer->address }}</span>
            <div class="bid-price">{!! format_price($bid->attempt->amount, null, $lot->currency) !!}</div>
        </div>

        <div class="info-bid">       
            <p class="title">Total bid price consists of:</p>
            <div>
                <p>Bid price:</p>
                <div class="bid-price"> {!! format_price($bid->attempt->amount, null, $lot->currency) !!} </div>
            </div>
            <div>   
                <p>Auction house premium:</p>
                <div class="bid-price">{!! format_price($lot->bidFee($bid->attempt->amount), null, $lot->currency) !!}</div>
            </div>
            <div>   
                <p>Total bid price:</p>
                <div class="bid-price">{!! format_price($lot->bidWithFee($bid->attempt->amount), null, $lot->currency) !!}</div>
            </div>
        </div>
        
    </div>

    <div class="bottom-text">Does not include auction house premium and shipping/delivery fees</div>

    <div class="card-bottom-part">
        <p><span>Start:</span>{{ $lot->start_date->format('j F Y, h:i A') }} EST</p>
        <p><span>End:</span>{{ $lot->end_date->format('j F Y, h:i A') }} EST</p>
    </div>
</div>
