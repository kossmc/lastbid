<?php

namespace App\Traits;

trait ModelImageTrait
{
    /**
     * Get image by size
     * @param null $width
     * @param null $height
     * @return string
     */
    public function getImagePath($width = null, $height = null)
    {
        $disk = "uploads";
        $path = $disk . '/' . 'coming_soon.jpg';

        /*if image present*/
        if ( ! (is_null($this->attributes['image'])) && \Storage::disk($disk)->has($this->attributes['image'])) {
            $path = ($width && $height) ? "{$width}x{$height}/" . $disk . '/' . $this->attributes['image']
                : $disk . '/' . $this->attributes['image'];
        }

        return asset($path);
    }

    public function getHeaderImagePath($width = null, $height = null)
    {
        $disk = "uploads";
        $path = $disk . '/' . 'coming_soon.jpg';
        /*if image present*/
        if ( ! (is_null($this->attributes['header_image'])) && \Storage::disk($disk)->has($this->attributes['header_image'])) {
            $path = ($width && $height) ? "{$width}x{$height}/" . $disk . '/' . $this->attributes['header_image']
                : $disk . '/' . $this->attributes['header_image'];
        }
        return asset($path);
    }

    public function getImageSection2Path($width = null, $height = null)
    {
        $disk = "uploads";
        $path = $disk . '/' . 'coming_soon.jpg';
        /*if image present*/
        if ( ! (is_null($this->attributes['image_section_2'])) && \Storage::disk($disk)->has($this->attributes['image_section_2'])) {
            $path = ($width && $height) ? "{$width}x{$height}/" . $disk . '/' . $this->attributes['image_section_2']
                : $disk . '/' . $this->attributes['image_section_2'];
        }
        return asset($path);
    }

    public function getImageSection3Path($width = null, $height = null)
    {
        $disk = "uploads";
        $path = $disk . '/' . 'coming_soon.jpg';
        /*if image present*/
        if ( ! (is_null($this->attributes['image_section_3'])) && \Storage::disk($disk)->has($this->attributes['image_section_3'])) {
            $path = ($width && $height) ? "{$width}x{$height}/" . $disk . '/' . $this->attributes['image_section_3']
                : $disk . '/' . $this->attributes['image_section_3'];
        }
        return asset($path);
    }

    public function getImageSection4Path($width = null, $height = null)
    {
        $disk = "uploads";
        $path = $disk . '/' . 'coming_soon.jpg';
        /*if image present*/
        if ( ! (is_null($this->attributes['image_section_4'])) && \Storage::disk($disk)->has($this->attributes['image_section_4'])) {
            $path = ($width && $height) ? "{$width}x{$height}/" . $disk . '/' . $this->attributes['image_section_4']
                : $disk . '/' . $this->attributes['image_section_4'];
        }
        return asset($path);
    }

    public function getTeamMember1ImagePath($width = null, $height = null)
    {
        $disk = "uploads";
        $path = $disk . '/' . 'coming_soon.jpg';
        /*if image present*/
        if ( ! (is_null($this->attributes['team_member_1_image'])) && \Storage::disk($disk)->has($this->attributes['team_member_1_image'])) {
            $path = ($width && $height) ? "{$width}x{$height}/" . $disk . '/' . $this->attributes['team_member_1_image']
                : $disk . '/' . $this->attributes['team_member_1_image'];
        }
        return asset($path);
    }

    public function getTeamMember2ImagePath($width = null, $height = null)
    {
        $disk = "uploads";
        $path = $disk . '/' . 'coming_soon.jpg';
        /*if image present*/
        if ( ! (is_null($this->attributes['team_member_2_image'])) && \Storage::disk($disk)->has($this->attributes['team_member_2_image'])) {
            $path = ($width && $height) ? "{$width}x{$height}/" . $disk . '/' . $this->attributes['team_member_2_image']
                : $disk . '/' . $this->attributes['team_member_2_image'];
        }
        return asset($path);
    }

    public function getTeamMember3ImagePath($width = null, $height = null)
    {
        $disk = "uploads";
        $path = $disk . '/' . 'coming_soon.jpg';
        /*if image present*/
        if ( ! (is_null($this->attributes['team_member_3_image'])) && \Storage::disk($disk)->has($this->attributes['team_member_3_image'])) {
            $path = ($width && $height) ? "{$width}x{$height}/" . $disk . '/' . $this->attributes['team_member_3_image']
                : $disk . '/' . $this->attributes['team_member_3_image'];
        }
        return asset($path);
    }

    public function getTeamMember4ImagePath($width = null, $height = null)
    {
        $disk = "uploads";
        $path = $disk . '/' . 'coming_soon.jpg';
        /*if image present*/
        if ( ! (is_null($this->attributes['team_member_4_image'])) && \Storage::disk($disk)->has($this->attributes['team_member_4_image'])) {
            $path = ($width && $height) ? "{$width}x{$height}/" . $disk . '/' . $this->attributes['team_member_4_image']
                : $disk . '/' . $this->attributes['team_member_4_image'];
        }
        return asset($path);
    }

    public function getTeamMember5ImagePath($width = null, $height = null)
    {
        $disk = "uploads";
        $path = $disk . '/' . 'coming_soon.jpg';
        /*if image present*/
        if ( ! (is_null($this->attributes['team_member_5_image'])) && \Storage::disk($disk)->has($this->attributes['team_member_5_image'])) {
            $path = ($width && $height) ? "{$width}x{$height}/" . $disk . '/' . $this->attributes['team_member_5_image']
                : $disk . '/' . $this->attributes['team_member_5_image'];
        }
        return asset($path);
    }

    public function getTeamMember6ImagePath($width = null, $height = null)
    {
        $disk = "uploads";
        $path = $disk . '/' . 'coming_soon.jpg';
        /*if image present*/
        if ( ! (is_null($this->attributes['team_member_6_image'])) && \Storage::disk($disk)->has($this->attributes['team_member_6_image'])) {
            $path = ($width && $height) ? "{$width}x{$height}/" . $disk . '/' . $this->attributes['team_member_6_image']
                : $disk . '/' . $this->attributes['team_member_6_image'];
        }
        return asset($path);
    }

    public function getImageDefaultPath($width = null, $height = null)
    {
        $disk = "uploads";
        $path = $disk . '/' . 'coming_soon.jpg';
        /*if image present*/
        if ( ! (is_null($this->attributes['image_default'])) && \Storage::disk($disk)->has($this->attributes['image_default'])) {
            $path = ($width && $height) ? "{$width}x{$height}/" . $disk . '/' . $this->attributes['image_default']
                : $disk . '/' . $this->attributes['image_default'];
        }
        return asset($path);
    }

    public function getImageHoverPath($width = null, $height = null)
    {
        $disk = "uploads";
        $path = $disk . '/' . 'coming_soon.jpg';
        /*if image present*/
        if ( ! (is_null($this->attributes['image_hover'])) && \Storage::disk($disk)->has($this->attributes['image_hover'])) {
            $path = ($width && $height) ? "{$width}x{$height}/" . $disk . '/' . $this->attributes['image_hover']
                : $disk . '/' . $this->attributes['image_hover'];
        }
        return asset($path);
    }

    /**
     * Check if image exist and return it path
     * If not return fake image
     * @return mixed|string
     */
    public function getImageAttribute()
    {
        return $this->getImagePath();
    }

    /**
     * Check if image exist and return it path
     * If not return fake image
     * @return mixed|string
     */
    public function getHeaderImageAttribute()
    {
        return $this->getHeaderImagePath();
    }

    public function getImageSection2Attribute()
    {
        return $this->getImageSection2Path();
    }

    public function getImageSection3Attribute()
    {
        return $this->getImageSection3Path();
    }

    public function getImageSection4Attribute()
    {
        return $this->getImageSection4Path();
    }

    public function getTeamMember1ImageAttribute()
    {
        return $this->getTeamMember1ImagePath();
    }

    public function getTeamMember2ImageAttribute()
    {
        return $this->getTeamMember2ImagePath();
    }

    public function getTeamMember3ImageAttribute()
    {
        return $this->getTeamMember3ImagePath();
    }

    public function getTeamMember4ImageAttribute()
    {
        return $this->getTeamMember4ImagePath();
    }

    public function getTeamMember5ImageAttribute()
    {
        return $this->getTeamMember5ImagePath();
    }

    public function getTeamMember6ImageAttribute()
    {
        return $this->getTeamMember6ImagePath();
    }

    public function getImageDefaultAttribute()
    {
        return $this->getImageDefaultPath();
    }

    public function getImageHoverAttribute()
    {
        return $this->getImageHoverPath();
    }
}
