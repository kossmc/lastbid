<?php

namespace App\Listeners\Bid;

use App\Events\Bid\BidExpired;

class BidExpiredListener extends SendGridAbstractBidListener
{
    /**
     * Buyer Email subject.
     */
    private $buyerSubject = 'Your bid has expired';

    /**
     * Buyer Email template.
     */
    private $buyerTemplate = '44e9d8bb-1f19-41f4-a14f-521a9b90105b';

    /**
     * Seller Email subject.
     */
    private $sellerSubject = 'Your bid has been expired';

    /**
     * Seller Email template.
     */
    private $sellerTemplate = '3507a15d-87f6-4909-b744-4f9e29d0129a';
    /**
     * Handle the event.
     *
     * @param  BidExpired  $event
     * @return void
     */
    public function handle(BidExpired $event)
    {
        $this->setData($event);
        /* notification to buyer */
        if ($this->bid->by_buyer) {
            $this->sendMail($this->buyer, $this->getData($this->buyerTemplate), $this->buyerSubject);
        } else {
            $this->sendMail($this->seller, $this->getData($this->sellerTemplate), $this->sellerSubject);
        }
    }
}
