<?php

namespace App\Listeners\Bid;

use App\Events\Bid\SystemDeclineBid;

class SystemDeclineBidListener extends SendGridAbstractBidListener
{

    /**
     * Buyer Email subject.
     */
    private $buyerSubject = 'Lot was closed ';

    /**
     * Buyer Email template.
     */
    private $buyerTemplate = 'ef86efc7-f90f-481c-bda6-427b1b29be4f';

    /**
     * Seller Email subject.
     */
    private $sellerSubject = '';

    /**
     * Seller Email template.
     */
    private $sellerTemplate = '';
    /**
     * Handle the event.
     *
     * @param  SystemDeclineBid  $event
     * @return void
     */
    public function handle(SystemDeclineBid $event)
    {
        $this->setData($event);
        /*notification to buyer*/
        $this->sendMail($this->buyer, $this->getData($this->buyerTemplate), $this->buyerSubject);
        /*notification to seller*/
        //$this->sendMail($this->seller, $this->getData($this->sellerTemplate), $this->sellerSubject);
    }
}
