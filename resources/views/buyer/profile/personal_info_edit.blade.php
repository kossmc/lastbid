@extends('buyer.profile.profile')
@section('title', 'Account info')
@section('profile_content')
    <div class="buyer-account--edit">
        <section class="account-info">
            <h3>Edit profile</h3>
            @include('inc.top_errors_list')
            <form action="" method="POST">
                {{csrf_field()}}
                <div class="form-controll">
                    <label>First Name</label>
                    <input type="text" name="first_name" placeholder="First Name" value="{{old('first_name',$user->first_name)}}"
                    class="{{ $errors->has('first_name') ? 'has-error--bgcolor' : '' }}">
                </div>
                <div class="form-controll">
                    <label>Middle Name</label>
                    <input type="text" name="middle_name" placeholder="Middle Name" value="{{old('middle_name',
                    $user->middle_name)}}" class="{{ $errors->has('middle_name') ? 'has-error--bgcolor' : '' }}">
                </div>
                <div class="form-controll">
                    <label>Last Name</label>
                    <input type="text" name="last_name" placeholder="Last Name" value="{{old('last_name',$user->last_name)}}"
                           class="{{ $errors->has('last_name') ? 'has-error--bgcolor' : '' }}">
                </div>
{{--                <div class="form-controll">--}}
{{--                    <label>Suffix</label>--}}
{{--                    <div class="select-style"><span @if($user->suffix) class="selected-suffix" @endif>--}}
{{--                            @if ($user->suffix)--}}
{{--                                {{$user->suffix}}--}}
{{--                            @else--}}
{{--                                Select suffix--}}
{{--                            @endif--}}
{{--                        </span>--}}
{{--                        <ul>--}}
{{--                            <li>Select suffix</li>--}}
{{--                            <li>Mister</li>--}}
{{--                            <li>Miss</li>--}}
{{--                        </ul>--}}
{{--                        <select name="suffix" style="display: none;">--}}
{{--                            @if ($user->suffix)--}}
{{--                                <option value="{{$user->suffix}}">{{$user->suffix}}</option>--}}
{{--                            @else--}}
{{--                                <option value="suffix">Value</option>--}}
{{--                            @endif--}}
{{--                        </select>--}}
{{--                    </div>--}}
{{--                </div>--}}
                <div class="form-controll form-controll--large">
                    <label>Your Email</label>
                    <input type="text" name="email" placeholder="name@example.com" value="{{old('email',$user->email)}}"
                           class="{{ $errors->has('email') ? 'has-error--bgcolor' : '' }}">
                </div>
                <div class="form-controll form-controll-mob form-controll--full text-center">
                    <div class="btn-wrap">
                        <a class="btn2 btn-buyer-cancel-info" href="{{route('account-info')}}">Cancel </a>
                        <button type="submit" class="btn btn-buyer-save-info" href="#">Save</button>
                    </div>
                </div>
            </form>
        </section>
    </div>
@endsection
@section('custom_modal')
@endsection
@section('user_script')

@endsection

