<div class="popup please-popup confirm-place-bid" id="modal-id">
    <div id="confirm_modal_container">
    <div class="title text-capitalize">Confirm your bid!</div>
    <p>
        Your bid of <b>{!! format_price($amount, null, $lot->currency) !!}</b> does not include <b>{!! format_price
        ($lot->bidFee($amount), null, $lot->currency) !!}</b> auction house premium. <br><br>
        The price will be <b style="font-size: 18px;">{!! format_price($lot->bidWithFee($amount), null,
        $lot->currency) !!}</b> (VAT on the buyer's premium and/or other applicable taxes, and shipping/delivery are not included)<br><br>
        <span class="text-justify">Please confirm</span>
    </p>
    <div class="form-group">
        <span class="btn-open-msg">You can leave a message below</span>
        <textarea class="textarea-scrollbar" name="message" rows="1" id="popup-comment" placeholder="Your message" maxlength="200"></textarea>
    </div>

    <div class="wrapper-sign-up-agreement wrapper-terms-conditions wrapper-terms-conditions-hidden">
        <div class="inner-wrapper-sign-up-agreement">
            @if(isset($terms))
                {!! $terms !!}
            @else
                @include('inc.terms')
            @endif
        </div>
    </div>

    <div class="wrap-check-policy">
        <input type="checkbox" id="terms-agreement" name="terms" value="1">
        <label class="" for="terms-agreement">
            I accept the <span class="terms-agreement-link">"Terms and Conditions"</span>.
        </label>
    </div>

    <div class="btn-wrap">
        <a class="btn2" id="decline" href="#">Cancel</a>
        <a class="btn disabled" id="deposit-info-btn" href="#">Confirm</a>
    </div>
    </div>
    <div id="bid_is_too_low" style="display: none">
        <div class="title text-capitalize">This value is below the current high offer. Please enter a higher bid</div>
        <div class="btn-wrap">
            <a class="btn2" id="decline" href="#">Close</a>
        </div>
    </div>
</div>
<script>
    // console.log('confirm');
    if($('#highest_bid_amount').val() > $('#offer-value').val()) {
      $('#confirm_modal_container').hide()
      $('#bid_is_too_low').show()
    } else {
      $(function(){
        $('#terms-agreement').on('change', function() {
          $('#deposit-info-btn').toggleClass('disabled');
        });

        $('.wrapper-terms-conditions').scrollbar({
          'disableBodyScroll': false,
          'ignoreMobile': false
        });

        function showTerms(e) {
          var dataAttrs = $('.place-bid-js').data();

          $.ajax({
            async: false,
            url: '/terms/'+dataAttrs.alias+'/get',
            type: 'get',
            dataType: 'json',
            success: function (result) {
              if(result.terms){
                $('.wrapper-sign-up-agreement .inner-wrapper-sign-up-agreement').html(result.terms.text);
                $("#accept").data('type', dataAttrs.alias);
              }
            }
          });
          e.preventDefault();
          $('.wrapper-terms-conditions').removeClass('wrapper-terms-conditions-hidden');
          $('.terms-agreement-link').off('click', showTerms);
        }

        $('.terms-agreement-link').on('click', showTerms);

        $('.wrapper-terms-conditions').addClass('scrollbar-outer');
      });
    }

</script>
