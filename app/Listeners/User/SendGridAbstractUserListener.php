<?php

namespace App\Listeners\User;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\URL;

class SendGridAbstractUserListener implements ShouldQueue
{
    protected $user;
    protected $name;
    protected $email;
    protected $referrer;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function setData($event)
    {
        $this->user = $event->user;
        $this->name = $event->user->full_name;
        $this->email = $event->user->email;
        $this->referrer = $event->referrer;
    }

    protected function getData($id)
    {
        $data = [
            "sub"=>[
                "-siteLink-" => [route('home')]
                ,"-emailPreferencesLink-" => [route('subscriptions')]
                ,"-currentYear-"=>[date('Y')]
                ,"-name-"=>[$this->name]
                ,'-verifyLink-'=> [route('account-confirm-email',
                    ['token' => $this->user->email_token, 'referrer'=>$this->referrer])]
            ],
            "filters"=>[
                "templates"=>[
                    "settings"=>[
                        "enable"=>1,
                        "template_id"=>$id
                    ]
                ],
                "subscriptiontrack"=>[
                    "settings"=>[
                        "enable"=>1,
                        "replace"=>'[unsubscribe]'
                    ]
                ]
            ]
        ];
        return asJSON($data);
    }

    protected function sendMail($data, $subject)
    {
        Mail::send('emails.empty', [], function ($message) use($subject, $data) {
            $message->getSwiftMessage()->getHeaders()->addTextHeader('X-SMTPAPI', $data);
            $message->to($this->email, $this->name)->subject($subject);
        });
    }
}