<?php

use Illuminate\Database\Seeder;

class FaqTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     * php artisan db:seed --class=FaqTableSeeder
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'title' => 'Getting started',
                'questions' => [
                    [
                        'question' => 'What is LastBid?',
                        'answer' => 'LastBid works with leading auction houses from around the world to offer you an online
platform where you can browse unsold auction lots – that is, a vast range of items of all types
that failed to sell at recent live auctions. (There are many reasons why they might not have
sold – for example, the original asking price might have been too high, or perhaps interested
bidders simply werent aware that the item was for sale on that day.)
Through LastBid, you can bid on these unsold lots for a limited time after the date of the
auction. Your bid will be passed to the auction house in question – and if your bid is
successful, you will pay the auction house exactly the same as if you were buying from them
directly. LastBid does not make any charge to the buyer.',
                    ],
                    [
                        'question' => 'Can I sell my collectible items on LastBid?',
                        'answer' => 'No; all items on LastBid are offered by established auction houses: we do not offer items for sale by private individuals.',
                    ],
                    [
                        'question' => 'Do I have to register on LastBid before I can view unsold auction lots?',
                        'answer' => 'No. You are free to browse without registering.',
                    ],
                    [
                        'question' => 'Do I have to register on LastBid before I can start placing bids?',
                        'answer' => 'Yes. You will need to register on LastBid before you can place a bid, plus you will need to read and accept the Terms & Conditions of the auction house selling that specific item – a very simple step that you will be guided through on the item description / bidding page.',
                    ],
                    [
                        'question' => 'How do I register on LastBid?',
                        'answer' => 'On the top right-hand corner of every page of the website you will find the ‘Register’ button. Click on it and you will be guided through a simple registration process. Please note, for identification purposes you will require a valid credit card to register for bidding but no payments will be taken. We do not, for example, require a deposit before you place a bid. Even when your bid is successful, LastBid does not take payments directly – purchases are always via the relevant auction house.',
                    ],
                    [
                        'question' => 'Does LastBid store my credit card details?',
                        'answer' => 'No. Your card details are used only for identification purposes as a one-off requirement when you register. The card details will not be retained. (LastBid also adopts a high level of security to prevent your card details from being accidentally disclosed during the registration process.)',
                    ],
                    [
                        'question' => 'What am I committing to if I register?',
                        'answer' => 'Nothing. There is no cost for registering – and no commitment. You can choose to subscribe to our newsletter and various updates but that is up to you.',
                    ]
                ]
            ],
            [
                'title' => 'Bidding',
                'questions' => [
                    [
                        'question' => 'How do I browse for items?',
                        'answer' => 'Click on the ‘Categories’ button on the left of the navigation bar at the top of each page. From the Categories page, you will find an extensive series of item types in the left-hand column… from which you can browse to your heart’s content. (There are also shortcuts to the most popular categories on the website home page. Click on the LastBid logo to get there.) Alternatively, if you have a clearer idea of what you want, use the search button on the far right of the navigation bar at the top of each page.',
                    ],
                    [
                        'question' => 'How do I place a bid?',
                        'answer' => 'When you are logged in as a subscriber, clicking on an individual item will bring up a more detailed description and a ‘Place a Bid’ window. Please note, the bid you place is in the currency specified (typically the local currency of the auction house) and does not include the auction house premium and shipping/delivery fees.',
                    ],
                    [
                        'question' => 'What additional costs do I pay on top of the bid price?',
                        'answer' => 'The final price paid via the LastBid platform will be exactly the same as if buying directly from the auction house. We do not make any charge to the buyer, as our commission is received from the auction house. You will, however, need to pay the auction house’s buyer’s premium, which will include VAT or the equivalent tax, if applicable, in the country in which the item is being offered for sale. There will also be a shipping charge, unless you choose to arrange collection yourself. The auction house premium and shipping/delivery fees depend on the auction house offering the item. For further details, please see the Terms & Conditions of the auction house in question, via a link under the detailed description of the item – where there is also a link to request a shipping estimate.',
                    ],
                    [
                        'question' => 'How do I know that items I bid on are authentic?',
                        'answer' => 'The descriptions of items are as provided by the auction house, and are the same as those that appear in the auction house’s catalogue/website. Please refer to the Terms & Conditions of the auction house offering the item for further information.',
                    ],
                    [
                        'question' => 'For how long is each item listed on LastBid?',
                        'answer' => 'Items are listed for around 30-45 days after the date of the auction, unless they are sold earlier via the LastBid platform.',
                    ],
                    [
                        'question' => 'Does my bid have a time limit?',
                        'answer' => 'Your bid remains valid until it’s rejected, or another, higher bid is accepted, or you receive a counter-offer from the auction house (i.e. a suggested price they will accept). However, if your bid meets the minimum acceptable price set by the auction house, then – in some cases – it will be automatically accepted.',
                    ],
                    [
                        'question' => 'Why am I only allowed 5 bids per item?',
                        'answer' => 'To avoid a potential buyer abusing the system by repeatedly bidding in tiny increments, LastBid will accept only 5 bids from a single registered buyer for a single item. (This effectively mimics a traditional live auction, when the auctioneer will typically refuse a bid that is – say – only £1 higher than the last bid.)',
                    ],
                    [
                        'question' => 'Why was my bid rejected?',
                        'answer' => 'This could be for several reasons. It might be because your bid was lower than the minimum price required by the seller (the ‘reserve’ price), or it might be that the auction house has received a higher bid.',
                    ],
                    [
                        'question' => 'Can I bid using my mobile device?',
                        'answer' => 'Yes, although you will currently need to do so via the browser, as a LastBid app is still in development.',
                    ],
                    [
                        'question' => 'Where can I view all the bids I have placed?',
                        'answer' => 'When you are logged in as a subscriber, you will see your name at the top right-hand corner of every web page. Hover the cursor over your name and a dropdown menu will appear, from which you can view your current active bids, purchased lots and bidding history.',
                    ],[
                        'question' => 'How will I know if my bid is accepted?',
                        'answer' => 'If your bid is successful, you will receive an email from the auction house to confirm that your bid has been accepted. Please note, even if you place a bid at the top end of the estimate, your bid has not necessarily been accepted until you receive a confirmation email.',
                    ],
                ]
            ],
//            [
//                'title' => 'What am I committing to when I place a bid? And what happens if I accidentally bid on an item – or change my mind after bidding?',
//                'questions' => [
//
//                ]
//            ],
            [
                'title' => 'Paying for my purchase:',
                'questions' => [
                    [
                        'question' => 'If my bid is successful, how do I pay?',
                        'answer' => 'Once your bid has been accepted, LastBid will provide the auction house with your details to allow them to send you an invoice for immediate payment. This will include details of how to pay. Please note: your payment will always be directly to the auction house, not to LastBid.',
                    ],
                ]
            ],
            [
                'title' => 'Receiving my item:',
                'questions' => [
                    [
                        'question' => 'Once I have paid, how do I receive my item? Will the auction house package and send it to me?',
                        'answer' => 'You can choose whether to collect the item yourself or ask the auction house to arrange delivery – for which they will charge a fee. Please refer to the Terms & Conditions of the auction house in question (via a link on the item description page). You can request a shipping estimate via the link on the item description page.',
                    ],
                ]
            ],
            [
                'title' => 'Keeping up-to-date:',
                'questions' => [
                    [
                        'question' => 'As a subscriber, what emails will I receive?',
                        'answer' => 'It’s up to you. If you bid on an item, you will of course receive emails from the relevant auction house but only for the purpose of communicating about your potential purpose. Otherwise, we won’t bother you unless you opt into certain sorts of announcements – to keep you up-to-date about forthcoming auctions and so on – and/or our newsletter. And it’s very easy to change your mind and opt out again. When you are logged in as a subscriber, hover your cursor over your name at the top right-hand corner of any LastBid web page and, from the dropdown menu, select ‘Personal info’. That will take you to the Profile page. From here, click on ‘Subscriptions’, where you can choose which communications to receive – or, indeed, whether to unsubscribe from any or all of them.',
                    ],
                ]
            ],
            [
                'title' => 'How do I…?',
                'questions' => [
                    [
                        'question' => 'Log into the website?',
                        'answer' => 'The log-in button is at the top right-hand side of every web page.',
                    ],
                    [
                        'question' => 'What if I forget my password?',
                        'answer' => 'Click on the log-in button at the top right-hand side of any web page and click on ‘Reset password’.',
                    ],
                    [
                        'question' => 'Change my email address, password, name and shipping address – or opt into or out of marketing emails?',
                        'answer' => 'When you are logged in as a subscriber, hover your cursor over your name at the top right-hand corner of any LastBid web page and, from the dropdown menu, select ‘Personal info’. That will take you to the Profile page. From here, choose from Account Info, Shipping Info or Subscriptions to amend your various details.',
                    ],
                ]
            ],
            [
                'title' => 'Terms & Conditions:',
                'questions' => [
                    [
                        'question' => 'Where do I find Terms & Conditions?',
                        'answer' => 'You will be asked to read and accept LastBid’s Terms & Conditions when you register on the site. If you wish to read them again, you can find a link to LastBid’s Terms & Conditions at the bottom right-hand side of every web page. Please note, however, that each auction house offering items for sale will have its own set of terms in place that will apply when you bid on that particular item. You can find a link to the auction’s Terms & Conditions on the item description page and you must read and accept these conditions before bidding on the item. Should you have any questions about an auction\'s terms, please contact that auction house directly.',
                    ],
                ]
            ],
        ];
        foreach ($data as $c => $category) {
            $cat = \App\Models\FaqCategory::create([
                'title' => $category['title'],
                'active' => 1,
                'order' => $c,
            ]);
            foreach ($category['questions'] as $q => $question) {
                $cat->questions()->create([
                    'question' => $question['question'],
                    'answer' => $question['answer'],
                    'active' => 1,
                    'order' => $q,
                ]);
            }
        }
    }
}
