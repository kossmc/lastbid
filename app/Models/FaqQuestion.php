<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class FaqQuestion extends Model
{
    use CrudTrait;

    public $timestamps = true;
    protected $guarded = [];

    public function category()
    {
        return $this->belongsTo('App\Models\FaqCategory', 'faq_category_id', 'id');
    }

    public function scopeActive($q)
    {
        return $q->where('active',1);
    }

    public function scopeOrdered($q)
    {
        return $q->orderBy('order');
    }

    public function scopeSearch($q, $queryString)
    {
        collect(explode(' ', $queryString))
        ->filter(function ($word) {
            return trim($word);
        })
        ->map(function ($word) {
            return '%' . str_replace('%', '\%', $word) . '%';
        })
        ->each(function ($el, $i) use (&$q) {
            if ($i === 0) {
                $q->where('question', 'like', $el)->orWhere('answer', 'like', $el);
            } else {
                $q->orWhere('question', 'like', $el)->orWhere('answer', 'like', $el);
            }
        });
        return $q;
    }
}
