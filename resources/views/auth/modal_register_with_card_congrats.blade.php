<div class="popup" id="modal-id">
    <div class="modal-content">
        <span class="icon-congrat"></span>
        <div class="title">Welcome to Lastbid!</div>
        <p>Thank you for completing the verification.
            <br>
            You are now a fully confirmed and registered user, and you are able to make a bid. Thank you.</p>
        <div class="btn-wrap">
            <a class="btn" href="javascript:void 0;" onclick="closeAndBuy()">Ok</a>
        </div>
    </div>
</div>

