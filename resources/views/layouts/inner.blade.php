@extends('layouts.main')
@section('top-links')
    @include('inc.top_links')
@stop
@php
    $buyer_account_routes = ['account-info','account-info-edit'];
    $buyer_payments_routes = ['payment-methods','payment-methods-accounts',
    'payment-methods-card-add','payment-methods-bank-add', 'payment-methods-card-add','payment-methods-card-edit',
    'payment-methods-bank-edit'];
    $shipping = ['shipping-info-new','shipping-info-edit','shipping-info'];
    $buyer_profile_routes = array_merge($buyer_account_routes,$buyer_payments_routes, $shipping,
    ['subscriptions']) ;
    $seller_account_routes =['seller-account-info'];
    $seller_payments_routes = ['seller-payment-methods', 'seller-payment-methods-bank-add'];
    $seller_profile_routes = array_merge($seller_account_routes,$seller_payments_routes);
@endphp
@section('content')
    <div class="page-wrap">
        <div class="wrap">
            <div class="page-profile page-buyer">
                <div class="sidebar-wrap profile-sidebar-wrap">
                    <div class="group-wrap group-title">
                        <h3>Profile</h3>
                    </div>
                    <div class="group-wrap group-nav">
                        <nav>
                            <ul>
                                @if (Auth::user()->hasRole('buyer'))
                                    <li class="expanded{!! areActiveRoutes($buyer_profile_routes) ?
                                    ' active"' : ''!!}"><a
                                                href="{{ route('account-info') }}">Personal Info</a>
                                        <ul class="group-sub-nav">
                                            <li{!! areActiveRoutes($buyer_account_routes) ? ' class="active"' : '' !!}>
                                                <a href="{{ route('account-info') }}">Account info</a></li>
                                            {{--<li{!! areActiveRoutes($buyer_payments_routes) ? ' class="active"' : '' !!}>
                                                <a href="{{ route('payment-methods') }}">Payment Methods</a></li>--}}
                                            <li{!! areActiveRoutes($shipping) ? ' class="active"' : '' !!}>
                                                <a href="{{ route('shipping-info') }}">Shipping Info</a></li>
                                            {{--<li{!! isActiveRoute('subscriptions') ? ' class="active"' : '' !!}>--}}
                                                {{--<a href="{{ route('subscriptions') }}">Subscriptions</a></li>--}}
                                        </ul>
                                    </li>
                                    <li{!! isActiveRoute('purchases') ? ' class="active"' : '' !!}><a href="{{ route('purchases') }}">My successful bids</a></li>
                                    <li class="expanded{{ areActiveRoutes(['buyer_received_bids', 'buyer_sent_bids', 'buyer_declined_bids', 'buyer_lot']) ? ' active' : '' }}">
                                        <a href="{{ route('buyer_received_bids') }}" class="side-counter-links bids-counter{{ Auth::user()->new_bids_count > 0 ? ' active' : '' }}"><span class="counter">{{ Auth::user()->new_bids_count }}</span>Bids</a>
                                        <ul class="group-sub-nav">
                                            <li{!! isActiveRoute('buyer_received_bids') || (isActiveRoute('buyer_lot') && $bid->by_seller && !($bid->is_expired || $bid->is_declined)) ? ' class="active"' : '' !!}><a href="{{ route('buyer_received_bids') }}">Received bids</a></li>
                                            <li{!! isActiveRoute('buyer_sent_bids') || (isActiveRoute('buyer_lot') && $bid->by_buyer && !($bid->is_expired || $bid->is_declined)) ? ' class="active"' : '' !!}><a href="{{ route('buyer_sent_bids') }}">Sent bids</a></li>
                                            <li{!! isActiveRoute('buyer_declined_bids') || (isActiveRoute('buyer_lot') && ($bid->is_expired || $bid->is_declined)) ? ' class="active"' : '' !!}><a href="{{ route('buyer_declined_bids') }}">Declined / Expired</a></li>
                                        </ul>
                                    </li>
                                @else
                                    <li class="expanded{!! areActiveRoutes($seller_profile_routes) ?
                                    ' active"' : ''!!}"><a
                                                href="{{ route('seller-account-info') }}">Personal Info</a>
                                        <ul class="group-sub-nav">
                                            <li{!! areActiveRoutes($seller_account_routes) ? ' class="active"' : '' !!}>
                                                <a href="{{ route('seller-account-info') }}">Account info</a></li>
                                            <li{!! areActiveRoutes($seller_payments_routes) ? ' class="active"' : '' !!}>
                                                <a href="{{ route('seller-payment-methods') }}">Bank Accounts</a></li>
                                        </ul>
                                    </li>
                                    <li{!! isActiveRoute('managers') ? ' class="active"' : '' !!}><a href="{{ route('managers') }}">Managers</a></li>
                                    <li class="expanded{{ areActiveRoutes(['seller_active_lots', 'seller_closed_lots', 'seller_sold_lots']) ? ' active' : '' }}">
                                        <a href="{{ route('seller_active_lots') }}">MY LOTS</a>
                                        <ul class="group-sub-nav">
                                            <li{!! isActiveRoute('seller_active_lots') ? ' class="active"' : '' !!}><a href="{{ route('seller_active_lots') }}">Active lots</a></li>
                                            <li{!! isActiveRoute('seller_closed_lots') ? ' class="active"' : '' !!}><a href="{{ route('seller_closed_lots') }}">Closed lots</a></li>
                                            <li{!! isActiveRoute('seller_sold_lots') ? ' class="active"' : '' !!}><a href="{{ route('seller_sold_lots') }}">Sold lots</a></li>
                                        </ul>
                                    </li>
                                    <li{!! areActiveRoutes(['seller_lot', 'seller_bids']) ? ' class="active"' : '' !!}>
                                        <a href="{{ route('seller_bids') }}" class="side-counter-links bids-counter{{ Auth::user()->new_bids_count > 0 ? ' active' : '' }}"><span class="counter">{{ Auth::user()->new_bids_count }}</span>Bids</a>
                                    </li>
                                @endif
                                <li class="expanded{{ areActiveRoutes(['messages_received', 'messages_sent', 'messages_archived', 'message_view', 'messages_search']) ? ' active' : '' }}">
                                    <a href="{{ route('messages_received') }}" class="side-counter-links messages-counter{{ Auth::user()->new_messages_count > 0 ? ' active' : '' }}"><span class="counter">{{ Auth::user()->new_messages_count }}</span>Messages</a>
                                    <ul class="group-sub-nav">
                                        <li{!! isActiveRoute('messages_received') || (isActiveRoute('message_view') && !$message->is_sent && !$message->is_archived) ? ' class="active"' : '' !!}><a href="{{ route('messages_received') }}">Received</a></li>
                                        <li{!! isActiveRoute('messages_sent') || (isActiveRoute('message_view') && $message->is_sent && !$message->is_archived) ? ' class="active"' : '' !!}><a href="{{ route('messages_sent') }}">Sent</a></li>
                                        <li{!! isActiveRoute('messages_archived') || (isActiveRoute('message_view') && $message->is_archived) ? ' class="active"' : '' !!}><a href="{{ route('messages_archived') }}">Archived</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </nav>
                    </div>
                    <div class="group-wrap group-nav"></div>
                </div>
                <div class="page-profile-content">
                    @yield('inner_content')
                </div>
            </div>
        </div>
    </div>
@endsection
