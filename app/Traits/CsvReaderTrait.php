<?php

namespace App\Traits;
use League\Csv\Reader;
use SplFileObject;

trait CsvReaderTrait
{
    private $headers = [];
    public function getCsvIterator($path)
    {
        $reader = Reader::createFromPath($path, 'r');
        $reader->setDelimiter($this->getFileDelimiter($path, 5)); //the delimiter will be the tab character
        $reader->setNewline("\r\n"); //use windows line endings for compatibility with some csv libraries
        $reader->addFilter(function ($row) {
            return count(array_filter($row));
        });
        $this->headers = $reader->fetchOne(0);
        $countCols = count($this->headers);
        $reader->addFilter(function ($row) use ($countCols) {
            return count($row) == $countCols;
        });
        return $reader->setOffset(1)->fetch(function ($row) {
            return array_map('trim', $row);
        });
    }

    public function getFileDelimiter($file, $checkLines = 2){
        $file = new SplFileObject($file);
        $delimiters = array(
            ',',
            '\t',
            ';',
            '|',
            ':'
        );
        $results = array();
        $i = 0;
        while($file->valid() && $i <= $checkLines){
            $line = $file->fgets();
            foreach ($delimiters as $delimiter){
                $regExp = '/['.$delimiter.']/';
                $fields = preg_split($regExp, $line);
                if(count($fields) > 1){
                    if(!empty($results[$delimiter])){
                        $results[$delimiter]++;
                    } else {
                        $results[$delimiter] = 1;
                    }
                }
            }
            $i++;
        }
        $results = array_keys($results, max($results));
        return $results[0];
    }
}
