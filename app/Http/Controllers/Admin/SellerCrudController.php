<?php

namespace App\Http\Controllers\Admin;

use App\Models\Fee;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use App\Http\Requests\SellerRequest;
use App\Models\Invoice;
use App\Repositories\LotRepository;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use App\Models\Lot;
use App\Models\User;
use Illuminate\Support\Facades\Mail;
use PDF;
use Spatie\Permission\Models\Role;

class SellerCrudController extends CrudController
{
    use SendsPasswordResetEmails;
    /**
     * The lot repository implementation.
     *
     * @var LotRepository
     */
    private $lot;

    /**
     * Create a new controller instance.
     *
     * @param LotRepository $lot
     * @return void
     */
    public function __construct(LotRepository $lot)
    {
        $this->lot = $lot;
        parent::__construct();
    }
    public function setup()
    {

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\User');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/seller');
        $this->crud->setEntityNameStrings('seller', 'sellers');

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */

//        $this->crud->setFromDb();

        // ------ CRUD FIELDS
        // $this->crud->addField($options, 'update/create/both');
        $this->crud->addFields([
            [
                'name' => 'first_name',
                'label' => 'First name'
            ],
            [
                'name' => 'last_name',
                'label' => 'Last name'
            ],
            [
                'name' => 'email',
                'label' => 'Email'
            ],
            [
                'name' => 'phone',
                'label' => 'Phone'
            ],
            [
                'name' => 'company_name',
                'label' => 'Company'
            ],
            [
                'name' => 'terms_link',
                'label' => 'Seller Terms and Conditions'
            ],
            [
                'type' => 'sellers_multiple_fees',
                'label' => 'Auction house fee',
                'name' => 'fees', // the db column for the foreign key
            ]
        ], 'both');
        /*$this->crud->addField(
            [
                'name' => 'password',
                'label' => 'Password',
                'type' => 'password'
            ], 'create');*/
        // $this->crud->removeField('name', 'update/create/both');
        // $this->crud->removeFields($array_of_names, 'update/create/both');

        // ------ CRUD COLUMNS
        $this->crud->setColumns(['first_name', 'last_name', 'email', 'phone', 'company_name', 'terms_link']);
        // $this->crud->addColumn(); // add a single column, at the end of the stack
        // $this->crud->addColumns(); // add multiple columns, at the end of the stack
        // $this->crud->removeColumn('column_name'); // remove a column from the stack
        // $this->crud->removeColumns(['column_name_1', 'column_name_2']); // remove an array of columns from the stack
        // $this->crud->setColumnDetails('column_name', ['attribute' => 'value']); // adjusts the properties of the passed in column (by name)
        // $this->crud->setColumnsDetails(['column_1', 'column_2'], ['attribute' => 'value']);

        // ------ CRUD BUTTONS
        // possible positions: 'beginning' and 'end'; defaults to 'beginning' for the 'line' stack, 'end' for the others;
        // $this->crud->addButton($stack, $name, $type, $content, $position); // add a button; possible types are: view, model_function
        // $this->crud->addButtonFromModelFunction($stack, $name, $model_function_name, $position); // add a button whose HTML is returned by a method in the CRUD model
        // $this->crud->addButtonFromView($stack, $name, $view, $position); // add a button whose HTML is in a view placed at resources\views\vendor\backpack\crud\buttons
        // $this->crud->removeButton($name);
        // $this->crud->removeButtonFromStack($name, $stack);
        // $this->crud->removeAllButtons();
        // $this->crud->removeAllButtonsFromStack('line');
//        $this->crud->removeButton('create');
        $this->crud->removeButton('delete');
        $this->crud->addButtonFromView('line','pass_reset','pass_reset','end');
        // ------ CRUD ACCESS
        $this->crud->allowAccess(['list', 'create', 'update',]);
        // $this->crud->denyAccess(['list', 'create', 'update', 'reorder', 'delete']);

        // ------ CRUD REORDER
        // $this->crud->enableReorder('label_name', MAX_TREE_LEVEL);
        // NOTE: you also need to do allow access to the right users: $this->crud->allowAccess('reorder');

        // ------ CRUD DETAILS ROW
        $this->crud->enableDetailsRow();
        $this->crud->allowAccess('details_row');
        // NOTE: you also need to do allow access to the right users: $this->crud->allowAccess('details_row');
        // NOTE: you also need to do overwrite the showDetailsRow($id) method in your EntityCrudController to show whatever you'd like in the details row OR overwrite the views/backpack/crud/details_row.blade.php

        // ------ REVISIONS
        // You also need to use \Venturecraft\Revisionable\RevisionableTrait;
        // Please check out: https://laravel-backpack.readme.io/docs/crud#revisions
        // $this->crud->allowAccess('revisions');

        // ------ AJAX TABLE VIEW
        // Please note the drawbacks of this though:
        // - 1-n and n-n columns are not searchable
        // - date and datetime columns won't be sortable anymore
        // $this->crud->enableAjaxTable();

        // ------ DATATABLE EXPORT BUTTONS
        // Show export to PDF, CSV, XLS and Print buttons on the table view.
        // Does not work well with AJAX datatables.
        // $this->crud->enableExportButtons();

        // ------ ADVANCED QUERIES
        // $this->crud->addClause('active');
        // $this->crud->addClause('type', 'car');
        // $this->crud->addClause('where', 'name', '==', 'car');
        // $this->crud->addClause('whereName', 'car');
        // $this->crud->addClause('whereHas', 'posts', function($query) {
        //     $query->activePosts();
        // });
        // $this->crud->addClause('withoutGlobalScopes');
        // $this->crud->addClause('withoutGlobalScope', VisibleScope::class);
        // $this->crud->with(); // eager load relationships
        // $this->crud->orderBy();
        // $this->crud->groupBy();
        // $this->crud->limit();
        $this->crud->addClause('role', 'seller');
    }

    public function store(SellerRequest $request)
    {
        $request = $this->setRoleToRequest($request);

        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);

        $this->createFees($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(SellerRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);

        $this->updateFees($request);
        // your additional operations after save here

        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function showDetailsRow($id)
    {
        $invoices = Invoice::where('user_id', $id)
            ->whereBetween('created_at', [Carbon::now()->subYear(), Carbon::now()])
            ->get();
        $statistic = DB::table('lots')
            ->select(
                DB::raw('any_value(sold_date) as `order_date`'),
                DB::raw('MONTH(any_value(sold_date)) as `month`'),
                DB::raw('YEAR(any_value(sold_date)) as `year`'),
                DB::raw("CONCAT_WS('-',MONTH(sold_date),YEAR(sold_date)) as monthyear"),
                DB::raw('sum(sold_amount) as `total`'),
                DB::raw('sum(sold_amount*auction_house_fee/100) as `total_fee`')
            )
            ->where('user_id', $id)
            ->where('status', Lot::STATUS_SOLD)
            ->whereYear('sold_date', '=', Carbon::now()->format('Y') )
            ->latest('order_date')
            ->groupBy('monthyear')
            ->get()
            ->map(function ($item) use ($invoices) {
                $invoice = $invoices->where('year', $item->year)->where('month', $item->month)->first();
                if ($invoice) {
                    $item->invoice_send = true;
                    $item->invoice_number = $invoice->id;
                } else {
                    $item->invoice_send = false;
                    $item->invoice_number = '';
                }
                $item->last_bid_fee = $item->total_fee * env('LAST_BID_FEE')/100;
                $sold = Carbon::parse($item->order_date)->endOfMonth();
                $item->can_send_invoice = $sold->isFuture() ? false:true;
                return $item;
            });
        return view('admin.inc.seller_detail_row',['statistic'=> $statistic, 'sellerId'=>$id]);
    }

    public function sellerMonthDetail($id, $year, $month)
    {
        $this->crud->setColumns([
            [
                'name' => 'id',
                'label' => 'Lot Number'
            ]
            ,[
                'name' => 'title',
                'label' => 'Lot Name'
            ]
            ,[
                'name' => 'sold_date',
                'label' => 'Purchase Date'
            ]
            ,[
                'name' => 'sold_amount',
                'label' => 'Purchase Sum',
                'type' => 'dollar'
            ]
            ,[
                'name' => "buyer_full_name",
                'label' => "Buyer Name",
            ]
            ,[
                'name' => "auction_house_fee_amount",
                'label' => "Auction House Fee",
                'type' => 'dollar'
            ]
            ,[
                'name' => "lastbid_fee_amount",
                'label' => "LastBid Fee",
                'type' => 'dollar'
            ]
        ]);
        $this->crud->disableDetailsRow();
        $this->crud->setModel('App\Models\Lot');
        $this->data['id'] = $id;
        $this->data['crud'] = $this->crud;
        $this->data['title'] = 'Seller sold lots detail statistic';

        $date = $this->data['date'] = Carbon::createFromDate($year, $month, 1);
        $this->data['entries'] = $this->lot->getSellerMonthStatistic($id, $date);
        $invoice = Invoice::where([['user_id', $id],['year', $year],['month',$month]])->first();
        $this->data['invoice'] = [
            'sent' => $invoice ? true:false,
            'can_send' => $date->isCurrentMonth()?false:true,
        ];
        return view('admin.seller_month_details', $this->data);
    }

    public function sendMonthSellerInvoice($id, $year ,$month)
    {
        $date = Carbon::createFromDate($year, $month);
        $comment = request('comment',null);
        if ($date->isCurrentMonth()) {
            return false;
        }
        $user = User::findOrFail($id);
        $invoice = Invoice::where([['user_id', $id], ['month', $date->month], ['year', $date->year]])->first();
        if ( ! $invoice || $comment || request()->has('due_date')) {
            $invoice = $invoice ? $invoice : Invoice::create([
                'user_id' => $id,
                'month' => $date->month,
                'year' => $date->year
            ]);
            $lots = $this->lot->getSellerMonthStatistic($id, $date);
            if (request()->has('due_date')) {
                $invoice->due_date = Carbon::createFromFormat('d/m/Y', request('due_date'));
            } else {
                $invoice->due_date =  Carbon::now()->addDays(10);
            }
            $sellerAccount = $user->accounts()->first();
            $pdf = PDF::loadView('admin.invoices.seller_month_invoice',
                compact(['lots', 'date', 'invoice','comment', 'user', 'sellerAccount']))
                ->save($invoice->storage_path);
            unset($pdf);
        }
        Mail::send('emails.empty', [], function ($message) use ($user, $invoice) {
            $message->to($user->email, $user->full_name)
                ->subject('LastBid Invoice')
                ->attach($invoice->storage_path, [
                    'as' => 'invoice.pdf',
                    'mime' => 'application/pdf',
                ]);
        });
        \Alert::success('Invoice sent!')->flash();
        return redirect()->back();
    }

    public function resetSellerPassword($id)
    {
        $seller = User::find($id);
        if ($seller && $seller->hasRole('seller')) {
            $message = 'Password reset email sent to '.$seller->full_name.' !';
            \Alert::success($message)->flash();
            $response = $this->broker()->sendResetLink(
                ['email' => $seller->email]
            );
        } else {
            \Alert::error('Error! Something went wrong!')->flash();
        }
        return redirect()->back();
    }

    private function setRoleToRequest($request)
    {
        $this->crud->addField(
            [
                'label' => 'Roles',
                'type' => 'select2_multiple',
                'name' => 'roles', // the method that defines the relationship in your Model
                'entity' => 'roles', // the method that defines the relationship in your Model
                'attribute' => 'name', // foreign key attribute that is shown to user
                'model' => config('laravel-permission.models.role'), // foreign key model
                'pivot' => true, // on create&update, do you need to add/delete pivot table entries?
            ], 'create');
//        $request = \Request::instance();
        /*add seller role to user*/
        $seller = Role::findByName('seller');

        $request->request->set('roles', [$seller->id]);

        return $request;
    }

    private function createFees($request)
    {
        $user_id = $this->crud->entry->id;

        foreach($request->fees as $fee_value){
            $fee = new Fee();

            $fee->user_id = $user_id;
            $fee->fee = $fee_value;

            $fee->save();
        }
    }

    private function updateFees($request)
    {
        $user_id = $this->crud->entry->id;

        Fee::where('user_id', $user_id)->delete();

        foreach($request->fees as $current_fee){

            $fee = new Fee();

            $fee->user_id = $user_id;
            $fee->fee = $current_fee ? $current_fee : Fee::DEFAULT_FEE ;

            $fee->save();
        }
    }
}
