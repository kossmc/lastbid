<?php

namespace App\Services;

use App\Models\TermsConditions;
use GuzzleHttp\Client;
use Symfony\Component\DomCrawler\Crawler;

class TermsConditionsCrawlerService {

    /**
     * config list of terms sites
     * @var array
     */

    public $availableDomains = [
        [
            'alias' => 'christies',
            'uri' => 'https://www.christies.com/about-us/contact/terms-and-conditions/?sc_lang=en&lid=1',
            'selector' => 'body #ctl14'
        ],
        [
            'alias' => 'sothebys',
            'uri' => 'https://www.sothebys.com/en/terms-conditions',
            'selector' => 'body .Page-description'
        ],
        [
            'alias' => 'phillips',
            'uri' => 'https://www.phillips.com/buysell/online-only/conditions',
            'selector' => 'body .content-body'
        ],
        [
            'alias' => 'forumauctions',
            'uri' => 'https://www.forumauctions.co.uk/terms-and-conditions',
            'selector' => 'body .item-page'
        ],
        [
            'alias' => 'maaklondon',
            'uri'=> 'http://www.maaklondon.com/terms-and-conditions',
            'selector' => 'body .col-md-8'
        ]
    ];

    /**
     * TermsConditionsCrawlerService constructor.
     */
    public function __construct()
    {
        $headers = ['Cookie' => 'ak_bmsc=parse;'];
        $this->client = new Client(['http_errors' => false, 'headers' => $headers]);
    }

    /**
     * Start parsing terms and conditions
     * @return array|bool
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function startCrawling()
    {
        foreach ($this->availableDomains as $domain){
            $response = $this->client->request('GET', $domain['uri']);
            if ($response->getStatusCode() != 200) {
                \Log::info(print_r($response, true));
                return false;
            }
            $page = new Crawler($response->getBody()->getContents(), $domain['uri']);
            $this->store($domain['alias'], $page->filter($domain['selector'])->html());
        }
        return true;
    }

    /**
     * Store terms and conditions to DB
     * @param $alias
     * @param $text
     */
    public function store($alias, $text)
    {
        $term = TermsConditions::firstOrNew(['alias' => $alias]);
        $term->text = $this->format($text);
        $term->save();
    }

    /**
     * delete phrases, tags, html attributes, add custom style
     * @param $text
     * @return string
     */
    public function format($text)
    {

        $toDelete = [
            'Sotheby\'s Digital Platform Terms and Conditions',
            'Back to top',
            'You may direct questions about these Terms and Conditions, or other issues, to:Sotheby\'s1334 York AvenueNew York, New York 10021Attn: Website ManagementLast Modified: April 7, 2011',
            'Last Modified: April 7, 2011',
            'https://www.forumauctions.co.uk/terms-and-conditions',
            'Terms and Conditions',
            'INCORPORATING TERMS OF CONSIGNMENT AND TERMS OF SALE',
            'General Information for Buyers at Auction',
            'Please refer to the ',
            '<a href="/privacy-policy">Maak Privacy Policy</a>',
            '<h2></h2>',
            '<h1><strong><br></strong></h1>',
            '<h1><a href="https://phillips.vo.llnwd.net/v1/web_prod/docs/forms/Conditions_of_online_only_sale_NY.pdf" target="_blank" class="download">CONDITIONS OF ONLINE-ONLY SALE</a></h1>',
            '<p><em>Effective as of October 19, 2018</em></p>'
        ];
        foreach ($toDelete as $pattern){
            $text = str_replace($pattern, "", $text);
        }
        $text = str_replace("<h1>", '<p style="text-align: left;font-size: 12px;">', $text);
        $text = str_replace("<h2>", '<p style="text-align: left;font-size: 12px;">', $text);
        $text = str_replace("</h1>", "</p>", $text);
        $text = str_replace("</h2>", "</p>", $text);
        $text = str_replace("<p>", '<p style="text-align: left;font-size: 12px;">', $text);
        $text = str_replace('<p style="padding-left: 60px;">', '<p style="text-align: left;font-size: 12px;">', $text);
        $text = str_replace('<p style="padding-left: 30px;">', '<p style="text-align: left;font-size: 12px;">', $text);

        $text = str_replace('<ol class="standard-list">', '', $text);
        $text = str_replace('</ol>', '', $text);
        $text = str_replace('<li class="has-icon">', '', $text);
        $text = str_replace('</li>', '', $text);

        $text = str_replace('<ul>', '', $text);
        $text = str_replace('</ul>', '', $text);
        $text = str_replace('<li>', '', $text);
        $text = str_replace('</li>', '', $text);

        for ($i =1; $i < 20; $i++){
            $text = str_replace('<div class="icon number long">'.$i.'</div>', "</p>", $text);
        }

        return trim($text);
    }

    /**
     *
     * trim special characters
     * @param $content
     * @return mixed
     */
    public function processContent($content) {
        return str_replace(["\n\r", "\0", "\x0B", "\n", "\r", "\t", PHP_EOL], '', $content);
    }
}