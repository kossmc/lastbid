<?php
namespace App\Repositories;

use Bosnadev\Repositories\Eloquent\Repository;
use App\Models\Category;

class CategoryRepository extends Repository
{
    public function model()
    {
        return Category::class;
    }

    public function featured($withChildren = false, $withLots = false )
    {
        $query = $this->model->featured();
        $withLots ? $query->with('latestLots') :'';
        $withChildren ? $query->with(['children'=>function($q){
            $q->has('latestLots')->with('latestLots');
        }]) :'';
        return $query->orderBy('lft')->get();
    }

    public function roots()
    {
        return $this->model->roots()
            ->whereHas('lots', function ($query) {
                $query->active();
            })
            ->with('specifics')
            ->with(['children'=> function ($query) {
                $query->whereHas('lots', function ($query) {
                    $query->active();
                });
            }])
            ->orderBy('lft')->get();
    }

    public function other()
    {
        return $this->model->other();
//            ->whereHas('lots', function ($query) {
//                $query->active();
//            })
//            ->with(['children'=> function ($query) {
//                $query->whereHas('lots', function ($query) {
//                    $query->active();
//                });
//            }])
//            ->orderBy('lft')->get();
    }

    public function findBySlug($slug)
    {
        return $this->model->where('slug', $slug)->with(['parent', 'children'])->first();
    }
}
