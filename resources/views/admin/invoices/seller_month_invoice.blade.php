@extends('admin.invoices.invoice_layout')
@section('top_left_col')
    <p> From: LastBid UK Ltd<br>
        4106 Stratford Park<br>
        IN, Patricksburg, 47455<br>
        tel/fax: +812-859-7918<br>
        <br></p>
    <p> To: {{$user->full_name}}
    @if($sellerAccount)
        <br>
        {{$sellerAccount->address}}<br>
        {{$sellerAccount->city}}{{$sellerAccount->zip?', '.$sellerAccount->zip:''}}<br>
    @endif
    </p>
@endsection
@section('bottom_left_col')
    <p> Bank Account Number: 11111111<br>
        Bank Routing Number: 1111-1111-1</p>
@endsection
@section('top_right_col')
    <p>Invoice No: {{$invoice->id}}<br>
        Date: {{$invoice->created_at->format('d/m/Y')}}<br>
        Due Date: {{$invoice->due_date->format('d/m/Y')}}
    </p>
@endsection
@section('message')
    @if ($comment)
        <div class="row" style="margin-top: 20px;margin-bottom: 20px">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <p><b>Message:</b></p>
                <p>{{$comment}}</p>
            </div>
        </div>
    @endif
@endsection
@section('table')
    <table class="table table-bordered">
        <thead>
        <tr>
            <th class="text-nowrap">LastBid Lot No</th>
            <th>Lot Name</th>
            <th class="text-nowrap">Price Sold</th>
            <th class="text-nowrap">LastBid Commission</th>
        </tr>
        </thead>
        <tbody>
        @php($totalSold = $totalFee = 0)
        @foreach($lots as $lot)
            <tr class="text-center">
                <td>{{$lot->id}}</td>
                <td class="text-left">{{$lot->title}}</td>
                <td>{!! format_money($lot->sold_amount) !!}</td>
                <td>{!! format_money($lot->lastbid_fee_amount) !!}</td>
                @php($totalSold += $lot->sold_amount)
                @php($totalFee += $lot->lastbid_fee_amount)
            </tr>
        @endforeach
        </tbody>
        <tfoot>
        <tr class="text-center">
            <td colspan="2" class="text-left">TOTALS:</td>
            <td>${!! format_money($totalSold) !!}</td>
            <td>${!! format_money($totalFee) !!}</td>
        </tr>
        </tfoot>
    </table>
@endsection
