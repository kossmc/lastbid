<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserSubscriptionsStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'subscriptions' =>'nullable|array',
            'subscriptions.*' =>'nullable|integer|exists:subscriptions,id',
            'un_subscribe' => 'nullable|integer'
        ];
    }
    public function messages()
    {
        return [
            'subscriptions.*' =>'Unknown subscription found!'
        ];
    }
}
