<div class="modal fade form-first-step-register" id="modal-id">
<div class="container-form-wrap log-in">
    <a href="#" class="esc" data-dismiss="modal" aria-hidden="true">
        <em class="icon-esc_icon"></em>
        esc
    </a>
    <div class="container-forms">
        <div class="container-info">
            <div class="info-item">
                <div class="table">
                    <div class="table-cell">
                        <div class="text">If you are already a LastBid account holder click here to log-in
                        </div>
                        <div class="btn" onclick="auth('login_form')">
                            Login
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <form action="" id="register-form">
            <div class="container-form">
                <div class="form-item sign-up">
                    <div class="table">
                        <div class="table-cell">
                            <div class="f-sign text-center">
                                <div class="title">Registration</div>

                                <div id="auth-tab1">

                                    
                                    @if (session()->has('socialAuth'))
                                    @php($user = session()->get('socialAuth'))
                                    <div class="sign-with-account" style="display: block;">
                                        <div class="row">
                                            <div class="inp-wrap">
                                                <input type="text" placeholder="First name" name="first_name"
                                                        required autofocus value="{{ old('first_name',
                                                isset($user->user['name']['givenName'])
                                                ?$user->user['name']['givenName']:$user->name) }}"
                                                        class="{{ $errors->has('first_name') ? 'has-error--bgcolor' : '' }}">
                                                @if ($errors->has('first_name'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('first_name') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                            <div class="inp-wrap">
                                            <input type="text" placeholder="Last name" name="last_name" required
                                            value="{{ old('last_name',isset($user->user['name']['familyName'])?$user->user['name']['familyName']:'') }}"
                                                    class="{{ $errors->has('last_name') ? 'has-error--bgcolor' : '' }}">
                                                @if ($errors->has('last_name'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('last_name') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="inp-wrap">
                                                <input type="text" placeholder="Your email" name="email" required
                                                    value="{{ old('email', $user->email) }}"
                                                        class="{{ $errors->has('email') ? 'has-error--bgcolor' : '' }}">
                                                @if ($errors->has('email'))
                                                    @include('auth.inc.email_error_reset_link')
                                                @endif
                                            </div>
                                            <div class="inp-wrap two clearfix">
                                                <input disabled type="password" placeholder="Password"
                                                        name="password" required>
                                                <input disabled type="password" placeholder="Repeat password"
                                                    name="password_confirmation" required id="password-confirm">
                                            </div>
                                        </div>
                                    </div>
                                    @else
                                        {{--<div class="row wrapper-sign-up-with">--}}
                                            {{--<a href="{{ route('socialAuth',['provider'=>'facebook']) }}"><span--}}
                                                        {{--class="btn-sign-up-with sign-up-with-fb">facebook</span></a>--}}
                                            {{--<a href="{{ route('socialAuth',['provider'=>'google']) }}">--}}
                                                {{--<span class="btn-sign-up-with sign-up-with-google">google</span>--}}
                                            {{--</a>--}}
                                        {{--</div>--}}
                                    <div class="sign-without-account">
                                        <div class="row">
                                            <div class="inp-wrap">
                                                <input type="text" placeholder="First name" name="first_name"
                                                    value="{{ old('first_name') }}" required autofocus
                                                        class="{{ $errors->has('first_name') ? 'has-error--bgcolor' : '' }}">
                                                @if ($errors->has('first_name'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('first_name') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                            <div class="inp-wrap">
                                                <input type="text" placeholder="Last name" name="last_name"
                                                    value="{{ old('last_name') }}" required
                                                        class="{{ $errors->has('last_name') ? 'has-error--bgcolor' : '' }}">

                                                @if ($errors->has('last_name'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('last_name') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="inp-wrap">
                                                <input type="text" placeholder="Your email" name="email"
                                                    value="{{ old('email') }}" required
                                                        class="{{ $errors->has('email') ? 'has-error--bgcolor' : '' }}">
                                                @if ($errors->has('email'))
                                                    @include('auth.inc.email_error_reset_link')
                                                @endif
                                            </div>
                                            <div class="inp-wrap two clearfix">
                                                <input type="password" placeholder="Password" name="password" required
                                                        class="{{ $errors->has('password') ? 'has-error--bgcolor' : '' }}">

                                                <input type="password" placeholder="Repeat password"
                                                    name="password_confirmation" required id="password-confirm">
                                                @if ($errors->has('password'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('password') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                    </div>
                                    @endif
                                        <div class="row">
                                            <div class="wrapper-sign-up-agreement">
                                                <div class="inner-wrapper-sign-up-agreement">
                                                    @if(isset($terms))
                                                        {!! $terms !!}
                                                    @else
                                                        @include('inc.terms')
                                                    @endif
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row wrap-check-policy">
                                        
                                            <p>Please read all terms and conditions to be able to sign.</p>
                                            <input disabled type="checkbox" id="sign-up-policy-agreement"
                                                    name="terms">
                                            <label class="disabled-label" for="sign-up-policy-agreement">
                                                I accept the Terms &amp; Conditions and Privacy Policy
                                            </label>

                                            <div class="btn-wrap">
                                                <a disabled class="btn btn-sign-up-next-step btn-sign-up-disabled"
                                                    href="javascript: void 0;" onclick="auth('register')">next step</a>
                                            </div>

                                        </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <input type="hidden" name="ip" value="" id="ip">
        </form>
    </div>
</div>
</div>
