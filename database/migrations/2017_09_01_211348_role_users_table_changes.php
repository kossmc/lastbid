<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RoleUsersTableChanges extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasTable('role_users')){
            Schema::rename('role_users', 'role_user');
        }
        if(Schema::hasTable('permission_users')){
            Schema::rename('permission_users', 'permission_user');
        }
        if(Schema::hasTable('permission_roles')){
            Schema::rename('permission_roles', 'permission_role');
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::rename('user_role', 'role_users');
        Schema::rename('permission_user', 'permission_users');
        Schema::rename('permission_role', 'permission_roles');
    }
}
