<?php

namespace App\Listeners\Message;

use App\Models\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class SendGridAbstractMessageListener implements ShouldQueue
{
    protected $message;
    protected $sender;
    protected $recipient;
    protected $lot;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function setData($event)
    {
        $this->message = $event->message;
        $this->sender = $this->message->sender;
        $this->recipient = $this->message->recipient;
        $this->lot = $this->message->lot;
    }

    protected function getData($id)
    {
        $data = [
            "sub"=>[
                "-recipientName-"=>[$this->recipient->full_name]
                ,"-senderName-"=>[$this->sender->short_name]
                ,"-lotName-"=>[$this->lot->title]
                ,"-lotLink-"=>[$this->lot->url]
                ,"-siteLink-" => [route('home')]
                ,"-currentYear-"=>[date('Y')]
                ,"-emailPreferencesLink-" => [route('subscriptions')]
                ,"-messageDate-"=>[$this->message->latest->created_at->setTimezone($this->recipient->timezone?$this->recipient->timezone:'America/New_York')->format('j F Y, h:i A')]
                ,"-messageLink-"=>[$this->message->url]
            ],
            "filters"=>[
                "templates"=>[
                    "settings"=>[
                        "enable"=>1,
                        "template_id"=>$id
                    ]
                ],
                "subscriptiontrack"=>[
                    "settings"=>[
                        "enable"=>1,
                        "replace"=>'[unsubscribe]'
                    ]
                ]
            ]
        ];
        return asJSON($data);
    }

    protected function sendMail(User $to, $data, $subject)
    {
        Mail::send('emails.empty',[], function ($message) use($to, $subject, $data) {
            $message->getSwiftMessage()->getHeaders()->addTextHeader('X-SMTPAPI', $data);
            $message->to($to->email, $to->full_name)->subject($subject);
        });
    }
}
