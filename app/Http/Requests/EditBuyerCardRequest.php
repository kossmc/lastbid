<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EditBuyerCardRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'address' => 'required|string|max:190',
            'city' => 'required|string|max:190',
            'country' => 'required|string|max:190',
        ];
    }
}
