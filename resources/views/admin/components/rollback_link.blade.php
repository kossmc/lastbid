
@if($model->status == \App\Models\LotFile::STATUS_DONE)
    <button class="btn" disabled>Done</button>
@else

    <button type="button" class="btn btn-default" data-toggle="modal" data-target="#exampleModal">
        Rollback
    </button>

    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Are you sure you want to rollback lots?</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <form action="{{url('admin\lot-file\rollback', $model->id)}}">

                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Rollback</button>
                    </div>

                </form>
            </div>
        </div>
    </div>
@endif