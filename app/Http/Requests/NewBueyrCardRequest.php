<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class NewBueyrCardRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'cc'=> ['required', 'string', 'min:17', 'max:19'],
            'card_name'=>'required|string|max:190',
            'address'=>'required|string|max:190',
            'city'=>'required|string|max:190',
            'country'=>'required|string|max:190',
            'card_cvv'=>'required|max:4|min:3',
            'month'=>'required',
            'year'=>'required',
        ];
    }
    public function messages()
    {
        return [
          'cc.regex' => 'Card number should be from 15 digits to 16 digits'
        ];
    }
}
