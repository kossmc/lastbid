<?php

namespace App\Listeners\User;

use App\Events\User\BuyerDeleteCard;
use Illuminate\Contracts\Queue\ShouldQueue;

class BuyerDeleteCardListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  BuyerDeleteCard  $event
     * @return void
     */
    public function handle(BuyerDeleteCard $event)
    {
        //
    }
}
