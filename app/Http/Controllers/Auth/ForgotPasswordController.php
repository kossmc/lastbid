<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;
use Validator;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function ajax_forgot_form()
    {
        return response()->json([
            'status' => false,
            'form' => view()->make('auth.modal_forgot_pass')->render()
        ]);
    }

    public function ajax_forgot_pass(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email|max:255',
        ]);

        if ($validator->fails()) {
            $responseData = [
                'errors' => $validator->errors(),
            ];

            return $this->sendResetLinkResponse($request, $responseData, false);
        }

        $this->broker()->sendResetLink(
            $request->only('email')
        );

        $responseData = [
            'message' => trans(Password::RESET_LINK_SENT),
        ];

        return $this->sendResetLinkResponse($request, $responseData, true);
    }

    /**
     * @param Request $request
     * @param array $data
     * @param bool $status
     * @return \Illuminate\Http\JsonResponse
     */
    private function sendResetLinkResponse(Request $request, $data, $status)
    {
        $request->flash();

        $form = view()->make('auth.modal_forgot_pass', $data)
            ->render();

        return response()->json([
            'status' => $status,
            'form'   => $form,
        ]);
    }
}
