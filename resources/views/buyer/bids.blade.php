@extends('layouts.inner')
@section('title', 'My bids')
@section('inner_content')
    <ul class="nav nav-tabs profile-tabs-nav lots-tabs-nav">
        <li{!! isActiveRoute('buyer_received_bids') ? ' class="active"' : '' !!}><a href="{{ route('buyer_received_bids') }}">Received bids</a></li>
        <li{!! isActiveRoute('buyer_sent_bids') ? ' class="active"' : '' !!}><a href="{{ route('buyer_sent_bids') }}">Sent bids</a></li>
        <li{!! isActiveRoute('buyer_declined_bids') ? ' class="active"' : '' !!}><a href="{{ route('buyer_declined_bids') }}">Declined / expired bids</a></li>
    </ul>
    @if (count($bids))
        <div class="tab-content profile-tab-content">
            <div class="tab-pane tab-bids fade in active">
                <div class="tab-inner">
                    <div class="filter-wrapper">
                        <div class="filter filter-default">
                            <div class="item">
                                <div class="name">Sort by</div>
                                <div class="select"><select>
                                        <option>Start date</option>
                                    </select></div>
                                <div class="sorting">
                                    <a href="javascript:void 0;" class="icon-sorting_up
                                @if(request('sort_latest', '') == 'ASC') icon-sorting--active @endif"></a>
                                    <a href="javascript:void 0;" class="icon-sorting_up down
                                @if(request('sort_latest', '') == 'DESC') icon-sorting--active @endif"></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="bids-list lots-list">
                        @foreach ($bids as $bid)
                            <div class="lots-list-i buyer-card--received">
                                @include('buyer.inc.short_bid', ['lotLink' => true])
                            </div>
                        @endforeach
                     </div>
                </div>
            </div>
        </div>
        @php request()->has('sort_latest') ? $append = ['sort_latest'=>request('sort_latest')]:$append = []; @endphp
        @include('inc.paginator_with_page_selector',
                ['items'=>$bids, 'append'=>$append])
    @else
        <p class="notification-no-content">No bids</p>
    @endif
@endsection
