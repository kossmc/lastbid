<?php

namespace App\Console\Commands;

use App\Jobs\TermsConditionsJob;
use App\Services\TermsConditionsCrawlerService;
use Illuminate\Console\Command;

class FetchTermsConditions extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fetch:terms';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to fetch terms and conditions';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $job = new TermsConditionsJob(TermsConditionsCrawlerService::class);
        dispatch($job);
    }
}
