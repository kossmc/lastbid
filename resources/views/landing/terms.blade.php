@extends('landing.content')
@section('title','Terms and condition')

@section('content_page')
    <div class="page-wrap page-terms">
        <div class="wrap">
            <div class="clearfix">
                <div class="content">
                    <div class="title-evets">
                        <h3>TERMS AND CONDITIONS</h3>
                    </div>
                    <div class="info">
                        @if(isset($terms))
                            {!! $terms !!}
                        @else
                            @include('inc.terms')
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection