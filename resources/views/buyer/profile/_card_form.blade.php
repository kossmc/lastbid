<div class="form-controll form-controll--medium">
    <label>Card owner</label>
    <input type="text" name="card_name" placeholder="Name"
           @if($edit){{' disabled '}}@endif
           value="{{old('card_name', $card->card_name)}}"
           class="{{ $errors->has('card_name') ? 'has-error--bgcolor' : '' }}">
</div>
<div class="form-controll form-controll--medium">
    <label>Company name (optional)</label>
    <input type="text" name="company" placeholder="Company name"
           value="{{old('company', $card->company)}}"
           class="{{ $errors->has('company') ? 'has-error--bgcolor' : '' }}">
</div>
@if ($edit)
    <div class="form-controll form-controll--medium">
        <label>Card number</label>
        <div class="disabled-wrapper"><span class="card_placeholder"> {{$card->card_number}}</span><span
                    class="card_placeholder-ax"> {{$card->card_number}}</span></div>
    </div>
@else
    <div class="form-controll form-controll--medium">
        <label>Card number</label>
        <input class="ccFormatMonitor{{ $errors->has('cc') ? ' has-error--bgcolor' : '' }}" type="text" name="cc"
               placeholder="Number" value="{{old('cc',$card->card_number )}}">
    </div>
    <div class="form-controll form-controll--smallest">
        <label>CVV</label>
        <input type="password" name="card_cvv" placeholder="Code" value="{{old('card_cvv')}}"
               class="{{ $errors->has('card_cvv') ? 'has-error--bgcolor' : '' }}">
    </div>
    <div class="form-controll form-controll--newline form-controll--small">
        <label>Expiration Month</label>
        <div class="select-style"><span
                    class="card_month @if(old('month'))selected-suffix @endif">{{old('month', 'Month')}}</span>
            <ul class="select-month" style="display: none;">
                <li>January</li>
                <li>February</li>
                <li>March</li>
                <li>April</li>
                <li>May</li>
                <li>June</li>
                <li>July</li>
                <li>August</li>
                <li>September</li>
                <li>October</li>
                <li>November</li>
                <li>December</li>
            </ul>
            <select name="month" style="display: none;">
                <option value="{{old('month')}}"></option>
            </select>
        </div>
    </div>
    <div class="form-controll form-controll--small">
        <label>Expiration Year</label>
        <div class="select-style"><span class="card_year @if(old('year'))selected-suffix @endif">
            {{old('year', 'Year')}}</span>
            <ul class="select-year" style="display: none;">
                <li>2017</li>
                <li>2018</li>
                <li>2019</li>
                <li>2020</li>
                <li>2021</li>
            </ul>
            <select name="year" style="display: none;">
                <option value="{{old('year')}}"></option>
            </select>
        </div>
    </div>
@endif
<div class="form-controll form-controll--newline">
    <h3>Billing address </h3>
</div>
<div class="form-controll form-controll--newline form-controll--medium">
    <label>Address</label>
    <input type="text" name="address" placeholder="Address"
           value="{{old('address', $card->address)}}"
           class="{{ $errors->has('address') ? 'has-error--bgcolor' : '' }}">
</div>
<div class="form-controll form-controll--newline form-controll--small">
    <label>City</label>
    <input type="text" name="city" placeholder="City"
           value="{{old('city', $card->city)}}"
           class="{{ $errors->has('city') ? 'has-error--bgcolor' : '' }}">
</div>
<div class="form-controll form-controll--small">
    <label>State / province</label>
    <input type="text" name="state" placeholder="State"
           value="{{old('state',$card->state)}}"
           class="{{ $errors->has('state') ? 'has-error--bgcolor' : '' }}">
</div>
<div class="form-controll form-controll--small form-controll--newline">
    <label>Country</label>
    <input type="text" name="country" placeholder="Country"
           value="{{old('country', $card->country)}}"
           class="{{ $errors->has('country') ? 'has-error--bgcolor' : '' }}">
</div>
<div class="form-controll form-controll--small">
    <label>Post code / zip</label>
    <input type="text" name="zip" placeholder="Code"
           value="{{old('zip', $card->zip)}}"
           class="{{ $errors->has('zip') ? 'has-error--bgcolor' : '' }}">
</div>
{{csrf_field()}}
