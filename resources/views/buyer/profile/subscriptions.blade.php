@extends('buyer.profile.profile')
@section('title', 'Subscriptions')
@section('profile_content')
    <div class="buyer-account">
        <form action="{{route('subscriptions-save')}}" method="POST">
            {{csrf_field()}}
            <section class="subscriptions">
                <h3>Active subscriptions</h3>
                @include('inc.top_errors_list')
                <p>Please tell us the types of emails you would like to receive.</p>
                    @foreach($subscriptions as $subscription)
                    <div class="form-controll">
                        <input type="checkbox" id="{{'unique'.$subscription->id}}"
                               value="{{$subscription->id}}" name="subscriptions[]"
                               @if($subscription->selected){{' checked="checked"'}}@endif>
                        <label for="{{'unique'.$subscription->id}}">{{$subscription->name}}</label>
                    </div>
                    @endforeach
            </section>
            <section class="subscriptions">
                <p>Don’t want to receive any emails?</p>
                    <div class="form-controll">
                        <input type="checkbox" id="unique" name="un_subscribe" value="1">
                        <label for="unique">Unsubscribe from all marketing emails</label>
                    </div>
                    <div class="form-controll">
                        <div class="btn-wrap"><button type="submit" class="btn">Save changes</button></div>
                    </div>
            </section>
        </form>
    </div>
@endsection
