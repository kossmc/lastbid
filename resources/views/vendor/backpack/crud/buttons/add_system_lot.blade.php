<a href="{{ url($crud->route.'/create?system=1') }}" class="btn btn-default ladda-button" data-style="zoom-in">
    <i class="fa fa-fw fa-gears"></i><span class="ladda-label"> Add system lot</span>
</a>