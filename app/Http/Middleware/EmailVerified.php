<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class EmailVerified
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!Auth::user()->email_confirmed) {
            return response()->json([
                'status' => false,
                'form' => view()->make('auth.modal_confirmation_sent')->render()
            ]);
        } else {
            return $next($request);
        }
    }
}
