<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Session;
use App\Models\Product;
use App\Repositories\LotRepository;
use App\Repositories\BidRepository;
use App\Http\Requests\PlaceBidRequest;

class BuyerController extends Controller
{

    /**
     * The lot repository implementation.
     *
     * @var LotRepository
     */
    private $lot;

    /**
     * The bid repository implementation.
     *
     * @var BidRepository
     */
    private $bid;

    /**
     * Create a new controller instance.
     *
     * @param LotRepository $lot
     * @param BidRepository $lot
     * @return void
     */
    public function __construct(LotRepository $lot, BidRepository $bid)
    {
        $this->lot = $lot;
        $this->bid = $bid;
    }

    public function receivedBids()
    {
        $bids = $this->bid->paginateReceived();
        return view('buyer.bids', compact('bids'));
    }

    public function sentBids()
    {
        $bids = $this->bid->paginateSent();
        return view('buyer.bids', compact('bids'));
    }

    public function declinedBids()
    {
        $bids = $this->bid->paginateDeclined();
        return view('buyer.bids', compact('bids'));
    }

    public function lot(Request $request)
    {
        $lot = $request->lot;
        $bid = $this->bid->getMyBid($lot->id);
        return view('buyer.lot', compact('lot', 'bid'));
    }
    
    public function switchBidStatus(Request $request, $method)
    {
        $bid = $request->bid;
        switch ($method) {
            case 'accept':
                $this->bid->acceptByBuyer($bid);
                Session::flash('message', 'Bid was accepted');
                break;
            case 'decline':
                $this->bid->declineByBuyer($bid);
                Session::flash('message', 'Bid was declined');
                break;
        }
        return redirect()->back();
    }
    
    public function counterBid(Request $request, PlaceBidRequest $formRequest)
    {
        $bid = $request->bid;
        $data = [
            'user_id' => Auth::id(),
            'lot_id' => $bid->lot_id,
            'amount' => $formRequest->amount,
            'message' => $formRequest->message
        ];
        $this->bid->counterByBuyer($bid, $data);
        Session::flash('message', 'Bid was countered');
        return response()->json([
            'status' => true,
            'link' => URL::previous()
        ]);
    }

}
