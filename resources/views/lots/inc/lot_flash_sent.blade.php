<div class="lot-top-notification">
    Your message to {{$message->recipient->company_name}} has been sent.
        <a href="{{$message->url}}" target="_blank"><span>View message</span></a>
</div>
