# Installation Overview
**1. Install prerequisites.**
The LastBid project will not run without the following:
- nginx
- php-fpm
- mysql
- git
- memcached
- gearman
- composer
- test

**2. Clone LastBid project.**
First  genarate SSH key and add it at github.com.
Then run next command in `/path_to_project` directory.
```sh
git clone https://github.com/GetCider/LastBid.git
```

**3. Configure nginx.**
Add next text to `/path-to-nginx-config-files-directory/lastbid.conf`.
```sh
server {
    listen       80;
    server_name getcider.com;

    # note that these lines are originally from the "location /" block
    root   /path_to_project/public;
    index index.php index.html index.htm;

    location ~*^/(\d+|-)x(\d+|-)/(.*\.(?:jpg|gif|png))$ {
        error_log /path_to_logs_dir/image_error.log;

        image_filter resize $1 $2;
        image_filter_buffer 10M;

        add_header  Cache-Control "max-age=604800";

        try_files /$3 @images;
    }

    location @images {
        internal;
        root /path_to_project/public;
    }

    location / {
        try_files $uri $uri/ /index.php?$query_string;
    }
    location ~ \.php$ {
        try_files $uri /index.php =404;
        fastcgi_split_path_info ^(.+\.php)(/.+)$;
        fastcgi_pass unix:/path_to_php-fpm_socket;
        fastcgi_index index.php;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        include fastcgi_params;
    }
    access_log /path_to_logs_dir/site_access.log combineduid;
    error_log /path_to_logs_dir/site_error.log;
}
```

Then reload nginx configuration using next command.
```sh
nginx -s reload
```

**4. Run the install script**
Run next command to install external libraries.
```sh
composer update
```

**5. Create MySQL database.**

**6. Configure application.**
Copy `/path_to_project/.env.example` file to `/path_to_project/.env`.
Then add right configuration information to the new file.
- database access info
- application URL info
- mail driver info

**7. Run migrations script using next command in `/path_to_project/` directory.**
```sh
php artisan migrate
```
**8. Crontab 
```
crontab -u www-data -e
```