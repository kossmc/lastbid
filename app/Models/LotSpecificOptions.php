<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LotSpecificOptions extends Model
{
    protected $table = 'lot_specific_options';
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];
    public $timestamps = false;
}
