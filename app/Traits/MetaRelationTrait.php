<?php

namespace App\Traits;

use App\Models\MetaData;

trait MetaRelationTrait
{
    public static $meta_fields = [[
        'name' => 'meta_description',
        'type' => 'textarea',
        'label' => 'Meta description'
    ]];

    public function metas()
    {
        return $this->morphMany('App\Models\MetaData', 'metable');
    }

    abstract function genMetaDescription();

    public function getMetaDescriptionAttribute()
    {
        $meta = MetaData::where('metable_type', get_class($this))->where('metable_id', $this->id)->first();
        if($meta) return $meta->content;

        return $this->genMetaDescription();
    }

    public function save(array $options = [])
    {
        if(parent::save($options) && request('meta_description')){
            $this->setMetaDescriptionAttribute(request('meta_description'));
        }
    }

    public function setMetaDescriptionAttribute($desc)
    {
        if(!$this->id) return;
        $meta = MetaData::where('metable_type', get_class($this))->where('metable_id', $this->id)->first();

        if(!$meta){
            $meta = new MetaData();
        }

        $meta->metable_type = get_class($this);
        $meta->metable_id = $this->id;
        $meta->name = 'description';
        $meta->content = $desc;
        $meta->save();
    }
}