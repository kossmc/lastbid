#!/usr/bin/env bash
echo "Purge apache..."
sudo service apache2 stop
sudo apt-get purge apache2 apache2-utils -y
sudo apt-get autoremove -y
sudo rm -rf /etc/apache2
sudo update-rc.d nginx enable
sudo service nginx restart