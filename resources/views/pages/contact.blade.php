@extends('layouts.main')

@include('inc.page_metas', ['defTitle' => 'Contact Us'])

@section('top-links')
    @include('inc.top_links')
@stop
@section('content')
   <div class="page-wrap">
       <input type="hidden" id="latitude" value="{!! $data->latitude !!}"/>
       <input type="hidden" id="longitude" value="{!! $data->longitude !!}"/>
        <div class="wrap">
            <div class="contact-page clearfix">
                <div class="content">
                    <div class="title-evets">
                        <h1>Contact us</h1>
                    </div>
                    <div class="info">
                    {!! $data->text !!}
                    </div>
                    <div class="call-wrap clearfix">
                        <div class="col urlsite">
                            <p>
                                <span>For all LastBid new registration enquiries:</span>
                                <a href="mailto:{!! $data->registration_email !!}">{!! $data->registration_email !!}</a>
                            </p>
                            <p>
                                <span>For all auction house enquiries and
                                to list your unsold lots with LastBid:</span>
                                <a href="mailto:{!! $data->auction_email !!}">{!! $data->auction_email !!}</a>
                            </p>
                            <p>
                                <a href="https://www.lastbid.com">www.lastbid.com</a>
                            </p>
                        </div>
                        <div class="col">
                            <p class="icon-contact_ph_icon">
                                <a href="tel:{!! $data->phone !!}">{!! $data->phone !!}</a>
                            </p>
                            <p class="icon-mail">
                                <a href="mailto:{!! $data->email !!}">{!! $data->email !!}</a>
                            </p>
                        </div>
                    </div>
                    <div class="map-wrap">
                        <div id="map" class="image"></div>
                        <div class="desc">
                            {{--LastBid (UK) Ltd<br>--}}
                            {{--1st Floor  <br>--}}
                            {{--16 Pall Mall <br>--}}
                            {{--St James’s <br>--}}
                            {{--London <br>--}}
                            {{--SW1Y 5LU <br>--}}
                            {{--United Kingdom--}}
{{--                            LastBid Investments Ltd<br>--}}
{{--                            Les Echelons Court, Les Echelons,<br>--}}
{{--                            St Peter Port<br>--}}
{{--                            Guernsey GY1 1AR.--}}
                            {!! $data->address !!}
                        </div>
                    </div>
                </div>
                <div class="email-wrap">
                    <div class="title">Email</div>
                    <form method="post" action="{{route('contact-send')}}">
                        @include('inc.flash_message')
                        @include('inc.top_errors_list')
                        <div class="item">

                            <input type="text" placeholder="Name" name="name" required
                            value="{{old('name', $user->first_name?$user->first_name." ".$user->last_name:'')}}"
                                   class="{{ $errors->has('name') ? 'has-error--bgcolor' : '' }}">
                        </div>
                        <div class="item">
                            <input type="text" placeholder="Email" name="email" required
                                   value="{{old('email', $user->email?$user->email:'')}}"
                                   class="{{ $errors->has('email') ? 'has-error--bgcolor' : '' }}">
                        </div>
                        <div class="item">
                            <textarea placeholder="Message" name="message" required
                                      class="{{ $errors->has('message') ? 'has-error--bgcolor' : '' }}">{{old('message')
                                      }}</textarea>
                        </div>
                        <div class="item">
                            <button class="btn" type="submit">Submit</button>
                        </div>
                        {{csrf_field()}}
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('remote_js_scripts')
    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDIuqVBVrcKzc2Nq_WLF9xsCMxs9gv3cjE&callback=initMap">
    </script>
@stop
