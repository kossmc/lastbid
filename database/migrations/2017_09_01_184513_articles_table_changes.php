<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ArticlesTableChanges extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::rename('articles', 'events');
        Schema::table('events', function (Blueprint $table) {
            $sql = 'UPDATE `events` SET `image`=REPLACE(`image`, \'articles/\', \'events/\')';
            DB::connection()->getPdo()->exec($sql);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::rename('events', 'articles');
        Schema::table('articles', function (Blueprint $table) {
            $sql = 'UPDATE `articles` SET `image`=REPLACE(`image`, \'events/\', \'articles/\')';
            DB::connection()->getPdo()->exec($sql);
        });
    }
}
