<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAboutUsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('about_us', function (Blueprint $table) {
            $table->increments('id');
            $table->text('text');
            $table->string('team_member_1_name');
            $table->text('team_member_1_text');
            $table->string('team_member_1_image');
            $table->string('team_member_2_name');
            $table->text('team_member_2_text');
            $table->string('team_member_2_image');
            $table->string('team_member_3_name');
            $table->text('team_member_3_text');
            $table->string('team_member_3_image');
            $table->string('team_member_4_name');
            $table->text('team_member_4_text');
            $table->string('team_member_4_image');
            $table->string('team_member_5_name');
            $table->text('team_member_5_text');
            $table->string('team_member_5_image');
            $table->string('team_member_6_name');
            $table->text('team_member_6_text');
            $table->string('team_member_6_image');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('about_us');
    }
}
