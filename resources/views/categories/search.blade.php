@extends('layouts.main')

@include('inc.page_metas', ['defTitle' => 'Search Results'])

@section('top-links')
    @include('inc.top_links')
@stop
@section('content')
    <div class="page-wrap">
        <div class="wrap">
            <div class="page-categories">
                <div class="sidebar-wrap sidebar-category">
                    <div class="group-wrap">
                        <h3 class="reset-filters"><a href="{{route('search')}}">Reset all</a></h3>
                    </div>
                    @if (isset($categories))
                    <div class="group-wrap">
                        <h3>Categories <span class="icon-right_arrow cat-icon-expanded"></span></h3>
                        <nav>
                            <ul>
                                @foreach($categories as $category)
                                    <li><a href="{{route('category', array_merge(['slug'=>$category->slug],$selected))}}">
                                            {{$category->title}}({{$category->lots_count}})
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                        </nav>
                    </div>
                    @endif
                    @if (isset($selectedPriceRange['max']) && $priceRange['max'] > 0)
                        <div class="group-wrap">
                            <h3>Price <span class="icon-right_arrow cat-icon-expanded"></span></h3>
                            <div class="range-wrap">
                                <div id="category-range-price"></div>
                                <div class="result clearfix">
                                    <input type="text" class="range_price_value" data-index="0" name="min_price"
                                           value="{{$selectedPriceRange['min']}}">
                                    <em>-</em>
                                    <input type="text" class="range_price_value" data-index="1" name="max_price"
                                           value="{{$selectedPriceRange['max']}}">
                                    {{--<em>$</em>--}}
                                    <button>OK</button>
                                </div>
                            </div>
                        </div>
                    @endif
                    <form action="#" id="category_filters">
                        @include('inc.filter_status')
                        <input type="hidden" name="query" value="{{ $query }}">
                    </form>
                </div>

                <div class="category-page">
                    <div class="title-evets">
                        <h3>{!! searchResults($lots->total(), $query) !!}</h3>
                        <form action="" id="top_sorting">
                        <div class="filter">
                            <div class="item">
                                <div class="name">Sort by</div>
                                <div class="select">
                                    <select name="sort_by" id="sort_by">
                                        <option value=0>Choose...</option>
                                        <option value="price"
                                        {{(isset($sort['sort_by'])&&$sort['sort_by']=='price')?' selected ':''}} >
                                            Price</option>
                                        <option value="latest"
                                        {{(isset($sort['sort_by'])&&$sort['sort_by']=='latest')?' selected ':''}} >
                                            Latest</option>
                                    </select>
                                </div>
                                <div class="sorting">
                                    <a id="sort_up" href="javascript:void 0;" class="icon-sorting_up
                                    {{(isset($sort['sort_direction'])&&$sort['sort_direction']=='asc')?
                                    ' active':''}}"></a>
                                    <a id="sort_down" href="javascript:void 0;" class="icon-sorting_up down
                                    {{(isset($sort['sort_direction'])&&$sort['sort_direction']=='desc')?
                                    ' active':''}}"></a>
                                    @php($sort_direction = isset($sort['sort_direction'])?$sort['sort_direction']:0)
                                    <input type="hidden" name="sort_direction"
                                           id="sort_direction" value="{{$sort_direction}}">
                                </div>
                            </div>
                        </div>
                        </form>
                    </div>

                    <div class="row">
                        @include('inc.lots_list')
                    </div>
                    @include('inc.paginator_with_page_selector', ['items' => $lots,'selector'=>false])
                </div>
            </div>
        </div>
    </div>
@endsection
@section('user_script')
    $('.reset-filters').on('click', function (e) {
        return document.location = '{{ route('search', ['query' => $query]) }}';
    });
    $('#sort_by').on('change', function () {
        if ($(this).val() != 0) {
            $('.sorting a#sort_up').click();
        } else {
            updateUrl([['sort_by'], ['sort_direction']]);
        }
    });
    $('.filter .item .sorting a').off('click').on('click', function(){
        var $input = $('#sort_direction'),
        $selected = $('select#sort_by').val();
        if ($selected != 0) {
            $(this).hasClass('down') ? $input.val('desc'):$input.val('asc');
            filterLots();
        }
    })
    function filterLots() {
        var $filters = $('#category_filters input, .range-wrap input, form#top_sorting');
        document.location = document.location.pathname + '?' + $filters.serialize();
    }
    $('#category_filters input, .range-wrap button').on('click', filterLots);
    $("#category-range-price").slider({
        range: true,
        min: {{$priceRange['min']}},
        max: {{$priceRange['max']}},
        step: 10,
        values: [{{$selectedPriceRange['min']}},
        {{$selectedPriceRange['max']}}],
        slide: function (event, ui) {
            for (var i = 0; i < ui.values.length; ++i) {
                $("input.range_price_value[data-index=" + i + "]").val(ui.values[i]);
            }
        }
    });

    $("input.range_price_value").change(function() {
        var $this = $(this);
        $("#category-range-price").slider("values", $this.data("index"), $this.val());
    });
@stop
