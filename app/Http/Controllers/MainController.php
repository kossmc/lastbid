<?php

namespace App\Http\Controllers;

use App\Models\CategoryIcons;
use App\Models\Lot;
use App\Models\PageMeta;
use App\Models\Partner;
use App\Models\RecentlyLot;
use App\Models\Slider;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Event;
use App\Http\Requests\ContactUsRequest;
use App\Models\User;
use App\Repositories\LotRepository;
use App\Repositories\BlogRepository;
use App\Repositories\CategoryRepository;
use App\Events\Form\CountactUsForm;

class MainController extends Controller
{

    /**
     * The lot repository implementation.
     *
     * @var LotRepository
     */
    private $lot;

    /**
     * The event repository implementation.
     *
     * @var BlogRepository
     */
    private $blog;

    /**
     * The category repository implementation.
     *
     * @var CategoryRepository
     */
    private $category;

    /**
     * Create a new controller instance.
     *
     * @param LotRepository $lot
     * @param BlogRepository $blog
     * @param CategoryRepository $category
     * @return void
     */
    public function __construct(LotRepository $lot, BlogRepository $blog, CategoryRepository $category)
    {
        $this->lot = $lot;
        $this->blog = $blog;
        $this->category = $category;
    }

    /**
     * Show the homepage.
     *
     * @return \Illuminate\Http\Response
     */
    public function home()
    {
        $lots = $this->lot->popular(4);
        $blog_posts = $this->blog->latest(3);
        $categories = $this->category->featured();
        $categories_icons = CategoryIcons::whereIn('id', array_unique($categories->pluck('categories_icons_id')->toArray()))->get();
        $slides = $this->getSlides();
        $recentlyLots = RecentlyLot::recentlyLotsList();
        $meta = PageMeta::byPage('home');
        $most_popular_lots = $this->getMostPopular();
        $h1_tag = env('APP_NAME');

        return view('main.home', compact(['lots', 'blog_posts', 'categories', 'categories_icons', 'slides',
            'recentlyLots', 'meta','most_popular_lots', 'h1_tag']));
    }

    /**
     * Get counters
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCounters()
    {
        return response()->json([
            'bids_count' => Auth::user()->new_bids_count,
            'messages_count' => Auth::user()->new_messages_count
        ]);
    }

    /**
     * @return mixed
     */
    public function getSlides()
    {
        $no_date_slides = Slider::active()->withoutDate()->sortByOrder()->get();
        $slides_with_date = Slider::active()->withDate()->sortByOrder()->get();
        $slides = $no_date_slides->merge($slides_with_date);
        return $slides;
    }


    /**
     * Show the Contact Us page.
     *
     * @return \Illuminate\Http\Response
     */
    public function contact()
    {
        $user = Auth::user()? Auth::user(): new User();
        $data = \DB::table('contact_us')->first();
        $meta = PageMeta::byPage('contact');
        return view('pages.contact', compact('user' , 'data', 'meta'));
    }

    /**
     * Send letters from Contact Us page.
     *
     * @return \Illuminate\Http\Response
     */
    public function contactSend(ContactUsRequest $request)
    {
        $data = $request->only('name', 'email', 'message');
        Event::fire(new CountactUsForm($data));

        Mail::send('emails.admin_contact_us_notice', ['data' => $data], function ($message) {
            $message->to([
                env('CONTACT_US_EMAIL', 'info@lastbid.com'),
                env('AUCTIONS_EMAIL', 'auctions@lastbid.com'),
                env('TEST_EMAIL', 'info@lastbid.com'),
            ]);
            $message->subject('You received message from Lastbid website');
        });
        return redirect()
            ->route('contact')
            ->with(['message' => 'Your message was sent']);
    }

    /**
     * Get terms block by popup
     * @return \Illuminate\Http\JsonResponse
     */
    public function getTerms()
    {
        return response()->json([
            'form' => view()->make('inc.modal_terms')->with(['terms'=>\DB::table('site_terms_conditions')->first()->text])->render()
        ]);
    }

    /**
     * Get custom terms terms block by popup
     * @return \Illuminate\Http\JsonResponse
     */
    public function getTermsCustom()
    {
        $lot = Lot::find(request('lot_id'));
        if($lot){
            $alias = $lot->getPartnerAlias();
            $trems = \DB::table('terms_conditions')
                ->where('alias', $alias)
                ->first();
            if($trems){
                return response()->json([
                    'form' => view()->make('inc.modal_custom_terms', [
                        'text' => $trems->text,
                        'partner'=> $lot->partner
                    ])->render()
                ]);
            }
        }
        return response()->json([
            'form' => view()->make('inc.modal_custom_terms')->render()
        ]);
    }

    /**
     * Get policy block by popup
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPolicy()
    {
        return response()->json([
            'form' => view()->make('inc.modal_policy')->render()
        ]);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     *
     */
    public function letsTalk(){
        return response()->json([
            'form' => view()->make('inc.lets_talk_modal')->render()
        ]);
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function getMostPopular() {
        $popular_count = Lot::where('status', 'active')->count();
        $popularIds = \DB::table('lots')->where('status', 'active')
            ->where('end_date', '>', Carbon::now()->format('Y-m-d h:i:s'))
            ->where('views_count', '<>', 0)
            ->orderBy('views_count', 'desc')
            ->limit(floor($popular_count*0.1))
            ->get()->pluck('id')->toArray();
        shuffle($popularIds);
        $lots = Lot::whereIn('id', array_slice($popularIds,0,4))->inRandomOrder()->get();
        return $lots;
    }
}
