<div class="block-card-lot block-card-lot--sold-lot">
    <p class="title">Lot is sold</p>
    <div class="bid-price">
        {!! format_price($lot->range_from, $lot->range_to,$lot->currency) !!}
        <p>Suggested bid range</p>
    </div>
    <div class="product-price-range-details-wrapper">
        <p>Does not include auction house premium and shipping/delivery fees</p>            
    </div>
    <div class="card-bottom-part">
        <p><span>Start:</span>{{ $lot->start_date->format('j F Y, h:i A') }} EST</p>
        <p><span>End:</span>{{ $lot->end_date->format('j F Y, h:i A') }} EST</p>
    </div>
</div>
