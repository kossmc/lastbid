@if($errors->has($name))
    <small class="form-text text-error">
        {{ $errors->get($name)[0] }}
    </small>
@endif
