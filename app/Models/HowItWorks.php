<?php


namespace App\Models;

use App\Traits\ModelImageTrait;
use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\Model;


class HowItWorks extends Model {

    use CrudTrait;
    use ModelImageTrait;
    protected $table = 'how_it_works';

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    public function setImageSection2Attribute($value)
    {
        $attribute_name = "image_section_2";
        $disk = "uploads";
        $destination_path = "how_it_works";
        // if the image was erased
        if ($value == null) {
            // delete the image from disk
            \Storage::disk($disk)->delete($this->image_section_2);

            // set null in the database column
            $this->attributes[$attribute_name] = null;
        }

        // if a base64 was sent, store it in the db
        if (starts_with($value, 'data:image')) {
            // 0. Make the image
            $image = \Image::make($value);
            // 1. Generate a filename.
            // @todo: why jpeg here?
            $filename = md5($value . time()) . '.jpg';
            // 2. Store the image on disk.
            \Storage::disk($disk)->put($destination_path . '/' . $filename, $image->stream());
            // 3. Save the path to the database
            $this->attributes[$attribute_name] = $destination_path . '/' . $filename;
        }
    }

    public function setImageSection3Attribute($value)
    {
        $attribute_name = "image_section_3";
        $disk = "uploads";
        $destination_path = "how_it_works";
        // if the image was erased
        if ($value == null) {
            // delete the image from disk
            \Storage::disk($disk)->delete($this->image_section_3);

            // set null in the database column
            $this->attributes[$attribute_name] = null;
        }

        // if a base64 was sent, store it in the db
        if (starts_with($value, 'data:image')) {
            // 0. Make the image
            $image = \Image::make($value);
            // 1. Generate a filename.
            // @todo: why jpeg here?
            $filename = md5($value . time()) . '.jpg';
            // 2. Store the image on disk.
            \Storage::disk($disk)->put($destination_path . '/' . $filename, $image->stream());
            // 3. Save the path to the database
            $this->attributes[$attribute_name] = $destination_path . '/' . $filename;
        }
    }

    public function setImageSection4Attribute($value)
    {
        $attribute_name = "image_section_4";
        $disk = "uploads";
        $destination_path = "how_it_works";
        // if the image was erased
        if ($value == null) {
            // delete the image from disk
            \Storage::disk($disk)->delete($this->image_section_4);

            // set null in the database column
            $this->attributes[$attribute_name] = null;
        }

        // if a base64 was sent, store it in the db
        if (starts_with($value, 'data:image')) {
            // 0. Make the image
            $image = \Image::make($value);
            // 1. Generate a filename.
            // @todo: why jpeg here?
            $filename = md5($value . time()) . '.jpg';
            // 2. Store the image on disk.
            \Storage::disk($disk)->put($destination_path . '/' . $filename, $image->stream());
            // 3. Save the path to the database
            $this->attributes[$attribute_name] = $destination_path . '/' . $filename;
        }
    }

    public function setVideoAttribute($value)
    {
        $attribute_name = "video";
        $disk = "public";
        $destination_path = "how_it_works";
        $request = \Request::instance();
        $file = $request->file($attribute_name);
        $file->move('uploads/how_it_works', $file->getClientOriginalName());
        //$file->storeAs($destination_path, $file->getClientOriginalName(), $disk);
        $this->attributes[$attribute_name] = $destination_path . '/' . $file->getClientOriginalName();
    }
}