<div class="block-card-lot block-card-lot--place-a-bid">
    <div class="title">Place a Bid</div>
    <form action="{{ route('place_bid', ['slug' => $lot->slug, 'id' => $lot->id]) }}" id="buy-form"
          class="price-block-wrapper" method="post" onsubmit="return false;">
        <input type="hidden" name="message" id="message" value="">
        <input type="hidden" name="confirmed" id="confirmed" value="0">
        <input type="hidden" name="highest_bid_amount" id="highest_bid_amount" value="{!! $highest_bid_amount !!}">
        {{ csrf_field() }}
        <div class="row clearfix">
            <div class="price" data-from="{{$lot->min_bid_amount}}" data-to="{{$lot->range_to}}">

                {!! format_price($lot->range_from, $lot->range_to, $lot->currency) !!}
                <span class="info">Suggested bid range</span>
            </div>
        </div>
        @if(\Carbon\Carbon::now()->gte($lot->start_date) && $lot->end_date->timestamp > \Carbon\Carbon::now()->timestamp)
            <div class="row clearfix place-bid-wrapper">
                <div class="title-wrap">Place a bid {!! $highest_bid_amount ? '(Lot under offer)' : '' !!}</div>
                <div class="bid-amount-error"></div>
                <div class="inp-wrap">
                    <input type="text" class="bid-amount-input" id="offer-value" name="amount"
                           placeholder="Type your price" required="1" pattern="^[0-9\.,]+$" maxlength="14">
                </div>

                <div class="bt-wrap bt-wrap2">
                    {{--<a href="#" class="btn" id="make-offer-btn">Place a bid</a>--}}
                    <button class="btn place-bid-js"
                            data-alias="{{ $lot->getPartnerAlias() }}"
                            data-target-modal="new-modal-1"
                            data-partner-name="{{ $lot->partner ? $lot->partner->title : ''}}">Place a bid
                    </button>
                </div>
                <div class="clear"></div>
                <div class="info">Your bid does not include auction house premium,<br/> VAT and/or other applicable
                    taxes, or shipping/delivery fees
                </div>
            </div>
        @endif
    </form>
    <div class="row bui-footer clearfix">
        @if ($lot->is_active)
            @if(\Carbon\Carbon::now()->gte($lot->start_date))
                <div class="time-wrap-line">
                    <div class="time-line">
                        <div class="time-line-left"
                             style="width:{{ percent_time_left($lot->start_date, $lot->end_date) }}%"></div>
                    </div>
                    <span class="time-text">
                       @if(\Carbon\Carbon::createFromTimeString($lot->end_date)->timestamp < \Carbon\Carbon::now()->timestamp)
                           Sorry, lot has already expired.
                        @else
                            Lot expires in: <b>{{ format_time_left($lot->end_date) }}</b>
                        @endif
                   </span>
                </div>
            @endif
            @if(\Carbon\Carbon::now()->lt($lot->start_date))
                <div class="time-wrap-line">
                    <span class="time-text">
                    Time remains before auction starts: <b>{{ format_time_left(\Carbon\Carbon::now(), $lot->start_date) }}</b>
               </span>
                </div>
            @endif
        @endif
    </div>
</div>
