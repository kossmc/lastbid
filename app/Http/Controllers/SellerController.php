<?php

namespace App\Http\Controllers;

use App\Http\Requests\LotCreateRequest;
use App\Http\Requests\LotImportRequest;
use App\Models\Category;
use App\Models\Fee;
use App\Models\Lot;
use App\Models\LotSpecificOptions;
use App\Models\Partner;
use App\Models\Specific;
use App\Repositories\CategoryRepository;
use App\Repositories\ImportRepository;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use App\Repositories\LotRepository;
use App\Repositories\BidRepository;
use App\Http\Requests\PlaceBidRequest;
use App\Http\Requests\LotEditRequest;

/**
 * Class SellerController
 * @package App\Http\Controllers
 */
class SellerController extends Controller
{

    /** @var LotRepository The lot repository implementation. */
    private $lot;

    /** @var BidRepository The bid repository implementation */
    private $bid;

    /** @var CategoryRepository The category repository implementation. */
    private $category;

    /**
     * SellerController constructor.
     * @param LotRepository $lot
     * @param BidRepository $bid
     */
    public function __construct(LotRepository $lot, BidRepository $bid, CategoryRepository $category)
    {
        $this->lot = $lot;
        $this->bid = $bid;
        $this->category = $category;
    }

    /**
     * Get active lot
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function activeLots()
    {
        $lots = $this->lot->paginateActive();
        return view('seller.lots', compact('lots'));
    }

    /**
     * Method close lot
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function closedLots()
    {
        $lots = $this->lot->paginateClosed();
        return view('seller.lots', compact('lots'));
    }

    /**
     * Method sold lot
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function soldLots()
    {
        $sort = request()->intersect('sort_by', 'sort_direction');
        $lots = $this->lot->paginateSold($sort);
        return view('seller.sold_lots', compact(['lots', 'sort']));
    }

    /**
     * Method lot bids
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function lotBids(Request $request)
    {
        $lot = $request->lot;
        $lotId = $lot->id;

        $receivedBids = $this->bid->lotBidsByStatus($lotId, 'received');
        $sentBids = $this->bid->lotBidsByStatus($lotId, 'sent');
        $declinedBids = $this->bid->lotBidsByStatus($lotId, 'declined');

        return view('seller.lot', compact('lot', 'receivedBids', 'sentBids', 'declinedBids'));
    }

    /**
     * Get bids
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function bids()
    {
        $lots = $this->lot->paginateBids();
        return view('seller.bids', compact('lots'));
    }

    /**
     * Change bit status
     * @param Request $request
     * @param $method
     * @return \Illuminate\Http\RedirectResponse
     */
    public function switchBidStatus(Request $request, $method)
    {
        $bid = $request->bid;

        switch ($method) {
            case 'accept':
                $this->bid->acceptBySeller($bid);
                Session::flash('message', 'Bid was accepted');
                break;
            case 'decline':
                $this->bid->declineBySeller($bid);
                Session::flash('message', 'Bid was declined');
                break;
        }
        return redirect()->back();
    }

    /**
     * Method count bit
     * @param Request $request
     * @param PlaceBidRequest $formRequest
     * @return \Illuminate\Http\JsonResponse
     */
    public function counterBid(Request $request, PlaceBidRequest $formRequest)
    {
        $bid = $request->bid;

        $data = [
            'user_id' => Auth::id(),
            'lot_id' => $bid->lot_id,
            'amount' => $formRequest->amount,
            'message' => $formRequest->message
        ];

        $this->bid->counterBySeller($bid, $data);
        Session::flash('message', 'Bid was countered');

        return response()->json([
            'status' => true,
            'link' => URL::previous()
        ]);
    }

    /**
     * Method close lot
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function closeLot($id)
    {
        try {
            $lot = Lot::findOrFail($id)->close();
            Session::flash('message', "Lot {$lot->id} was closed and moved to the 'Closed lots' tab");
        } catch (\Exception $e) {
            Session::flash('message', 'Lot not found!');
        }

        return redirect()->route('seller_active_lots');
    }

    /**
     * Edit page
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function editLots($id)
    {
        $lot = $this->lot->findById($id);
        $categories = $this->categoriesTree();
        $fees = \Auth::user()->fees->keyBy('fee');
        $defaultFee = Fee::DEFAULT_FEE;

        $specifics = !$lot->lot_specifics_options->isEmpty() ? $lot->lot_specifics_options->keyBy('specific_id') : '' ;

        if(isset(Session::all()['errors'])) Session::put('message', 'Lot hasn\'t been updated');

        return view('seller.lot_edit', compact('lot', 'categories', 'specifics', 'fees', 'defaultFee'));
    }

    /**
     * Method save lot data
     * @param $id
     * @param LotEditRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function saveLots($id, LotEditRequest $request)
    {
        $data = $request->except('_token', 'category', 'photos', 'specifics_data', 'photosDel');

        $lot = $this->lot->findById($id);
        if(!$lot){
            Session::flash('message', 'Lot not found!');
            return redirect()->route('seller_active_lots');
        }

        $data['category_id'] = $this->getCurrentCategoryId($request->get('category'));

        $oldPhotos = $this->filterOldPhotos($id, $request);
        $photos = $this->getPhotos($id, $request);
        if($photos || $request->photosDel){
            $data['photos'] = json_encode(array_merge($photos, $oldPhotos));
        }

        $this->saveSpecificOptions($lot, $request);

        if(!$this->lot->update($data, $id)){
            Session::flash('message', 'Lot not saved!');
            return redirect()->route('seller_edit_lots', ['id' => $id]);
        }

        Session::flash('message', 'Lot was saved!');
        return redirect()->back();
    }

    /**
     * Create page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function createLots()
    {
        $lot = new Lot();
        $categories = $this->categoriesTree();
        $fees = \Auth::user()->fees->keyBy('fee');
        $defaultFee = round(Fee::DEFAULT_FEE);
//        $partners = Partner::all()->pluck('title');

        if(isset(Session::all()['errors'])) Session::put('message', 'Lot hasn\'t been created');

        return view('seller.lot_create', compact('lot', 'categories', 'fees', 'defaultFee', 'partners'));
    }

    public function createLotsDisabled()
    {
        Session::flash('message', "Lot create coming soon!");
        return redirect()->back();
    }

    public function addBulk()
    {
        return view('seller.add_bulk', [
            'categories' => Category::all(),
            'partners' => Partner::all()
        ]);
    }

    public function addBulkDisabled()
    {
        Session::flash('message', "Bulk upload coming soon!");
        return redirect()->back();
    }

    public function bulkProcess(LotImportRequest $request, ImportRepository $import)
    {
        //dd($request->all());
        $path = $request->csv_file->store('csv');
        $name = $request->csv_file->getClientOriginalName();
        if ($totalLots = $import->processFile(storage_path('app/' . $path), $name)) {
            Session::flash('message', $totalLots . ' lots has been imported successfully');
            return redirect()->back();
        }
        else {
            Session::flash('message', 'Couldn\'t process CSV file');
            return redirect()->back();
        }
    }

    /**
     * Method store lot data
     * @return \Illuminate\Http\RedirectResponse
     */
    public function storeLots(LotCreateRequest $request)
    {
        $data = $request->except('_token', 'category', 'photos', 'specifics_data', 'partner');

        $lot = $this->lot;

        $data['category_id'] = $this->getCurrentCategoryId($request->get('category'));
        $data['user_id'] = \Auth::id();
        $data['status'] = Lot::STATUS_ACTIVE;
        $data['popular'] = 0;
        $data['start_date'] = strtotime($data['start_date']);
        $data['end_date'] = strtotime($data['end_date']);
//        $data['partner_id'] = \Auth::user()->partner->id;

        if(!$this->lot->saveModel($data)){
            Session::flash('message', 'Lot not saved!');
            return redirect()->back();
        }
        $lot = $this->lot->findById($lot->getCreatedId());

        $this->saveSpecificOptions($lot, $request);

        $photos = $this->getPhotos($lot->id, $request);
        if($photos){
            $data['photos'] = json_encode($photos);
            $this->lot->update($data, $lot->id);
        }

        Session::flash('message', 'Lot was created!');
        return redirect()->route('seller_edit_lots', ['id' => $lot->id]);
    }

    private function saveSpecificOptions($lot, $request){
        if(empty($request->specifics_data)) return;
        $lot->lot_specifics_options()->delete();

        foreach($request->specifics_data as $specific_id => $option_id){
            $lotSpecificOption = new LotSpecificOptions();

            $lotSpecificOption->lot_id = $lot->id;
            $lotSpecificOption->lot_id = $lot->id;
            $lotSpecificOption->specific_id = $specific_id;

            if(Specific::findOrFail($lotSpecificOption->specific_id)->type == 'text'){
                if(!$option_id) continue;

                $lotSpecificOption->value = $option_id;
            }else{
                $lotSpecificOption->specific_option_id = $option_id;
            }

            $lotSpecificOption->save();
        }
    }

    public function getCategorySpecificTable($catTitle)
    {
        $category = Category::where('title','=', $catTitle)->first();

        return response()->json([
            'form' => view()->make('seller.inc._specifics', ['category' => $category, 'specifics' => ''])->render()
        ]);
    }

    private function merge($scat, $list){
        if($scat->depth > 1){
            $this->merge($scat->parent, $list->push($scat->parent));
        }
    }

    private function categoriesTree()
    {
        $roots = $this->category->roots();
        $featured = $this->category->featured(1,1);
        $categories = $featured->merge($roots)->sortBy('lft');
        $cat = Category::whereIn('id', array_unique(Lot::active()->get()->pluck('category_id')->toArray()))
            ->where('featured', 0)
            ->get();
        foreach ($cat as $scat){
            $this->merge($scat, $cat);
        }

        return buildTree($categories->merge($cat)->unique()->sortBy('lft'));
    }

    private function getCurrentCategoryId($catName)
    {
        $category = $this->category->findBy('title', $catName);
        if(!$category){
            Session::flash('message', 'Not find active category!');
            return redirect()->back();
        }

        return $category->id;
    }

    private function getPhotos($lot_id, $request)
    {
        $photos = [];
        if($files = $request->file('photos')){

            foreach($files as $file){

                $name = md5($file->getClientOriginalName()).'.'.$file->getClientOriginalExtension();

                $photoPath = 'lots/' . $lot_id . DIRECTORY_SEPARATOR;

                $dirPath = 'uploads/' . $photoPath;

                if(!is_dir($dirPath)){
                    mkdir($dirPath);
                }

                $file->move($dirPath, $name);

                $photos[] = $photoPath . $name;
            }
        }

        return $photos;
    }

    private function filterOldPhotos($lot_id, $request)
    {
        $photos = $this->lot->findById($lot_id)->photos;
        if($photosDel = $request->photosDel){
            $prepare = function ($photodel){
                return str_replace(url('/uploads/').'/', '', $photodel);
            };

            $photosDel = array_map($prepare, $photosDel);

            $photos = array_diff($photos, $photosDel);
        }

        return $photos;
    }

    private function getPartnerId($title)
    {
        $partner = Partner::where('title', $title)->first();
        if(!$partner){
            Session::flash('message', 'Not find partner!');
            return redirect()->back();
        }

        return $partner->id;

    }
}