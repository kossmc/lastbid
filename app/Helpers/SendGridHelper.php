<?php
if ( ! function_exists('asJSON')) {
    /*
    |--------------------------------------------------------------------------
    | Convert array to json
    |--------------------------------------------------------------------------
    */
    function asJSON($data)
    {
        $json = json_encode($data);

        $json = preg_replace('/(["\]}])([,:])(["\[{])/', '$1$2 $3', $json);

        return $json;
    }
}
