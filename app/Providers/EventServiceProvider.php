<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use App\Models\Lot;

class EventServiceProvider extends ServiceProvider
{
    /*simply add listeners and events to your EventServiceProvider and
    use the php artisan event:generate command.
    This command will generate any events or listeners that are listed in your EventServiceProvider.
    Of course, events and listeners that already exist will be left untouched*/

    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        /*User events and listeners*/
        'App\Events\User\BuyerRegistered' => [
            'App\Listeners\User\BuyerRegisteredListener',
        ],
        'App\Events\User\BuyerResendConfirmation' => [
            'App\Listeners\User\BuyerResendConfirmationListener',
        ],
        'App\Events\User\BuyerAddNewCard' => [
            'App\Listeners\User\BuyerAddNewCardListener',
        ],
        'App\Events\User\SellerAddNewBankAccount' => [
            'App\Listeners\User\SellerAddNewBankAccountListener',
        ],
        'App\Events\User\SellerDeleteBankAccount' => [
            'App\Listeners\User\SellerDeleteBankAccountListener',
        ],
        'App\Events\User\BuyerDeleteCard' => [
            'App\Listeners\User\BuyerDeleteCardListener',
        ],
        'App\Events\User\ResetPassword' => [
            'App\Listeners\User\ResetPasswordListener',
        ],
        /*Bid events and listeners*/
        'App\Events\Bid\BuyerAutoAcceptBid' => [
            'App\Listeners\Bid\BuyerAutoAcceptBidListener',
        ],
        'App\Events\Bid\BuyerCreateBid' => [
            'App\Listeners\Bid\BuyerCreateBidListener',
        ],
        'App\Events\Bid\BuyerCreateCounterBid' => [
            'App\Listeners\Bid\BuyerCreateCounterBidListener',
        ],
        'App\Events\Bid\BuyerAcceptCounterBid' => [
            'App\Listeners\Bid\BidAcceptedListener',
            'App\Listeners\Bid\BuyerAcceptCounterBidListener',
        ],
        'App\Events\Bid\BuyerDeclineCounterBid' => [
            'App\Listeners\Bid\BuyerDeclineCounterBidListener',
        ],
        'App\Events\Bid\SellerAcceptBid' => [
            'App\Listeners\Bid\BidAcceptedListener',
            'App\Listeners\Bid\SellerAcceptBidListener',
        ],
        'App\Events\Bid\SellerDeclineBid' => [
            'App\Listeners\Bid\SellerDeclineBidListener',
        ],
        'App\Events\Bid\SellerCreateCounterBid' => [
            'App\Listeners\Bid\SellerCreateCounterBidListener',
        ],
        'App\Events\Bid\BidExpired' => [
            'App\Listeners\Bid\BidExpiredListener',
        ],
        'App\Events\Bid\SystemDeclineBid' => [
            'App\Listeners\Bid\SystemDeclineBidListener',
        ],
        'App\Events\Bid\SystemAutoAcceptBid' => [
            'App\Listeners\Bid\SystemAutoAcceptListener',
        ],
        /*Lot events and listeners*/
        'App\Events\Lot\LotExpired' => [
            'App\Listeners\Lot\LotExpiredListener',
        ],

        /*Message events and listeners*/
        'App\Events\Message\BuyerCreateMessage' => [
            'App\Listeners\Message\CreateMessageListener',
        ],
        'App\Events\Message\SellerCreateMessage' => [
            'App\Listeners\Message\CreateMessageListener',
        ],
        /*Form events and listeners*/
        'App\Events\Form\CountactUsForm' => [
            'App\Listeners\Form\CountactUsFormListener',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        Lot::saving('App\Listeners\Lot\LotChangedListener');
    }
}
