<div class="slide-{{ $slideId or '' }} {{ $class or '' }}" data-slideid="{{ $slideId or '' }}">
    <img src="{{asset('300x400/uploads/'.$src )}}" alt="{{ $alt or '' }}" title="{{ $title or '' }}">
    <a class="zoom-wrap icon-zoom product-group" href="{{asset('800x1200/uploads/'.$src)}}">
        <span>zoom <br>image</span>
    </a>
</div>