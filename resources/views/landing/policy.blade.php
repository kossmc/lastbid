@extends('landing.content')
@section('title','Privacy policy')

@section('content_page')
    <div class="page-wrap how-it-works">
        <div class="full-width-page clearfix">
            <div class="image-head-page small" style="background-image:url(img/about_us_wallpaper_210821018.png);">
                <div class="wrap clearfix">
                    <div class="desc-wrap">
                        <h3>Privacy policy</h3>
                    </div>
                </div>
            </div>

            <div class="content">
                <div class="wrap">
                    @include('inc.policy')
                </div>
            </div>
        </div>
    </div>
@endsection

@section('user_script')
    <script>

        $(document).ready(function(){
            $.ajax({
                'type': 'get',
                'url': "https://lastbid.com/ajax_get_policy",
                'data': {},
                'success': function (data) {

                    $('.policy').html(data.form);
                }
            });
        });

    </script>
@endsection