<?php

namespace App\Http\Controllers;

use App\Models\BidAttempt;
use App\Models\Category;
use App\Models\CategoryIcons;
use App\Models\Lot;
use App\Models\PageMeta;
use App\Repositories\CategoryRepository;
use App\Repositories\LotRepository;
use App\Repositories\LotStateRepository;
use Illuminate\Support\Facades\Cookie;

/**
 * Class CatalogController
 * @package App\Http\Controllers
 */
class CatalogController extends Controller
{

    /**
     * The category repository implementation.
     *
     * @var CategoryRepository
     */
    private $category;

    /**
     * The lot repository implementation.
     *
     * @var LotRepository
     */
    private $lot;

    /**
     * The lot state repository implementation.
     *
     * @var LotStateRepository
     */
    private $lotState;

    /**
     * Create a new controller instance.
     *
     * @param CategoryRepository $category
     * @param LotRepository $lot
     * @param LotStateRepository $lotState
     * @return void
     */
    public function __construct(CategoryRepository $category, LotRepository $lot, LotStateRepository $lotState)
    {
        $this->category = $category;
        $this->lot = $lot;
        $this->lotState = $lotState;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function categories()
    {
        $roots = $this->category->roots();
        $featured = $this->category->featured(1,1);
        list($other_categories_root, $other_categories_with_children) = $this->getOtherCategories();
        $categories = $featured->merge($roots)->merge($other_categories_root)->merge($other_categories_with_children)->sortBy('lft');
        $links = $this->breadcrumbs();
        $cat = Category::whereIn('id', array_unique(Lot::active()->original()->get()->pluck('category_id')->toArray()))
            ->where('featured', 0)
            ->get();
        foreach ($cat as $scat){
            $this->merge($scat, $cat);
        }
        $categories = buildTree($categories->merge($cat)->unique()->sortBy('lft'));
        $categories_icons = CategoryIcons::whereIn('id', array_unique($this->category->featured()->pluck('categories_icons_id')->toArray()))->get();

        $h1_tag = env('APP_NAME').' Categories';
        $meta = PageMeta::byPage('categories');

        return view('categories.list', compact(['categories','categories_icons', 'featured', 'links', 'meta', 'h1_tag']));
    }

    public function merge($scat, $list){
        if($scat->depth > 1){
            $this->merge($scat->parent, $list->push($scat->parent));
        }
    }

    /**
     * @param null $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function category($slug = null)
    {
        //temporary disabled
        /*if($slug){
            $slug = array_slice(explode('/',$slug), -1)[0];
        }*/
        if ($category = $this->category->findBySlug($slug)) {
            $filters = $category->filters;
            $priceRange = $this->lot->getPriceRangeByCategory($category->getTreeIds());
            if (count($selected = request()->except(['per_page','page']))){
                $selectedPriceRange = [
                    'min'=> request('min_price', $priceRange['min']),
                    'max'=> request('max_price', $priceRange['max'])
                ];
                $selectedSpecifics = collect(array_filter($selected, function ($k) {
                    return strpos($k, 'spec_') !== false;
                }, ARRAY_FILTER_USE_KEY));
                $selectedIds = $selectedSpecifics->flatten();
            } else {
                $selectedPriceRange = $priceRange;
                $selectedSpecifics = [];
                $selectedIds = [];
            }
            $sort = request()->intersect('sort_by', 'sort_direction');
            if (!count($sort)) {
                unset($selected['sort_by'], $selected['sort_direction']);
            }
            request()->has('status')?'' : request()->merge(['status'=>'active']);
            $selectedStatus = request('status');

            $roots = $this->category->roots();
            $featured = $this->category->featured(1,1);
            list($other_categories_root, $other_categories_with_children) = $this->getOtherCategories();
            $categories = $featured->merge($roots)
                ->merge($other_categories_root)
                ->merge($other_categories_with_children)
                ->sortBy('lft');

            $cat = Category::whereIn('id', array_unique(Lot::active()->original()->get()->pluck('category_id')->toArray()))
                ->where('featured', 0)
                ->get();
            foreach ($cat as $scat){
                $this->merge($scat, $cat);
            }
            $categories = buildTree($categories->merge($cat)->unique()->sortBy('lft'));
            $query = trim(request('query'));
            $lots = $this->lot->filterAndPaginateByCategory($category->getTreeIds(), $selectedIds,
               $selectedPriceRange, $sort, $selectedStatus, $query)->appends($selected)
                ->appends($sort)->appends(['query'=> $query]);
            $activeCat = $category->id;

            if ($category->parent_id) {
                $links = [
                    ['link' => $category->parent->url, 'title' => $category->parent->title],
                    ['link' => $category->url, 'title' => $category->title]
                ];
            } else {
                $links = [['link' => $category->url, 'title' => $category->title]];
            }
            $links = $this->breadcrumbs($links);
            $h1_tag = env('APP_NAME').' '. $category->title;
            return view('categories.view',
                compact(['category', 'categories', 'lots', 'filters', 'selectedSpecifics', 'sort', 'links',
                    'priceRange', 'selectedPriceRange','selectedIds', 'selectedStatus','query','activeCat', 'h1_tag']));
        }
        abort(404);
    }

    /**
     * Search lot by catalog
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function search()
    {
        $query = trim(request('query'));
        $priceRange = $this->lot->getPriceRangeByQuery($query);

        if (count($selected = request()->except(['per_page', 'page']))){
            $selectedPriceRange = [
                'min'=> request('min_price', $priceRange['min']),
                'max'=> request('max_price', $priceRange['max'])
            ];
        } else {
            $selectedPriceRange = $priceRange;
        }

        $sort = request()->intersect('sort_by', 'sort_direction');
        if (!count($sort)) {
            unset($selected['sort_by'], $selected['sort_direction']);
        }
        request()->has('status') ? '' : request()->merge(['status' => 'active']);

        $selectedStatus = request('status');

        $lots = $this->lot->paginateSearch($query, $selectedPriceRange, $sort, $selectedStatus)->appends($selected)->appends($sort);
        $categories = $this->lot->getCategoriesWithCountByQuery($query, $selectedPriceRange, $selectedStatus);
        $meta = PageMeta::byPage('categories-search');
        $h1_tag = env('APP_NAME').' Search';

        return view('categories.search', compact(['lots', 'selectedPriceRange', 'priceRange',
            'query', 'selectedStatus', 'sort', 'categories', 'selected', 'meta', 'h1_tag']));
    }

    /**
     * @param null $id
     * @param null $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function lot($id = null, $slug = null)
    {
        if ($lot = $this->lot->with(['seller', 'partner'])->withCount(['newBids'])->findById($id)) {
            $lotSpecifics = $this->lot->getSelectedSpecifics($lot);
            $categories = $this->category->roots();
            $this->lot->resetScope();
            $this->lot->makeModel();
            $latest = $this->lot->related($lot);
            $featured = $this->category->featured();
            $bid = $this->lot->getActualBid($lot);
            $bids = $lot->bids()->where('status', 'pending')->pluck('id')->toArray();
            $highest_bid_attempt = BidAttempt::whereIn('bid_id', $bids)
                ->where('status', 'pending')
                ->orderBy('amount', 'desc')
                ->first();
            $highest_bid_amount= $highest_bid_attempt ? $highest_bid_attempt->amount : false;
            //dd($highest_bid_amount);
            $canPlaceBid = $this->lot->canPlaceBid($lot);
            $this->lotState->setLot($lot);
            $this->lotState->setBid($bid);
            $lotState = $this->lotState;
            if($lot->category){
                if ($lot->category->parent_id) {
                    $links = [
                        ['link' => $lot->category->parent->url, 'title' => $lot->category->parent->title],
                        ['link' => $lot->category->url, 'title' => $lot->category->title]
                    ];
                } else {
                    $links = [['link' => $lot->category->url, 'title' => $lot->category->title]];
                }

            } else {
                $links = [['link' => '#', 'title' => 'Uncategorized']];
            }
            $links = $this->breadcrumbs($links);
            $categories_icons = CategoryIcons::whereIn('id', array_unique($this->category->featured()->pluck('categories_icons_id')->toArray()))->get();
            return view('lots.view', compact(['lot', 'latest', 'featured','lotSpecifics',
                'categories','categories_icons', 'lotState', 'bid', 'canPlaceBid', 'highest_bid_amount', 'links']));
        }
        abort(404);
    }

    /**
     * @param null $links
     * @return \Illuminate\Support\Collection|static
     */
    private function breadcrumbs($links = null)
    {
        $breadcrumbs = collect([
            ['link' => url('/'), 'title' => 'Home'],
            ['link' => route('catalog'), 'title' => 'Categories']
        ]);
        return is_null($links) ? $breadcrumbs : $breadcrumbs->concat($links);
    }

    /**
     * @return array
     */
    public function getOtherCategories()
    {
        $other_categories_ids = Lot::where('status', 'active')->pluck('category_id')->toArray();
        $other_categories_root = Category::whereIn('id', $other_categories_ids)->whereNull('parent_id')
            ->where('featured', 0)
            ->get();
        $other_categories_with_children_ids = Category::whereIn('id', $other_categories_ids)->whereNotNull('parent_id')
            ->where('featured', 0)
            ->pluck('parent_id')->toArray();
        $other_categories_with_children = Category::whereIn('id', $other_categories_with_children_ids)
            ->where('featured', 0)
            ->with('children')
            ->get();
        return array($other_categories_root, $other_categories_with_children);
    }
}
