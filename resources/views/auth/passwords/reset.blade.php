@extends('layouts.main')
@section('title','Reset Password')

@section('content')
<div class="container form-reset-password">
    <div class="row">
        <form method="POST" action="{{ route('password.request') }}">
            {{ csrf_field() }}

            <input type="hidden" name="token" value="{{ $token }}">
            <div class="title">
                restore password
            </div>
            <div class="row">
                <div class="inp-wrap">
                    <input id="email" type="email" name="email" placeholder="Your email"
                           value="{{ $email or old('email') }}" required autofocus
                           class="form-control{{ $errors->has('email') ? ' has-error--bgcolor' : '' }}">

                    @if ($errors->has('email'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="inp-wrap">
                    <input id="password" type="password" placeholder="New password" name="password" required
                           class="form-control{{ $errors->has('password') ? ' has-error--bgcolor' : '' }}">
                    @if ($errors->has('password'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="inp-wrap">
                    <input id="password" type="password" placeholder="Confirm password" class="form-control" name="password_confirmation" required>
                </div>
            </div>
            <div class="row">
                <div class="btn-wrap">
                    <button type="submit" class="btn" href="#">confirm</button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
