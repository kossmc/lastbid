<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Support\Facades\Session;
use Socialite;
use App\Models\User;
use GuzzleHttp\Exception\ClientException;
use Laravel\Socialite\Two\InvalidStateException;
use League\OAuth1\Client\Credentials\CredentialsException;

class AuthController extends Controller
{
    /**
     *  Create a new controller instance
     *
     * @return  void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }
    /**
     * Redirect the user to the OAuth Provider.
     *
     * @return Response
     */
    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    /**
     * Obtain the user information from provider.  Check if the user already exists in our
     * database by looking up their provider_id in the database.
     * If the user exists, log them in. Otherwise, create a new user then log them in. After that
     * redirect them to the authenticated users homepage.
     *
     * @return Response
     */
    public function handleProviderCallback($provider)
    {
        try {
            $user = Socialite::driver($provider)->user();
            $socialUser = User::whereHas('authSocial', function ($q) use ($user) {
                $q->where('provider_id', $user->id);
            })->first();
            if ($socialUser) {
                return $this->login($socialUser);
            }
            /*Check if user exist, add only social acc and log him*/
            if ($user->email && $regUser = $this->getExistingUser($user->email)) {
                $regUser->authSocial()->create([
                    'provider' => $provider,
                    'provider_id' => $user->id,
                    'token' => $user->token
                ]);
                return $this->login($regUser);
            }
            $user->provider = $provider;
            Session::put('socialAuth', $user);
            return redirectToAuth('register_form');
        } catch (InvalidStateException $e) {
            return $this->redirectToProvider($provider);
        } catch (ClientException $e) {
            return $this->redirectToProvider($provider);
        } catch (CredentialsException $e) {
            return $this->redirectToProvider($provider);
        }
    }

    private function login($user)
    {
        Auth::login($user, true);
        return redirect('/');
    }

    private function getExistingUser($email)
    {
        return User::where('email',$email)->first();
    }
}
