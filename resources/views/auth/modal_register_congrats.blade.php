<div class="popup" id="modal-id">
    <div class="modal-content">
        <span class="icon-congrat"></span>
        <div class="title">Welcome to Lastbid!</div>
        <p>Thank you for completing the registration.
        <br>
        Please verify your email address by following the link in the email.</p>
        <div class="btn-wrap">
            <a class="btn" href="javascript:void 0;" onclick="closeModal()">Ok</a>
        </div>
    </div>
</div>