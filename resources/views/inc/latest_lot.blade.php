<div>
    <a href="{{ $lot->url }}" class="item">
        <div class="inner-item">
            <div class="image">
                <div class="vertical-middle">
                    <div class="hover-info">
                        <div class="table">
                            <div class="vertical-middle">
                                <div class="col a-right">{{ $lot->bids_count }}<span>bids</span></div>
                                <div class="col a-left">{{ $lot->views_count }}<span>views</span></div>
                            </div>
                        </div>
                    </div>
                    <span><img src="{{$lot->getImagePath(225,225)}}" alt=""></span>
                </div>
            </div>
            <div class="desc-wrap">
                <div class="price">{!! format_price($lot->range_from, $lot->range_to, $lot->currency) !!}</div>
                <div class="desc">
                    <span>{{ str_limit($lot->title, $limit = 80, $end = ' ...') }}</span>
                </div>
                @if ($lot->is_active)
                    <div class="hover-info-bottom">
                        <div class="time-wrap-line" style="width: 100%">
                            <div class="time-line">
                                <div class="time-line-left" style="width:{{ percent_time_left($lot->start_date, $lot->end_date) }}%"></div>
                            </div>
                            <span class="time-text">
                                {{ format_time_left($lot->end_date) }} left
                            </span>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </a>
</div>
