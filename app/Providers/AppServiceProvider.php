<?php

namespace App\Providers;

use App\Models\Fee;
use App\Models\Lot;
use App\Observers\LotObserver;
use App\Services\Contracts;
use App\Services;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\URL;
use Validator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Lot::observe(LotObserver::class);
        Schema::defaultStringLength(255);

        if($this->app->environment('production')) {
            
            URL::forceScheme('https');

            $this->app['request']->server->set('HTTPS', true);
        }

        Validator::extend('greater_than_field', function ($attribute, $value, $parameters, $validator) {
            $data = $validator->getData();
            $compareFieldName = $parameters[0];
            $compareFieldValue = $data[$compareFieldName];

            return $value > $compareFieldValue;
        });

        Validator::replacer('greater_than_field', function ($message, $attribute, $rule, $parameters) {
            return str_replace('_', ' ' , 'The ' . $attribute . ' must be greater than the ' . $parameters[0]);
        });

        Validator::extend('less_than_field', function ($attribute, $value, $parameters, $validator) {
            $data = $validator->getData();
            $compareFieldName = $parameters[0];
            $compareFieldValue = $data[$compareFieldName];

            return $value < $compareFieldValue;
        });

        Validator::replacer('less_than_field', function ($message, $attribute, $rule, $parameters) {
            return str_replace('_', ' ' , 'The ' . $attribute . ' must be less than the ' . $parameters[0]);
        });

        Validator::extend('password_correct', function ($attribute, $value, $parameters, $validator) {
            return Hash::check($value, Auth::user()->password);
        });

        Validator::extend('availableFee', function ($attribute, $value, $parameters, $validator) {
            $fees = \Auth::user()->fees->pluck('fee');
            $fees->push(Fee::DEFAULT_FEE);

            if(\Request::route('id')) {
                $fees->merge(Lot::where('id', \Request::route('id'))->pluck('auction_house_fee'));
            }

            return $fees->contains($value);
        });
        Validator::replacer('availableFee', function ($message, $attribute, $rule, $parameters) {
            return str_replace('_', ' ' , 'The ' . $attribute . ' contains an unavailable value.');
        });

        $this->app->bind(Contracts\EmailMarketingServiceInterface::class, Services\EmailMarketingService::class);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
