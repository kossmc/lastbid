<div class="popup" id="modal-id">
    <div class="modal-content" style="height: 620px">
        <form action="" id="reg-form">
            <div class="container-form">
                <div class="form-item log-in">
                    <div class="table">
                        <div class="table-cell">
                            <div class="title" style="margin: 8px 0 40px;">LET'S TALK
                                @if (!empty($mess))
                                    <div class="row">
                                            <span class="error-notification"
                                                  style="width: 400px; margin: 20px auto 0;">{{$mess}}</span>
                                    </div>
                                @endif
                            </div>
                            <p>We’re keen to start a dialogue with those
                                in the auction world to discuss how we could help,
                                so please do get in touch.
                            </p>
                            <div class="row">
                                <div class="inp-wrap">
                                    <input type="text" placeholder="Name *" name="name" required
                                           class="{{ $errors->has('name') ? 'has-error--bgcolor' : '' }}">
                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="inp-wrap">
                                    <input type="text" placeholder="Company *" name="company" required
                                           class="{{ $errors->has('company') ? 'has-error--bgcolor' : '' }}">
                                    @if ($errors->has('company'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('company') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="inp-wrap">
                                    <input type="text" placeholder="Job title *" name="job_title" required
                                           class="{{ $errors->has('job_title') ? 'has-error--bgcolor' : '' }}">
                                    @if ($errors->has('job_title'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('job_title') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="inp-wrap">
                                    <input type="email" placeholder="Email *" name="email" required
                                           class="{{ $errors->has('email') ? 'has-error--bgcolor' : '' }}">
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="inp-wrap">
                                    <input type="text" placeholder="Telephone" name="telephone"
                                           class="{{ $errors->has('telephone') ? 'has-error--bgcolor' : '' }}">
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="inp-wrap">
                                    <textarea placeholder="Message" name="message"
                                              class="{{ $errors->has('message') ? 'has-error--bgcolor' : '' }}"></textarea>
                                    @if ($errors->has('message'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('message') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="inp-wrap">
                                    <a href="javascript:void 0;" class="btn" onclick="auth('login')">Send</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

