<?php

namespace App\Listeners\Lot;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Event;
use App\Events\Lot\LotExpired;
use App\Models\Lot;

class LotChangedListener implements ShouldQueue
{


    /**
     * Handle the event.
     *
     * @param Lot $lot
     * @return void
     */
    public function handle(Lot $lot)
    {
        $changed = $lot->isDirty() ? $lot->getDirty() : false;
        // close lot in admin panel
        if ($changed && isset($changed['status']) && ($changed['status'] == Lot::STATUS_CLOSED || $changed['status'] == Lot::STATUS_SOLD_OUTSIDE)) {
            Event::fire(new LotExpired($lot));
        }
    }
}
