<?php

if (!function_exists('buildTree')) {
    /*
   |--------------------------------------------------------------------------
   | Build tree by parent id
   |--------------------------------------------------------------------------
   |
   | Recursive build tree with children's
   | Very useful for build tre of nested models
   |
   */
    function buildTree($elements, $parentId = 0)
    {
        $branch = array();

        foreach ($elements as $element) {
            if ($element->parent_id == $parentId) {
                $child = buildTree($elements, $element->id);
                if ($child) {
                    $element->child = $child;
                }
                $branch[] = $element;
            }
        }

        return $branch;
    }
}

if (!function_exists('recursive')) {
    function recursive($array)
    {
        foreach ($array as $key => $value) {
            //If $value is an array.
            if (isset($value['children']) && is_array($value['children'])) {
                //We need to loop through it.
                recursive($value['children']);
            } else {
                //It is not an array, so print it out.
                echo "$value->title, depth: $value->depth", '<br>';
            }
        }
    }
}


if (!function_exists('createTreeView')) {
    function createTreeView($array, $activeCat, &$html = '')
    {
        foreach ($array as $item) {
            $lot_count = \App\Models\Lot::where('category_id', $item->id)->active()->original()->count();
           // dump($lot_count);
            if (isset($item['child'])) {
                foreach ($item['child'] as $child_cat) {
                    $lot_count = $lot_count + \App\Models\Lot::where('category_id', $child_cat->id)->active()->original()->count();
                }
            }
            if (isset($item['child'])) {

                $active = '';
                if ((collect($item['child'])->contains('id', $activeCat) || $item->id == $activeCat) && !$item->parent_id) {
                    $active = 'active';
                }
                if ($item->id == $activeCat) {
                    $item_title = '<b>' . $item->title . " (" . $lot_count . ")" . '</b>';
                } else {
                    $item_title = $item->title . " (" . $lot_count . ")";
                }
                $html .= '<li class="level ' . $active . '">
                            <em class="arrow-icon"></em>
                                <a href="' . $item->url . '">' . $item_title . '</a>
                                    <ul>';
                $html .= createTreeView($item['child'], $activeCat);
                $html .= '</ul>
                           </li>';
            } else {
                if ($item->id == $activeCat) {
                    $item_title = '<b>' . $item->title . " (" . $lot_count . ")" . '</b>';
                } else {
                    $item_title = $item->title . " (" . $lot_count . ")";
                }
                $html .= '<li><a href="' . $item->url . '">' . $item_title . '</a></li>';
            }
        }
        return $html;
    }
}

if (!function_exists('createTreeCategories')) {
    function createTreeCategories($categories, $activeCat = '', &$html = '')
    {

        foreach ($categories as $item) {
            $active = (collect($item['child'])->contains('id', $activeCat) || $item->id == $activeCat) && !$item->parent_id ? 'active' : '';
            $item_title = $item->id == $activeCat ? '<b>' . $item->title . '</b>' : $item->title;
            $level = $item['child'] ? 'level' : '';

            $html .= '<li class="' . $level . ' ' . $active . '">';
            $html .= $level ? '<em class="arrow-icon"></em>' : '';
            $html .= '<div>' . $item_title . '</div>';

            if ($item['child']) {
                $html .= '<ul>' . createTreeCategories($item['child'], $activeCat) . '</ul>';
            }

        }

        return $html;
    }
}