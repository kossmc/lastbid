<?php
namespace App\Repositories;

use Bosnadev\Repositories\Eloquent\Repository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Event;
use App\Models\Message;
use App\Events\Message\BuyerCreateMessage;
use App\Events\Message\SellerCreateMessage;

/**
 * Class MessageRepository
 * @package App\Repositories
 */
class MessageRepository extends Repository
{

    public function model()
    {
        return Message::class;
    }

    /**
     * Create message
     * @param array $data
     * @return \Illuminate\Database\Eloquent\Model|null|static
     */
    public function create(array $data)
    {
        $data['buyer_id'] = Auth::id();

        /*reply of new*/
        $chainExist = Message::with('messageBuyer', 'messageSeller')
            ->where([
            ['lot_id', $data['lot_id']],
            ['buyer_id', $data['buyer_id']]
        ])->first();

        if ($chainExist) {
            if ($chainExist->reply($data['text'])) {

                if(!isset($data['not_send'])){
                    Event::fire(new BuyerCreateMessage($chainExist));
                }
            }
        } else {
            if ($mess = $this->model->newChain($data)) {
                if(!isset($data['not_send'])){
                    Event::fire(new BuyerCreateMessage($mess));
                }
            }
        }
        return $chainExist ? $chainExist : $mess;
    }

    /**
     * Show paginate and sorted received messages collection
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function received()
    {
        $query = $this->model->with( 'lot', 'messageBuyer', 'messageSeller')
            ->byUser(Auth::id())
            ->received()
            ->active();
        $query = $this->_setSorting($query);
        return $this->_paginate($query);
    }

    /**
     * Show paginate and sorted sent messages collection
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function sent()
    {
        $query = $this->model->with('lot', 'messageBuyer', 'messageSeller')
            ->byUser(Auth::id())
            ->sent()
            ->active();
        $query = $this->_setSorting($query);
        return $this->_paginate($query);
    }

    /**
     * Show paginate and sorted archive messages collection
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function archive()
    {
        $query = $this->model->with('lot', 'messageBuyer', 'messageSeller')
            ->byUser(Auth::id())
            ->archived();
        $query = $this->_setSorting($query);
        return $this->_paginate($query);
    }

    /**
     * User messages change action
     * make messages for logged user
     * as Read, Unread, Archived etc.
     * @param string $method
     * @param array $ids
     * @return void
     */
    public function switchStatus($method, $ids)
    {
        $this->model->switchStatus($method, $ids);
    }
    
    public function search($q)
    {
        $res = $this->model->withOutDeleted()
            ->messagesSearch($q)
            ->usersSearch($q)
            ->byUser(Auth::id());
        return $this->_paginate($res);
    }

    public function reply($id, $text)
    {
        if ($text) {
            $message = $this->model->with('messageBuyer', 'messageSeller')
                ->findOrFail($id);
            if ($message->reply($text)) {
                if ($message->by_seller) {
                    Event::fire(new SellerCreateMessage($message));
                }
                elseif ($message->by_buyer) {
                    Event::fire(new BuyerCreateMessage($message));
                }
            }
        }
    }

    public function getChain($id, $markAsRead = true)
    {
        $message = $this->model->with('lot', 'messageBuyer', 'messageSeller', 'history')
                            ->findOrFail($id);
        if ($markAsRead && !$message->is_read) {
            /*set message read*/
            $this->model->switchStatus('read', [$id]);
        }
        return $message;
    }
    /**
     * Sort messages collection
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    private function _setSorting($query)
    {
        if (request('sort_by_lot', 0)) {
           return $query->lotEndDate();
        }
        return $query->latestUpdated();
    }

    /**
     * Paginate messages collection
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    private function _paginate($query)
    {
        return $query->paginate(request('per_page', env('PAGINATE_MESSAGES')));
    }
}
