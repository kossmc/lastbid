@extends('layouts.inner')
@section('title', $lot->title)

@section('inner_content')
	<div class="lots-list-i current-bid-i">
		<div class="back-action">
			<a href="{{ route('seller_bids') }}" class="back-link-i"><em class="icon-right_arrow"></em></a>
		</div>
		<div class="image">
			<a href="{{ $lot->url }}">
				<img src="{{ $lot->getImagePath(78, 78) }}" alt="">
			</a>
		</div>
		<div class="description left">
			<div class="title"><h4><a href="{{ $lot->url }}">{{ $lot->title }}</a></h4></div>
			<div class="lot-number-block">
				<span class="text-small">LastBid Lot No</span>
				<span class="text-normal lot-number">{{$lot->id}}</span>
			</div>
			<div class="price-block">
				<span class="text-bigger price">{!! format_price($lot->range_from, $lot->range_to, $lot->currency)
				!!}</span>
				<span class="text-small">Suggested bid range</span>
			</div>
		</div>
		<div class="description right">
			<ul>
				<li>
					<span class="first-col">Start:</span>
					<span class="second-col">{{ $lot->start_date->format('j F Y, h:i A') }} EST</span>
				</li>
				<li>
					<span class="first-col">End:</span>
					<span class="second-col">{{ $lot->end_date->format('j F Y, h:i A') }} EST</span>
				</li>
				<li>
					<span class="first-col">Bids:</span>
					<span class="second-col">{{ $lot->bids_count }}</span>
				</li>

			</ul>
		</div>
	</div>
	<ul class="nav nav-tabs profile-tabs-nav current-bid-tabs-nav">
		<li class="active"><a data-toggle="tab" href="#tab-received-bids">Received bids</a></li>
		<li><a data-toggle="tab" href="#tab-sent-bids">Sent bids</a></li>
		<li><a data-toggle="tab" href="#tab-declined-bids">Declined / expired</a></li>
	</ul>
	<div class="tab-content profile-tab-content current-bid-tabs-content">
		<div id="tab-received-bids" class="tab-pane tab-lots fade in active">
			<div class="tab-inner">
                @if (empty($receivedBids))
                    <p class="notification-no-content">No received bids</p>
                @else
                    <ul class="nav nav-tabs current-bid-tabs-nav-level2">
                        @each('seller.inc.short_bid', $receivedBids, 'bid')
                    </ul>
                    <div class="tab-content current-bid-tabs-content-level2">
                        @foreach ($receivedBids as $bid)
                            @include('seller.inc.full_bid')
                        @endforeach 
                    </div>
                @endif
			</div>
		</div>
		<div id="tab-sent-bids" class="tab-pane tab-lots fade">
			<div class="tab-inner">
                @if (empty($sentBids))
                    <p class="notification-no-content">No sent bids</p>
                @else
                    <ul class="nav nav-tabs current-bid-tabs-nav-level2">
                        @each('seller.inc.short_bid', $sentBids, 'bid')
                    </ul>
                    <div class="tab-content current-bid-tabs-content-level2">
                        @foreach ($sentBids as $bid)
                            @include('seller.inc.full_bid')
                        @endforeach 
                    </div>
                @endif
			</div>
		</div>
		<div id="tab-declined-bids" class="tab-pane tab-lots fade">
			<div class="tab-inner">
                @if (empty($declinedBids))
                    <p class="notification-no-content">No declined / expired bids</p>
                @else
                    <ul class="nav nav-tabs current-bid-tabs-nav-level2">
                        @each('seller.inc.short_bid', $declinedBids, 'bid')
                    </ul>
                    <div class="tab-content current-bid-tabs-content-level2">
                        @foreach ($declinedBids as $bid)
                            @include('seller.inc.full_bid')
                        @endforeach 
                    </div>
                @endif
			</div>
		</div>
	</div>
@endsection
@section('user_script')
    $(function(){
        var fragment = window.location.hash;
        if (fragment == '#sent') {
            $('.nav-tabs a[href="#tab-sent-bids"]').tab('show');
            $('#tab-sent-bids .nav-tabs li:first a').trigger('click');
        } else if (fragment == '#received') {
            $('.nav-tabs a[href="#tab-received-bids"]').tab('show');
            $('#tab-received-bids .nav-tabs li:first a').trigger('click');
        }
    });
@endsection
@section('custom_modal')
	@if (!empty($receivedBids))
		@foreach($receivedBids as $bid)
		@include('inc.modal_confirm_accept_bid',['item'=>$bid, 'route'=>'seller_switch_bid_status'])
		@endforeach
	@endif
@endsection
