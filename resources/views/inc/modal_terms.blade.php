<div class="popup terms-popup" id="modal-id">
    <div class="modal-content">
        <div class="title">Terms and Conditions</div>
        <div class="wrapper-sign-up-agreement wrapper-terms-conditions">
            <div class="inner-wrapper-sign-up-agreement">
                @if(isset($terms))
                    {!! $terms !!}
                @else
                    @include('inc.terms')
                @endif
            </div>
        </div>
        <div class="btn-wrap">
            <a class="btn" href="javascript:void 0;" onclick="closeModal()">close</a>
        </div>
    </div>
</div>