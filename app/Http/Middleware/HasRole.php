<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class HasRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $roles = array_slice(func_get_args(), 2);
        if (!Auth::guest() && Auth::user()->hasAnyRole($roles)) {

            return $next($request);
        }
        if ($request->ajax()) {
            if(Auth::guest()){
                return response()->json([
                    'auth' => true,
                ]);
            }
            if(!Auth::user()->hasAnyRole($roles)){
                return response()->json([
                    'buyer' => false,
                    'modal' => view('auth.modal_login_as_buyer')->render()
                ]);
            }

        } else {
            return redirect()->guest('login');
        }
    }
}
