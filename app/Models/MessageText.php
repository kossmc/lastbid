<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class MessageText extends Model
{
    protected $table = 'message_texts';
    public $timestamps = true;
    protected $fillable = ['message_id', 'user_id', 'text'];

    public function letterStatus()
    {
        return $this->user_id == Auth::id() ? 'Sent' : 'Received';
    }
}
