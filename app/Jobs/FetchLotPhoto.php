<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class FetchLotPhoto implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 1;
    /**
     * @var
     */
    private $from;
    /**
     * @var
     */
    private $to;
    /**
     * @var
     */
    private $name;

    /**
     * Create a new job instance.
     *
     * @param $from
     * @param $to
     * @param $name
     */
    public function __construct($from, $to, $name)
    {
        $this->from = $from;
        $this->to = $to;
        $this->name = $name;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try{
            copy($this->from, $this->to);
        } catch (\Exception $exception){
            //@Todo implement log here
        }
    }


    public function failed(\Exception $e)
    {
        //@Todo implement log here
    }
}
