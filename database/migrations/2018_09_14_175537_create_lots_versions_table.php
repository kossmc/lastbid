<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLotsVersionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lots_version', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('version');
            $table->integer('lot_file_id')->unsigned();
            $table->integer('lot_id')->unsigned();

            $table->integer('category_id');
            $table->integer('partner_id');
            $table->string('title');
            $table->string('subtitle')->nullable();
            $table->string('slug');
            $table->text('description');
            $table->text('shipping_description');
            $table->integer('views_count')->default(0);
            $table->integer('bids_count')->default(0);
            $table->text('photos');
            $table->integer('user_id')->unsigned();
            $table->decimal('auction_house_fee',4,2)->default('20.00');
            $table->decimal('min_bid_amount',16,2);
            $table->decimal('range_from',16,2)->default('0.00');
            $table->decimal('range_to',16,2)->default('0.00');
            $table->enum('status', ['active', 'expired', 'sold', 'closed', 'sold_outside'])->default('active');
            $table->timestamp('start_date');
            $table->timestamp('end_date');
            $table->decimal('sold_amount',16,2);
            $table->integer('buyer_id');
            $table->timestamp('sold_date');
            $table->timestamp('last_bid_date');
            $table->decimal('auto_accept_price',16,2);
            $table->tinyInteger('popular')->default(0);
            $table->string('currency')->default('usd');
            $table->timestamps();

            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lots_version');
    }
}
