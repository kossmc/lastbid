<h3>You have received a new message from LastBid</h3>

<p>LET'S TALK</p>

<p>Message from: {{request('name')}}</p>
<p>Message company: {{request('company')}}</p>
<p>Message job title: {{request('job_title')}}</p>
<p>Message email: {{request('email')}}</p>
<p>Message telephone: {{request('telephone')}}</p>
<p>Message text: {{request('message')}}</p>
