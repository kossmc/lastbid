@if (Auth::check())
    <!-- Left side column. contains the sidebar -->
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- Sidebar user panel -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="https://placehold.it/160x160/00a65a/ffffff/&text={{ mb_substr(Auth::user()->name, 0, 1) }}"
                         class="img-circle" alt="User Image">
                </div>
                <div class="pull-left info">
                    <p>{{ Auth::user()->full_name }}</p>
                    <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                </div>
            </div>
            <!-- sidebar menu: : style can be found in sidebar.less -->
            <ul class="sidebar-menu">
                <li class="header">{{ trans('backpack::base.administration') }}</li>
                <!-- ================================================ -->
                <!-- ==== Recommended place for admin menu items ==== -->
                <!-- ================================================ -->
                <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/partner') }}"><i
                                class="fa fa-info"></i>
                        <span>Partners</span></a>
                <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/category') }}"><i
                                class="fa fa-book"></i>
                        <span>Categories</span></a>
                </li>
                <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/category-alias') }}"><i
                                class="fa fa-book"></i>
                        <span>Categories aliases</span></a>
                </li>
                <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/category-icons') }}"><i
                                class="fa fa-book"></i>
                        <span>Categories icons</span></a>
                </li>
                {{--<li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/lot') }}"><i class="fa fa-info"></i>--}}
                {{--<span>Lots</span></a>--}}
                {{--</li>--}}

                <li class="treeview">
                    <a href="{{ url(config('backpack.base.route_prefix', 'admin').'/lot') }}"><i class="fa fa-info"></i>
                        <span>Lots</span> <i
                                class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/lot-file') }}"><i
                                        class="fa fa-indent"></i>
                                <span>Import log</span></a>
                        </li>
                    </ul>
                </li>

                <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/specific') }}"><i
                                class="fa fa-info"></i>
                        <span>Specifics</span></a>
                </li>
                <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/event') }}"><i
                                class="fa fa-info"></i>
                        <span>Events</span></a>
                </li>
                <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/blog') }}"><i
                                class="fa fa-info"></i>
                        <span>Blog</span></a>
                </li>
                <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/slider') }}"><i
                                class="fa fa-image"></i>
                        <span>Slider</span></a>
                </li>
                <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/terms-conditions') }}"><i
                                class="fa fa-info-circle"></i><span>Terms & Conditions parser</span></a>
                </li>

                <!-- Users, Roles Permissions -->
                <li class="treeview">
                    <a href="#"><i class="fa fa-group"></i> <span>Users control</span> <i
                                class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/seller') }}"><i
                                        class="fa fa-user"></i><span>Sellers</span></a>
                        <li><a href="{{ url(config('backpack.base.route_prefix', 'admin') . '/user') }}"><i
                                        class="fa fa-user"></i> <span>Users</span></a></li>
                        <li><a href="{{ url(config('backpack.base.route_prefix', 'admin') . '/role') }}"><i
                                        class="fa fa-group"></i> <span>Roles</span></a></li>
                        <li><a href="{{ url(config('backpack.base.route_prefix', 'admin') . '/permission') }}"><i
                                        class="fa fa-key"></i> <span>Permissions</span></a></li>
                    </ul>
                </li>

                <!-- ======================================= -->
                <!-- FAQ questions -->
                <li class="treeview">
                    <a href="#"><i class="fa fa-search"></i> <span>FAQ</span> <i
                                class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/faq_category') }}"><i
                                        class="fa fa-comment"></i><span>FAQ categories</span></a>
                        <li><a href="{{ url(config('backpack.base.route_prefix', 'admin') . '/faq_question') }}"><i
                                        class="fa fa-comments"></i> <span>FAQ questions</span></a></li>
                    </ul>
                </li>
                <li class="treeview">
                    <a href="{{ url(config('backpack.base.route_prefix', 'admin').'/page-meta') }}"><i class="fa fa-tag"></i> <span>Pages Meta</span> </a>
                </li>
                <li class="treeview">
                    <a href="#"><i class="glyphicon glyphicon-duplicate"></i> <span>Static pages</span> <i
                                class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/contact-us') }}"><i
                                        class="glyphicon glyphicon-file"></i><span>Contact Us</span></a>
                        <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/site-terms-conditions') }}"><i
                                        class="glyphicon glyphicon-file"></i><span>Lastbid Terms & Conditions</span></a>
                        <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/how-it-works') }}"><i
                                        class="glyphicon glyphicon-file"></i><span>How It Works</span></a>
                        <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/about-us') }}"><i
                                        class="glyphicon glyphicon-file"></i><span>About Us</span></a>
{{--                        <li><a href="{{ url(config('backpack.base.route_prefix', 'admin') . '/user') }}"><i--}}
{{--                                        class="fa fa-user"></i> <span>Users</span></a></li>--}}
{{--                        <li><a href="{{ url(config('backpack.base.route_prefix', 'admin') . '/role') }}"><i--}}
{{--                                        class="fa fa-group"></i> <span>Roles</span></a></li>--}}
{{--                        <li><a href="{{ url(config('backpack.base.route_prefix', 'admin') . '/permission') }}"><i--}}
{{--                                        class="fa fa-key"></i> <span>Permissions</span></a></li>--}}
                    </ul>
                </li>

                <!-- ======================================= -->
                <li class="header">Actions</li>
                <li><a href="javascript:void 0;"
                       onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i
                                class="fa fa-sign-out"></i> <span>{{ trans('backpack::base.logout')
          }}</span></a></li>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </ul>
        </section>
        <!-- /.sidebar -->
    </aside>
@endif