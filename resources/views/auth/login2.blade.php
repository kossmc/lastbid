@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Fill account data</div>
                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" action="{{ url('/login_2') }}">
                            {{ csrf_field() }}
                            <input type="hidden" value="{{$url}}" name="url">
                            <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                                <label for="phone" class="col-md-4 control-label">Phone</label>

                                <div class="col-md-6">
                                    <input id="phone" type="text" class="form-control" name="phone"
                                    @if(!empty($user->phone)) value="{{$user->phone}}" @endif>

                                    @if ($errors->has('phone'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                                <label for="address" class="col-md-4 control-label">Address</label>

                                <div class="col-md-6">
                                    <input id="address" type="text" class="form-control" name="address"
                                           @if(!empty($user->address)) value="{{$user->address}}" @endif>

                                    @if ($errors->has('address'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('card_number') ? ' has-error' : '' }}">
                                <label for="card_number" class="col-md-4 control-label">Card number</label>

                                <div class="col-md-6">
                                    <input id="card_number" type="text" class="form-control" name="card_number"
                                           @if(!empty($user->card_number)) value="{{$user->card_number}}" @endif>

                                    @if ($errors->has('card_number'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('card_number') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('cvv') ? ' has-error' : '' }}">
                                <label for="cvv" class="col-md-4 control-label">Cvv</label>

                                <div class="col-md-6">
                                    <input id="cvv" type="text" class="form-control" name="cvv"
                                           @if(!empty($user->cvv)) value="{{$user->cvv}}" @endif>

                                    @if ($errors->has('cvv'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('cvv') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('exp_date') ? ' has-error' : '' }}">
                                <label for="exp_date" class="col-md-4 control-label">Expired date</label>

                                <div class="col-md-6">
                                    <input id="exp_date" type="text" class="form-control" name="exp_date"
                                           @if(!empty($user->exp_date)) value="{{$user->exp_date}}" @endif>

                                    @if ($errors->has('exp_date'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('exp_date') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Save
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
