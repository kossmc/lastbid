<?php
namespace App\Repositories;

use App\Models\LotSpecificOptions;
use App\Models\Specific;
use App\Models\Lot;
use App\Models\Category;
use App\Models\SpecificOption;
use Bosnadev\Repositories\Eloquent\Repository;
use Illuminate\Support\Collection;
use Illuminate\Container\Container as App;
use App\Traits\CsvReaderTrait;

class ImportSpecificRepository extends Repository
{
    use CsvReaderTrait;
    private $categories;

    /**
     * @param App $app
     * @param Collection $collection
     * @throws \Bosnadev\Repositories\Exceptions\RepositoryException
     */
    public function __construct(App $app, Collection $collection)
    {
        $this->categories = collect([]);
        parent::__construct($app, $collection);
    }

    public function model()
    {
        return Specific::class;
    }

    public function processFile($path)
    {
        $total = 0;
        foreach ($this->getCsvIterator($path) as $i => $row) {
            try {
                $data = $this->getData($row);
                /*save specific*/
                if ($specific = Specific::firstOrCreate($data['specific'])) {
                    $total ++;
                    /*save options if any*/
                    if ($data['options']->isNotEmpty()) {
                        $data['options']->each(function ($option) use ($specific) {
                            $option = SpecificOption::firstOrCreate([
                                'name' => $option,
                                'specific_id' => $specific->id
                            ]);
                            /*find all lots fitted with options*/
                            if ($option) {
                                $lots = Lot::where('category_id', $specific->category_id)
                                    ->where('description', 'like', "%{$option->name}%")
                                    ->get();
                                /*add specific and option for it*/
                                if ($lots && $lots->isNotEmpty()) {
                                    $lots->each(function ($lot) use ($specific, $option) {
                                        LotSpecificOptions::firstOrCreate([
                                            'lot_id'=> $lot->id,
                                            'specific_id'=> $specific->id,
                                            'specific_option_id' => $option->id
                                        ]);
                                    });
                                }
                            }
                            return true;
                        });
                    }
                }
            } catch (\Exception $e) {
            }
        }
        return $total ? $total : false;
    }

    protected function getData($row)
    {
        $category = $this->getCategory(trim($row[0]));
        $data = [
            'category' => $category,
            'specific' => [
                'category_id' => $category->id,
                'name' => trim($row[1]),
                'type' => $this->getType(trim($row[3])),
                'filter_type' => $this->getFilterType(trim($row[3])),
                'use_in_filter' => 1,
                'required' => 0,
                'order' => 0,
            ],
            'options' => collect(explode(',', trim($row[2])))->map(function ($e) {
                return trim($e);
            })
        ];
        return $data;
    }

    protected function getCategory($title) {
        if ($category = $this->categories->where('title', $title)->first()) {
            return $category;
        }
        if ($category = Category::firstOrCreate(['title' => $title])) {
            $this->categories->push($category);
            return $category;
        }
        return false;
    }

    protected function getType($type) {
        return $type == 'multi-select' ? 'multi-select' : 'select';
    }

    protected function getFilterType($type) {
        return $type == 'multi-select' ? 'checkbox' : 'radio';
    }
}
