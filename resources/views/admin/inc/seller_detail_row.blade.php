<div class="m-t-10 m-b-10 p-l-10 p-r-10 p-t-10 p-b-10">
    <div class="row">
        <div class="col-md-12">
            <!-- Default box -->
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title text-center">Seller sold lots statistic for last
                        12 months:</h3>
                </div>
                <div class="box-body">
                    <table class="table table-striped table-hover table-bordered">
                        <thead>
                        <tr>
                            <th>Month/Year</th>
                            <th>Total Sold</th>
                            <th>Total Auction House Fee</th>
                            <th>LastBid Fee</th>
                            <th>Status</th>
                            <th>Invoice Number</th>
                            <th>&nbsp;</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($statistic as $month)
                            <tr>
                                <td>{{$month->monthyear}}</td>
                                <td>{!! format_price($month->total) !!}</td>
                                <td>{!! format_price($month->total_fee) !!}</td>
                                <td>{!! format_price($month->last_bid_fee) !!}</td>
                                <td>{{$month->invoice_send ? 'Sent':'Not sent'}}</td>
                                <td>{{$month->invoice_number}}</td>
                                <td>
                                    <a href="{{route('seller-month-details',['id'=>$sellerId,
                                    'year'=>$month->year,'month'=>$month->month])}}"
                                       class="btn btn-xs btn-default"><i class="fa fa-eye"></i>Details</a>
                                    @if ($month->can_send_invoice)
                                        <form action="{{route('seller-send-month-invoice',['id'=>$sellerId,
                                    'year'=>$month->year,'month'=>$month->month])}}" method="post"
                                        style="display: inline-block">
                                        <button class="btn btn-xs btn-default">
                                            <i class="fa fa-envelope"></i>
                                            {{$month->invoice_send ? 'Resend Invoice':'Send Invoice'}}
                                        </button>
                                            {{csrf_field()}}
                                        </form>
                                    @endif
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="7"><h4 class="text-center">This seller sell nothing during last
                                        12 months</h4></td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div>
</div>
<div class="clearfix"></div>
