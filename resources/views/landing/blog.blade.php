@extends('landing.content')
@section('title','News')

@section('content_page')
    @include('blog.list_content', ['landing' => true])
@endsection