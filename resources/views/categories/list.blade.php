@extends('layouts.main')

@include('inc.page_metas', ['defTitle' => 'Catalog'])

@section('top-links')
    @include('inc.top_links')
@stop

@section('user_css')
    <link rel="stylesheet" href="/css/categoryLotsSlider.css">
@endsection
@section('scripts')
    <script src="{{ asset('js/lots-slider.js') }}"></script>

    <script type="text/javascript" src="{{ asset('js/jquery.touchSwipe.min.js') }}"></script>
@endsection

@section('side_menu')
    <div class="open-categories-left side-menu-category-page visible-xs">
        <a href="#" class="categories-left">
            <em class="icon-categories_icon"></em>
            <span class="text">categories</span>
        </a>
        <div class="sidebar-wrap">
            @include('inc.side_nav')
        </div>
    </div>
@endsection

@section('content')
    <div class="page-wrap">
        <div class="wrap">
            <div class="page-categories clearfix">
                <div class="sidebar-wrap hidden-xs">
                    @include('inc.side_nav', ['collapsable' => false])
                </div>
                <div class="category-wrap-item">
                    <div class="item-cat">
                        @include('inc.breadcrumbs')
                    </div>
                    @foreach ($featured as $category)
                        @php
                            $icon_default = '';
                        @endphp
                        @foreach($categories_icons as $icons)
                            @if($icons->id == $category->categories_icons_id)
                                @php
                                    $icon_default = $icons->image_default;
                                @endphp
                            @endif
                        @endforeach

                        <div class="item-cat">
                            <div class="title-cat">
                                @if($icon_default == '')
                                    <em class="{{ $category->icon }}"></em>
                                @else
                                    <img width="75px" src="{!! $icon_default !!}" alt="">
                                @endif
                                <div class="title-cat-text"> {{ $category->title }}</div>
                                <div class="link"><a href="{{ $category->url }}">Open Category</a></div>
                            </div>

                            <div class="most-popular clearfix hidden-xs">
                                @include('categories.inc.category_list_lot_wrapper', ['parent' => $category])
                            </div>

                            <div id="slider-category-{{ $category->id }}" class="clearfix visible-xs cat-lots-slider">
                                @include('categories.inc.category_list_lot_wrapper', ['parent' => $category])
                            </div>

                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

    <script>
        var sliders = {};
    </script>
@endsection

@section('user_script')
    let options = {
        'loop' : true,
        'itemPerSlide' : 2
    };

    @foreach($featured as $category )
        sliders.category{{$category->id}}_LotsSlider = new categoryLotsSlider('slider-category-'+{{$category->id}}, options);
        {{--sliders.category{{$category->id}}_LotsSlider = new categoryLotsSlider('slider-category-'+{{$category->id}}, options);--}}
    @endforeach
@endsection
