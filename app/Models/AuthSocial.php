<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AuthSocial extends Model
{
    protected $table = 'auth_social';
    public $timestamps = true;
    protected $guarded = [];
    public $appends = [];
}
