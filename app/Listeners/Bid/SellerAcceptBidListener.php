<?php

namespace App\Listeners\Bid;

use App\Events\Bid\SellerAcceptBid;

class SellerAcceptBidListener extends SendGridAbstractBidListener
{
    /**
     * Buyer Email subject.
     */
    private $buyerSubject = 'Congratulations! Your bid has been accepted';

    /**
     * Buyer Email template.
     */
    private $buyerTemplate = '691c811f-e839-4d70-8768-01c4f36e563b';

    /**
     * Seller Email subject.
     */
    private $sellerSubject = 'You have accepted a bid';

    /**
     * Seller Email template.
     */
    private $sellerTemplate = 'a5bf3cd7-ab1e-499e-b4ca-f3c62c603cdb';
    /**
     * Handle the event.
     *
     * @param  SellerAcceptBid  $event
     * @return void
     */
    public function handle(SellerAcceptBid $event)
    {
        $this->setData($event);
        /*notification to seller*/
        $this->sendMail($this->seller, $this->getData($this->sellerTemplate), $this->sellerSubject);
        /*notification to buyer*/
        $this->sendMail($this->buyer, $this->getData($this->buyerTemplate), $this->buyerSubject);
        /*notification to admin*/
        $this->sendMailToAdmin($this->getData($this->sellerTemplate), $this->sellerSubject);

    }
}
