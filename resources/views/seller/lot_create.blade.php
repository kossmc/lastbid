@extends('layouts.inner')
@section('title', 'Add new lot')

@section('user_css')

	<link rel="stylesheet" type="text/css" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
	<link rel="stylesheet" href="/css/jquery-ui-timepicker-addon.css">
	<link rel="stylesheet" href="/css/image_upload.css">

@endsection

@section('inner_content')

	@include('inc.flash_message')

	<div class="lot-edit lots-list-i current-bid-i">

		<form id="add-bank-account" method="POST" enctype="multipart/form-data"
			  action="{{route('seller_store_lots')}}" >

			<div class="form-controll form-controll--newline">
				<h3>Add new lot </h3>
			</div>
			<div class="form-controll form-controll--bigger form-controll--newline">
				<label>Lot name*</label>
				<input type="text" name="title" placeholder="Title"
					   value="{{old('title', $lot->title)}}"
					   class="{{ $errors->has('title') ? 'has-error--bgcolor' : '' }}"
						required>
				@include( 'seller.inc._input_error', ['name' => 'title'])
			</div>

			<div class="form-controll form-controll--newline form-controll--bigger">
				<label>Category*</label>
				<div class="select-style select-category">
					<span class="selected-suffix
						{{ $errors->has('category') ? 'has-error--bgcolor' : '' }}">
					</span>

					<ul class="select-category" style="display: none;">
						{!! createTreeCategories($categories) !!}
					</ul>

					<select name="category" style="display: none;">
						<option value="{{old('category', '')}}">{{old('category', '')}}</option>
					</select>

					@include( 'seller.inc._input_error', ['name' => 'category'])
				</div>
			</div>

			<div class="form-controll form-controll--newline form-controll--bigger" id="category-specific-table">
				@include('seller.inc._specifics', ['category' => $lot->category])
			</div>


			{{--<select name="status" style="display: none;">--}}
				{{--<option value="{{old('status', $lot->status)}}">{{old('status', $lot->status)}}</option>--}}
			{{--</select>--}}

			<div class="form-controll form-controll--small">
				<label>Start date*</label>
				<input type="text" name="start_date" placeholder="Start date"
					   value="{{old('start_date', date('Y-m-d', strtotime(Carbon\Carbon::now())))}}"
					   class="start_date form-control {{ $errors->has('start_date') ? 'has-error--bgcolor' : '' }}"
					   readonly="readonly"
						required>
				@include( 'seller.inc._input_error', ['name' => 'start_date'])
			</div>

			<div class="form-controll form-controll--small">
				<label>End date*</label>
				<input type="text" name="end_date" placeholder="End date"
					   value="{{old('end_date', date('Y-m-d', strtotime(Carbon\Carbon::now())))}}"
					   class="end_date form-control {{ $errors->has('end_date') ? 'has-error--bgcolor' : '' }}"
					   readonly="readonly"
						required>
				@include( 'seller.inc._input_error', ['name' => 'end_date'])
			</div>

			<div class="form-controll form-controll--small">
				<label>Lot photos</label>
				<div class="fileContainer btn-upload">
					Upload photos
					<input type="file" name="photos[]" class="image-uploader" id="lot_images" multiple/>
				</div>
			</div>

			<div data-target="lot_images" class="form-controll form-controll--bigger form-controll--newline uploaded-lot-images-block">
				<label>Uploaded photos</label>
				<div class="images-block-content"></div>
			</div>

			<div class="form-controll form-controll--bigger form-controll--newline">
				<label>Lot description</label>
				<textarea name="description" id="description" class="{{ $errors->has('description') ? 'has-error--bgcolor' : '' }}">
					{{old('description', $lot->description)}}
				</textarea>
				@include( 'seller.inc._input_error', ['name' => 'description'])
			</div>

			<div class="form-controll form-controll--bigger form-controll--newline">
				<label>Shipping description</label>
				<textarea name="shipping_description" id="shipping_description"  class="{{ $errors->has('shipping_description') ? 'has-error--bgcolor' : '' }}">
					{{old('shipping_description', $lot->shipping_description)}}
				</textarea>
				@include( 'seller.inc._input_error', ['name' => 'shipping_description'])
			</div>

			{{--last block--}}

			<div class="form-controll form-controll--half form-controll--newline">
				<label>Auction House Fee*</label>
				<div class="select-style">
					<span class="card_month selected-suffix {{ $errors->has('auction_house_fee') ? 'has-error--bgcolor' : '' }}">
						{{old('auction_house_fee', $fees->isNotEmpty() ? round($fees->first()->fee) :'')}}
					</span>
					<ul class="select-month" style="display: none;">
						@forelse($fees as $fee)
							<li>{{ round($fee->fee) }}</li>
						@empty
							<li>{{ round($defaultFee) }}</li>
						@endforelse
					</ul>

					<select name="auction_house_fee" style="display: none;">
						<option value="{{old('auction_house_fee', $fees->isNotEmpty() ? round($fees->first()->fee) :'') }}">
							{{old('auction_house_fee', $fees->isNotEmpty() ? round($fees->first()->fee) :'') }}
						</option>
					</select>
					@include( 'seller.inc._input_error', ['name' => 'auction_house_fee'])
				</div>
			</div>

			<div class="form-controll form-controll--half">
				<label>Currency*</label>
				<div class="select-style"><span
							class="card_month selected-suffix">{{old('currency', 'USD')}}</span>
					<ul class="select-month" style="display: none;">
						@foreach( App\Models\Lot::CURRENCY as $currency)
							<li>{{ $currency }}</li>
						@endforeach
					</ul>
					<select name="currency" style="display: none;">
						<option value="{{old('currency', 'USD')}}">{{old('currency', 'USD')}}</option>
					</select>
					@include( 'seller.inc._input_error', ['name' => 'currency'])
				</div>
			</div>

			<div class="form-controll form-controll--half form-controll--newline">
				<label>Low estimate*</label>
				<input type="text" name="range_from" placeholder="Price range from"
					   value="{{old('range_from', $lot->range_from)}}"
					   class="{{ $errors->has('range_from') ? 'has-error--bgcolor' : '' }}"
					   required>
				@include( 'seller.inc._input_error', ['name' => 'range_from'])
			</div>

			<div class="form-controll form-controll--half">
				<label>High Estimate</label>
				<input type="text" name="range_to" placeholder="Range To"
					   value="{{old('range_to', $lot->range_to)}}"
					   class="{{ $errors->has('range_to') ? 'has-error--bgcolor' : '' }}">
				@include( 'seller.inc._input_error', ['name' => 'range_to'])
			</div>

			<div class="form-controll form-controll--half form-controll--newline">
				<label>Auto Decline*</label>
				<input type="text" name="min_bid_amount" placeholder="Minimum bid amount"
					   value="{{old('min_bid_amount', $lot->min_bid_amount)}}"
					   class="{{ $errors->has('min_bid_amount') ? 'has-error--bgcolor' : '' }}"
						required>
				@include( 'seller.inc._input_error', ['name' => 'min_bid_amount'])
			</div>

			<div class="form-controll form-controll--half">
				<label>Auto Accept</label>
				<input type="text" name="auto_accept_price" placeholder="Price to auto-accept bid"
					   value="{{old('auto_accept_price', $lot->auto_accept_price)}}"
					   class="{{ $errors->has('auto_accept_price') ? 'has-error--bgcolor' : '' }}">
				@include( 'seller.inc._input_error', ['name' => 'auto_accept_price'])
			</div>

			<div class="form-controll form-controll--newline form-controll--large">
				<div class="btn-wrap">
					<button class="btn" type="submit">
						{{'Create new Lot'}}
					</button>
					<a class="btn2" href="{{route('seller_active_lots')}}">Cancel</a>
				</div>
			</div>
			{{csrf_field()}}
		</form>

	</div>

@endsection

@section('remote_js_scripts')
	<script type="text/javascript" src="/vendor/backpack/ckeditor/ckeditor.js"></script>
	<script type="text/javascript" src="/js/jquery-ui-timepicker-addon.js"></script>
	<script type="text/javascript" src="/js/jquery-ui-sliderAccess.js"></script>
	<script type="text/javascript" src="/js/image_upload.js"></script>

	<script>
		CKEDITOR.replace('description',{resize_enabled: false, height:'100px'});
		CKEDITOR.replace('shipping_description',{resize_enabled: false, height:'100px'});

        function initEndDatePicker(){
            $('.end_date').datetimepicker({
                dateFormat: 'yy-mm-dd',
                minDate: new Date($('.start_date').val())
            });
        }

        $('.start_date').change(function(){
            $('.end_date').datepicker("destroy");
            initEndDatePicker();
        });
	</script>
@endsection

@section('user_script')
		$('.start_date').datetimepicker({
			dateFormat: 'yy-mm-dd'
		});

		initEndDatePicker();
@endsection