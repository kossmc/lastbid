<?php

namespace App\Events\User;

use App\Models\User;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;

class BuyerRegistered
{
    use Dispatchable, SerializesModels;

    public $user;
    public $referrer;

    /**
     * Create a new event instance.
     *
     * @param User $user
     * @param string $referrer
     * @return void
     */
    public function __construct(User $user, $referrer)
    {
        $this->user = $user;
        $this->referrer = $referrer;
    }
}
