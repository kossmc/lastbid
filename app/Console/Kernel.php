<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\ExpiredBid::class,
        Commands\ExpiredLot::class,
        Commands\GenerateBuyersInvoices::class,
        Commands\Spider::class,
        Commands\FetchTermsConditions::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('bid:mark-expired')->everyMinute()->withoutOverlapping();
        $schedule->command('lot:mark-expired')->everyMinute()->withoutOverlapping();
        $schedule->command('fetch:terms')->daily()->withoutOverlapping();
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
