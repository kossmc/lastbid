<?php

namespace App\Http\Controllers;

use App\Models\TermsConditions;

class TermsController extends Controller {

    /**
     * Api for getting terms and conditions by alias
     * @param string $alias
     * @return \Illuminate\Http\JsonResponse
     */
    public function getTerms ($alias = 'all'){
        if($alias == 'all'){
            return response()->json([
                'terms' => TermsConditions::all(['alias', 'text'])
            ], 200);
        }
        if($alias == 'lastbid'){
            return response()->json([
                'form' => view()->make('inc.modal_terms')
                    ->with(['terms'=>\DB::table('site_terms_conditions')->first()->text])
                    ->render()
            ], 200);
        }
        return response()->json([
            'terms' => TermsConditions::where('alias', $alias)->first(['alias', 'text'])
        ], 200);
    }
}