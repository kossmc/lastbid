@extends('layouts.main')
@section('title', $blog_post->title)
@section('meta_description', $blog_post->meta_description)

@section('top-links')
    @include('inc.top_links')
@stop
@section('content')
    @include('inc.discover_more_modal')
    <div class="page-wrap">
        <div class="article-page clearfix">
            <div class="image-post" style="background-image:url('{{$blog_post->getHeaderImagePath(1680,308)}}');"></div>
            <div class="wrap clearfix">
                <div class="content-post">
                    <div class="title-page">
                        <h3>{{ $blog_post->title }}</h3>
                    </div>
                    <div class="status-wrap clearfix">
                        @if ($blog_post->post_date)
                            <div class="item-status">{{ $blog_post->post_date->format('j F Y') }}</div>
                        @endif
                    </div>
                    <div class="article">
                        {!! $blog_post->body !!}
                    </div>
                    {{--<p><a class="btn discoverMoreBtn">subscribe</a></p>--}}

                </div>
                <div class="sidebar-post">
                    <div class="title"><h3>Other News</h3>
                        <a href="{{url('blog')}}" class="view">view all news</a></div>
                    <div class="row-post">
                        @foreach ($blog_posts as $blog_post)
                        <div class="item clearfix">
                            <div class="image"><a href="{{ $blog_post->url }}"><img src="{{$blog_post->getImagePath
                            (90,68)}}" alt="{{ $blog_post->title }}"></a></div>
                            <div class="desc-wrap">
                                <div class="status-wrap clearfix">
                                    @if ($blog_post->post_date)
                                        <div class="item-status">{{ $blog_post->post_date->format('j F Y') }}</div>
                                    @endif
                                </div>
                                <div class="desc"><a href="{{ $blog_post->url }}">{{ $blog_post->title }}</a></div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
