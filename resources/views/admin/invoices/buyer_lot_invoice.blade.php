@extends('admin.invoices.invoice_layout')
@section('top_left_col')
    @if($sellerAccount)
        <p> From: {{$sellerAccount->holder_name}}<br>
            {{$sellerAccount->address}}<br>
            {{$sellerAccount->city}}{{$sellerAccount->zip?', '.$sellerAccount->zip:''}}<br>
            {{$seller->phone}}<br></p>
    @endif
    <p> To: {{$buyer->full_name}}<br></p>
@endsection
@section('top_right_col')
    <p>Invoice No: {{$lot->id}}<br>Date: {{$lot->sold_date->format('d/m/Y')}}</p>
@endsection
@section('bottom_left_col')
    @if($sellerAccount)
        <p> Bank Name: {{$sellerAccount->bank_name}}<br>
            Account Number: {{$sellerAccount->account_number}}<br>
            Routing Number: {{$sellerAccount->routing_number}}<br>
            Swift code : {{$sellerAccount->swift_code}}</p>
    @endif
@endsection
@section('table')
    <table class="table table-bordered">
        <thead>
        <tr class="text-nowrap">
            <th>LastBid Lot No</th>
            <th>Lot name</th>
            <th>Purchase Date</th>
            <th>Purchase Price</th>
            <th>Auction House Fee</th>
            <th>Total</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>{{$lot->id}}</td>
            <td class="text-left">{{$lot->title}}</td>
            <td>{{$lot->sold_date->format('d/m/Y')}}</td>
            <td>{!! format_money($lot->bidWithoutFee($lot->sold_amount)) !!}</td>
            <td>{!! format_money($lot->bidFee($lot->sold_amount)) !!}</td>
            <td>{!! format_money($lot->sold_amount) !!}</td>
        </tr>
        </tbody>
        <tfoot>
        <tr>
            <td colspan="5" class="text-left">TOTAL:</td>
            <td>${!! format_money($lot->sold_amount) !!}</td>
        </tr>
        </tfoot>
    </table>
@endsection
