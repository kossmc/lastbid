@extends('buyer.profile.profile')
@section('title', 'Shipping Info')
@section('profile_content')
    <div class="buyer-account">
        <section class="shipping">
            <div class="title">
                <h3>Shipping addresses</h3>
                <div class="btn-wrap"><a class="btn btn-buyer-add-shipping-card" 
                     href="{{route('shipping-info-new')}}">Add one more</a></div>
            </div>
            @forelse($addresses as $address)
            <div class="subtitle">
                <p>Address - {{$address->address_name}}</p>@if($address->default)<span class="default
                icon-done">Default</span>@else
                <a class="set-default" href="{{route('shipping-info-default',$address->id)}}">Set as default</a>@endif
            </div>
            <p><span>Address:</span>{{$address->address?$address->address:'&nbsp;'}}</p>
            <p><span>City:</span>{{$address->city?$address->city:'&nbsp;'}}</p>
            <p><span>State / province:</span>{{$address->state ? $address->state : '&nbsp;'}}</p>
            <p><span>Country:</span>{{$address->country?$address->country:'&nbsp;'}}</p>
            <p><span>Post code / zip:</span>{{$address->zip ? $address->zip : '&nbsp;'}}</p>
            <div class="btn-wrap"><a class="btn btn-buyer-edit-shipping" 
                         href="{{route('shipping-info-edit',$address->id)}}">Edit</a><a
                        class="btn2" href="javascript:void 0;" data-toggle="modal"
                        data-target="#conf_modal_{{$address->id}}">Delete</a></div>
            @empty
                <h1 class="notification-no-content">You have no active addresses</h1>
            @endforelse
        </section>
    </div>
@endsection
@section('custom_modal')
    @include('buyer.inc.confirm_deleting_pop_up',
    ['items'=>$addresses,'route'=>'shipping-info-delete', 'name'=>'shipping address'])
@endsection
