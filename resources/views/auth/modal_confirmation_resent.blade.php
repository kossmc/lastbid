<div class="modal fade" id="modal-id">
    <div class="modal-content">
        <div class="title">Email Verification</div>
        <p>Confirmation email was resent</p>
        <div class="btn-wrap">
            <a class="btn" href="javascript:void 0;" onclick="closeModal()">Ok</a>
        </div>
    </div>
</div>
