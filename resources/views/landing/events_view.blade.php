@extends('landing.content')
@section('title','Event')

@section('content_page')
    @include('events.view_content', ['landing' => true])
@endsection