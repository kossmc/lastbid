<?php

namespace App\Listeners\User;

use App\Events\User\SellerDeleteBankAccount;
use Illuminate\Contracts\Queue\ShouldQueue;

class SellerDeleteBankAccountListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SellerDeleteBankAccount $event
     * @return void
     */
    public function handle(SellerDeleteBankAccount $event)
    {
        //
    }
}
