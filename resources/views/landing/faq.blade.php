@extends('landing.content')
@section('title','faq')

@section('content_page')
    @include('pages.faq_content', ['search' => false])
@endsection