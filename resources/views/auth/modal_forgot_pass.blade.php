<div class="modal fade form-reset-lognin-password" id="modal-id">
    <div class="container-form-wrap">
        <a href="#" class="esc" data-dismiss="modal" aria-hidden="true">
            <em class="icon-esc_icon"></em>
            esc
        </a>
        <div class="container-forms">
            <div class="container-info">
                <div class="info-item">
                    <div class="table">
                        <div class="table-cell">
                            <div class="text">If you already have an account click the button below to enter your
                                account.
                            </div>
                            <div class="btn">
                                sign in
                            </div>
                        </div>
                    </div>
                </div>
                <div class="info-item">
                    <div class="table">
                        <div class="table-cell">
                            <div class="text">If you don't already have an account, click the button below to create your
                                account.
                            </div>
                            <div class="btn" onclick="auth('register_form')">
                                Register
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <form action="" id="forgot-pass-form">
            <div class="container-form">
                <div class="form-item log-in">
                    <div class="table">
                        <div class="table-cell">
                            <a href="javascript:void 0;"
                               onclick="closeModal()" class="icon-back_arrow"></a>
                            <div class="title">Reset password
                            </div>
                            <div class="restore-pass">
                                <div class="row">
                                    <div class="inp-wrap">
                                        @if (empty($message))
                                            <input type="text" placeholder="Your email" name="email"
                                                   value="{{ old('email') }}"
                                                   required
                                                   autofocus>
                                            @if ($errors->has('email'))
                                                <span class="help-block">
                                                 <strong>{{ $errors->first('email') }}</strong>
                                                </span>
                                            @endif
                                            @if (!empty($error))
                                                <span class="help-block">
                                                 <strong>{{ $error }}</strong>
                                                </span>
                                            @endif
                                        @else
                                            <p>{{$message}}</p>
                                        @endif
                                    </div>
                                </div>
                                <div class="btn-wrap">
                                    @if (empty($message))
                                        <a href="javascript:void 0;" class="btn" onclick="auth('forgot')">Send</a>
                                    @endif
                                </div>
                                <div class="row info">
                                    <a href="javascript:void 0;" class="btn-remember-return"
                                       onclick="auth('login_form')">I remembered my password</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </form>
        </div>
    </div>
    <div id="popup-overlay"></div>
</div>
