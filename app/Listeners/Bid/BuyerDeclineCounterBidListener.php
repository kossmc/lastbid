<?php

namespace App\Listeners\Bid;

use App\Events\Bid\BuyerDeclineCounterBid;
use App\Models\Bid;

class BuyerDeclineCounterBidListener extends SendGridAbstractBidListener
{
    /**
     * Buyer Email subject.
     */
    private $buyerSubject = 'You have declined a counter offer';

    /**
     * Buyer Email template with attempts.
     */
    private $buyerTemplateWithAttempts = 'baa09f02-da72-4694-9cd4-15e9de88b11b';

    /**
     * Buyer Email template without attempts.
     */
    private $buyerTemplateWithoutAttempts = 'ee6dd951-8698-4fe8-bc81-7a14d8c8a5f4';

    /**
     * Seller Email subject.
     */
    private $sellerSubject = 'Your countered offer was declined';

    /**
     * Seller Email template.
     */
    private $sellerTemplate = '66926da9-64ff-4705-a903-4caa4cdddff9';
    /**
     * Handle the event.
     *
     * @param  BuyerDeclineCounterBid  $event
     * @return void
     */
    public function handle(BuyerDeclineCounterBid $event)
    {
        $this->setData($event);
        /*notification to seller*/
        $this->sendMail($this->seller, $this->getData($this->sellerTemplate), $this->sellerSubject);
        /*notification to buyer*/
        $this->sendMail($this->buyer, $this->getData($this->getBuyerEmailTemplate()), $this->buyerSubject);
    }

    public function getBuyerEmailTemplate() {
        if ($this->lot->hasEnoughtAttempts($this->buyer->id)) {
            return $this->buyerTemplateWithAttempts;
        }
        return $this->buyerTemplateWithoutAttempts;
    }
}
