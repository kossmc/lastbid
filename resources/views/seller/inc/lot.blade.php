<div class="lots-list-i">
    <a href="{{ route('seller_lot', ['id' => $lot->id]) }}">
        <div class="statistic-short-info{{ $lot->new_bids_count == 0 ? ' statistic-short-info-middle' : ''}}">
            @if ($lot->new_bids_count > 0)
                <div class="new-bids">
                    <span class="text">new bids</span>
                    <span class="counter">{{ $lot->new_bids_count }}</span>
                </div>
            @endif
            <div class="total-bids">
                <span class="text">total bids</span>
                <span class="counter">{{ $lot->bids_count }}</span>
            </div>
        </div>
        <div class="image"><img src="{{ $lot->getImagePath(78, 78) }}" alt=""></div>
        <div class="description left">
            <div class="title"><h4>{!! $lot->title !!}</h4></div>
            <div class="lot-number-block">
                <span class="text-small">LastBid Lot No</span>
                <span class="text-normal lot-number">{{ $lot->id }}</span>
            </div>
            <div class="price-block">
                <span class="text-bigger price">{!! format_price($lot->range_from, $lot->range_to, $lot->currency) !!}</span>
                <span class="text-small">Suggested bid range</span>
            </div>
        </div>
        <div class="description right">
            <ul>
                <li>
                    <span class="first-col">Start:</span>
                    <span class="second-col">{{ $lot->start_date->setTimezone(session('time_zone'))->format('j F Y, h:i A') }}</span>
                </li>
                <li>
                    <span class="first-col">End:</span>
                    <span class="second-col">{{ $lot->end_date->setTimezone(session('time_zone'))->format('j F Y, h:i A') }}</span>
                </li>

            </ul>
        </div>
    </a>

</div>
