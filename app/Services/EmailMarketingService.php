<?php

namespace App\Services;

use App\Services\Contracts\EmailMarketingServiceInterface;
use CS_REST_Subscribers;

/**
 * Class EmailMarketingService
 * @package App\Services
 */
class EmailMarketingService implements EmailMarketingServiceInterface
{
    /**
     * @param string $email
     * @param string $listId
     * @return bool
     */
    public function subscribe($email, $listId)
    {
        $service = new CS_REST_Subscribers($listId, $this->getAuth());

        $result = $service->add([
            'EmailAddress'   => $email,
            'ConsentToTrack' => 'yes',
            'Resubscribe'    => true
        ]);

        return $result->was_successful();
    }

    /**
     * @param string $email
     * @param string $listId
     * @return bool
     */
    public function isSubscribed($email, $listId)
    {
        $service = new CS_REST_Subscribers($listId, $this->getAuth());

        $result = $service->get($email);

        if ($result->was_successful() && $result->response->State) {
            return 'Active' === $result->response->State;
        }

        return false;
    }

    /**
     * @return array
     */
    private function getAuth()
    {
        return [
            'api_key' => config('services.campaignmonitor.key')
        ];
    }
}
