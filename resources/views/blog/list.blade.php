@extends('layouts.main')

@include('inc.page_metas', ['defTitle' => 'Blog'])

@section('top-links')
    @include('inc.top_links')
@stop
@section('content')
    @include('blog.list_content', ['landing' => false])
@endsection
