<?php

namespace App\Events\User;

use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;

class ResetPassword
{
    use Dispatchable, SerializesModels;

    public $token;
    public $user;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($token, $user)
    {
        $this->token = $token;
        $this->user = $user;
    }
}
