<h3>You have received a new message from LastBid</h3>
@if(request()->has('type') && request()->get('type') == 'NL')
<p>NEWSLETTER</p>
@else
<p>DISCOVER MORE</p>
@endif

<p>Message email: {{request('email')}}</p>