<div class="group-wrap">
    <h3>Lot Status<span class="icon-right_arrow cat-icon-expanded cat-icon-mobile"></span></h3>
    <div class="status mobile-range-wrap">
        <ul>
            <li class="select-option">
                <input name="status" id="status-active" @if($selectedStatus == 'active') {{'checked'}}@endif value="active" type="radio">
                <label for="status-active" class="styled-select-button">Active</label>
            </li>
            <li class="select-option">
                <input name="status" id="status-sold" @if($selectedStatus == 'sold') {{'checked'}}@endif value="sold" type="radio">
                <label for="status-sold" class="styled-select-button">Sold</label>
            </li>
            <li class="select-option">
                <input name="status" id="status-closed" @if($selectedStatus == 'closed') {{'checked'}}@endif value="closed" type="radio">
                <label for="status-closed" class="styled-select-button">Closed</label>
            </li>
        </ul>
    </div>
</div>