<!-- array input -->

<?php
    $item_name = strtolower(isset($field['entity_singular']) && !empty($field['entity_singular']) ? $field['entity_singular'] : $field['label']);

    $items = old($field['name']) ? (old($field['name'])) : (isset($field['value']) ? ($field['value']) : (isset($field['default']) ? ($field['default']) : '' ));
    $input_index = 0;
    if (is_array($items)) {
        if (count($items)) {
            foreach ($items as $i => $v) {
                $items[$i]['input_id'] = $input_index = $i;
            }
            $items = json_encode($items);
        } else {
            $items = '[]';
        }
    } elseif (is_string($items) && !is_array(json_decode($items))) {
        $items = '[]';
    }

?>
<div @include('crud::inc.field_wrapper_attributes') >

    <label>{!! $field['label'] !!}</label>


    <div class="array-container form-group" id="specific_options">
        <input class="array-json" :value="output" type="hidden" id="{{ $field['name'] }}" name="{{ $field['name'] }}">

        <div>
            <dynamic-input
                    v-for="(input, index) in entireForm"
                    :index="index"
                    :key="input.input_id"
                    :inputdata="input"
            @user-input-changed="updateForm"
            @user-input-deleted="deleteEl"
            >

            </dynamic-input>
        </div>
        <div class="array-controls">
            <span class="btn btn-sm btn-default" @click="addNewOpt">
                <i class="fa fa-plus"></i> {{trans('backpack::crud.add')}} {{ $item_name }}
            </span>
        </div>
    </div>
</div>

{{-- ########################################## --}}
{{-- Extra CSS and JS for this particular field --}}
{{-- If a field type is shown multiple times on a form, the CSS and JS will only be loaded once --}}
@if ($crud->checkIfFieldIsFirstOfItsType($field, $fields))

    {{-- FIELD CSS - will be loaded in the after_styles section --}}
    @push('crud_fields_styles')
    {{-- @push('crud_fields_styles')
        {{-- YOUR CSS HERE --}}
    @endpush

    {{-- FIELD JS - will be loaded in the after_scripts section --}}
    @push('crud_fields_scripts')
        {{-- YOUR JS HERE --}}
        <script type="text/javascript" src="{{asset('js/vue.js')}}"></script>
        <script type="text/x-template" id="dynamicInput">
            <div>
                <input v-model="userInput.name" @keyup="grabUserInput($event.target.value)">
                <span class="btn btn-sm btn-default" @click="removeItem">
                    <i class="fa fa-trash"></i>
                </span>
            </div>
        </script>
        <script>
            var dynamicInput = Vue.component('dynamic-input', {
                template: '#dynamicInput',
                props: ['index','inputdata'],
                methods: {
                    grabUserInput: function(data) {
                        if (this.userInput.id) {
                            this.userInput.edited = true;
                        }
                        this.$emit('user-input-changed', this.userInput, this.index);
                    },
                    removeItem: function () {
                        this.$emit('user-input-deleted', this.userInput);
                        console.log(this.userInput, this.index);
                    }
                },
                data: function() {
                    return {
                        userInput: this.inputdata
                    };
                }
            });


            new Vue({
                el: '#specific_options',
                data: function() {
                    return {
                        entireForm: {!! $items !!},
                        deleted:[],
                        input_id: {!! $input_index !!}
                    };
                },
                computed:{
                    output: function () {
                        var form = {
                            data:this.entireForm,
                            deleted:this.deleted
                        };
                        return JSON.stringify(form);
                    }
                },
                methods: {
                    addNewOpt: function() {
                        this.input_id++;
                        this.entireForm.push({
                            input_id:this.input_id,
                            id:'',
                            name:'',
                            specific_id:'',
                            edited:false
                        });
                    },
                    updateForm: function (data, index) {
                        this.entireForm.splice(index, 1, data);
                    },
                    deleteEl: function (data) {
                        this.entireForm.splice(this.entireForm.indexOf(data),1);
                        if (data.id){
                            this.deleted.push(data);
                        }
                    }
                }
            });
        </script>

    @endpush
@endif
{{-- End of Extra CSS and JS --}}
{{-- ########################################## --}}
