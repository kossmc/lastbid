<?php

namespace App\Services;

use Cartalyst\Stripe\Laravel\Facades\Stripe;

class StripeService
{
    /**
     * The Stripe library implementation.
     *
     * @var Stripe
     */
    private $stripe;

    /**
     * Create a new service instance.
     *
     * @param Stripe $stripe
     * @return void
     */
    public function __construct()
    {
        $this->stripe = Stripe::make(env('STRIPE_SECRET'));
    }

    public function getCardToken($cardNumber, $expMonth, $expYear, $cvv)
    {
        $token = [];
        try {
            $token = $this->stripe->tokens()->create([
                'card' => [
                    'number' => $cardNumber,
                    'exp_month' => $expMonth,
                    'exp_year' => $expYear,
                    'cvc' => $cvv,
                ],
            ]);
            if ( ! isset($token['id'])) {
                $token['error'] = 'The Stripe Token was not generated correctly';
            }
        } catch (\Cartalyst\Stripe\Exception\CardErrorException $e) {
            $token['error'] = $e->getMessage();
        } catch (\Cartalyst\Stripe\Exception\MissingParameterException $e) {
            $token['error'] = $e->getMessage();
        } catch (\Exception $e) {
            $token['error'] = $e->getMessage();
        }
        return $token;
    }

    public function createUser($email)
    {
        return $this->stripe->customers()->create([
            'email' => $email
        ]);
    }

    public function addCartToUser($stripeUserId, $tokenId)
    {
        return $this->stripe->cards()->create($stripeUserId, $tokenId);
    }

    public function updateCard($stripeUserId, $stripeCardId, $data)
    {
        $params = ['address_city', 'address_line1', 'address_line2', 'address_state', 'address_zip',
            'exp_month', 'exp_year', 'name'];
        $data = array_intersect($params, $data);
        $card = $this->stripe->cards()->update($stripeUserId, $stripeCardId, $data);
        return $card;
    }
}
