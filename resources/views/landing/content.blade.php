
@extends('landing.main')

@section('user_css')
    <link href="{{ asset('css/landing.css?ver='.time())}}" rel="stylesheet">
@endsection

@section('content')
    <div class="header-phone">
        <div class="wrap">
            <div class="block-phone">
                <a href="tel:4402071833007">+44 (0) 20 7183 3007</a>
            </div>
        </div>
    </div>
    <div class="footer-fix">
        <div class="content-wrap">
            <div class="top">
                <div class="wrap">
                    <header>
                        <div class="logo"><a href="/"><img src="{{ asset('img/logo.svg')}}" alt="LastBid"></a></div>
                    </header>
                    <p class="part-one">where unsold auction lots come to be sold</p>
                    <p class="part-two brown">launching on march 31, 2019</p>
                </div>
            </div>

            <div class="wrap">
                <div class="head-mobile top-primary-menu">
                    <button class="hamburger">&#9776;</button>
                    <button class="cross">&#735;</button>
                </div>
            </div>

            <nav class="menu-mobile" style="display: none;">
                <ul>
                    <li><a href="/"  @if (Request::is('landing')) class="active" @endif>Home</a></li>
                    <li><a href="/how-it-works" @if (Request::is('landing/how-it-works')) class="active" @endif>How it works</a></li>
                    <li><a href="/about" @if (Request::is('landing/about')) class="active" @endif>About us</a></li>
                    {{--<li><a href="/events" @if (Request::is('landing/events')) class="active" @endif>Upcoming events</a></li>--}}
                    <li><a href="/news" @if (Request::is('landing/news')) class="active" @endif>News</a></li>
                    <li><a href="/faq" @if (Request::is('landing/faq')) class="active" @endif>Faq</a></li>
                </ul>
            </nav>
            <nav class="top-primary-menu">
                <ul>
                    <li><a href="/"  @if (Request::is('landing')) class="active" @endif>Home</a></li>
                    <li><a href="/how-it-works" @if (Request::is('landing/how-it-works')) class="active" @endif>How it works</a></li>
                    <li><a href="/about" @if (Request::is('landing/about')) class="active" @endif>About us</a></li>
                    {{--<li><a href="/events" @if (Request::is('landing/events')) class="active" @endif>Upcoming events</a></li>--}}
                    <li><a href="/news" @if (Request::is('landing/news')) class="active" @endif>News</a></li>
                    <li><a href="/faq" @if (Request::is('landing/faq')) class="active" @endif>Faq</a></li>
                </ul>
            </nav>


            @yield('content_page')

            <div class="footer clearfix">
                <footer>
                    <div class="wrap">

                        <div class="clear"></div>
                        <div class="social">
                            <a target="_blank" href="https://www.facebook.com/LastBidAuctions/" class="icon-facebook"></a>
                            <a target="_blank" href="https://www.instagram.com/lastbid_auctions" class="icon-instagram"></a>
                            <a target="_blank" href="https://www.linkedin.com/company/lastbid" class="icon-linkedin"></a>
                        </div>
                        <div class="copy">
                            <a id="privacy" class="privacy" href="{{env('APP_LANDING_URL') . 'privacy'}}">Privacy
                                Policy </a> &copy; <a href="#"> Copyright LastBid {{date('Y')}}</a>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
    </div>

@endsection
