<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class CategoryAlias extends Model
{
    use CrudTrait;
    protected $table = 'aliases';
    protected $guarded = [];

    public function categories()
    {
        return $this->belongsTo(Category::class, 'category_id', 'id');
    }
}
