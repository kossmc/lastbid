<div class="popup lot-email-verification" id="modal-id">
    <div class="modal-content">
        <div class="title">Email Verification</div>
        <p>To place a bid you need to verify your email address by clicking the link in the email</p>
        <p>Didn't receive confirmation email?</p>
        <div class="btn-wrap">
            <a class="btn" href="javascript:void 0;" onclick="auth('resend_confirmation')">Resend</a>
        </div>
        <p>
            <a class="link" href="{{route('account-info-edit')}}">Change email</a>
        </p>
    </div>
</div>
