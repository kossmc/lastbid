<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class BuyerProfileUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name'=>'required|string|max:190',
            'last_name'=>'required|string|max:190',
            'middle_name'=>'nullable|string|max:190',
            'suffix'=>'nullable|string|max:190',
            'email'=> [
                'required',
                'email',
                Rule::unique('users')->ignore(Auth::id()),
            ],
        ];
    }
}
