<?php

namespace App\Http\Controllers;

use App\Models\Event;
use App\Repositories\EventRepository;
use Carbon\Carbon;

class EventController extends Controller
{

    /**
     * The event repository implementation.
     *
     * @var EventRepository
     */
    private $event;

    /**
     * Create a new controller instance.
     *
     * @param EventRepository $event
     * @return void
     */
    public function __construct(EventRepository $event)
    {
        $this->event = $event;
    }

    /**
     * Get all events
     * @param null $year
     * @param null $month
     * @param null $day
     * @return mixed
     */
    private function all($year = null, $month = null, $day = null)
    {
        $date = null;
        if ($year) {
            $date = Carbon::createFromDate($year, $month, $day)->startOfDay();
        }
        $events = $date ? $this->event->getEventsByDate($date): $this->event->getEvents();

        $calendar = $this->event->getCalendar();

        return ['events' => $events, 'calendar'=>$calendar, 'date'=>$date ? $date: Carbon::now()->startOfDay()];
    }

    /**
     * Get event
     * @param $slug
     * @return mixed
     */
    private function get($slug)
    {
        if ($event = $this->event->findBySlug($slug)) {
            return ['event' => $event, 'events' => $this->event->latest(6, [$event->id])];
        }

        return null;
    }

    public function show_list($year = null, $month = null, $day = null)
    {
        if(!$day) return $this->show_month($year, $month);
        $events = $this->all($year, $month, $day);
        $meta = \App\Models\PageMeta::byPage('auction-calendar');
        return view('events.list', compact('events', 'meta'));
    }

    public function show_month($year = null, $month = null)
    {
        $date = Carbon::createFromDate($year,$month, 1);
        $start = Carbon::createFromDate($year,$month, 1)->firstOfMonth()->format('Y-m-d h:i:s');
        $end = Carbon::createFromDate($year,$month, 1)->lastOfMonth()->format('Y-m-d h:i:s');
        $events = $this->event->getMonthEvents($start, $end);
        $calendar = $this->event->getCalendar();
        $meta = \App\Models\PageMeta::byPage('auction-calendar');

        return view('events.list', compact('events', 'calendar', 'date', 'meta'));
    }

    /**
     * View event
     * @param null $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function view($slug = null)
    {
        $data = $this->get($slug);
        if(!$data)
            abort(404);

        return view('events.view', $data);
    }

    /**
     * Show all events landing
     * @param null $year
     * @param null $month
     * @param null $day
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show_list_landing($year = null, $month = null, $day = null)
    {
        \URL::forceRootUrl(env('APP_LANDING_URL'));

        return view('landing.events', $this->all($year, $month, $day));
    }

    /**
     * Views event landing
     * @param null $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function view_landing($slug = null)
    {
        \URL::forceRootUrl(env('APP_LANDING_URL'));

        $data = $this->get($slug);
        if(!$data)
            abort(404);

        return view('landing.events_view', $data);
    }
}
