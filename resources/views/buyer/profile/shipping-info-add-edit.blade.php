@extends('buyer.profile.profile')
@section('title', 'Shipping')
@php(!isset($edit)?$edit=false:'')
@section('profile_content')
    <div class="buyer-account">
        <section class="shipping">
            <h3>{{$edit?'Edit address':'Add new address'}}</h3>
            @include('inc.top_errors_list')
            <form method="POST" class="addEdit__form" action="{{$edit ? route('shipping-info-edit-save', $address->id)
                  :route('shipping-info-new')}}">
                {{csrf_field()}}
                <div class="form-controll form-controll-mob form-controll--medium">
                    <label>Address name</label>
                    <input type="text" name="address_name" placeholder="Name"
                           value="{{old('address_name', $address->address_name)}}"
                           class="{{ $errors->has('address_name') ? 'has-error--bgcolor' : '' }}">
                </div>
                <div class="form-controll form-controll-mob form-controll--medium">
                    <label>Address</label>
                    <input type="text" name="address" placeholder="Address"
                           value="{{old('address', $address->address)}}"
                           class="{{ $errors->has('address') ? 'has-error--bgcolor' : '' }}">
                </div>
                <div class="form-controll form-controll-mob form-controll--small form-controll--newline">
                    <label>City</label>
                    <input type="text" name="city" placeholder="City"
                           value="{{old('city', $address->city)}}"
                           class="{{ $errors->has('city') ? 'has-error--bgcolor' : '' }}">
                </div>
                <div class="form-controll form-controll-mob form-controll--small">
                    <label>State / province</label>
                    <input type="text" name="state" placeholder="State"
                           value="{{old('state', $address->state)}}"
                           class="{{ $errors->has('state') ? 'has-error--bgcolor' : '' }}">
                </div>
                <div class="form-controll form-controll-mob form-controll--small form-controll--newline">
                    <label>Country</label>
                    <input type="text" name="country" placeholder="Country"
                           value="{{old('country', $address->country)}}"
                           class="{{ $errors->has('country') ? 'has-error--bgcolor' : '' }}">
                </div>
                <div class="form-controll form-controll-mob form-controll--small">
                    <label>Post code / zip</label>
                    <input type="text" name="zip" placeholder="Code"
                           value="{{old('zip', $address->zip)}}"
                           class="{{ $errors->has('zip') ? 'has-error--bgcolor' : '' }}">
                </div>
                <div class="form-controll form-controll-mob form-controll--medium text-center">
                    <div class="btn-wrap">
                        <a class="btn2 btn-buyer-cancel-address" href="{{route('shipping-info')}}">Cancel</a>
                        <button type="submit" class="btn btn-buyer-save-address">Save</button>
                    </div>
                </div>
            </form>
        </section>
    </div>
@endsection
