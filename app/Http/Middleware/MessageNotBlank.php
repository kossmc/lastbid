<?php

namespace App\Http\Middleware;

use Closure;

class MessageNotBlank
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->input('letter-reply-msg', '')) {
            return $next($request);
        }
        return response()->json([
            'status' => false,
            'error' => 'Message can not be blank'
        ]);
    }
}
