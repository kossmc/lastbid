@extends('layouts.main')
@section('title','Register')
@section('top-links')
    @include('inc.top_links')
@stop
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Register</div>
                <div class="panel-body">
                    @if(Session::has('message'))
                        <div class="notification-sent-msg">
                            <p>{{ Session::pull('message') }}</p>
                        </div>
                    @endif
                    @if(Session::has('error'))
                        <div class="notification-sent-msg">
                            <p>{{ Session::pull('error') }}</p>
                        </div>
                    @endif
                    @include('inc.top_errors_list')
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                            <label for="first_name" class="col-md-4 control-label">First Name</label>

                            <div class="col-md-6">
                                <input id="first_name" type="text" class="form-control" name="first_name" value="{{ old('first_name') }}" required autofocus>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                            <label for="last_name" class="col-md-4 control-label">Last Name</label>

                            <div class="col-md-6">
                                <input id="last_name" type="text" class="form-control" name="last_name" value="{{ old('last_name') }}" required>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">Email Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                            <label for="phone" class="col-md-4 control-label">Phone</label>

                            <div class="col-md-6">
                                <input id="phone" type="text" class="form-control" name="phone" required
                                       value="{{ old('phone') }}">
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                            <label for="address" class="col-md-4 control-label">Address</label>

                            <div class="col-md-6">
                                <input id="address" type="text" class="form-control" name="address" required
                                       value="{{ old('address') }}">
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('card_number') ? ' has-error' : '' }}">
                            <label for="card_number" class="col-md-4 control-label">Card Number</label>

                            <div class="col-md-6">
                                <input id="card_number" type="password" class="form-control" name="card_number" required
                                       value="{{ old('card_number') }}">
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('cvv') ? ' has-error' : '' }}">
                            <label for="cvv" class="col-md-4 control-label">CVV</label>

                            <div class="col-md-6">
                                <input id="cvv" type="password" class="form-control" name="cvv" required
                                       value="{{ old('cvv') }}">
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('exp_date') ? ' has-error' : '' }}">
                            <label for="exp_date" class="col-md-4 control-label">Expiry Date</label>

                            <div class="col-md-6">
                                <input id="exp_date" type="text" class="form-control" name="exp_date" required
                                       value="{{ old('exp_date') }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Register
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('user_script')
    $('#exp_date').inputmask({mask: "99/99", placeholder: "mm/yy"});
@stop
