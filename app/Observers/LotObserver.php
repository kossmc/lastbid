<?php

namespace App\Observers;

use App\Models\Lot;
use Illuminate\Support\Facades\File;

class LotObserver
{
    /**
     * Listen to the Lot created event.
     *
     * @param  Lot  $lot
     * @return void
     */
    public function created(Lot $lot)
    {
        if(!File::exists(public_path("uploads/lots/$lot->id"))) {
            mkdir(public_path("uploads/lots/$lot->id"));
        }
        $lotPhotos = [];
        if(count($lot->photos)){
            foreach ($lot->photos as $photo){
                $name =  explode('/', $photo)[1];
                if (File::copy(public_path("uploads/$photo"), public_path("uploads/lots/{$lot->id}/$name"))) {
                    unlink(public_path("uploads/$photo"));
                    $lotPhotos[] = "lots/{$lot->id}/$name";
                }
            }
            if(count($lotPhotos)){
                \DB::table('lots')->where('id', $lot->id)->update(['photos' => json_encode($lotPhotos)]);
            }

        }
    }


    /**
     * Listen to the Lot deleting event.
     *
     * @param  Lot  $lot
     * @return void
     */
    public function deleted(Lot $lot)
    {
        if(file_exists(public_path("uploads/lots/{$lot->id}"))){
            rmdir(public_path("uploads/lots/{$lot->id}"));
        }
    }
}