<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>LastBid – selling unsold auction lots online to a global audience</title>
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">

    <meta name="description" content="LastBid offers auction houses the chance to sell their unsold auction lots online, from wine and collectables to antiques and jewellery.">

    <!-- Styles -->
    <link href="{{ asset('css/jquery.formstyler.css')}}" rel="stylesheet">
    <link href="{{ asset('css/glyphter.css')}}" rel="stylesheet">
    <link href="{{ asset('css/colorbox.css')}}" rel="stylesheet">

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css?ver=1.1') }}">
    <link href="{{ asset('css/jquery.scrollbar.css?ver=1.1')}}" rel="stylesheet">
    <link href="{{ asset('css/style.css')}}" rel="stylesheet">
    <link href="{{ asset('css/modifications.css?ver=1.1')}}" rel="stylesheet">

    <link href="{{ asset('css/zabuto_calendar.css?ver=201820071') }}" rel="stylesheet">


    @yield('user_css')

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>

    <!-- Google Tag Manager -->

    <script>(function (w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start': new Date().getTime(), event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],

                    j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =

                    'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);

        })(window, document, 'script', 'dataLayer', 'GTM-TWSC7T9');</script>

    <!-- End Google Tag Manager -->
</head>
<body>

    @yield('content')


<!-- Scripts -->
<script src="{{ asset('js/jquery-1.12.3.min.js')}}"></script>
<script src="{{ asset('js/jquery-ui.js')}}"></script>
<script src="{{ asset('js/jquery.bxslider.js')}}"></script>
<script src="{{ asset('js/jquery.formstyler.js')}}"></script>
<script src="{{ asset('js/jquery.colorbox.min.js')}}"></script>
<script src="{{ asset('js/jquery.inputmask.bundle.min.js')}}"></script>
<script src="{{ asset('js/jquery.scrollbar.min.js')}}"></script>
<script src="{{ asset('js/autosize.js')}}"></script>

<script src="{{ asset('js/script.js')}}"></script>
<script src="{{ asset('js/slim.min.js') }}"></script>
<script src="{{ asset('js/validation.js') }}"></script>

<!-- Latest compiled and minified bootstrap JS -->
<script src="{{ asset('js/bootstrap.min.js')}}"></script>
<script src="{{ asset('js/privacy.js')}}"></script>

<link rel="stylesheet" type="text/css"
      href="https://cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.css"/>

<script src="https://cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.js"></script>
<!--<script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.js"></script>-->
<script>
    window.addEventListener("load", function () {
        window.cookieconsent.initialise({
            "palette": {
                "popup": {
                    "background": "#ffffff",
                    "text": "#333333"
                },
                "button": {
                    "background": "#caac8a",
                    "text": "#ffffff"
                }
            },
            "cookie": {
                "secure": true
            },
            "content": {
                "message": "This site uses cookies to improve your browsing experience, perform analytics and research, and conduct advertising. To change your preferences, see our Cookies & Tracking Notice. Otherwise, closing the banner or clicking Accept all Cookies indicates you agree to the use of cookies on your device.",
                "dismiss": "Accept all Cookies",
                "link": "Cookies &amp; Privacy Notice",
                "href": "/policy",
                "target": "_blank"
            }
        })
    });

    $(document).on('click', '.cc-link', function (e) {
        e.preventDefault();

        $('.privacy').click();
    });

</script>
    <script>
        $(document).ready(function(){

            $('.slider-head .slider').bxSlider({
                minSlides: 1,
                maxSlides: 1,
                moveSlides: 1,
                slideMargin: 0,
                responsive: true,
                auto: true,
                autoStart: true,
                autoHover: true,
                infiniteLoop: true,
                controls: true,
                pager: true,
            });
            $('.overlay').click(function () {
                $(this).remove();
            });
            $(".cross").hide();
            $(".menu-mobile").hide();
            $(".hamburger").click(function () {
                $(".menu-mobile").slideToggle("slow", function () {
                    $(".hamburger").hide();
                    $(".cross").show();
                });
            });
            $(".cross").click(function () {
                $(".menu-mobile").slideToggle("slow", function () {
                    $(".cross").hide();
                    $(".hamburger").show();
                });
            });
            (function ($) {
                $(function () {
                    $('.select select').styler();
                });
            })(jQuery);
        });
    </script>
<style>
    .cc-theme-block {
        height: 120px;
    }

    .cc-banner {
        border-top: 1px solid #E4E4E4;
    }

    .cc-compliance .cc-btn {
        display: inline-block;
        height: 44px;
        padding: 0 40px;
        border-radius: 22px;
        line-height: 44px;
        font-weight: bold;
        border: 0;
        font-size: 12px;
        color: #fff;
        text-transform: uppercase;
        background-color: #CAAC8A;
    }

    @media screen and (max-width: 480px) {
        .cc-compliance .cc-btn {
            display: block;
        }
    }
</style>

<script>
    $(document).ready(function () {
        @yield('user_script')
    });
</script>

    @yield('remote_js_scripts')

</body>
</html>