@extends('layouts.main')
@section('title', $event->title)
@section('meta_description', $event->meta_description)

@section('top-links')
    @include('inc.top_links')
@stop
@section('content')
    @include('events.view_content', ['landing' => false])
@endsection
