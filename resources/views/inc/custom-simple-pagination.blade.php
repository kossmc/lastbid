@if ($paginator->hasPages())
    <ul class="pagination">
        @if ($paginator->onFirstPage())
            <li class="disabled"><a href="javascript:void 0;" aria-label="Previous"><span aria-hidden="true"><em class="icon-right_arrow"></em></span></a></li>
            <li class="disabled active"><a href="#">1</a></li>
            <li><a href="{{ $paginator->url(2) }}">2</a></li>
            <li class="active"><a href="#">{{ '...' }}</a></li>
            <li><a href="{{ $paginator->url($paginator->lastPage()-1) }}">{{$paginator->lastPage()-1}}</a></li>
            <li><a href="{{ $paginator->url($paginator->lastPage()) }}">{{$paginator->lastPage()}}</a></li>
            <li><a href="{{ $paginator->nextPageUrl() }}" aria-label="Next"><span aria-hidden="true"><em
                                class="icon-right_arrow"></em></span></a></li>
        @elseif($paginator->currentPage() == $paginator->lastPage())
            <li><a href="{{ $paginator->previousPageUrl() }}" aria-label="Previous"><span aria-hidden="true"><em class="icon-right_arrow"></em></span></a></li>
            <li><a href="{{ $paginator->url(1) }}">1</a></li>
            <li><a href="{{ $paginator->url(2) }}">2</a></li>
            <li class="active"><a href="#">{{ '...' }}</a></li>
            <li><a href="{{ $paginator->url($paginator->lastPage()-1) }}">{{$paginator->lastPage()-1}}</a></li>
            <li class="disabled active"><a href="#">{{$paginator->lastPage()}}</a></li>
            <li class="disabled"><a href="#" aria-label="Next"><span aria-hidden="true"><em
                                class="icon-right_arrow"></em></span></a></li>
        @else
            <li><a href="{{ $paginator->previousPageUrl() }}" aria-label="Previous"><span aria-hidden="true"><em
                                class="icon-right_arrow"></em></span></a></li>
            <li><a href="{{ $paginator->url(1) }}">1</a></li>
            @if($paginator->currentPage()==2)
                <li class="disabled active"><a href="#">{{ $paginator->currentPage() }}</a></li>
                <li class="active"><a href="#">{{ '...' }}</a></li>
                <li><a href="{{ $paginator->url($paginator->lastPage()-1) }}">{{$paginator->lastPage()-1}}</a></li>
                <li><a href="{{ $paginator->url($paginator->lastPage()) }}">{{$paginator->lastPage()}}</a></li>
                <li><a href="{{ $paginator->nextPageUrl() }}" aria-label="Next"><span aria-hidden="true"><em
                                    class="icon-right_arrow"></em></span></a></li>
            @elseif ($paginator->currentPage()==$paginator->lastPage()-1)
                <li><a href="{{ $paginator->url(2) }}">2</a></li>
                <li class="active"><a href="#">{{ '...' }}</a></li>
                <li class="disabled active"><a href="#">{{ $paginator->currentPage() }}</a>
                </li>
                <li><a href="{{ $paginator->url($paginator->lastPage()) }}">{{$paginator->lastPage()}}</a></li>
                <li><a href="{{ $paginator->nextPageUrl() }}" aria-label="Next"><span aria-hidden="true"><em
                                    class="icon-right_arrow"></em></span></a></li>
            @else
                <li><a href="{{ $paginator->url(2) }}">2</a></li>
                @if($paginator->currentPage()!==3)
                    <li class="active"><a href="#">{{ '...' }}</a></li>
                @endif
                <li class="disabled active"><a href="#">{{ $paginator->currentPage() }}</a>
                @if($paginator->currentPage()!== $paginator->lastPage()-2)
                    <li class="active"><a href="#">{{ '...' }}</a></li>
                @endif
                <li><a href="{{ $paginator->url($paginator->lastPage()-1) }}">{{$paginator->lastPage()-1}}</a></li>
                <li><a href="{{ $paginator->url($paginator->lastPage()) }}">{{$paginator->lastPage()}}</a></li>
                <li><a href="{{ $paginator->nextPageUrl() }}" aria-label="Next"><span aria-hidden="true"><em
                                    class="icon-right_arrow"></em></span></a></li>
            @endif
        @endif
    </ul>
@else
    <ul class="pagination"><li></li></ul>
@endif
