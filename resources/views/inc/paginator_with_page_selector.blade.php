<div class="page-bottom">
    @if (isset($items) && $items->isNotEmpty())
        @if (isset($append))
            @php($items->appends($append))
        @endif
        @php($items->appends(['per_page' => $items->perPage()]))

        @if ($items->lastPage() < 9)
                {{ $items->links('inc.custom-pagination') }}
        @else
                {{ $items->links('inc.custom-simple-pagination') }}
        @endif
        @php (!isset($selector)?$selector=true:$selector=$selector)
        @if ($selector && $items->total() > 25)
            @include('inc.paginator_per_page', ['per_page' => $items->perPage()])
        @endif
    @endif
</div>
