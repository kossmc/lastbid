@if(Session::has('message'))
    <div class="notification-sent-msg">
        <p>{{ Session::pull('message') }}</p>
    </div>
@endif
