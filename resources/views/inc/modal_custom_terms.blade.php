<div class="popup terms-popup" id="modal-id">
    <div class="modal-content">
        <div class="title">Terms and Conditions</div>
        @if(!isset($text))
        <div class="title">LASTBID</div>
        <div class="wrapper-sign-up-agreement wrapper-terms-conditions" id="scrollColorCustom">
            <div class="inner-wrapper-sign-up-agreement">
                <p style="text-align: left;font-size: 12px;"><strong>Terms of Use</strong></p>
                <p style="text-align: left;font-size: 12px;">Your use of Our Site and the Service is subject to these terms and conditions together with any additional terms and policies referred to in them.</p>
                <div class="intro">
                    <p style="text-align: left;font-size: 12px;"><a href="#intro">INTRODUCTION</a></p>
                    <p style="text-align: left;font-size: 12px;"><a href="#glossary">Glossary</a></p>
                    <p style="text-align: left;font-size: 12px;"><a href="#site-use">USE OF OUR SITE AND THE SERVICE</a></p>
                    <p style="text-align: left;font-size: 12px;"><a href="#personal-data">Personal Data</a></p>
                    <p style="text-align: left;font-size: 12px;"><a href="#consumer_terms">Consumer Terms</a></p>
                    <p style="text-align: left;font-size: 12px;"><a href="#eligible_use">Eligible Use</a></p>
                    <p style="text-align: left;font-size: 12px;"><a href="#language">Language</a></p>
                    <p style="text-align: left;font-size: 12px;"><a href="#our_service">OUR SERVICE</a></p>
                    <p style="text-align: left;font-size: 12px;"><a href="#agreements_for_items">AGREEMENTS FOR ITEMS</a></p>
                    <p style="text-align: left;font-size: 12px;"><a href="#bidding_process">BIDDING AND PURCHASE PROCESS</a></p>
                    <p style="text-align: left;font-size: 12px;"><a href="#bidding">Bidding</a></p>
                    <p style="text-align: left;font-size: 12px;"><a href="#purchase_agreements">Purchase Agreements</a></p>
                    <p style="text-align: left;font-size: 12px;"><a href="#prices">PRICES</a></p>
                    <p style="text-align: left;font-size: 12px;"><a href="#your_responsibilities">YOUR RESPONSIBILITIES</a></p>
                    <p style="text-align: left;font-size: 12px;"><a href="#cancellations_and_refunds">CANCELLATIONS AND REFUNDS</a></p>
                    <p style="text-align: left;font-size: 12px;"><a href="#faulty_items">Faulty Items</a></p>
                    <p style="text-align: left;font-size: 12px;"><a href="#promo">PROMO CODES AND REFERRALS</a></p>
                    <p style="text-align: left;font-size: 12px;"><a href="#prohibited use">PROHIBITED USE</a></p>
                    <p style="text-align: left;font-size: 12px;"><a href="#intellectual_property">INTELLECTUAL PROPERTY</a></p>
                    <p style="text-align: left;font-size: 12px;"><a href="#disclaimer">DISCLAIMER</a></p>
                    <p style="text-align: left;font-size: 12px;"><a href="#cannot_guarantee">We cannot guarantee Our Site or the Service</a></p>
                    <p style="text-align: left;font-size: 12px;"><a href="#indemnity">INDEMNITY</a></p>
                    <p style="text-align: left;font-size: 12px;"><a href="#modification_and_termination">MODIFICATION AND TERMINATION</a></p>
                    <p style="text-align: left;font-size: 12px;"><a href="#severability">SEVERABILITY</a></p>
                    <p style="text-align: left;font-size: 12px;"><a href="#governing_law_and_jurisdiction">GOVERNING LAW AND JURISDICTION</a></p>
                </div>
                <p class="privacy_heading" id="intro"><strong>INTRODUCTION</strong></p>
                <p style="text-align: left;font-size: 12px;">Thank you for choosing Last Bid, an online marketplace for items sold by auction.</p>
                <p style="text-align: left;font-size: 12px;">Our Terms constitute a legally binding agreement between you and Last Bid Investments Ltd (<b>“Last Bid”</b>, <b>“we”</b>, <b>"us”</b> or <b>“our”</b>). Last Bid is a private limited company registered in Guernsey whose registered office is located at the following address: Les Echelons Court Les Echelons St Peter Port GUERNSEY GY1 1AR.</p>
                <p class="privacy_heading" id="glossary"><strong>Glossary</strong></p>
                <p style="text-align: left;font-size: 12px;">In Our Terms, we use the following terms with the associated meanings:</p>
                <table>
                    <tr>
                        <td><b>"Auction House"</b></td>
                        <td>means the independent auctioneer providing the Item through our Service;</td>
                    </tr>
                    <tr>
                        <td><b>"Auction House Terms"</b></td>
                        <td>means the relevant Auction House terms of business that you will be required to agree and enter into before
                            you proceed with a Bid on any Item;
                        </td>
                    </tr>
                    <tr>
                        <td><b>"Bid"</b></td>
                        <td>means your offer to purchase an Item for the amount you specify;</td>
                    </tr>
                    <tr>
                        <td><b>"Charges"</b></td>
                        <td>means fees payable to the Auction House, delivery costs and VAT where applicable (which are payable in
                            addition to your Bid);
                        </td>
                    </tr>
                    <tr>
                        <td><b>"Item"</b></td>
                        <td>means a product, good or other item listed for sale by an Auction House on Our Site;</td>
                    </tr>
                    <tr>
                        <td><b>"Our Site"</b></td>
                        <td>means the website at [www.lastbid.com];</td>
                    </tr>
                    <tr>
                        <td><b>"Our Terms"</b></td>
                        <td>means these terms and conditions together with any terms and policies referred to in them;</td>
                    </tr>
                    <tr>
                        <td><b>"Purchase Agreement"</b></td>
                        <td>means the agreement (on the basis of the Auction House Terms) that is entered into between you and the
                            Auction House when you purchase an Item;
                        </td>
                    </tr>
                    <tr>
                        <td><b>"Privacy Policy"</b></td>
                        <td>means our statement describing how we will use your personal information, which can be accessed here
                            [www.lastbid.com];
                        </td>
                    </tr>
                    <tr>
                        <td><b>"Successful Bid Notification"</b></td>
                        <td>means an email sent to you to notify you that your Bid has been accepted by the Auction House;</td>
                    </tr>
                    <tr>
                        <td><b>"the Service"</b></td>
                        <td>means the online auction service provided through Our Site;</td>
                    </tr>
                </table>
                <p class="privacy_heading" id="site-use"><strong>USE OF OUR SITE AND THE SERVICE</strong></p>
                <p style="text-align: left;font-size: 12px;">Our Terms govern your use of Our Site and the Service. By using Our Site or the Service, you agree to comply with Our Terms.</p>
                <p style="text-align: left;font-size: 12px;">Please read Our Terms carefully and thoroughly. If you do not accept Our Terms, you must not register for an account with us or make any Bids for Item</p>
                <p class="privacy_heading" id="personal-data"><strong>Personal Data</strong></p>
                <p style="text-align: left;font-size: 12px;">In order to provide the Service, we will collect personal information from you, such as your name, contact information and credit card details. We respect your right to privacy and we will comply with applicable data protection legislation and our Privacy Policy. Our Privacy Policy forms part of Our Terms and set outs further information about how and why we collect your personal information, and for what purposes we are permitted to use it. Before registering for an account with us or submitting any Bids, please read our Privacy Policy [www.lastbid.com]. Our Site and the Service are not intended for persons under the age of 18, and we do not knowingly collect personal information from persons under the age of 18. We welcome any questions you may have about our handling of your personal information, and you can contact us at <a href="mailto:info@lastbid.com">info@lastbid.com</a>.</p>
                <p style="text-align: left;font-size: 12px;">We will never charge any amount to your credit card and we will use it for identification verification purposes only, in accordance with our Privacy Policy.</p>
                <p class="privacy_heading" id="consumer_terms"><strong>Consumer Terms</strong></p>
                <p style="text-align: left;font-size: 12px;">The Consumer Contracts (Information, Cancellation and Additional Charges) Regulations 2013 require us to give you certain key information for there to be a legally binding contract between you and us. This information is set out below and is also linked in the email which we will send to you to confirm any Bid you submit.</p>
                <p class="privacy_heading" id="eligible_use"><strong>Eligible Use</strong></p>
                <p style="text-align: left;font-size: 12px;">You confirm that you are of legal age to access and use Our Site and / or the Service and are of legal capacity to agree to Our Terms. You are not eligible to use the Service if you are under the age of 18.</p>
                <p class="privacy_heading" id="language"><strong>Language</strong></p>
                <p style="text-align: left;font-size: 12px;">We only conclude contracts in the English language.</p>
                <p class="privacy_heading" id="our_service"><strong>OUR SERVICE</strong></p>
                <p style="text-align: left;font-size: 12px;">The Service allows you to place Bids on a range of Items, which are provided in each and every case by independent Auction Houses. We have no responsibility for any Items which you purchase from an Auction House through us: we are simply involved with administering the bidding process.</p>
                <p style="text-align: left;font-size: 12px;">The Service we offer allows you to search through Our Site and submit Bids on Items made available by a number of Auction Houses. As part of the Service, we may also provide you with customer service assistance.</p>
                <p style="text-align: left;font-size: 12px;">The contract for the purchase of any Item is between you and the Auction House. This means that it is the Auction House (not us) who is legally responsible for providing the Item to you. As the provider of the Service, we are not responsible for providing the Item to you. However, we remain responsible in respect of our obligations to you in relation to the Service in accordance with Our Terms which shall be legally binding. If you have any questions or complaints, we can be contacted directly at <a href="mailto:info@lastbid.com">info@lastbid.com</a>. We will remain the point of contact for you for customer service questions.</p>
                <p style="text-align: left;font-size: 12px;">We shall, if requested, provide intermediary services between you and an Auction House in connection with customer service or dispute resolution matters.</p>
                <p class="privacy_heading" id="agreements_for_items"><strong>AGREEMENTS FOR ITEMS</strong></p>
                <p style="text-align: left;font-size: 12px;">When your Bid for any Item is successful, you will buy that Item directly from the relevant Auction House (not from us) and the contractual relationship in relation to the sale will be only between you and that Auction House. We will not be a party to it. The relevant Auction House will be responsible for the sale, delivery and other after-sale care and our role is limited to acting as a communication channel between you and the Auction House. </p>
                <p style="text-align: left;font-size: 12px;">You will be invoiced for the Item by the Auction House directly and must pay the Auction House in accordance with their terms; we do not collect payment from you on behalf of the Auction House.</p>
                <p style="text-align: left;font-size: 12px;">Though we may assist with certain practical issues on behalf of the relevant Auction House, we do not have any contractual obligations to you and you do not have any contractual rights against us regarding any Item on which you Bid or which is sold to you by any Auction House.</p>
                <p style="text-align: left;font-size: 12px;">You must agree to any Auction House Terms presented to you before you will be able to proceed. All Bids submitted by you and any resulting purchases made (and Purchase Agreements entered into) by you will be subject to the Auction House Terms to which you agree before you proceed.</p>
                <p class="privacy_heading" id="bidding_process"><strong>BIDDING AND PURCHASE PROCESS</strong></p>
                <p class="privacy_heading" id="bidding"><strong>Bidding</strong></p>
                <p style="text-align: left;font-size: 12px;">Bids may be submitted by clicking on the Item(s) on which you wish to bid and then following the prompts that will appear on-screen. You may check and correct any input errors in your Bid up until the point at which you submit your Bid. The Bid is submitted by clicking the ["Place a Bid"] button on the Item page. </p>
                <p style="text-align: left;font-size: 12px;">After submitting a Bid, you will receive an acknowledgment email that your Bid has been received [and giving you a Bid reference number]. Please note that this does not mean that your Bid has been accepted. Your Bid constitutes an offer to the relevant Auction House to buy the Item(s) at the price specified in the Bid. All Bids are subject to acceptance by the relevant Auction House. The Auction House is not obliged to accept your Bid and may, at their discretion, decline to accept any Bid, even if your Bid is the highest bid on an Item.</p>
                <p style="text-align: left;font-size: 12px;">You may withdraw a Bid at any time before it is accepted by the Auction House, by clicking on ["Withdraw Bid"] on the Item page</p>
                <p style="text-align: left;font-size: 12px;">You do, however, acknowledge that by clicking on the ["Place a Bid"] button, you will in accordance with the relevant Auction House Terms enter into an obligation to pay for the Item and the Charges if your bid is accepted by the Auction House, unless you first withdraw the Bid. Where your Bid is accepted, such acceptance will be confirmed to you by sending you a Successful Bid Notification. You will receive a Successful Bid Notification in respect of each Item for which your Bid is accepted.</p>
                <p style="text-align: left;font-size: 12px;">If your Bid is not successful, or the Auction House wishes to make a counter-bid, you will be sent an email notifying you.</p>
                <p class="privacy_heading" id="purchase_agreements"><strong>Purchase Agreements</strong></p>
                <p style="text-align: left;font-size: 12px;">The Purchase Agreement will only be formed and become binding when the Successful Bid Notification is sent to you. After entering into the Purchase Agreement, the relevant Auction House will be responsible for supplying you with the Item in accordance with the Purchase Agreement and the relevant Auction House Terms.</p>
                <p style="text-align: left;font-size: 12px;">The Purchase Agreement will relate only to the Item(s) which have been confirmed in the Successful Bid Notification. The relevant Auction House will not be obliged to supply any other Item(s) for which you may have submitted a Bid, unless and until such Item(s) have been confirmed in a separate Successful Bid Notification.</p>
                <p style="text-align: left;font-size: 12px;">You will be invoiced by the Auction House and must pay for the Item in accordance with the Purchase Agreement. Delivery of the Item will be fulfilled in accordance with the Purchase Agreement or as otherwise agreed between you and the Auction House.</p>
                <p style="text-align: left;font-size: 12px;">Please also note that you must comply with all applicable laws and regulations of the country for which the Item(s) are destined. The relevant Auction House will not be liable for any breach by you of any such laws.</p>
                <p class="privacy_heading" id="prices"><strong>PRICES</strong></p>
                <p style="text-align: left;font-size: 12px;">Your Bid does not include applicable Charges, which are payable in addition to the amount of your Bid if successful. The Charges will be shown before you click on the ["Confirm"] button and by clicking on the ["Confirm"] button, you agree to pay the Charges if your Bid is successful.</p>
                <p style="text-align: left;font-size: 12px;">Charges are liable to change at any time, but changes will not affect Bids in respect of which you have already been sent a Successful Bid Notification.</p>
                <p style="text-align: left;font-size: 12px;">Full details of Charges are set out on Our Site.</p>
                <p class="privacy_heading" id="your_responsibilities"><strong>YOUR RESPONSIBILITIES</strong></p>
                <p style="text-align: left;font-size: 12px;">In addition to the responsibilities noted above, it is your responsibility to provide complete and accurate information at the time of registering your account and making a Bid. Failure to provide complete and accurate information may result in a rejection of your Bid, cancellation of your Bid, or an inability of the Auction House to provide the Item. Such failure may also result in loss or incorrect delivery of your acknowledgement email or Successful Bid Notification.</p>
                <p style="text-align: left;font-size: 12px;">You must not share your account details (including your password) with anyone else. Any activity, including Bids, made on your account will be treated as having been made by you. You must tell us immediately if you suspect there has been any fraudulent activity on your account.</p>
                <p class="privacy_heading" id="cancellations_and_refunds"><strong>CANCELLATIONS AND REFUNDS</strong></p>
                <p style="text-align: left;font-size: 12px;">You may have statutory rights to cancel your Purchase Agreement and obtain a refund for an Item. The Purchase Agreement will provide further information.</p>
                <p class="privacy_heading" id="faulty_items"><strong>Faulty Items</strong></p>
                <p style="text-align: left;font-size: 12px;">If any Item you receive is damaged or faulty when delivered to you or has developed a fault, you may have one or more legal remedies available to you, depending on when you make the relevant Auction House aware of the problem, in accordance with your legal rights. If you believe an Item was delivered damaged or faulty or has developed a fault, you should inform the relevant Auction House as soon as possible, preferably in writing, giving your name, address and purchase reference (if provided).</p>
                <p style="text-align: left;font-size: 12px;">Please note that we have no control over any Auction House or the quality of any Items the Auction House provides, we do not give any commitment regarding them, and we are not able to provide, and have no responsibility or liability for providing, any compensation to you on behalf of any Auction House.</p>
                <p style="text-align: left;font-size: 12px;">Nothing in this section affects your legal rights.</p>
                <p class="privacy_heading" id="promo"><strong>PROMO CODES AND REFERRALS</strong></p>
                <p style="text-align: left;font-size: 12px;">We may from time to time create and offer promotional codes and referral codes (together "Codes") that can be redeemed against Bids on Items. Codes will only be valid for a period of time stated on or with them. </p>
                <p style="text-align: left;font-size: 12px;">Codes do not have any cash value. Codes may:</p>
                <p style="text-align: left;font-size: 12px;">(1) only be used for personal and non-commercial purposes. You can share your unique code with your personal connections via social media where you are the primary content owner. Codes may not be duplicated, sold, transferred, distributed or made available to others online (including through public sites such as coupon sites) or by other means;</p>
                <p style="text-align: left;font-size: 12px;">(2) not be promoted in any way including via a search engine;</p>
                <p style="text-align: left;font-size: 12px;">(3) not be exchanged for cash;</p>
                <p style="text-align: left;font-size: 12px;">(4) only be used once and only one Code may be used per person; and</p>
                <p style="text-align: left;font-size: 12px;">(5) may be subject to specific terms which will be made available by us, and must only be used in accordance with those terms.</p>
                <p style="text-align: left;font-size: 12px;">In addition, from time to time we may issue referral codes to allow you to refer friends and family to the Service, and which may be used as credit towards a Bid (<b>"Referral Codes"</b>). Referral Codes may also be subject to specific terms which will be made available by us, and you must only use the Referral Code in accordance with those terms.</p>
                <p style="text-align: left;font-size: 12px;">Discounts cannot be used in conjunction with any other offers or discounts.</p>
                <p class="privacy_heading" id="prohibited use"><strong>PROHIBITED USE</strong></p>
                <p style="text-align: left;font-size: 12px;">
                    The Service is for your personal and non-commercial use and must only be used for the purposes of submitting or reviewing Bids on Items as expressly described above. You must not use Our Site or the Service to do any of the following (each of which is strictly prohibited):
                </p>
                <ul>
                    <li style="font-size: 12px;text-align: left;">Restrict or inhibit any other user from using and enjoying the Service;</li>
                    <li style="font-size: 12px;text-align: left;">Infringe the privacy rights, property rights, or other civil rights of any person;</li>
                    <li style="font-size: 12px;text-align: left;">Harvest, data-mine or otherwise collect information about others, including email addresses, without their
                        consent;
                    </li>
                    <li style="font-size: 12px;text-align: left;">Use technology or other means to access our computer network, unauthorised content or non-public spaces;</li>
                    <li style="font-size: 12px;text-align: left;">Introduce or attempt to introduce any viruses or any other harmful code, files or programs that interrupt or otherwise or limit the Service or Our Site's functionality, or damage, disable or otherwise impair our servers or networks or attempt to do the same; or</li>
                    <li style="font-size: 12px;text-align: left;">Engage in or encourage others to engage in criminal or unlawful conduct or breach Our Terms including misuse of the Service for unlawful or unauthorised purposes.</li>
                </ul>
                <p style="text-align: left;font-size: 12px;">You agree not to breach Our Terms in any way which may result in, among other things, termination or suspension of your access to the Service.</p>
                <p class="privacy_heading" id="intellectual_property"><strong>INTELLECTUAL PROPERTY</strong></p>
                <p style="text-align: left;font-size: 12px;">LAST BID, the Last Bid logo, brand and all other intellectual property rights, trademarks, service marks, graphics and logos used in connection with the Our Site, or the Service (whether registered or unregistered) belong to us or our licensors (as are applicable) and are protected by intellectual property law. Nothing in Our Terms grants you any rights in Our Site or the Service or the content within the same. All rights are reserved.</p>
                <p class="privacy_heading" id="disclaimer"><strong>DISCLAIMER</strong></p>
                <p style="text-align: left;font-size: 12px;">As noted above, we act only as a communication channel between you and the Auction Houses. We are not liable or responsible for the fulfilment of any successful bid or the performance of the Auction House. You acknowledge and agree that we are not responsible for addressing any claims you have as regards any Auction House, however we do try to assist by providing the intermediary services around resolving disputes and complaints as mentioned above.</p>
                <p class="privacy_heading" id="cannot_guarantee"><strong>We cannot guarantee Our Site or the Service</strong></p>
                <p style="text-align: left;font-size: 12px;">Nothing in Our Terms will exclude or limit any warranty implied by law that it would be unlawful to exclude or limit and nothing in Our Terms will exclude or limit our liability in respect of any: death or personal injury caused by the negligence of Last Bid, fraud or fraudulent misrepresentation by Last Bid, or any matter for which it would be illegal or unlawful for Last Bid to exclude or limit, or to attempt or purport to exclude or limit, its liability.</p>
                <p style="text-align: left;font-size: 12px;">We are not liable or responsible for any errors in or failure to provide the Service due to your error or failure to provide accurate and complete information.</p>
                <p style="text-align: left;font-size: 12px;">Whilst we make every effort to ensure that the Service is available, we do not enter into any agreement to the effect that the Service will be available at all times or that the use by you of the Service will be entirely uninterrupted or error-free. We reserve the right to suspend or cease the operation of all or part of the Service from time to time at our sole discretion.</p>
                <p style="text-align: left;font-size: 12px;">Our Site and the Service is not intended for business use and is for consumer use only.  In agreeing to Our Terms you represent that your use of Our Site and the Service will not be for business purposes.  In no event shall we be liable to you for any business losses including any relevant loss of use, loss of data, lost revenues, loss of goodwill, or loss of anticipated saving or profits, arising out of or in any way connected with the use or performance of Our Site or the Service, or with the delay or inability to use Our Site or the Service, or with the provision of or failure to provide Our Site or the Service.</p>
                <p class="privacy_heading" id="indemnity"><strong>INDEMNITY</strong></p>
                <p class="privacy_heading" id="modification_and_termination"><strong>MODIFICATION AND TERMINATION</strong></p>
                <p style="text-align: left;font-size: 12px;">We may modify Our Terms or terminate use of the Service at any time by giving notice to you. If you do not agree to any changes, you must stop using the Service. We may also change, suspend, terminate or discontinue any aspect of the Service including availability of certain features at any time for any reason.</p>
                <p class="privacy_heading" id="severability"><strong>SEVERABILITY</strong></p>
                <p style="text-align: left;font-size: 12px;">If any provision of Our Terms is deemed or becomes invalid, the validity of the other provisions shall not be affected.</p>
                <p class="privacy_heading" id="governing_law_and_jurisdiction"><strong>GOVERNING LAW AND JURISDICTION</strong></p>
                <p style="text-align: left;font-size: 12px;">You agree that Our Terms for all purposes, shall be governed by and construed in accordance with English law. You also agree to submit to the exclusive jurisdiction of the English courts as regards any claim or matter arising under Our Terms.</p>
                <p style="text-align: left;font-size: 12px;">Please email <a href="mailto:info@lastbid.com">info@lastbid.com</a> for any enquiries.</p>
            </div>
        </div>
        @else
        <div class="title">{{ $partner->title }}</div>
        <div class="wrapper-sign-up-agreement wrapper-terms-conditions" id="scrollColorCustom">
                <div class="inner-wrapper-sign-up-agreement">
                    {!! $text !!}
                </div>
            </div>
        @endif
        <div class="btn-wrap">
            <button class="btn" id="lastBidTermsBtn" onclick="closeModal()" disabled>Close</button>
        </div>
    </div>
</div>