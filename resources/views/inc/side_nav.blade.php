@php (empty($activeCat) ? (empty($lot)?$activeCat = '':$activeCat = $lot->category_id) : true)
<div class="group-wrap {{ $class or '' }}">
    <h3 class="hidden-xs">ALL CATEGORIES</h3>
    <nav>
        <ul>
            {!! createTreeView($categories, $activeCat) !!}
        </ul>
    </nav>
</div>
