@if ($paginator->hasPages())
    <ul class="pagination">
        @if ($paginator->onFirstPage())
            <li class="disabled"><a href="javascript:void 0;" aria-label="Previous"><span aria-hidden="true"><em class="icon-right_arrow"></em></span></a></li>
        @else
            <li><a href="{{ $paginator->previousPageUrl() }}" aria-label="Previous"><span aria-hidden="true"><em class="icon-right_arrow"></em></span></a></li>
        @endif
            @foreach ($elements as $element)
                {{-- "Three Dots" Separator --}}
                @if (is_string($element))
                    <li class="active"><a href="#">{{ $element }}</a></li>
                @endif

                @if (is_array($element))
                    @foreach ($element as $page => $url)
                        @if ($page == $paginator->currentPage())
                            <li class="active"><a href="#">{{ $page }}</a></li>
                        @else
                            <li><a href="{{ $url }}">{{ $page }}</a></li>
                        @endif
                    @endforeach
                @endif
            @endforeach
        @if ($paginator->hasMorePages())
            <li><a href="{{ $paginator->nextPageUrl() }}" aria-label="Next"><span aria-hidden="true"><em class="icon-right_arrow"></em></span></a></li>
        @else
            <li><a href="javascript:void 0;" aria-label="Next"><span aria-hidden="true"><em class="icon-right_arrow"></em></span></a></li>
        @endif
    </ul>
@else
    <ul class="pagination"><li></li></ul>
@endif
