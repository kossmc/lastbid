@extends('layouts.main')

@include('inc.page_metas', ['defTitle' => 'Partners'])

@section('top-links')
    @include('inc.top_links')
@stop
@section('content')
    <div class="page-wrap">
        <div class="wrap">
            <div class="title-page">
                <h1>Partners</h1>
            </div>
            <div class="partners-page clearfix">
                <div class="row">
                    @foreach($partners as $partner)
                    <div class="item">
                        <div class="image"><img src="{{asset('uploads/'.$partner->image)}}" alt=""></div>
                        <div class="desc-wrap">
                            <h3>{{$partner->name}}</h3>
                            <div class="site"><a href="{{$partner->link}}" target="_blank">{{$partner->link}}</a>
                            </div>
                            <div class="desc">
                                {!! $partner->description !!}
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
                @include('inc.paginator_with_page_selector', ['items'=>$partners, 'append'=>[]])
            </div>
        </div>
    </div>
@endsection
