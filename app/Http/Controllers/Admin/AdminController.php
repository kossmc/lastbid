<?php

namespace app\Http\Controllers\Admin;

use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use GuzzleHttp;

/**
 * Class AdminController
 * @package app\Http\Controllers\Admin
 */
class AdminController extends \Backpack\Base\app\Http\Controllers\AdminController
{


    /**
     * Show the admin dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function dashboard()
    {
        $this->data['title'] = trans('backpack::base.dashboard'); // set the page title
        return view('backpack::dashboard', $this->data);
    }

    /**
     * Purge cache
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function purge(Request $request)
    {
        $urls = $request->input('urls');
        if ($urls) {

            $urlsOfArray = array_map('trim', explode(',', $urls));

            $client = new Client();
            $headers = [
                'X-Auth-Email' => env('CLOUD_FLARE_EMAIL'),
                'X-Auth-Key' => env('CLOUD_FLARE_KEY'),
                'Content-Type' => 'application/json',
            ];

            try {

                $client->post(env('CLOUD_FLARE_URL'),
                    ['headers' => $headers, 'body' => json_encode(['files' => $urlsOfArray])]);

                \Alert::info('Cache purge')->flash();
            }
            catch (GuzzleHttp\Exception\ClientException $e) {

                $res = $e->getResponse();

                $responseBodyAsString = $res->getBody()->getContents();

                \Alert::error($responseBodyAsString)->flash();
            }
        }
        return redirect()->back();
    }

    /**
     * Get all active bids
     * @param Request $request
     */
    public function activeBids(Request $request){

        $activeBids = \DB::table('bids')
            ->leftJoin('lots', 'bids.lot_id', '=', 'lots.id')
            ->leftJoin('partners', 'lots.partner_id', '=', 'partners.id')
            ->leftJoin('users', 'bids.user_id', '=', 'users.id')
            ->leftJoin('users as sellers', 'bids.user_id', '=', 'sellers.id')
            ->join('bid_attempts', function ($join) {
                $join->on('bids.id', '=', 'bid_attempts.bid_id');
                $join->on('bids.user_id', '=', 'bid_attempts.user_id');
                $join->whereIn('bid_attempts.status', ['pending', 'countered']);
            })
            ->whereIn('bids.status', ['pending', 'countered'])
            ->orderBy('bid_attempts.id', 'asc')
            ->groupBy('bid_attempts.id')
            ->select([
                'lots.id as id',
                'lots.title as title',
                'lots.range_to as range_to',
                'users.first_name as u_first_name',
                'users.last_name as u_last_name',
                'sellers.first_name as s_first_name',
                'sellers.first_name as s_last_name',
                'partners.title as partner_title',
                'bid_attempts.amount as amount',
                'lots.currency as currency',
                'bid_attempts.created_at as created_at',
                'lots.end_date as end_date',
            ])
            ->get()->toArray();
        $new = [];
        $totalUds = [];
        $totalGpb = [];
        $totalEur = [];
        foreach ($activeBids as $bid){
            $currency = strtoupper($bid->currency);
            $bid->seller_name = "{$bid->s_first_name} {$bid->s_last_name}";
            $bid->user_name = "{$bid->u_first_name} {$bid->u_last_name}";
            $bid->estimate = "{$bid->range_to} {$currency}";
            if($bid->currency == 'usd'){
                array_push($totalUds, $bid->amount);
            }
            if($bid->currency == 'gbp'){
                array_push($totalGpb, $bid->amount);
            }
            if($bid->currency == 'eur'){
                array_push($totalEur, $bid->amount);
            }
            $bid->amount = "{$bid->amount} {$currency}";
            $bid->bid_date = "{$bid->created_at}";
            $bid->time_left = format_time_left($bid->end_date, $bid->created_at);
            unset($bid->u_first_name);
            unset($bid->u_last_name);
            unset($bid->s_first_name);
            unset($bid->s_last_name);
            unset($bid->created_at);
            unset($bid->end_date);
            unset($bid->currency);
            unset($bid->range_to);
            $new[$bid->id] = $bid;
        }
        return response()->json(['data' => array_values($new), 'total' => [
            'usd' => array_sum($totalUds),
            'gbp' => array_sum($totalGpb),
            'eur' => array_sum($totalEur),
        ]]);
    }
}