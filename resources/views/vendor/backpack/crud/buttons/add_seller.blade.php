<a href="{{ url($crud->route.'/add_seller/new') }}" class="btn btn-primary ladda-button" data-style="zoom-in">
    <span class="ladda-label"><i class="fa fa-plus"></i>
        Add seller
    </span></a>
