<?php

namespace App\Http\Middleware;

use Closure;
use App\Repositories\BidRepository;

class BidIsMine
{
    /**
     * The bid repository implementation.
     *
     * @var BidRepository
     */
    private $bid;

    /**
     * Create a new middleware instance.
     *
     * @param BidRepository $bid
     * @return void
     */
    public function __construct(BidRepository $bid)
    {
        $this->bid = $bid;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($bid = $this->bid->isMine($request->id)) {
            $request->bid = $bid;
            $request->lot = $bid->lot;
            return $next($request);
        }

    }
}
