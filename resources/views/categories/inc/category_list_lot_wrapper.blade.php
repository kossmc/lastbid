<div class="viewport">
    @if ($parent->latestLots->isNotEmpty())
            @include('categories.inc.category_list_lot', ['category' => $parent])
    @elseIf ($parent->children->isNotEmpty())
        @foreach ($parent->children->take(1) as $category)
            @if ($category->latestLots->isNotEmpty())
                @include('categories.inc.category_list_lot', ['category' => $category])
            @endif
        @endforeach
    @endif
</div>


<a href="#" class="prev slider-arrow" style="display: none"><em class="icon-right_arrow"></em></a>
<a href="#" class="next slider-arrow" style="display: none"><em class="icon-right_arrow"></em></a>


