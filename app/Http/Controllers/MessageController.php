<?php

namespace App\Http\Controllers;

use App\Http\Requests\MessageRequest;
use Illuminate\Http\Request;
use App\Repositories\MessageRepository;
use Illuminate\Support\Facades\URL;

class MessageController extends Controller
{
    /**
     * The message repository implementation.
     *
     * @var MessageRepository
     */
    private $message;

    /**
     * Create a new controller instance.
     *
     * @param MessageRepository $message
     * @return void
     */
    public function __construct(MessageRepository $message)
    {
        $this->message = $message;
    }

    /**
     * Show user received message tab
     */
    public function received()
    {
        return view('messages.list', ['messages' => $this->message->received()]);
    }

    /**
     * Show user sent message tab
     */
    public function sent()
    {
        return view('messages.list', ['messages' => $this->message->sent()]);
    }

    /**
     * Show user archived message tab
     */
    public function archived()
    {
        return view('messages.list', ['messages' => $this->message->archive()]);
    }

    /**
     * Show message search tab
     */
    public function search(Request $request)
    {
        $q = $request->input('q','');
        if ($q) {
            return view('messages.search', ['messages' => $this->message->search($q), 'q'=>$q ]);
        }
        return redirect()->back();
    }

    /**
     * User messages change action
     * make messages for logged user
     * as Read, Unread, Archived etc.
     */
    public function switchStatus(Request $request, $method)
    {
        $ids = $request->get('messages_ids', []);
        $this->message->switchStatus($method, $ids);
        if (count($ids) && URL::previous() == URL::route('message_view', current($ids))) {
            $messageId = current($ids);
            if ($message = $this->message->getChain($messageId, false)) {
                if ($message->is_sent) {
                    return redirect()->route('messages_sent');
                }
                else {
                    return redirect()->route('messages_received');
                }
            }
        }
        return redirect()->back();
    }

    public function view($id)
    {
        $message = $this->message->getChain($id);
        return view('messages.view', ['message' => $message]);
    }

    public function reply(MessageRequest $request, $id)
    {
        $text = $request->get('letter-reply-msg', '');
        $this->message->reply($id, $text);
        return redirect()->back();
    }

    public function create(Request $request)
    {
        $mess = $this->message->create([
            'text'=> $request->input('letter-reply-msg'),
            'lot_id' => $request->id
        ]);
        return response()->json([
            'status' => true,
            'message' => view()->make('lots.inc.lot_flash_sent',['message'=> $mess])->render()
        ]);
    }
}
