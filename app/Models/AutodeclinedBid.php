<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AutodeclinedBid extends Model
{
    protected $dates = ['created_at', 'updated_at'];

    protected $table = 'autodeclined_bids';
    public $timestamps = true;
    protected $fillable = ['user_id', 'lot_id', 'amount'];
}
