@extends('seller.profile')
@section('title', 'Account info')
@section('profile_content')
    <div class="buyer-account">
        <section class="account-info">
            <h3>Profile</h3>
            <p><span>First Name:</span>{{$user->first_name}}</p>
            <p><span>Last Name:</span>{{$user->last_name}}</p>
            <p><span>Your Email:</span>{{$user->email}}</p>
        </section>
    </div>
@endsection
