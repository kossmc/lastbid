@extends('layouts.backpack_list')
@section('header')
    <section class="content-header">
        <h1>
            <span class="text-capitalize">Seller details for {{$date->format('M Y')}}</span>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="{{ url(config('backpack.base.route_prefix'), 'dashboard') }}">{{ trans('backpack::crud.admin') }}</a>
            </li>
            <li><a href="{{ url($crud->route) }}" class="text-capitalize">{{ $crud->entity_name_plural }}</a></li>
            <li class="active">Details view</li>
        </ol>
        <h4>Invoice status: {{$invoice['sent']?'Sent' : 'Not send'}}</h4>
        @if ($invoice['can_send'])
            <form action="{{route('seller-send-month-invoice',['id'=>$id,'year'=>$date->year,'month'=>$date->month])}}"
                  method="post"
                  role="form">
                <legend>{{$invoice['sent']?'Resend invoice' : 'Send invoice'}}</legend>

                <div class="form-group">
                    <label for="">Add comment</label>
                    <input type="text" class="form-control" name="comment" placeholder="Enter comment...">
                </div>

                <div class="form-group">
                    <label for="">Due date</label>
                    <input type="text" class="form-control datepicker" name="due_date">
                </div>

                <button type="submit" class="btn btn-primary">{{$invoice['sent']?'Resend' : 'Send'}}</button>
                {{csrf_field()}}
            </form>
        @endif
    </section>
@stop
@push('crud_list_styles')
    <link rel="stylesheet" href="{{ asset('vendor/adminlte/plugins/datepicker/datepicker3.css') }}">
@endpush
@push('crud_list_scripts')
    <script src="{{ asset('vendor/adminlte/plugins/datepicker/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
    <script>
        jQuery(document).ready(function ($) {
            $('.datepicker').datepicker({
                format: 'dd/mm/yyyy'
            });
        });
    </script>
@endpush
