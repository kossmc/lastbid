<span class="help-block">
    @php($emailError = $errors->first('email'))
    <strong>{{$emailError}}
        @if ((strpos($emailError, 'taken.') !== false))
            <a href="javascript:void 0;"
               style="font-weight: bold;"
               onclick="auth('forgot_form')">Reset Password</a>
        @endif
    </strong>
</span>
