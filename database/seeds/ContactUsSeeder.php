<?php

use Illuminate\Database\Seeder;

class ContactUsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(DB::table('contact_us')->count() == 0) {
            DB::table('contact_us')->insert([
                'id' => 1,
                'text' => 'We welcome your feedback, either as a user of LastBid or as an auction house wishing to sign up.',
                'phone' => '+44 (0) 20 7183 3007',
                'email' => 'info@lastbid.com',
                'auction_email' => 'auctions@lastbid.com',
                'registration_email' => 'register@lastbid.com',
                'address' => nl2br('LastBid Investments Ltd
                Les Echelons Court, Les Echelons,
                St Peter Port
                Guernsey GY1 1AR.'),
                'longitude' => '-2.535270',
                'latitude' => '49.459810'
            ]);
        }
    }
}
