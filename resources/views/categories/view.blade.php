@extends('layouts.main')
@section('title', $category->title)
@section('meta_description', $category->meta_description)
@section('top-links')
    @include('inc.top_links')
@stop

@section('scripts')
    <script src="{{ asset('js/category.js') }}"></script>
@endsection


@section('side_menu')
    <div class="open-categories-left side-menu-category-page visible-xs">
        <a href="#" class="categories-left">
            <em class="icon-categories_icon"></em>
            <span class="text">categories</span>
        </a>
        <div class="sidebar-wrap">
            @include('inc.side_nav')
        </div>
    </div>
@endsection

@section('content')
    <div class="page-wrap">
        <div class="wrap">
            <div class="page-categories">
                <div class="sidebar-wrap sidebar-category">
                    <div class="group-wrap">
                        <h3 class="reset-filters" onclick="resetFilters()">Reset all</h3>
                    </div>

                    <div class="group-wrap">
                        <h3>Price <span class="icon-right_arrow cat-icon-expanded cat-icon-mobile"></span></h3>
                        <div class="range-wrap mobile-range-wrap">
                            <div id="category-range-price"></div>
                            <div class="result clearfix">
                                <input type="text" class="range_price_value" data-index="0" name="min_price"
                                       value="{{$selectedPriceRange['min']}}">
                                <em>-</em>
                                <input type="text" class="range_price_value" data-index="1" name="max_price"
                                       value="{{$selectedPriceRange['max']}}">
                                {{--<em>$</em>--}}
                                <button>OK</button>
                            </div>
                        </div>
                    </div>

                    <form action="#" id="category_filters">
                        <input type="hidden" name="query" value="{{ $query }}">
                        @include('inc.filter_status')
                        @if ($filters && $filters->isNotEmpty())
                            @foreach($filters as $filter)
                                <div class="group-wrap">
                                    <h3>{{$filter->name}}<span class="icon-right_arrow  cat-icon-expanded"></span></h3>
                                    <div class="status">
                                        <ul>
                                            @foreach($filter->specific_options as $option)
                                                @if($loop->index == env('FILTER_SEE_MORE',5))
                                                    <li><span class="see-more-cat-items">See more +</span></li>
                                                @endif
                                                @if($filter->is_filter_checkbox)
                                                    <li class="@if($loop->index >= env('FILTER_SEE_MORE',5)){{'hidden'}}@endif">
                                                        <input class="sort-box-cb"
                                                               id="{{'spec_'.$filter->id.'_'.$option->id}}"
                                                               value="{{$option->id}}" type="checkbox"
                                                               @if(isset($selectedSpecifics['spec_'.$filter->id]) &&
                                                               in_array($option->id,$selectedSpecifics['spec_'.$filter->id]))
                                                               {{'checked'}} {{'previous=true'}} @endif
                                                               name="{{'spec_'.$filter->id.'[]'}}">
                                                        <label for="{{'spec_'.$filter->id.'_'.$option->id}}" onclick="">
                                                            {{$option->name}}</label>
                                                    </li>
                                                @elseif($filter->is_filter_radio)
                                                    <li class="select-option @if($loop->index >= env('FILTER_SEE_MORE',5))
                                                    {{' hidden'}}@endif">
                                                        <input name="{{'spec_'.$filter->id}}"
                                                               id="{{'spec_'.$filter->id.'_'.$option->id}}"
                                                               @if(isset($selectedSpecifics['spec_'.$filter->id]) &&
                                                               $selectedSpecifics['spec_'.$filter->id] == $option->id)
                                                               {{'checked'}} {{'previous=true'}}@endif
                                                               value="{{$option->id}}" type="radio">
                                                        <label for="{{'spec_'.$filter->id.'_'.$option->id}}" onclick=""
                                                               class="styled-select-button">
                                                            {{$option->name}}</label>
                                                    </li>
                                                @endif
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            @endforeach
                        @endif
                    </form>
                    @include('inc.side_nav', ['class' => 'hidden-xs'])
                </div>
                <div class="category-page">
                    <div class="title-evets">
                        @include('inc.breadcrumbs')
                        <p>{!! $category->description !!}</p>
                        <form action="" id="top_sorting">
                            <div class="filter">
                                <div class="item">
                                    <div class="name">Sort by</div>
                                    <div class="select">
                                        <select name="sort_by" id="sort_by">
                                            <option value=0>Choose...</option>
                                            <option value="price"
                                                    {{(isset($sort['sort_by'])&&$sort['sort_by']=='price')?' selected ':''}} >
                                                Price</option>
                                            <option value="latest"
                                                    {{(isset($sort['sort_by'])&&$sort['sort_by']=='latest')?' selected ':''}} >
                                                Latest</option>
                                        </select>
                                    </div>
                                    <div class="sorting">
                                        <a id="sort_up" href="javascript:void 0;" class="icon-sorting_up
                                    {{(isset($sort['sort_direction'])&&$sort['sort_direction']=='asc')?
                                    ' active':''}}"></a>
                                        <a id="sort_down" href="javascript:void 0;" class="icon-sorting_up down
                                    {{(isset($sort['sort_direction'])&&$sort['sort_direction']=='desc')?
                                    ' active':''}}"></a>
                                        @php($sort_direction = isset($sort['sort_direction'])?$sort['sort_direction']:0)
                                        <input type="hidden" name="sort_direction"
                                               id="sort_direction" value="{{$sort_direction}}">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="row">

                        @if(count($lots) > 0)
                            @include('inc.lots_list')
                        @else
                            <div style="margin-left: 15px">
                                <p>No items listed for category: {{$category->title}}</p>
                            </div>
                            <div class="talk-form">
                                <div class="title">let's talk</div>
                                <p>We’re keen to start a dialogue with those
                                    in the auction world to discuss how we could help,
                                    so please do get in touch.
                                </p>

                                <form method="post" action="landing" id="form-talk">
                                    <div class="item item__fullWidth">
                                        <input type="text" placeholder="Name *" name="name" required
                                               class="{{ $errors->has('name') ? 'has-error--bgcolor' : '' }}">
                                    </div>
                                    <div class="item">
                                        <input type="text" placeholder="Company *" name="company" required
                                               class="{{ $errors->has('company') ? 'has-error--bgcolor' : '' }}">
                                    </div>
                                    <div class="item">
                                        <input type="text" placeholder="Job title *" name="job_title" required
                                               class="{{ $errors->has('job_title') ? 'has-error--bgcolor' : '' }}">
                                    </div>
                                    <div class="item">
                                        <input type="email" placeholder="Email *" name="email" required
                                               class="{{ $errors->has('email') ? 'has-error--bgcolor' : '' }}">
                                    </div>
                                    <div class="item">
                                        <input type="text" placeholder="Telephone" name="telephone"
                                               class="{{ $errors->has('telephone') ? 'has-error--bgcolor' : '' }}">
                                    </div>
                                    <div class="item item__fullWidth">
                                <textarea placeholder="Message" name="message"
                                          class="{{ $errors->has('message') ? 'has-error--bgcolor' : '' }}"></textarea>
                                    </div>
                                    <div class="wrap-check-policy">
                                        <input type="checkbox" id="t-agreement" name="terms" value="1">
                                        <label class="" for="t-agreement">
                                            I accept Lastbid's <a href="{{ url('/terms') }}" class="terms-agreement-link">Terms & Conditions</a> and <a href="{{ url('/privacy') }}" class="terms-agreement-link">Privacy policy</a>
                                        </label>
                                    </div>
                                    <div class="item">
                                        <button class="btn popup-btn" type="submit" id="sendBtn" disabled="disabled">send</button>
                                    </div>
                                </form>

                            </div>
                        @endif

                    </div>
                    @include('inc.paginator_with_page_selector', ['items' => $lots,'selector'=>false])
                </div>
            </div>
        </div>
    </div>

@endsection

@section('user_script')
    window.sliderVars = {
        min: {{$priceRange['min']}},
        max: {{$priceRange['max']}},
        step: 1,
        values: [
            {{$selectedPriceRange['min']}},
            {{$selectedPriceRange['max']}}
        ],
    };
    window.initRangeSlider();
    $('.reset-filters').on('click', function (e) {
    return document.location = '{{ route('search', ['query' => $query]) }}';
    });
    $('#sort_by').on('change', function () {
    if ($(this).val() != 0) {
    $('.sorting a#sort_up').click();
    } else {
    updateUrl([['sort_by'], ['sort_direction']]);
    }
    });
    $('.filter .item .sorting a').off('click').on('click', function(){
    var $input = $('#sort_direction'),
    $selected = $('select#sort_by').val();
    if ($selected != 0) {
    $(this).hasClass('down') ? $input.val('desc'):$input.val('asc');
    filterLots();
    }
    })
    function filterLots() {
    var $filters = $('#category_filters input, .range-wrap input, form#top_sorting');
    document.location = document.location.pathname + '?' + $filters.serialize();
    }
    $('#category_filters input, .range-wrap button').on('click', filterLots);
    $("#category-range-price").slider({
    range: true,
    min: {{$priceRange['min']}},
    max: {{$priceRange['max']}},
    step: 10,
    values: [{{$selectedPriceRange['min']}},
    {{$selectedPriceRange['max']}}],
    slide: function (event, ui) {
    for (var i = 0; i < ui.values.length; ++i) {
    $("input.range_price_value[data-index=" + i + "]").val(ui.values[i]);
    }
    }
    });

    $("input.range_price_value").change(function() {
    var $this = $(this);
    $("#category-range-price").slider("values", $this.data("index"), $this.val());
    });

@stop
