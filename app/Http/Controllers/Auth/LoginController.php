<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Jobs\UpdateUserLocation;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Validator;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except(['logout','login2','login2save','ajax_login', 'ajaxLoginForm']);
    }

    public function redirectTo()
    {
        return session('url.intended')? \Redirect::back() : $this->redirectTo;
    }

    public function ajaxLoginForm()
    {
        forgetCookie('auth');
        if (Auth::guest()) {
            return response()->json([
                'status' => false,
                'form' => view()->make('auth.modal_login')->render()
            ]);
        }
        return response()->json(['status' => true ]);
    }
    /**
     * Login user.
     *
     * @param  Request $request
     * @return Response
     */
    public function ajax_login(Request $request)
    {

        $data = $request->intersect(['email', 'password', 'ip']);
        $validator = Validator::make($data, [
            'email' => 'required|string|email|max:255',
            'password' => 'required|string|max:255',
        ]);
        /*Check sent params*/
        if ($validator->fails()) {
            $request->flash();
            return response()->json([
                'status' => false,
                'form' => view()->make('auth.modal_login', ['errors'=>$validator->errors()])->render()
            ]);
        }
        if ($this->attemptLogin($request)) {
            $user = \Auth::user();
            $ip = isset($data['ip']) ? $data['ip'] : '';
            dispatch((new UpdateUserLocation($user->id, $ip))->onQueue('default'));
            if (isReferrerRoute('lot')) {
                return response()->json([
                    'status' => true,
                    'reload' => true,
                    'top' => view()->make('inc.top_links')->render()
                ])->withCookie(cookie('reload', true, 60));
            } else {
                return response()->json([
                    'status' => true,
                    'top' => view()->make('inc.top_links')->render()
                ]);
            }
        }
        $request->flash();
        // @todo: why 'mess'?
        // @todo: use l18n
        return response()->json([
            'status' => false,
            'form' => view()
                ->make('auth.modal_login', ['mess'=>'User or password incorrect! Check it once more!'])
                ->render()
        ]);
    }

//    public function login2(Request $request)
//    {
//        return view('auth.login2', ['user'=>Auth::user(),'url'=> $request->server('HTTP_REFERER', url('/'))]);
//    }
//
//    public function login2save(Request $request)
//    {
//        $data = $request->only('phone', 'address', 'cvv', 'exp_date');
//        $user = Auth::user();
//        if ($user) {
//            foreach ($data as $k=>$v) {
//                if ($data[$k]) {
//                    $user[$k] = $v;
//                }
//            }
//            $user->save();
//        }
//        return redirect($this->redirectTo);
//    }

    /**
     * Show the application's login form in popup.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm()
    {
        return redirectToAuth('login_form');
    }

    /**
     * redirect login request to the application to popup.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        return $this->showLoginForm();
    }

    public function logout(){
        if (Auth::guard('web')->check()) {
            Auth::logout();
        }
        return redirect()->to(route('home'), 302);
    }
}
