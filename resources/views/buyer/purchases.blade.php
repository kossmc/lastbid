@extends('layouts.inner')
@section('title', 'MY PURCHASES')
@section('inner_content')
    <div class="page-profile-content-header">
        <h3>Purchases</h3>
        <div class="filter-wrapper">
            @include('inc.top_sorting_price_purchase_date')
        </div>
    </div>
    @if ($lots->isNotEmpty())
    <ul class="purchased-lots-list purchased-lots-list--buyer">
        @foreach($lots as $lot)
        <li>
            <div class="description-wrapper visible-xs">
                <h3><a href="{{ $lot->url }}">{{ $lot->title }}</a></h3>
                <div class="lot-number"><span>LastBid Lot No</span><span>{{$lot->id}}</span></div>
                <ul class="seller-info">
                    <li><span>Auction House: </span>{{$lot->seller->full_name}}</li>
                    <li><span>Sale date: </span>{!! $lot->sold_date->setTimezone(session('time_zone'))->format('j F Y, h:i A') !!}</li>
                </ul>
            </div>
            <div class="image-wrapper"><a href="{{ $lot->url }}"><img
                            src="{{ $lot->getImagePath(180, 190) }}" alt=""></a></div>
            <div class="purchase-card-wrapper">
                <div class="price">{!! format_price($lot->sold_amount, null, $lot->currency)!!} </div>
                <ul class="lot-info">
                    <li>
                        <p>Auction house premium:</p>
                        <div class="price">{!! format_price($lot->bidFee($lot->sold_amount), null, $lot->currency) !!}</div>
                    </li>
                    <li>
                        <p>Total lot price:</p>
                        <div class="price">{!! format_price($lot->bidWithFee($lot->sold_amount), null, $lot->currency) !!}</div>
                    </li>
                </ul>
                <ul class="actions-wrapper">
                    <li class="action-step"><span class="icon-invoice"></span><span class="text">Invoice</span></li>
                    <li class="action-step"><span class="icon-paid"></span><span class="text">Paid</span></li>
                    <li class="action-step"><span class="icon-send"></span><span class="text">Send</span></li>
                    <li class="action-step"><span class="icon-recieved">            </span><span
                                class="text">Received</span></li>
                </ul>
            </div>
            <div class="description-wrapper hidden-xs">
                <h3><a href="{{ $lot->url }}">{{ $lot->title }}</a></h3>
                <div class="lot-number"><span>LastBid Lot No</span><span>{{$lot->id}}</span></div>
                <ul class="seller-info">
                    <li><span>Auction House: </span>{{$lot->seller->full_name}}</li>
                    <li><span>Sale date: </span>{!! $lot->sold_date->setTimezone(session('time_zone'))->format('j F Y, h:i A') !!}</li>
                </ul>
            </div>
        </li>
        @endforeach
    </ul>
    @php request()->has('sort_sold_date') ? $append = ['sold_date'=>request('sold_date')]:$append = []; @endphp
    @include('inc.paginator_with_page_selector',
            ['items'=>$lots, 'append'=>$append])
    @else
        <h1 class="notification-no-content">You have not made any successful bids</h1>
    @endif
@endsection
@section('user_script')
    $('#sort_by').off().on('change', function () {
    if ($(this).val() != 0) {
    $('.sorting a#sort_up').click();
    } else {
    updateUrl([['sort_by'], ['sort_direction']]);
    }
    });

    $('.filter .item .sorting a').off('click').on('click', function () {
    var $input = $('#sort_direction'),
    $selected = $('select#sort_by').val();
    if ($selected != 0) {
    $(this).hasClass('down') ? $input.val('desc') : $input.val('asc');
    document.location = document.location.pathname + '?' + $('form#top_sorting').serialize();
    }
    });
@stop
