<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Subscription extends Model
{
    use SoftDeletes;
    protected $table = 'subscriptions';
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];
    public $timestamps = true;

    public function users()
    {
        return $this->belongsToMany(User::class, 'subscription_user',
            'subscription_id', 'user_id');
    }

    public static function getSelectedByUser($user)
    {
        $subscriptions = self::all();
        $userSubscriptions = $user->subscriptions;
        return $subscriptions->map(function ($subscription) use ($userSubscriptions) {
            if ($userSubscriptions->contains('id', $subscription->id)) {
                $subscription->selected = true;
                return $subscription;
            }
            $subscription->selected = false;
            return $subscription;
        });
    }
}
