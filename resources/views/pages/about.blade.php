@extends('layouts.main')

@include('inc.page_metas', ['defTitle' => 'About Us'])

@section('top-links')
    @include('inc.top_links')
@stop
@section('content')
   @include('pages.about_content')
@endsection
