<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CardDetailsFilled
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!Auth::user()->cardDetailsFilled()) {
            return response()->json([
                'status' => false,
                'form' => view()->make('auth.modal_add_info', ['buy'=>true])->render()
            ]);
        } else {
            return $next($request);
        }
    }
}
