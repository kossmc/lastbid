<?php
namespace App\Mail\Bid;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Traits\BidNoticeTrait;

class SellerExpiredBidNotice extends Mailable
{
    use Queueable, SerializesModels, BidNoticeTrait;

    /**
     * Email subject.
     *
     */
    protected $bid_notice_subject = 'Your bid was expired';

    /**
     * Email template.
     *
     */
    protected $bid_notice_template = 'emails.seller_expired_bid_notice';
}
