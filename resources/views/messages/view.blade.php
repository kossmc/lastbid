@extends('layouts.inner')
@section('title', $message->lot->title)
@section('inner_content')
    <div class="page-buyer--msg">
        <div class="tab-content profile-tab-content tab-content--opened-msg">
            <div class="tab-pane fade in active" id="tab-received-letters">
                <div class="tab-inner">
                    <ul class="message-list">
                        <li class="message-item message-item--unread  message-item--archived">
                            <div class="message-item--expanded" style="display: block;">
                                <div class="letter-lot-view">
                                    <img src="{{$message->lot->getImagePath(55, 71)}}" alt="lot name">
                                    <h1>{{ $message->lot->title }}</h1>
                                    <p>LastBid Lot No:<span> {{$message->lot->id}}</span></p>
                                </div>
                                <div class="letter-top-controls">
                                    @if($message->is_archived)
                                        <span class="letter-btn-back-to-msg icon-right_arrow"
                                              data-href="{{ route('messages_archived')}}">
                                            Back to archived</span>
                                    @elseif($message->is_sent)
                                        <span class="letter-btn-back-to-msg icon-right_arrow"
                                              data-href="{{ route('messages_sent')}}">
                                            Back to sent</span>
                                    @else
                                        <span class="letter-btn-back-to-msg icon-right_arrow"
                                              data-href="{{ route('messages_received')}}">
                                            Back to received</span>
                                    @endif
                                    <div class="labels">
                                        <form action="" method="post" id="bulk_action">
                                            <input type="hidden" name="messages_ids[]" value="{{$message->id}}">
                                            {{csrf_field()}}
                                            <span class="btn-hide-archive-msgs">Hide history</span>
                                            @if($message->is_archived)
                                                <span class="btn-msg-restore"
                                                      data-href="{{ route('message_switch_status', ['method' =>
                                                      'restore']) }}">restore</span>
                                                <span class="btn-msg-delete"
                                                      data-href="{{ route('message_switch_status', ['method' =>
                                                      'delete'])}}">delete</span>
                                            @else
                                                @if (!$message->is_sent)
                                                    <span class="btn-msg-unread-mark hidden-xs"
                                                          data-href="{{ route('message_switch_status', ['method' => 'unread']) }}">
                                                        Mark as unread</span>
                                                @endif
                                            <span class="btn-msg-archive"
                                                  data-href="{{ route('message_switch_status', ['method' => 'archive']) }}">
                                                Archive</span>
                                            @endif
                                        </form>
                                    </div>
                                </div>
                                @if ($message->history && $message->history->count()>1)
                                <span class="btn-open-archived-letters"><span>Show full history</span></span>
                                <div class="letter-archived-wrapper">
                                    @foreach($message->history as $messageText)
                                        @if ( ! $loop->last)
                                <span class="btn-open-previous-sent">
                                  <p class="time">{{$messageText->created_at->setTimezone(session('time_zone'))->format('j F Y, h:i A')}} EST</p>
                                    <span>{{$messageText->letterStatus()}}</span>
                                  <p>From: <span>{{$message->fromName($messageText->user_id)}}</span>
                                  To:<span>{{$message->toName($messageText->user_id)}}</span></p>
                                  <p><span>{{$message->lot->title}}</span></p>
                                </span>
                                <ul class="previous-wrapper">
                                        <li class="previous-received">
                                            <div class="previous-received-head">
                                                <div>
                                                    <p class="time">{{$messageText->created_at->setTimezone(session('time_zone'))->format('j F Y, h:i A')}} EST</p>
                                                </div>
                                                <p>From: <span>{{$message->fromName($messageText->user_id)}}</span></p>
                                                <p>To:<span>{{$message->toName($messageText->user_id)}}</span></p>
                                                <p>Title: <span>{{$message->lot->title}}</span>
                                                </p>
                                            </div>
                                            <div class="previous-received-text">
                                                <p>
                                                    {{$messageText->text}}
                                                </p>
                                            </div>
                                        </li>
                                    </ul>
                                        @endif
                                    @endforeach
                                </div>
                                @endif
                                @php
                                 if($message->history) $last_mess = $message->history->last()
                                @endphp
                                @if ($last_mess)
                                <div class="letter-wrapper">
                                    <div class="letter-box-head">
                                        <div>
                                            <p class="time">{{$last_mess->created_at->setTimezone(session('time_zone'))->format('j F Y, h:i A')}}</p>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-3 col-sm-1">From: </div>
                                            <div class="col-xs-9 col-sm-11">{{$message->fromName($last_mess->user_id)}}</div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-3 col-sm-1">To: </div>
                                            <div class="col-xs-9 col-sm-11">{{$message->toName($last_mess->user_id)}}</div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-3 col-sm-1">Title: </div>
                                            <div class="col-xs-9 col-sm-11">{{$message->lot->title}}</div>
                                        </div>
                                    </div>
                                    <div class="letter-box-body">
                                        <p>
                                            {{$last_mess->text}}
                                        </p>
                                    </div>
                                </div>
                                <div class="letter-box-reply">
                                    <div class="reply-placeholder">Click here to Reply</div>
                                    <div class="reply-box-opened">
                                        <form action="{{route('message_reply', ['message'=>$message->id])}}"
                                              method="post" id="reply-msg">
                                            <div class="reply-msg">
                                            <p>Reply to: <span>{{$message->fromName($last_mess->user_id)}}</span></p>
                                                <span class="btn-clear-reply-msg">Clear reply</span>
                                                <textarea name="letter-reply-msg" pattern="[^\s]+"
                                                          cols="30" rows="10" required></textarea>
                                            </div>
                                        <div class="reply-btns"><button type="submit"
                                                                        class="btn-send-letter btn">Send</button><span
                                                        class="btn-cancel-send-letter">Cancel</span></div>
                                            {{csrf_field()}}
                                        </form>
                                    </div>
                                </div>
                                @endif
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('custom_modal')
    @include('messages.modal_confirm_delete')
@endsection
@section('user_script')
    //not allow to send blank form or with whitespace characters
    $('#reply-msg').on('submit', function(){
        var text = $('textarea[name="letter-reply-msg"]'),
            qnt = text.val().trim().length;
        if (qnt == 0){
            text.val('')
            alert('Message can not be empty!');
            return false;
        }
    });
    // open modal dialog before delete
    $('.btn-msg-delete').on('click', function(){
    $('#popup-overlay').show();
    $('.popup-wrap').show();
    $('.popup-wrap').find('.popup').addClass('popup-active');
    });
    // close modal dialog
    $('.popup-wrap').on('click', function(e){
    $(this).hide();
    $('#popup-overlay').hide();
    $('.popup-wrap').find('.popup').removeClass('popup-active');
    if ($(e.target).hasClass('popup-btn-cancel')) {// delete action
        $('form#bulk_action').attr('action', '{{route('message_switch_status',['method'=>'delete'])}}').submit();
    }
    });
    // buttons actions
    $('.btn-msg-unread-mark, .btn-msg-archive, .btn-msg-restore').on('click', function(e){
    var el = $(e.target),
    href = el.attr('data-href');
    el.parents('form').attr('action', href).submit();
    });
    //go to received
    $('.letter-btn-back-to-msg').on('click', function(e){
        window.location = $(e.target).attr('data-href');
    });

    // open reply section in letter
    $('.reply-placeholder').on('click', function(){
    $(this).css('display', 'none');
    $(this).next('.reply-box-opened').css('display', 'block');
    });
    $('.btn-cancel-send-letter').on('click', function(){
        if( document.documentElement.clientWidth > 768){
            var replyBox = $(this).closest('.reply-box-opened');
            replyBox.css('display', 'none');
            replyBox.prev('.reply-placeholder').css('display', 'block');
        }else{
            document.location.href = "/messages";
        }
    });
    $('.btn-clear-reply-msg').on('click', function(){
    $(this).next('textarea').val('');
    });
    // open archived letters
    $('.btn-open-archived-letters span').on('click', function(){
    var btn = $(this).parent();
    btn.css('display', 'none');
    btn.next('.letter-archived-wrapper').css('display', 'block');
    $('.btn-hide-archive-msgs').css('display', 'inline-block');
    });

    $('.btn-hide-archive-msgs').on('click', function(){
    $('.letter-archived-wrapper').css('display', 'none');
    $('.letter-archived-wrapper .previous-wrapper').css('display', 'none');
    $('.btn-open-archived-letters').css('display', 'block');
    $('.btn-open-previous-received').css('display', 'block');
    $('.btn-open-previous-sent').css('display', 'block');
    $(this).css('display', 'none');
    });


    $('.btn-open-previous-received, .btn-open-previous-sent').on('click', function(){
    $(this).css('display', 'none');
    $(this).next('.previous-wrapper').css('display', 'block');
    });
@endsection
