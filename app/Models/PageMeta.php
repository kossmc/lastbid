<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class PageMeta extends Model
{
    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'page_meta';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
    protected $fillable = ['page', 'name', 'content'];
    // protected $hidden = [];
    // protected $dates = [];

    public static $pages = [
        'home' => 'Home',
        'contact' => 'Contact Us',
        'about' => 'About Us',
        'faq' => 'FAQ',
        'how_it_works' => 'How it works',
        'policy' => 'Policy',
        'terms' => 'Terms',
        'categories-search' => 'Categories search',
        'categories' => 'Catalog',
        'partners' => 'Auction Partners',
        'auction-calendar' => 'Auction Calendar',
        'news' => 'News'
    ];

    public static $metas = [
        'description' => 'Description',
        'title' => 'Title'
    ];
    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */
    public function scopeByPage($query, $page)
    {
        return $query->where('page', $page)->get()->pluck('content', 'name');
    }
    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
