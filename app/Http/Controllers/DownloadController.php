<?php

namespace App\Http\Controllers;

use App\Models\Invoice;
use App\Models\Lot;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DownloadController extends Controller
{
    public function sellerInvoice($id)
    {
        $invoice = Invoice::findOrFail($id);
        return $this->_showFile($invoice->storage_path);
    }

    public function buyerInvoice($id)
    {
        $lot = Lot::findOrFail($id);
        //show invoice only for buyer who buy lot or lot owner
        if (($lot->buyer_id = Auth::id()) || ($lot->user_id = Auth::id())) {
            return $this->_showFile($lot->buyer_invoice_path);
        }
        abort(404);
    }

    public function _showFile($path)
    {
        if (file_exists($path)) {
            return response()->file($path);
        }
        abort(404);
    }
}
