<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NewMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('messages');
        Schema::dropIfExists('message_user');
        Schema::create('messages', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('lot_id');
            $table->integer('buyer_id');
            $table->integer('seller_id');
            $table->enum('buyer_state', ['read', 'unread'])->default('unread');
            $table->enum('buyer_status', ['active', 'archived', 'deleted'])->default('active');
            $table->enum('seller_state', ['read', 'unread'])->default('unread');
            $table->enum('seller_status', ['active', 'archived', 'deleted'])->default('active');
            $table->enum('last_message_by', ['seller', 'buyer'])->default('buyer');
            $table->timestamp('last_message_at')->nullable();
        });
        Schema::create('message_texts', function (Blueprint $table) {
            $table->integer('message_id');
            $table->integer('user_id');
            $table->text('text')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('messages');
        Schema::dropIfExists('message_texts');
    }
}
