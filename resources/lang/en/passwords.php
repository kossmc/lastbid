<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Passwords must be at least six characters and match the confirmation.',
    'reset'    => 'Your password has been reset!',
//    'sent'     => 'We will e-mailed your password reset link if user with the specified email exists in our system!',
    'sent'     => 'If we find an account with the specified email, we will send you a letter with instructions for password recovery.',
    'token'    => 'This password reset token is invalid.',
    'user'     => "We can't find a user with that e-mail address.",

];
